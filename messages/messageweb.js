var express = require('express'),
    http = require('http');
var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);

//// module required
var mysql = require('mysql');
var bodyParser = require('body-parser')
app.use(bodyParser.json());

//// variables used ///
var userlist = [];
var allClients = [];
var sockets = new Array;

var BASEURL = '';
var AVATAR_URL = 'http://192.168.1.204/unideal-web/uploads/profile/';

var db_config = {
    host: '192.168.1.209',
    user: 'root',
    password: '',
    database: 'unideal'
};

var connection;

function handleDisconnect() {
  connection = mysql.createConnection(db_config); // Recreate the connection, since
  connection.connect(function(err) {              // The server is either down
    if(err) {                                     // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    }                                     // to avoid a hot loop, and to allow our node script to
  });                                     // process asynchronous requests in the meantime.
                                          // If you're also serving http, display a 503 error.
  connection.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
}
handleDisconnect();

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
    res.connection.setTimeout(0);
});

io.on('connection', function (socket) {
   
    socket.on('addUser', function (user_id) {
        try {
             if (userlist.hasOwnProperty(user_id)) {
                     userlist[user_id].push(socket);
              }else{ 
                  userlist[user_id] = new Array();
                  userlist[user_id].push(socket);
              }
             console.log("connected user: "+ user_id)
             allClients.push(socket);
//             send_disconnect(user_id)
            
      } catch (err) {
            console.log('Add user error' + err)
        }
        
    });
    
       
   socket.on('sendMessage', function (send_array) {
            try {
              //New code start
                var main_thread_id = '';
                if(send_array.thread_id != 0)
                {
                    main_thread_id = send_array.thread_id;
                    sendafterthreadid(main_thread_id,send_array);
                }
                else
                {
                    var sender_id = send_array.sender_id;
                    var receiver_id = send_array.receiver_id;
                    var job_id = send_array.job_id;
                    var queryForResponse = connection.query('SELECT thread_id from job_messages_1 WHERE sender_id = ' + sender_id + ' AND job_id =' + job_id, function (err, rows, fields) {

                        if (!err) {
                            if (rows.length > 0) {

                                for (i = 0; i < 1; i++) {
                                    main_thread_id = rows[i].thread_id;
                                    sendafterthreadid(main_thread_id,send_array)
                                }
                                 
                            } else {
                                 main_thread_id = Math.random().toString(36).substring(7);
                                 sendafterthreadid(main_thread_id,send_array)
                            }
                        } else {
                            console.log(err);
                        }
                    });
                }
                console.log("main " + send_array.thread_id);
                //New code end
                
                
                function sendafterthreadid(main_thread_id,send_array)
                {
                    console.log(send_array);
                    if (userlist.hasOwnProperty(send_array.sender_id)) {
                        
                        //when user is online
                        var date_time = new Date().toISOString().slice(0, 19).replace('T', ' ');
                        var msecond = date_time +':'+ new Date().getMilliseconds();

                        var post = {
                            sender_id: send_array.sender_id,
                            receiver_id: send_array.receiver_id,
                            text: send_array.text,
                            image_url:send_array.image_url,
                            read_flag: '0',
                            message_type: send_array.message_type,
                            thread_id: main_thread_id,
                            user_type: '3',
                            created_date: msecond,
                            job_id:send_array.job_id
                        }; 


                        var query = connection.query('INSERT INTO job_messages_1 SET?', post, function (error, result) {
                            if (error) {
                                console.log(error.message);
                            } else {
                                user_chat(result.insertId, send_array.receiver_id, send_array.sender_id,send_array.requester_id,send_array.agent_id);
                            }
                        });

                    } else {

                        //when user is offline.
                        var date_time = new Date().toISOString().slice(0, 19).replace('T', ' ');
                        var msecond = date_time +':'+ new Date().getMilliseconds();

                        var post = {
                            sender_id: send_array.sender_id,
                            receiver_id: send_array.receiver_id,
                            text: send_array.text,
                            image_url:send_array.image_url,
                            read_flag: '0',
                            message_type: send_array.message_type,
                            thread_id: main_thread_id,
                            user_type: '3',
                            created_date: msecond,
                            job_id : send_array.job_id,
                        };

                        var query = connection.query('INSERT INTO job_messages_1 SET?', post, function (error, result) {
                            if (error) {
                                console.log(error.message);
                            } else {
                                user_chat(result.insertId, send_array.receiver_id, send_array.sender_id,send_array.requester_id,send_array.agent_id);
                            }
                        });
                    }
                
                }
            
            } catch(err) {
                console.log(err);
            }
        function user_chat(msgid, receiver_id, sender_id,requester_id,agent_id){ 
            try {
                    var queryForResponse = connection.query('SELECT mm.message_id,ui1.id AS user_id, mm.sender_id,mm.image_url, mm.message_type, mm.receiver_id, mm.user_type, mm.thread_id, mm.text,mm.read_flag,mm.created_date,ui1. NAME AS NAME,ui. NAME AS receivername, CONCAT("' + AVATAR_URL + '",ui1.profile_pic) AS profile_image FROM job_messages_1 mm LEFT JOIN front_users ui ON ui.id = mm.receiver_id LEFT JOIN front_users ui1 ON ui1.id = mm.sender_id WHERE mm.message_id = ' + msgid, function (err, rows, fields) {

                                var singlemsg2 = new Object();
                               
                                if (!err) {
                                    if (rows.length > 0) {
                                        
                                        for (i = 0; i < rows.length; i++) {
                                           
                                            singlemsg2.message_id = rows[i].message_id;
                                            singlemsg2.sender_id = rows[i].sender_id;
                                            singlemsg2.receiver_id = rows[i].receiver_id;
                                            singlemsg2.profile_image = rows[i].profile_image;
                                            singlemsg2.user_type = rows[i].user_type;
                                            singlemsg2.name = rows[i].name;
                                            singlemsg2.thread_id = rows[i].thread_id;
                                            singlemsg2.chat_type = rows[i].message_type;
                                            singlemsg2.image_url = rows[i].image_url;
                                            singlemsg2.text = rows[i].text;
                                            singlemsg2.created_date = time_formate(rows[i].created_date);
                                        }
                                        
                                    }
                                     
                                    var ownid = singlemsg2.sender_id;
                                  
                                    if (userlist.hasOwnProperty(ownid)) {
                                       
                                        for(x in userlist[ownid])
                                        { 
                                          userlist[ownid][x].emit('returnAck', singlemsg2);
                                        }
                                    }
                                    if (userlist.hasOwnProperty(requester_id)) {
                                        for(x in userlist[requester_id])
                                        { 
                                            userlist[requester_id][x].emit('onMessageReceive',singlemsg2);
                                        }
                                    }
                                    if (userlist.hasOwnProperty(agent_id)) {
                                        for(x in userlist[agent_id])
                                        { 
                                            userlist[agent_id][x].emit('onMessageReceive',singlemsg2);
                                        }
                                    }
                                } else {
                                    console.log(err);
                                }
                            });

                        } catch (err) {
                            console.log('this is 1to1:' + err);
                        }
        }
    });
    
//    socket.on('askchathistory', function(data){
//    console.log(data.last_block);
//    //SELECT gm.id, gm.sender_id, gm.to_user, gm.thread_id, gm.msg, gm.created_on, CONCAT( ui.firstname, " ", ui.lastname ) AS NAME, CONCAT( "' + AVTAR_URL + '", ui.image ) AS profile_image FROM messages AS gm LEFT JOIN users ui ON ui.id = gm.sender_id WHERE gm.thread_id = 'sjk455dfgh' ORDER BY gm.id DESC
//    try {
//            if(data.last_block){       
//            var queryForResponse = connection.query("SELECT * FROM (SELECT jm.message_id,jm.user_type,jm.image_url,jm.message_type,jm.text,jm.created_date,jm.sender_id,ji.user_id as agent_id,j.user_id as requester_id,jm.job_id FROM job_messages_1 jm LEFT JOIN jobs j ON j.job_id = jm.job_id LEFT JOIN job_invite ji ON ji.job_id = jm.job_id WHERE ( jm.message_id <" + data.last_block + " AND jm.job_id =" + data.job_id + " AND jm.sender_id = " + data.sender_id + ") OR (jm.message_id <" + data.last_block + " AND jm.job_id = " + data.job_id + " AND jm.sender_id = " + data.requester_id + " ) OR ( jm.message_id <" + data.last_block + " AND jm.job_id = " + data.job_id + " AND jm.sender_id = " + data.agent_id + " ) ORDER BY created_date DESC limit 6) sub ORDER BY created_date ASC", function (err, rows, fields) {
//                       
//                        var return_history = new Array();
//                        var return_main_data = new Array();
//                        if (!err) {
//                            if (rows.length > 0) {
//                                for (i = 0; i < rows.length; i++) {
//                                return_history.push({
//                                        msg_id: rows[i].message_id,
//                                        user_type:rows[i].user_type,
//                                        questionar:rows[i].questionar,
//                                        agent_name:rows[i].agent_name,
//                                        receiver_id: rows[i].receiver_id,
//                                        sender_id: rows[i].sender_id,
//                                        thread_id: rows[i].thread_id,
//                                        msg: rows[i].text,
//                                        image_url: rows[i].image_url,
//                                        chat_type : rows[i].message_type,
//                                        msg_time: time_formate(rows[i].created_date)
//                                    });
//                                }
//                                return_main_data.push({'history':return_history});
//                                var sender_id = data.sender_id;
//                                if (userlist.hasOwnProperty(sender_id)) {
//                                    for(x in userlist[sender_id])
//                                        { 
//                                            userlist[sender_id][x].emit('returnHistory',return_main_data);
//                                        }
//                                    }
//                                
//                            }
//                        } else {
//                            console.log(err);
//                        }
//                    });
//                }
//                else{
//                    var queryForResponse = connection.query("SELECT * FROM (SELECT jm.message_id,jm.user_type,jm.image_url,jm.message_type,jm.text,jm.created_date,jm.sender_id,ji.user_id as agent_id,j.user_id as requester_id,jm.job_id FROM job_messages_1 jm LEFT JOIN jobs j ON j.job_id = jm.job_id LEFT JOIN job_invite ji ON ji.job_id = jm.job_id WHERE (  jm.job_id =" + data.job_id + " AND jm.sender_id = " + data.sender_id + ") OR (jm.job_id = " + data.job_id + " AND jm.sender_id = " + data.requester_id + " ) OR ( jm.job_id = " + data.job_id + " AND jm.sender_id = " + data.agent_id + " ) ORDER BY created_date DESC limit 6) sub ORDER BY created_date ASC", function (err, rows, fields) {
//                       
//                        var return_history = new Array();
//                        var return_main_data = new Array();
//                        if (!err) {
//                            if (rows.length > 0) {
//                                for (i = 0; i < rows.length; i++) {
//                                return_history.push({
//                                        msg_id: rows[i].message_id,
//                                        user_type:rows[i].user_type,
//                                        questionar:rows[i].questionar,
//                                        agent_name:rows[i].agent_name,
//                                        receiver_id: rows[i].receiver_id,
//                                        sender_id: rows[i].sender_id,
//                                        thread_id: rows[i].thread_id,
//                                        msg: rows[i].text,
//                                        image_url: rows[i].image_url,
//                                        chat_type : rows[i].message_type,
//                                        msg_time: time_formate(rows[i].created_date)
//                                    });
//                                }
//                                return_main_data.push({'history':return_history});
//                                var sender_id = data.sender_id;
//                                if (userlist.hasOwnProperty(sender_id)) {
//                                    for(x in userlist[sender_id])
//                                        { 
//                                            userlist[sender_id][x].emit('returnHistory',return_main_data);
//                                        }
//                                    }
//                                
//                            }
//                        } else {
//                            console.log(err);
//                        }
//                    });
//                }
//                } catch (err) {
//                    console.log(err);
//                }
//    });
 
    
});  
function time_formate(time) {

    // var date_time = new Date().toISOString().slice(0, 19).replace('T', ' ');
    var d = new Date(time);
    var curr_date = ("0" + (d.getDate())).slice(-2).toString();
    var curr_month = ("0" + (d.getMonth() + 1)).slice(-2).toString();
    var curr_year = d.getFullYear();
    var curr_hours = ("0" + (d.getHours())).slice(-2).toString();
    var curr_minutes = ("0" + (d.getMinutes())).slice(-2).toString();
    var curr_sec = ("0" + (d.getSeconds())).slice(-2).toString();
//    return curr_year + "-" + curr_month + "-" + curr_date + " " + curr_hours + ":" + curr_minutes + ":" + curr_sec;
    return  curr_hours + ":" + curr_minutes + ":" + curr_sec;
    
}
server.listen(7870, function (msg) {
    console.log('listening on hardika server *:7870');
});