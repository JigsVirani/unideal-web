var express = require('express'),
    http = require('http');
var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);

//// module required
var mysql = require('mysql');
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

//// variables used ///
var userlist = [];
var adminlist = []; //new admin list.
var templist = [];
var allClients = [];
var alladminlist = []; // all admin list.
var sockets = new Array;
var print_flag = true;

// constants.
var STATUS_TRUE = 1;
var PAGE_LIMIT = 10;
var HISTORY_PAGE_LIMIT = 50;
var STATUS_FALSE = 0;
var STATUS_200 = 200; //available.
var STATUS_201 = 201; // empty data.
var STATUS_600 = 600; // not authenticate.


var AUTH_SUCCESS = "Auth Success";
var AUTH_FAILED = "Auth Failed";
var NO_DATA = "No data available."

var BASEURL = '';
var AVATAR_URL = 'http://192.168.1.208/unideal-web/uploads/profile/';
var AVA
// constant end.
var db_config = {
    host: '192.168.1.209',
    user: 'root',
    password: '',
    database: 'unideal'
};

var connection;

function handleDisconnect() {
  connection = mysql.createConnection(db_config); // Recreate the connection, since
  connection.connect(function(err) {              // The server is either down
    if(err) {                                     // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    }else{
         print_data('connected to database:' + db_config);
    }                                     // to avoid a hot loop, and to allow our node script to
  });                                     // process asynchronous requests in the meantime.
                                          // If you're also serving http, display a 503 error.
  connection.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
}

handleDisconnect();

// This is for mysql connection error handling.
connection.on('error', function(err) {

    //- The server close the connection.
    if(err.code === "PROTOCOL_CONNECTION_LOST"){    
        console.log("/!\\ Cannot establish a connection with the database. /!\\ ("+err.code+")");
        connection = reconnect(connection);
    }

    //- Connection in closing
    else if(err.code === "PROTOCOL_ENQUEUE_AFTER_QUIT"){
        console.log("/!\\ Cannot establish a connection with the database. /!\\ ("+err.code+")");
        connection = reconnect(connection);
    }

    //- Fatal error : connection variable must be recreated
    else if(err.code === "PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR"){
        console.log("/!\\ Cannot establish a connection with the database. /!\\ ("+err.code+")");
        connection = reconnect(connection);
    }

    //- Error because a connection is already being established
    else if(err.code === "PROTOCOL_ENQUEUE_HANDSHAKE_TWICE"){
        console.log("/!\\ Cannot establish a connection with the database. /!\\ ("+err.code+")");
    }

    //- Anything else
    else{
        console.log("/!\\ Cannot establish a connection with the database. /!\\ ("+err.code+")");
        connection = reconnect(connection);
    }

});

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
    res.connection.setTimeout(0);
});


io.on('connection', function (socket) {
    
    //TO NOTIFY THE REQUESTER FROM * API JOBS MODEL* FILE IN LARAVEL.
//    app.post('/notifyrequester', function (req, res) {
//        var content = req.body;
//        console.log(content);
//        NotifyRequesterForNewproposal(content);
//        res.end('ok');
//        
//        });
    
     //notity the requester for the new proposal.
//    function NotifyRequesterForNewproposal(content){
//    
//            try{
//                
//                 if (userlist.hasOwnProperty(content.sender_id)) {
//                    if (data.length > 0){
//                           Returndata = {success: 1, status_code: STATUS_200, message: 'Done'};
//                           console.log(JSON.stringify(Returndata));
//                           userlist[content.sender_id].emit('onNewAgentMessages', JSON.stringify(Returndata));
//                    }
//                
//            }
//            }
//            catch(err){
//
//                console.log('Error in NotifyRequesterForNewproposal.');
//                console.log(err);
//
//             }
//        }
    
   
    // TO AUTHENTICATE THE USER.
    socket.on('requestAuthentication', function (user_id,user_type) {
         
                var toAuthenticate = connection.query('SELECT id FROM front_users where status = "1" and user_delete = "0" and id = ' + user_id, function (err, rows, fields) {
                            if (!err) {
                                if (rows.length > 0) {
                                        if (!userlist.hasOwnProperty(user_id)){
                                             userlist[user_id] = socket;
                                             allClients.push(socket);
                                        }
                                        var authcheck = 1;
                                        Onresponse(user_id,authcheck);
                                        print_data('connected user-->' + user_id + ' -- ' + user_type);    

                                } else {
                                    templist[user_id] = socket;
                                    var authcheck = 0;
                                    Onresponse(user_id,authcheck);
                                }
                        } else {
                             var authcheck = 0;
                             templist[user_id] = socket;
                             Onresponse(user_id,authcheck);
                             print_data('on authenticate:');
                             print_data(err);
                        }

                    });
        
            function Onresponse(user_id, authcheck){
            
                print_data('this is authenticated: '+ user_id);
                  var singlemsg = {}; 
                if(authcheck == 1){
                        if (userlist.hasOwnProperty(user_id)) {
                            
                              singlemsg = { success: STATUS_TRUE,
                                              status_code: STATUS_200,  
                                              message: AUTH_SUCCESS };

                            console.log(singlemsg);
                            userlist[user_id].emit('onAuthSuccess', JSON.stringify(singlemsg));
                        }
                } else {
                        if (templist.hasOwnProperty(user_id)) {
                            
                                singlemsg = { success: STATUS_FALSE,
                                              status_code: STATUS_201,  
                                              message: AUTH_FAILED };
                            
                                console.log(singlemsg);
                                templist[user_id].emit('onAuthSuccess', JSON.stringify(singlemsg));
                        }
                }
            }
    });
    
    // END AUTHENTICATION USER.
    
    // NEW ADMIN USER TO JOINS THE CHAT.
        socket.on('addAdminUser', function (job_id) {
                  try {
                         if (adminlist.hasOwnProperty(job_id)) {
                                 adminlist[job_id].push(socket);
                          }else{ 
                              adminlist[job_id] = new Array();
                              adminlist[job_id].push(socket);
                          }
                         console.log("connected admin user: "+ job_id)
                       
                         alladminlist.push(socket);
                         //send_disconnect(user_id)

                  } catch (err) {
                        console.log('Add Admin user error' + err)
                    }

                });
    // END ADMIN USER TO THE CHAT.
    
    
    // TO GET MESSAGING LIST.
    socket.on('requestMessages', function (user_id,user_type, page_index) {
    
            try{
                
                if (user_id != "") {
                        
                        var $startFrom  = PAGE_LIMIT * page_index;
                        var $startFromMore  = PAGE_LIMIT * page_index + PAGE_LIMIT;
                        var hasMore  = true;
                         var temp = connection.query('SELECT SQL_CALC_FOUND_ROWS jm.message_id,IFNULL(jm.height, "") as height, IFNULL(jm.width, "") as width, jm.thread_id, ji.user_id, jm.job_id,IFNULL((SELECT text FROM job_messages WHERE thread_id = jm.thread_id ORDER BY message_id DESC LIMIT 1), "") as text, IFNULL((SELECT image_url FROM job_messages WHERE thread_id = jm.thread_id ORDER BY message_id DESC LIMIT 1), "") as image_url, (SELECT sender_id from job_messages WHERE job_id =jm.job_id and thread_id = jm.thread_id and user_type = 2 LIMIT 1) as agent_id, (SELECT name from front_users WHERE id = agent_id) as agent_name,(SELECT COUNT(message_id) from job_messages WHERE thread_id = jm.thread_id and receiver_id = '+ user_id +' and sender_id = agent_id and read_flag != 2) as total_unread, (SELECT profile_pic from front_users WHERE id = agent_id) as agent_profile_pic, (SELECT from_social_network from front_users WHERE id = agent_id) as from_social_network, j.job_status, jm.message_type, jm.created_date FROM job_messages AS jm LEFT JOIN job_invite AS ji ON jm.job_id = ji.job_id LEFT JOIN jobs AS j ON j.job_id = ji.job_id WHERE message_id IN (SELECT MAX(message_id) FROM job_messages GROUP BY thread_id) AND j.job_status IN (1,3,4,5) AND j.user_id = '+ user_id +' GROUP BY (jm.thread_id) ORDER BY (jm.message_id) DESC LIMIT '+ $startFrom +', '+ PAGE_LIMIT +'', function (err, rows, fields) 
                         {
                                if (!err) {
                                    var messageList = [];
                                    var TotalRows = 0; 
                                    if (rows.length > 0) {

                                        for (i = 0; i < rows.length; i++) {
                                            
                                                var profile_pic = '';
                                                if(ValidURL(rows[i].agent_profile_pic)){
                                                    profile_pic = rows[i].agent_profile_pic;
                                                } else {
                                                    if(rows[i].agent_profile_pic != ''){
                                                        
                                                        profile_pic = AVATAR_URL + "thumb_"+rows[i].agent_profile_pic;
                                                    } else {
                                                        profile_pic = AVATAR_URL+ 'no_image_user.png';
                                                    }
                                                }
                                                // To send notification to agent for received.
                                                
                                                if(rows[i].total_unread > 0){
                                                    
                                                      if (userlist.hasOwnProperty(rows[i].agent_id)) {
                                                            Returndata = {thread_id: rows[i].thread_id, receiver_id: rows[i].agent_id};
                                                                console.log(JSON.stringify(Returndata));
                                                                userlist[rows[i].agent_id].emit('onReceivedMessages', JSON.stringify(Returndata));
                                                     }
                                                }
                                            
                                                messageList.push({
                                                    thread_id: rows[i].thread_id,
                                                    job_id: rows[i].job_id,
                                                    job_status: rows[i].job_status,
                                                    message_type: rows[i].message_type,
                                                    total_unread: rows[i].total_unread,
                                                    agent_id: rows[i].agent_id,
                                                    agent_name: rows[i].agent_name,
                                                    agent_profile_pic: profile_pic,
                                                    created_date: return_date_function(rows[i].created_date),
                                                    data:{'text': rows[i].text, 'image_url': rows[i].image_url, 'height': rows[i].height, 'width': rows[i].width}
                                                });
                                      }
                                       
                                        var temp1 = connection.query('SELECT FOUND_ROWS() as totalcount', function (err, rows1, fields){
                                                   TotalRows = rows1[0].totalcount;
                                                    if($startFromMore >= TotalRows){
                                                        hasMore = false;
                                                    }
                                                    console.log($startFrom);
                                                    console.log(hasMore);
                                                    after_count(messageList, hasMore);
                                         });
                                        //after_count(messageList, hasMore);
                                    } else {
                                         after_count(messageList, hasMore);
                                    }
                                } else {
                                    console.log(err);
                                }
                            });
                }
            } catch (err) {
                    console.log('Error in  ask userlist channel:' + err);
            }
    
    
         // to send message list to requester.
         function after_count(messageList,hasMore) {
             
         
            var Returndata = {};
            
            if (userlist.hasOwnProperty(user_id)) {
                if (messageList.length > 0){
                    Returndata = {success: STATUS_TRUE, status_code: STATUS_200, has_more: hasMore, data: messageList};
                    console.log(JSON.stringify(Returndata));
                    userlist[user_id].emit('onMessagesReceived', JSON.stringify(Returndata));
                }
                else{
                    Returndata = {success: STATUS_FALSE, status_code: STATUS_201, has_more: false, message:NO_DATA, data: messageList};
                    console.log(JSON.stringify(Returndata));
                    userlist[user_id].emit('onMessagesReceived', JSON.stringify(Returndata));
                }   
            }
        }
    });
    
    // END MESSAGING LIST.
    
    
   // To get latest updated message list.
     
    socket.on('requestUpdatedMessages', function (user_id,user_type, last_update_date) {
        
        console.log('last date: '+last_update_date);
        
         try{   
                if (user_id != "") {
                        
                      //  var $startFrom  = PAGE_LIMIT * page_index;
                        // var $startFromMore  = PAGE_LIMIT * page_index + PAGE_LIMIT;
                        var hasMore  = STATUS_FALSE;
//                        var temp = connection.query('SELECT SQL_CALC_FOUND_ROWS jm.message_id, jm.thread_id, ji.user_id, IFNULL(jm.height) as height, IFNULL(jm.width) as width, jm.job_id,IFNULL((SELECT text FROM job_messages WHERE thread_id = jm.thread_id ORDER BY message_id DESC LIMIT 1), "") as text, IFNULL((SELECT image_url FROM job_messages WHERE thread_id = jm.thread_id ORDER BY message_id DESC LIMIT 1), "") as image_url, (SELECT sender_id from job_messages WHERE job_id =jm.job_id and thread_id = jm.thread_id and user_type = 2) as agent_id, (SELECT COUNT(message_id) from job_messages WHERE thread_id = jm.thread_id and receiver_id = '+ user_id +' and sender_id = agent_id and read_flag = 0 limit 1) as total_unread, (SELECT name from front_users WHERE id = agent_id) as agent_name, (SELECT profile_pic from front_users WHERE id = agent_id) as agent_profile_pic, (SELECT from_social_network from front_users WHERE id = agent_id) as from_social_network, j.job_status, jm.message_type, jm.created_date FROM job_messages AS jm LEFT JOIN job_invite AS ji ON jm.job_id = ji.job_id LEFT JOIN jobs AS j ON j.job_id = ji.job_id WHERE j.job_status IN (1,3,4,5) and j.user_id = '+ user_id +' and jm.created_date > "'+ last_update_date +'" GROUP BY (jm.thread_id) ORDER BY (jm.message_id) DESC', function (err, rows, fields)  
                        var temp = connection.query('SELECT SQL_CALC_FOUND_ROWS jm.message_id, jm.thread_id, ji.user_id, jm.job_id,IFNULL((SELECT text FROM job_messages WHERE thread_id = jm.thread_id ORDER BY message_id DESC LIMIT 1), "") as text, IFNULL((SELECT image_url FROM job_messages WHERE thread_id = jm.thread_id ORDER BY message_id DESC LIMIT 1), "") as image_url, (SELECT sender_id from job_messages WHERE job_id =jm.job_id and thread_id = jm.thread_id and user_type = 2 	LIMIT 1 ) as agent_id, (SELECT COUNT(message_id) from job_messages WHERE thread_id = jm.thread_id and receiver_id = '+ user_id +' and sender_id = agent_id and read_flag != 2) as total_unread, (SELECT name from front_users WHERE id = agent_id) as agent_name, (SELECT profile_pic from front_users WHERE id = agent_id) as agent_profile_pic, (SELECT from_social_network from front_users WHERE id = agent_id) as from_social_network, j.job_status, jm.message_type, jm.created_date FROM job_messages AS jm LEFT JOIN job_invite AS ji ON jm.job_id = ji.job_id LEFT JOIN jobs AS j ON j.job_id = ji.job_id WHERE message_id IN (SELECT MAX(message_id) FROM job_messages GROUP BY thread_id) AND j.job_status IN (1,3,4,5) and j.user_id = '+ user_id +' and jm.created_date > "'+ last_update_date +'" GROUP BY (jm.thread_id) ORDER BY (jm.message_id) DESC', function (err, rows, fields) 
                         {
                                if (!err) {
                                    
                                    var messageList = [];
                                    var TotalRows = 0; 
                                    if (rows.length > 0) {

                                        for (i = 0; i < rows.length; i++) {
                                            
                                                var profile_pic = '';
                                                if(ValidURL(rows[i].agent_profile_pic)){
                                                    profile_pic = rows[i].agent_profile_pic;
                                                } else{
                                                    if(rows[i].agent_profile_pic != ''){
                                                        profile_pic = AVATAR_URL+rows[i].agent_profile_pic;
                                                    } else {
                                                        profile_pic = AVATAR_URL+ 'no_image_user.png';
                                                    }
                                                }
                                            
                                            if(rows[i].total_unread > 0){
                                                    
                                                      if (userlist.hasOwnProperty(rows[i].agent_id)) {
                                                            Returndata = {thread_id: rows[i].thread_id, receiver_id: rows[i].agent_id};
                                                                console.log(JSON.stringify(Returndata));
                                                                userlist[rows[i].agent_id].emit('onReceivedMessages', JSON.stringify(Returndata));
                                                     }
                                                }
                                            
                                                messageList.push({
                                                    thread_id: rows[i].thread_id,
                                                    job_id: rows[i].job_id,
                                                    job_status: rows[i].job_status,
                                                    message_type: rows[i].message_type,
                                                    total_unread: rows[i].total_unread,
                                                    agent_id: rows[i].agent_id,
                                                    agent_name: rows[i].agent_name,
                                                    agent_profile_pic: profile_pic,
                                                    created_date: return_date_function(rows[i].created_date),
                                                    data:{'text': rows[i].text, 'image_url': rows[i].image_url, 'height': rows[i].height, 'width': rows[i].width}
                                                });
                                      }
                                       
//                                        var temp1 = connection.query('SELECT FOUND_ROWS() as totalcount', function (err, rows1, fields){
//                                                   TotalRows = rows1[0].totalcount;
//                                                    if($startFromMore >= TotalRows){
//                                                        hasMore = false;
//                                                    }
//                                                    console.log($startFrom);
//                                                    console.log(hasMore);
//                                                    after_count(messageList, hasMore);
//                                         });
                                        afterMessageUpdateProcess(messageList, hasMore);
                                    } else {
                                         afterMessageUpdateProcess(messageList, hasMore);
                                    }
                                } else {
                                    console.log('Error in  ask update message list channel:' + err);  
                                    var messageList = [];
                                    var hasMore = STATUS_FALSE;
                                    afterMessageUpdateProcess(messageList, hasMore);
                                }
                            });
                }
            } catch (err) {
                console.log('Error in  ask update message list channel:' + err);   
            }
    
    
        // to send message list to requester.
         function afterMessageUpdateProcess(messageList,hasMore) {
             
           // var jsonStr = JSON.stringify(messageList);
            var Returndata = {};
            
            if (userlist.hasOwnProperty(user_id)) {
                if (messageList.length > 0){
                    Returndata = {success: STATUS_TRUE, status_code: STATUS_200, has_more: STATUS_FALSE, data: messageList};
                    console.log(JSON.stringify(Returndata));
                    userlist[user_id].emit('onUpdatedMessages', JSON.stringify(Returndata));
                }
                else {
                    Returndata = {success: STATUS_FALSE, status_code: STATUS_201, has_more: STATUS_FALSE, message:NO_DATA, data: messageList};
                    console.log(JSON.stringify(Returndata));
                    userlist[user_id].emit('onUpdatedMessages', JSON.stringify(Returndata));
                }   
            }
        }
    });
    
    
     // TO SEND MESSAGE.
    socket.on('messageSend', function (data) {
    
        var objectRequest =  JSON.parse(data);
        //var objectRequestData = JSON.parse(objectRequest.data);
        console.log('input json data');
        console.log(data);
        console.log('input data');
        console.log(objectRequest);
        
        try {
                 var post = {
                        job_id: objectRequest.job_id,
                        sender_id: objectRequest.sender_id,
                        receiver_id: objectRequest.receiver_id,
                        thread_id: objectRequest.thread_id,
                        message_type: objectRequest.message_type,
                        user_type: objectRequest.user_type,
                        image_url: objectRequest.data.image_url,
                        height: objectRequest.data.height,
                        width: objectRequest.data.width,
                        text: objectRequest.data.text,
                        read_flag: 0,
                        created_date: getTimeUtc()
                    };
                    var query = connection.query('INSERT INTO job_messages SET?', post, function (error, result) {
                        if (error) {
                            console.log(error.message);
                        } else {
                            user_chat(result.insertId, objectRequest);
                        }
                    });
            
               
        }
        catch(err){
            console.log(err);
            console.log('error in message send.')
        }
        
        // To send message to user.
        function user_chat(message_id, objectRequest){
                
            var sendMessage = {};
            
                try{
                    var queryForResponse = connection.query('SELECT message_id, job_id, thread_id, sender_id,height,width, receiver_id, user_type, read_flag, created_date, message_type, IFNULL(text, "") as text, IFNULL(image_url, "") as image_url FROM job_messages WHERE message_id = '+ message_id +'', function (err, rows, fields) {
                          if (!err) {
                                if (rows.length > 0) {
                                        sendMessage = { message_id: rows[0].message_id,
                                                       job_id: rows[0].job_id,
                                                       thread_id: rows[0].thread_id,
                                                       sender_id: rows[0].sender_id,
                                                       receiver_id: rows[0].receiver_id,
                                                       user_type: rows[0].user_type,
                                                       status: rows[0].read_flag,
                                                       send_time: time_formate(rows[0].created_date),
                                                       message_type: rows[0].message_type,
                                                       data:{text: rows[0].text,
                                                             image_url:rows[0].image_url,
                                                             height: rows[0].width,
                                                             width: rows[0].height
                                                        }
                                                    };
                                         if (userlist.hasOwnProperty(objectRequest.sender_id)) {
                                             userlist[objectRequest.sender_id].emit('onMessageReceived', JSON.stringify(sendMessage));
                                         }
                                         if (userlist.hasOwnProperty(objectRequest.receiver_id)) {
                                             userlist[objectRequest.receiver_id].emit('onMessageReceived', JSON.stringify(sendMessage));
                                         }else
                                         {
                                            console.log('Receiver is not online.');
                                         }
                                    
                                        //send to admin also if disputed job.
                                        if(objectRequest.is_dispute == 1){
                                                
                                             if (adminlist.hasOwnProperty(objectRequest.job_id)) {
                                                    for(x in adminlist[objectRequest.job_id])
                                                    { 
                                                        adminlist[objectRequest.job_id][x].emit('onMessageReceive', JSON.stringify(sendMessage));
                                                    }
                                                }
                                            
                                        }
                                }
                          }
                        else{
                            console.log('error in sending message.');
                        }
                    
                    });
                }catch(err){
                    
                }
        }
        
    });
    
    // END SEND MESSAGE.
    
    
    // TO GET HISTORY OF USER.
    socket.on('requestHistory', function (user_id, user_type, thread_id, page_index, receiver_id) {
             
            var sendMessage = {};
            var data = new Array();
            var $startFrom  = HISTORY_PAGE_LIMIT * page_index;
            var $startFromMore  = HISTORY_PAGE_LIMIT * page_index + PAGE_LIMIT;
            var hasMore  = true;
            
                try 
                    {
                    
                    // To pre read the status of the user.
                     var preReadUpdate = connection.query('UPDATE job_messages set read_flag = 2 WHERE thread_id = "'+ thread_id +'"  and receiver_id = '+ user_id +'', function (err, rows, fields) {
                            if (!err) 
                             {
                               
                               console.log('updated:' + thread_id);
                             }
                        });
                    
                    var queryForResponse = connection.query('SELECT SQL_CALC_FOUND_ROWS jm.message_id, jm.job_id, jm.thread_id, jm.sender_id, jm.receiver_id, jm.user_type, jm.read_flag, jm.created_date, jm.message_type, jm.text, jm.height, jm.image_url, jm.width FROM job_messages AS jm WHERE thread_id = "'+ thread_id + '" ORDER BY jm.message_id DESC LIMIT ' + $startFrom + ', '+ HISTORY_PAGE_LIMIT +'', function (err, rows, fields) {
                          if (!err) {
                                    var messageList = [];
                                    var TotalRows = 0; 
                                    if (rows.length > 0) {
                                        
                                         // update the job messages as read status.
                                         var queryForUpdate = connection.query('UPDATE job_messages set read_flag = 2 WHERE thread_id = "'+ thread_id +'"  and receiver_id = '+ user_id +'', function (err, rows, fields) {
                                              if (!err) 
                                              {
                                                console.log('rows');
                                                console.log('updated:' + thread_id);
                                              } 
                                         });

                                        for (i = 0; i < rows.length; i++) {
                                            
                                              data.push({
                                                    message_id: rows[i].message_id,
                                                    job_id: rows[i].job_id,
                                                    thread_id: rows[i].thread_id,
                                                    sender_id: rows[i].sender_id,
                                                    receiver_id: rows[i].receiver_id,
                                                    user_type: rows[i].user_type,
                                                    status: rows[i].read_flag,
                                                    message_type: rows[i].message_type,
                                                    send_time: return_date_function(rows[i].created_date),
                                                    data:{'text': rows[i].text, 'image_url': rows[i].image_url, 'height': rows[i].height, 'width': rows[i].width}
                                                });
                                            
                                      }
                                       
                                        var temp1 = connection.query('SELECT FOUND_ROWS() as totalcount', function (err, rows1, fields){
                                                   TotalRows = rows1[0].totalcount;
                                                    if($startFromMore >= TotalRows){
                                                        hasMore = false;
                                                    }
                                                   
                                                    processhistory(data, hasMore, page_index);
                                         });
                                        //after_count(messageList, hasMore);
                                    } else {
                                         processhistory(data, hasMore, page_index);
                                    }
                                }
                        else {
                            console.log(err);
                            console.log('error in history message.');
                        }
                        
                        function processhistory(data, hasMore, page_index){
                            
                              //  console.log(data);
                                var Returndata = {};
            
                                if (userlist.hasOwnProperty(user_id)) {
                                    if (data.length > 0){
                                        Returndata = {success: STATUS_TRUE, status_code: STATUS_200, has_more: hasMore, data: data};
                                      //  console.log(JSON.stringify(Returndata));
                                        userlist[user_id].emit('onHistoryReceived', JSON.stringify(Returndata));
                                    }
                                    else {
                                        Returndata = {success: STATUS_FALSE, status_code: STATUS_201, has_more: STATUS_FALSE, message:NO_DATA, data: data};
                                       // console.log(JSON.stringify(Returndata));
                                        userlist[user_id].emit('onHistoryReceived', JSON.stringify(Returndata));
                                    }   
                                }
                                
                                // Lastly inform the sender.
                                if(userlist.hasOwnProperty(receiver_id)){
                                    
                                        var toInformUser = {thread_id: thread_id,receiver_id: receiver_id};
                                        userlist[receiver_id].emit('onReadMessages', JSON.stringify(toInformUser));
                                
                                }
                        }
                    });
                } catch(err){
                    
                }
    });
    //END  THE CHAT HISTORY.
    
    
    // MESSAGE UPDATE STATUS IS START.
    
     socket.on('statusSend', function (sender_id, receiver_id, status, message_id) {
        
         console.log('sender_id' + sender_id);
         console.log('receiver_id'+ receiver_id);
         console.log('status'+ status);
         console.log('message_id' + message_id);
         
         try {
                      try{
                            var queryForResponse = connection.query('UPDATE job_messages set read_flag = '+ status +' WHERE message_id = '+ message_id +'  and sender_id = '+ sender_id +'  and receiver_id = '+ receiver_id +'', function (err, rows, fields) {
                                  if (!err) 
                                  {
                                      if (userlist.hasOwnProperty(sender_id)) {
                                           // if (data.length > 0){
                                          
                                                Returndata = {message_id: message_id, status: status, sender_id: sender_id, receiver_id: receiver_id};
                                                console.log(JSON.stringify(Returndata));
                                                userlist[sender_id].emit('onStatusReceived', JSON.stringify(Returndata));
                                          
                                           // }
        //                                    else {
        //                                        Returndata = {success: STATUS_FALSE, status_code: STATUS_201, has_more: STATUS_FALSE, message:NO_DATA, data: data};
        //                                        console.log(JSON.stringify(Returndata));
        //                                        userlist[sender_id].emit('onHistoryReceived', JSON.stringify(Returndata));
        //                                    }   
                                        }

                                  }
                            });



                 } catch(err){

                    console.log('Error in message send status.');
                    console.log(err);

                 }
         }
         catch(err){
             
            console.log('Error in message send status.');
            console.log(err);
             
         }
     
     
     });
    
    // MESSAGE UPDATE STATUS IS END.
    
    
     // SEND MESSAGING FROM ADMIN TO USER.
     socket.on('sendMessageAdmin', function (objectRequest) {
         
       console.log(objectRequest.job_id);
             
                     try {
                                            var sendMessage = {};
                                            sendMessage = { message_id: objectRequest.message_id,
                                                               job_id: objectRequest.job_id,
                                                               thread_id: objectRequest.thread_id,
                                                               sender_id: objectRequest.sender_id,
                                                               receiver_id: 0,
                                                               user_type: objectRequest.user_type,
                                                               status: objectRequest.read_flag,
                                                               send_time: time_formate(objectRequest.created_date),
                                                               message_type: objectRequest.message_type,
                                                               data:{text: objectRequest.text,
                                                                     image_url: '',
                                                                     height: 0,
                                                                     width: 0
                                                                }
                                                            };

                                                 if (userlist.hasOwnProperty(objectRequest.agent_id)) {
                                                     userlist[objectRequest.agent_id].emit('onMessageReceived', JSON.stringify(sendMessage));
                                                 }
                                                 if (userlist.hasOwnProperty(objectRequest.requester_id)) {
                                                     userlist[objectRequest.requester_id].emit('onMessageReceived', JSON.stringify(sendMessage));
                                                 }else
                                                 {   
                                                    console.log('Receiver is not online.');
                                                 }
                     }
                catch(err){

                        console.log('Error in message send admin status.');
                        console.log(err);

                     }
     });
    
    //End.
    
    // To notity the requester for new proposal.
    socket.on('requestSendProposal', function(user_id){
        
    console.log('In requestSendProposal -->' + user_id);
    
            try {
                 if (userlist.hasOwnProperty(user_id)) {
                   
                    //if (data.length > 0){
                           var Returndata = {success: 1, status_code: STATUS_200, message: 'New update’s available.'};
                           console.log(JSON.stringify(Returndata));
                           userlist[user_id].emit('onNewAgentMessages', JSON.stringify(Returndata));
                   // }
                 }
            }
            catch(err){
                console.log('Error in NotifyRequesterForNewproposal.');
                console.log(err);
            }
    });
    //End notify the user.
   
    
    //////////////////////////////////////////////////////////////////////////////////
    // GLOBAL FUNCTION FOR CHAT.                                                    //
    //////////////////////////////////////////////////////////////////////////////////
    
//    function getJobStatusForAdminMessage($job_id){
//    
//    
//    
//    }
    
    // To validate the picture of the user.
    function ValidURL(str) {
              var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
              if(!regex .test(str)) {
                return false;
              } else {
                return true;
              }
            }
    
        // to check file is exist or not.
        function ImageExist(url) 
        {
           var img = new Image();
           img.src = url;
           return img.height != 0;
        }
    
    
     try {
        
        socket.on('disconnect', function () {
            
            console.log('Disconnected user:' + socket.id);
            var socketId = socket.id;
            var i = allClients.indexOf(socket);
            delete allClients[i];
            for (var k in userlist) {
                if (socketId == userlist[k].id) {
                    console.log('Disconnected user:' + userlist[k]);
                    delete userlist[k];
                    break;
                }
            }
            
             // disconnect the user.
                var ii = alladminlist.indexOf(socket);
                delete alladminlist[ii];
                for (var kk in adminlist) {
                    var currentuser = adminlist[kk];
                        for(var xx in currentuser)
                        {
                            if (socketId == currentuser[xx].id) 
                            {
                                currentuser.splice(xx,1);
                                // send_disconnect(kk);
                                break;
                            }
                        }
                }
            // end admin list.
        });
        
    } catch (err) {
        console.log(err);
    }
    
});  

    //This function print data.
    function print_data(data) {
        if (print_flag == true) {
            console.log(data);
            console.log('--------------');
        }
    }
    
    // Function for date function.
    function return_date_function(msg_date) {
           return time_formate(msg_date);
    }

    //For time formate.
function time_formate(time) {

    // var date_time = new Date().toISOString().slice(0, 19).replace('T', ' ');
    var d = new Date(time);
    var curr_date = ("0" + (d.getDate())).slice(-2).toString();
    var curr_month = ("0" + (d.getMonth() + 1)).slice(-2).toString();
    var curr_year = d.getFullYear();
    var curr_hours = ("0" + (d.getHours())).slice(-2).toString();
    var curr_minutes = ("0" + (d.getMinutes())).slice(-2).toString();
    var curr_sec = ("0" + (d.getSeconds())).slice(-2).toString();
    return curr_year + "-" + curr_month + "-" + curr_date + " " + curr_hours + ":" + curr_minutes + ":" + curr_sec;
    
}
function getTimeUtc() {

    var d = new Date();
    var curr_date = ("0" + (d.getDate())).slice(-2).toString();
    var curr_month = ("0" + (d.getMonth() + 1)).slice(-2).toString();
    var curr_year = d.getFullYear();
    var curr_hours = ("0" + (d.getHours())).slice(-2).toString();
    var curr_minutes = ("0" + (d.getMinutes())).slice(-2).toString();
    var curr_sec = ("0" + (d.getSeconds())).slice(-2).toString();
    return curr_year + "-" + curr_month + "-" + curr_date + " " + curr_hours + ":" + curr_minutes + ":" + curr_sec;
    
}


server.listen(7879, function (msg) {
    console.log('listening on UNIDeal server *:7879');
});