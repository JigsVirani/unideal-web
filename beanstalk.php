<?php

/* 1. Setup & connect */

// Include composer libraries
require 'vendor/autoload.php';

$queue_name = 'default';
$job_data   = 'lorem ipsum';

$beanstalkd = new Pheanstalk_Pheanstalk('127.0.0.1:11300');

/* 2. Provider */

// Enqueue a job
/*$beanstalkd
  ->useTube($queue_name)
  ->put($job_data);*/

/* 3. Worker */

// Loop through all jobs
while ($job = $beanstalkd->watch($queue_name)->reserve(5)) {
    try {
        $job_data = $job->getData();

        echo $job_data . PHP_EOL;

        // Dequeue a job
//        $beanstalkd->delete($job);
    } catch (Exception $e) {
        // Bury a job
//        $beanstalkd->bury($job);
        echo $e->getMessage();
    }
}
?>