<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */
                ############################## FRONT PAGES URLS ####################

Route::group(array('prefix' => 'front'), function() {
 
});

Route::any("termsandconditions", array('as' => 'termsandconditions', 'uses' => 'HomeController@getTermscondition'));
Route::any("helpscreen", array('as' => 'helpscreen', 'uses' => 'ApiconfigController@getHelpscreen'));

               ############################## FRONT PAGES ROUTES ENDS #######


                ######### MANAGE SECTION ROUTES STARTS HERE ######################

Route::group(array('prefix' => 'manage'), function() {
    //login bypass for the below listed controllers
    Route::controller('login', 'AdminloginController');
    Route::controller('forgotpassword', 'AdminforgotpasswordController');
});

Route::group(array('before' => 'managelogin', 'prefix' => ''), function()  //the before here is the  auth check if user isnt login then redirect him login
{	
    Route::controller('manage', 'AdminhomeController');
    

});

Route::group(array('before' => 'manage_ajax', 'prefix' => ''), function()  //the before here is the  auth check if user isnt login then redirect him login
{
  Route::controller('dashboard', 'AdmindashboardController');
  Route::controller('settings', 'AdminsettingsController');
  Route::controller('users', 'AdminusersController');
  Route::controller('jobs', 'AdminjobsController');
  Route::controller('adminprofile', 'AdminprofileController');
});


//logout.
Route::any("logout", array('as' => 'logout', 'uses' => 'AdminloginController@getLogout'));

                       ###### MANAGE SECTION ROUTES ENDS HERE ################


###################### API ROUTES STARTS ########################


//route define for api with api prefix
Route::group(array('prefix' => 'api'), function() {
    Route::controller('users', 'ApiuserController'); 
    Route::controller('payment', 'ApipaymentController');
    Route::controller('job', 'ApijobsController');
    Route::controller('config', 'ApiconfigController');
    Route::controller('message', 'ApichatController');
    Route::controller('queue', 'QueueController');
    
});
//Route::controller('cron', 'CronController');
App::missing(function($exception) {
    
   return View::make('layout.managemaster'); 
});


Route::controller('user', 'AdminUsersController');
//Route::any("queue", array('uses' => 'QueueController'));

