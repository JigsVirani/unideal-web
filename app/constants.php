<?php
//define document root

define('SITE_TITLE', 'UNIDeal');
define('BASEURL',url('/'));
define('BASEURL_MANAGE',url('/').'/manage/');
define('DIR_UPLOADS', url('/') . '/uploads/');

define('ASSETS_URL', url('/') . '/assets/');
define('MANAGE_TEXT', 'manage/');
define('ADMIN_TEXT', 'admin/');
define('ASSETS',url('/').'/assets/');
define('ASSETS_IMAGE',url('/').'/assets/images/');
define('ASSETS_IMAGE_FRONT',url('/').'/assets/images/');
define('DIR_PROFILE_PIC',public_path('') . '/uploads/profile/');
define('DIR_MESSAGE_PIC',public_path('') . '/uploads/messages/');
define('DIR_MESSAGE_PIC_URL',url('/') . '/uploads/messages/');
define('DIR_PROFILE_PIC_URL',url('/') . '/uploads/profile/');
define('DIR_PROFILE_DOC',public_path('') . '/uploads/documents/');
define('DIR_PROFILE_DOC_URL',url('/') . '/uploads/documents/');
define('DIR_IOS_PUSH', public_path('') . '/uploads/iospush/');

define('ADMINPROFILE_IMAGE_URL',  url('/') . '/uploads/profile/');
define('ADMINPROFILE_IMAGE_PATH',  public_path('') . '/uploads/profile/');
define('ADMINPDF_USERS_PATH',  public_path('') . '/uploads/users_list/');

define('DIR_EXPORTS',public_path('') . '/uploads/exports/');
define('DIR_JOBS', public_path('') . '/uploads/jobs/');
define('DIR_THUMBS_JOBS', public_path('') . '/uploads/jobs/thumbs/');
define('URL_JOBS_IMAGES',  url('/') . '/uploads/jobs/');
define('URL_JOBS_THUMBS_IMAGES',  url('/') . '/uploads/jobs/thumbs/');

define('NEW_JOBS_IMAGES',public_path('') . '/uploads/jobs/');
define('NEW_JOBS_IMAGES_URL',url('/') . '/uploads/jobs/');

define('HELP_SCREEN_URL', BASEURL . '/helpscreen');
define('TERMSANDCONDITION_SCREEN_URL', BASEURL . '/termsandconditions');


define('MORE_FROM_LAST', 'More From Last Month');
define('LEASS_FROM_LAST', 'LESS From Last Month');

define('NOREPLY_EMAIL','noreply@unideal.com');
define('ADMIN_EMAIL','jimmypoon@rubiksc.com');
define('ADMIN_EMAIL_2','bhargav@creolestudios.com');

define('STATUS_CODE_201', 201); //When Success is false it will return 201 (general error).
define('STATUS_CODE_200', 200); // When Success is true it will return 200.
define('STATUS_CODE_301', 301); //Request User is not exist.
define('STATUS_CODE_302', 302); //User deactivated by admin.
define('STATUS_CODE_303', 303); //Email Address not Verified.

## Global status.
define('STATUS_FALSE', 0);
define('STATUS_TRUE', 1);

define('COULD_NOT_UPLOAD', 'Could not upload');
## Job status codes.
define('JOB_STATUS_OPEN', 1); // jobs status is in open.
define('JOB_STATUS_APPLIED', 2); // job status is applied.
define('JOB_STATUS_INPROCESS', 3); // job status in process.
define('JOB_STATUS_COMPLETED', 4); // job status in completed
define('JOB_STATUS_PAUSED', 5); // job status paused.
define('JOB_STATUS_RESUME', 6); // job status resume.
define('JOB_STATUS_CANCELLED', 7); //job status cancelled.


define('SERVICE_FEES_ADVANCE', 1);
define('JOB_BUDGET_ADVANCED', 2);
define('COMMISION_CHARGE', 3);
define('PROMOCODE_ID', 5);
define('MAX_ID', 7);
define('MIN_ID', 6);

define('AWARDED', 1);//Job applicant awarded
define('DECLINE', 2);//Job applicant decline

define('OS_TYPE_IOS', 1);
define('OS_TYPE_ANDROID', 2);

define('IS_AGENT', 2);
define('IS_REQUESTER', 1);
define('IOS_PASSPHARSE', 1234);
define('GATEWAY_PUSH_APPLE','ssl://gateway.sandbox.push.apple.com:2195');

##admin email.
//define('ADMIN_EMAIL','jimmypoon@rubiksc.com');



?>
