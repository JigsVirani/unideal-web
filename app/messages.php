<?php

//################## General message ##############################/
define('INVALID_PARAMS', 'Invalid parameters.');
define('INVALID_CREDENTIALS_PARAMS', 'Invalid Credentials!!');
define('INVALID_IMAGE', 'Image is not valid.');
define('WELCOME_MESSAGE', 'Welcome to UNIDeal!!');
define('GENERAL_ERROR', 'Oops! Something just went wrong. Please try again later.');
define('GENERAL_SUCCESS', 'Done!');
define('GENERAL_NO_CHANGES', 'You have made no changes.');
define('GENERAL_NO_DATA', 'No data available.');
define('NO_USER_DATA', 'No data available.');
define('PASSWORD_FAIL', 'Password was not updated. Try Again!');
define('INVALID_CURRENT_PASSWORD', 'Current password is incorrect.');
define('PASSWORD_SUCCESS_CHANGE', 'Password updated successfully.');
//################## Admin panel messages start ##############################/
define('LOGIN_ERROR', 'Invalid email address or password.');
define('LOGIN_SUCCESS', 'Authentication completed successfully.');
define('LOGIN_FAIL_NO_ACCOUNT', 'The account does not exist. Kindly register to proceed.');
define('LOGIN_FAIL_INCORRECT_PASSWORD', 'Incorrect password.');
define('LOGIN_FAIL_ACCOUNT_INACTIVE', 'Your account has been deactivated.Kindly contact administrator.');
define('LOGIN_FAIL_ACCOUNT_NOT_VERIFIED', 'Your account has not been verified. Kindly check email for verification.');
define('LOGOUT', 'You have been successfully logged out.');
define('EMAIL_NOT_EXIST', 'Email is not matched with account.');
define('FORGOT_PASSWORD_SUCCESS', 'An email has been sent to you. Please check your inbox.');
define('INVALID_OLD_PASSWORD', 'Your old password is incorrect.');
define('SUCCESS_PASSWORD_CHANGE', 'Password updated successfully. Redirecting to Login...');
define('ADMIN_TOKEN_EXPIRED', 'Link has been expired.');
define('SUCCESS_PROFILE_UPDATE', 'Profile has been updated.');
define('OLD_PASSWORD_NOT_OK', 'Old Password is not correct.');
define('PASSWORD_CHANGED', 'Password has been changed.');
define('SAME_PASSWORD', 'You cannot set the current password as the new password.');


//####################### Admin profile section ##########################################
define('SUCCESS_ADMIN_PROFILE_UPDATE', 'Profile updated successfully.');
define('ERROR_ADMIN_PROFILE_UPDATE', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_ADMIN_CHANGE_PASSWORD', 'Your have changed your password successfully');
define('ERROR_ADMIN_CHANGE_PASSWORD', 'Oops! Something just went wrong. Please try again later.');
define('ERROR_INVALID_CURRENT_PASSWORD', 'Invalid current password!'); 
//####################### Mange section ##########################################
define('SUCCESS_CATEGORY_ADD','Category added successfully ');
define('ERROR_CATEGORY_ADD', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_CATEGORY_EDIT','Category updated successfully ');
define('SUCCESS_PAYMENT_EDIT', 'Updated successfully.');
define('ERROR_PAYMENT_EDIT', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_COMMISSION_EDIT', 'Commission updated successfully.');
define('ERROR_COMMISSION_EDIT', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_TERMSCONDITION_EDIT', 'Terms & condition updated successfully.');
define('ERROR_TERMSCONDITION_EDIT', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_CONTACTUS_TEXT_EDIT', 'Contact Us updated successfully.');
define('ERROR_CONTACTUS_TEXT_EDIT', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_DISCLAIMER_EDIT', 'Disclaimer Text updated successfully.');
define('ERROR_JOBPOSTING_EDIT', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_JOBAPPLYING_EDIT', 'Terms & Condition for job applying updated successfully.');
define('ERROR_JOBAPPLYING_EDIT', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_PROMOCODE_ADD','Promo code added successfully ');
define('ERROR_PROMOCODE_ADD', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_REFERRAL_AMOUNT_EDIT','Referral amount updated successfully ');
define('ERROR_REFERRAL_AMOUNT_EDIT', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_ADDITIONAL_CATEGORY_ADD','Additional category saved successfully ');
define('SUCCESS_ADDITIONAL_CATEGORY_DELETE','Category deleted successfully ');
define('ERROR_ADDITIONAL_CATEGORY_ADD', 'Oops! Something just went wrong. Please try again later.');
?>
