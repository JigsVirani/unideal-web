<?php

class AdminjobsController extends BaseController {

    protected $layout = 'layout.managemaster';
	
	 ## Jigs Virani 18th Oct 2016
     ## To get all the users.
	public function anyAlljobs()
	{ 
		$users = DB::table('jobs AS jbs')->select('jbs.job_id', 'jbs.job_title','jc.category_name','jbs.is_dispute',
                                                  DB::raw('CONCAT(DATE_FORMAT(jbs.job_start_on,"%d-%m-%Y %l:%i")) as job_start_on'), DB::raw('CONCAT(jbs.consignment_size," HK$") as consinment_size'), 
                                                  'jbs.job_status')
                 ->leftJoin('job_categories AS jc', 'jbs.category_id', '=', 'jc.category_id')
                 ->orderBy('jbs.job_id', 'DESC');
        if(Input::get('jobs_categories') !='' && Input::get('jobs_categories') !='0'){ 
           $users->where('jbs.category_id','=',Input::get('jobs_categories'));
        }
        if(Input::get('jobs_status') !='' && Input::get('jobs_status') !='0') { 
           $users->where('jbs.job_status','=', Input::get('jobs_status'));
        }
       
        return Datatables::of($users)
                           ->edit_column('job_title', ' <a class="disable_link"  href="manage/jobinformation/{{base64_encode($job_id)}} ">{{$job_title}}</a>')
                           ->make(true);
	}
    
    
    ## Jigs Virani 19th Oct 2016.
    ## To get all the job categories.
    public function getGetjobscategories(){
    
         $jobs_cat['cate_array'] = Adminjobs::getalljobscategories();        
        // Pass the message with redirection on listing page.
        return Response::json($jobs_cat);
    
    }
    
    public function getCountdispute(){
    
         $jobs_dispute['count'] = Adminjobs::countdispute();        
        // Pass the message with redirection on listing page.
        return Response::json($jobs_dispute);
    
    }
    
    
    ## Jigs Virani 19th Oct 2016
   ## To export user data as xlsx 
    
    public function getGeneratecsv($categoty_id, $status_id) {
        
        //global declaration
        $ResponseData = array();
        $UpdateArray = array();
       
        
            //call function to get user data
            $ResultUserList = Adminjobs::generatecsv($categoty_id, $status_id);
          
        
            if($ResultUserList){
                
                //define array.
                $OutPutHeaderArray = array('Id', 'Job Title', 'Category', 'Start Date', 'Budget', 'Status');

                //call excel library for file generation.
                Excel::create('UserList-' . date('dSMY'), function($excel) use($OutPutHeaderArray, $ResultUserList) {
                    $excel->sheet('Sheetname', function($sheet) use($OutPutHeaderArray, $ResultUserList) {

                        //append header
                        $sheet->rows(array($OutPutHeaderArray));
                        //check for data availability
                        if (isset($ResultUserList) && !empty($ResultUserList)) {
                            //loop through data and create csv
                            foreach ($ResultUserList as $UserKey => $UserVal) {
                                $sheet->appendRow(array($UserVal['job_id'], $UserVal['job_title'], $UserVal['category_name'], $UserVal['job_start_on'], $UserVal['consignment_size'], $UserVal['job_status']));
                            }
                        }
                    });
                })->export('xlsx');
            }
            exit;
    }
    
    ## Jigs Virani 18th Oct 2016
   ## To export user data as pdf 
    
    public function getGeneratepdf($categoty_id, $status_id) {
        
        //global declaration
        $ResponseData = array();
        $UpdateArray = array();
       
        
            //call function to get user data
            $ResultUserList = Adminjobs::generatecsv($categoty_id, $status_id);
          
        //for new html.
       $html ='<div class="container" style=" font-size: 14px; background-color:#fff;">
                    <br>
                    <table cellpadding="0" cellspacing="0" width="100%" align="center" id="mainTable" style=" background:rgb(255,255,255);">
                    <tr>
                          <td>
                              <table cellpadding="0" cellspacing="0" width="100%">

                                    <tr>

                                        <td>
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td>
                                                    <br><br>
                                                    <img src="' . ASSETS_IMAGE . '/logo-big.png" style="display:block; line-height:300px; margin: 0 auto;" width="117px" border="0" ></td>
                                                    <td><br><br>
                                                    <span style="text-transform:uppercase;font-size:14px"></span> </td>
                                                    <td>&nbsp; </td>
                                                </tr>
                                            </table>

                                         </td>

                                    </tr>
                                </table>
                             </td>
                        </tr>
                        </table>
                         <br>
                         <br>
                          <br>
                           
                            <h2 style="text-align: center;">UNIDeal Jobs List</h2>
                       <table cellpadding="5" cellspacing="5" width="100%">
                          <thead class="thead-inverse">
                            <tr>
                              <th>ID#</th>
                              <th>Job Title</th>
                              <th>Category Name</th>
                              <th>Job Start On</th>
                              <th>Consinment Price</th>
                              <th>Job Status</th>
                             
                            </tr>
                          </thead>
                          <tbody>';
                           foreach ($ResultUserList as $UserKey => $UserVal) {
                                
                                     $html .= '<tr>
                                          <th scope="row">'. $UserVal['job_id']. '</th>
                                          <td style="text-align: center;">'.  $UserVal['job_title'] .'</td>
                                          <td style="text-align: center;">'.  $UserVal['category_name'] .'</td>
                                          <td style="text-align: center;">'. $UserVal['job_start_on'] .'</td>
                                          <td style="text-align: center;">'. $UserVal['consignment_size'] .'</td>
                                          <td style="text-align: center;">'. $UserVal['job_status'] .'</td>
                                        </tr>';
                           }
                            
            $html .=  '</tbody></tbody>
                        </table>
                </div>';
             // echo $html;die;
            $filename = "UNIDeal_Jobs_" . date('dSMY') . ".pdf";
            $pdf = App::make('dompdf');
            $pdf->loadHTML($html);
            $final_path = ADMINPDF_USERS_PATH . $filename;

            return $pdf->download($filename);
        
    }
    
    
    ## Jignesh Virani
    ## To get job details for job.
    
    public function getJobsdetails(){
        
         $returndata = array();
         $returndata['success'] = false;
         $job_id  = Input::get('job_id');
         $details = Adminjobs::getjobsdetails($job_id);
        
         
         if($details['success'] == 1){
             
                   $returndata['details'] = $details['data'];
                   $returndata['success'] = true;
         }
        
        // Pass the message with redirection on listing page.
        return Response::json($returndata);
    
    }
    
    
     public function getUserimage() {

        $data['user_image'] = USER_IMG . Auth::manage_user()->get()->image;

        return Response::json($data);
    }

    ## function for get user id.

    public function getChatuserid() {
        $data['user_id'] = Auth::manage_user()->get()->id;
        $data['unread_count'] = Message::getunreadmessages($data['user_id']);
        return Response::json($data);
    }

    ## function for get user id.

    public function postMessageimageupload() {
        $params = Input::all();
        $data['data'] = Message::getimageurl($params);

        return Response::json($data);
    }
    
    public function getHistory() {
        $params = Input::all();
        $data['data'] = Message::gethistory($params);

        return Response::json($data);
    }
    
    public function postGetchathistory(){
        
        $params = Input::all();
        $data['data'] = Message::getchathistory($params);
        return Response::json($data);
    }
    
    public function postMessagesend(){
        
        $params = Input::all();
        $data['data'] = Message::messagesend($params);
        return Response::json($data);
    }
    
}
