<?php

/**
 * This file demonstrates User registration,login,forgot password,delete & reactive account.
 * @author Bhargav Bhanderi <bhargav@creolestudios.com>
 * @version 1.0
 * @package : Login,Register,Forgot Password,Re active & Delete account
 * @copyright  Creole Studios 2015
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 */
class ApiconfigController extends BaseController {

    protected $layout = 'layout.managemaster';

    //###########################################################
    //Function : getIndex
    //purpose : for index action
    //input : void
    //output : error/scuccess message
    //###########################################################
    public function getIndex() {
        App::setLocale('en');
        echo trans('messages.welcome');
        exit;
    }
   
    public function postAllappconfig(){
    
        //global declaration.
        $ResponseData = array();
        
        $GetAllconfig = Apiconfig::GetAllappconfig();
        
        if (isset($GetAllconfig) && $GetAllconfig['status'] == '1') {
            $ResponseData['success'] = STATUS_TRUE;
            $ResponseData['message'] = GENERAL_SUCCESS;
             $ResponseData['status_code'] = STATUS_CODE_200;
            $ResponseData['data'] = $GetAllconfig['data'];
            
        } else {
            $ResponseData['success'] = STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = GENERAL_NO_DATA;
            $ResponseData['data'] = new stdClass();
        }
        
         return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
  //   return Response::json($ResponseData);
    }
    
    public function getHelpscreen(){
       
          $ResponseData = array();
          $GetAllconfig = Apiconfig::GetAllhelpquestion();
          
          if (isset($GetAllconfig) && $GetAllconfig['status'] == '1') {
          
            $ResponseData['data'] = $GetAllconfig['data'];
            return View::make('frontsite.helpscreen',$ResponseData);
        }
        else {
            $ResponseData['success'] = STATUS_FALSE;
            $ResponseData['message'] = GENERAL_NO_DATA;
            $ResponseData['data'] = new stdClass();
            return View::make('frontsite.helpscreen',$ResponseData);
        }
         
    }
}