<?php
class AdminsettingsController extends BaseController {
	 protected $layout = 'layout.managemaster';

    //###########################################################
    //Function : getAlljobcategories
    //purpose : Fetch all job categories
    //Author: Jitendra Parmar
    //###########################################################
    public function getAlljobcategories() {
		
        $data = DB::table('job_categories')
                ->select('category_id','category_name','category_id as id')->orderBy('created_on','ASC')
                ->where('status', 1);
        
		return Datatables::of($data)
			->edit_column('category_name', '<a href="javascript:;" class="view_jobs" data-id="{{$id}}">{{$category_name}}</a>')		
			->edit_column('id', '<a href="javascript:;" class="btn btn-green edit_category"  data-id="{{$id}}" ><i class="fa fa-pencil"></i> Edit </a> &nbsp; <a href="javascript:;" class="btn btn-danger delete_category"  data-id="{{$id}}" ><i class="fa fa-close"></i> Delete </a>')
          //  ->edit_column('id', '')
        ->make(true);
    }
    
    public function postCategorydelete(){
    
        $PostData = Input::all();
		$response = array();
		if(isset($PostData) && !empty($PostData)){
		  
			$add_category_status = Manage::DeleteJobCategory($PostData);
			if($add_category_status['status']){
				$response['success'] = true;
				$response['message'] = SUCCESS_ADDITIONAL_CATEGORY_DELETE;
			}
			else
			{
				$response['success'] = false;
				$response['message'] = ERROR_CATEGORY_ADD;
			}
			return Response::json($response);
		}
    
    }
	
    //###########################################################
    //Function : postAddjobcategory
    //purpose : Add job category
    //Author: Jitendra Parmar
    //###########################################################
    public function postAddjobcategory() {
		
		$PostData = Input::all();
		$response = array();
		if(isset($PostData) && !empty($PostData)){
			$PostData['created_on'] = gmdate("Y-m-d H:i:s");
			$PostData['status'] = 1;
			$add_category_status = Manage::AddJobCategory($PostData);
			if($add_category_status['status']){
				$response['success'] = true;
				$response['message'] = SUCCESS_CATEGORY_ADD;
			}
			else
			{
				$response['success'] = false;
				$response['message'] = ERROR_CATEGORY_ADD;
			}
			return Response::json($response);
		}
	}
	
	//###########################################################
    //Function : postJobcategorydetail
    //purpose : Get job category detail by id
    //Author: Jitendra Parmar
    //###########################################################
    public function postJobcategorydetail() {
		
		$PostData = Input::all();
		$response = array();
		if(isset($PostData) && !empty($PostData)){
			$CategoryData = Manage::JobCategoryDetail($PostData['category_id']);
			if($CategoryData){
				$response['success'] = true;
				$response['category_id'] = $CategoryData['category_id'];
				$response['category_name'] = $CategoryData['category_name'];
			}
			else
			{
				$response['success'] = false;	
			}
			return Response::json($response);
		}
	}
	
	/*##########################################################################
     * Function : postEditjobcategory()
     * Purpose  : To update category detail
     * @author : Jitendra Parmar
     * #########################################################################
     */
    public function postEditjobcategory(){        
        $PostData = Input::all();
		$response = array();
		if(isset($PostData) && !empty($PostData)){
			$CategoryData['category_name'] = $PostData['category_name'];
			$CategoryData['updated_on'] = gmdate("Y-m-d H:i:s");
			$CategoryID = $PostData['category_id'];
			$CategoryData = Manage::UpdateUserMultipleColumn($CategoryData,$CategoryID);
			if($CategoryData){
				$response['success'] = true;
				$response['message'] = SUCCESS_CATEGORY_EDIT;
			}
			else
			{
				$response['success'] = false;
				$response['message'] = ERROR_CATEGORY_EDIT;
			}
			return Response::json($response);
		}
    }
    
    ## Hardika Satasiya 18 nov 2016
    ## to get job sub categoey list.
    
    public function getAllsubcategories() {
        
	$data = DB::table('job_sub_categories as js')
                ->select('js.sub_category_id','js.category_id','js.sub_category_name','js.sub_category_id as id','jc.category_name')
                ->leftjoin('job_categories as jc','jc.category_id','=','js.category_id')
                ->orderBy('js.created_on','ASC')
                ->where('jc.status', 1)
                ->where('js.status', 1);
        
        return Datatables::of($data)
	->edit_column('id', '<a href="javascript:;" class="btn btn-green edit_sub_category" ng-click="GetSubCategory({{$id}})"  data-id="{{$id}}" ><i class="fa fa-pencil"></i> Edit </a><a href="javascript:;" class="btn btn-danger delete_sub_category"   data-id="{{$id}}" ><i class="fa fa-close"></i> Delete </a>')
        ->make(true);
        
    }
    
    ## Hardika Satasiya 18 nov 2016
    ## to get job sub categoey details
    
     public function postSubcategorydetail() {
		
		$PostData = Input::all();
		$response = array();
		if(isset($PostData) && !empty($PostData)){
			$CategoryData = Manage::SubCategoryDetail($PostData['category_id']);
			//print_r($CategoryData);die;
			if($CategoryData){
				$response['success'] = true;
                                $response['sub_cat'] = $CategoryData['category'];
				$response['sub_category_id'] = $CategoryData['sub_category']['sub_category_id'];
				$response['sub_category_name'] = $CategoryData['sub_category']['sub_category_name'];
				$response['category_id'] = $CategoryData['sub_category']['category_id'];
				$response['category_name'] = $CategoryData['sub_category']['category_name'];
				
			}
			else
			{
				$response['success'] = false;	
			}
			return Response::json($response);
		}
	}
	
     public function getMaincategorydetail() {
         $response = array();
         $CategoryData = Manage::AllMainCategory();
            if($CategoryData){
            foreach($CategoryData as $row){
                $ResultData['category_id'] = $row['category_id'];
		$ResultData['category_name'] = $row['category_name'];
                $FinalData[] = $ResultData;
            }
               $response['data'] = $FinalData;
               $response['success'] = true;
            }else{
                  $response['success'] = false;
            }
           return Response::json($response);
	}
	
        public function postAddsubcategory() {
                
	$PostData = Input::all();
                $response = array();
		if(isset($PostData) && !empty($PostData)){
			$PostData['created_on'] = gmdate("Y-m-d H:i:s");
			$PostData['status'] = 1;
			$add_category_status = Manage::AddSubCategory($PostData);
			if($add_category_status['status']){
				$response['success'] = true;
				$response['message'] = SUCCESS_CATEGORY_ADD;
			}
			else
			{
				$response['success'] = false;
				$response['message'] = ERROR_CATEGORY_ADD;
			}
			return Response::json($response);
		}
	}
        
        public function postEditsubcategory(){        
        $PostData = Input::all();
		$response = array();
		if(isset($PostData) && !empty($PostData)){
			$CategoryData['sub_category_name'] = $PostData['sub_category_name'];
			$CategoryData['sub_category_id'] = $PostData['sub_category_id'];
			$CategoryData['category_id'] = $PostData['selectcategory_id'];
                      //  $CategoryData['category_id'] = $PostData['category_id'];
			$CategoryData['updated_on'] = gmdate("Y-m-d H:i:s");
			$CategoryID = $PostData['sub_category_id'];
			$CategoryData = Manage::UpdateUsersubcateColumn($CategoryData,$CategoryID);
			if($CategoryData){
				$response['success'] = true;
				$response['message'] = SUCCESS_CATEGORY_EDIT;
			}
			else
			{
				$response['success'] = false;
				$response['message'] = ERROR_CATEGORY_EDIT;
			}
			return Response::json($response);
		}
    }
        
    /*##########################################################################
     * Function : getAdvancepayment()
     * Purpose  : To get advance payment data
     * @author : Jitendra Parmar
     * #########################################################################
    */
	public function getAdvancepayment(){
		$response = array();
		$response['success'] = false;
		$PaymentData = Manage::AdvancePaymentData();
		if($PaymentData){
			foreach($PaymentData as $row){
				$response['success'] = true;
				$ResultData['setting_id'] = $row['setting_id'];
				$ResultData['setting_name'] = $row['setting_name'];
				$ResultData['setting_value'] = $row['setting_value'];
				$ResultData['setting_status'] = $row['setting_status'];
				$FinalData[] = $ResultData;
			}
			$response['data'] = $FinalData;
		}
		return Response::json($response);
	}
	
	/*##########################################################################
     * Function : postUpdatepayment()
     * Purpose  : To update service fee advance payment / Job budget advance paymnet
     * @author : Jitendra Parmar
     * #########################################################################
    */
	public function postUpdatepayment(){
		$PostData = Input::all();
		$response = array();
		if(isset($PostData) && !empty($PostData)){
			if(isset($PostData['servicefee_advance_payment']) && !empty($PostData['servicefee_advance_payment'])){
				$UpdateData['setting_value'] = $PostData['servicefee_advance_payment'];
			}
			if(isset($PostData['jobbudget_advance_payment']) && !empty($PostData['jobbudget_advance_payment'])){
				$UpdateData['setting_value'] = $PostData['jobbudget_advance_payment'];
			}
			$SettingID = $PostData['setting_id'];
			$UpdateData['updated_on'] = gmdate("Y-m-d H:i:s");
			$ServiceFeePatmentData = Manage::UpdatePaymnet($UpdateData,$SettingID);
			if($ServiceFeePatmentData){
				$response['success'] = true;
				$response['message'] = SUCCESS_PAYMENT_EDIT;
			}
			else
			{
				$response['success'] = false;
				$response['message'] = ERROR_PAYMENT_EDIT;
			}
			return Response::json($response);
		}	
	}
	
	//###########################################################
    //Function : postCheckcategoryname
    //purpose : Check category name exist or not
    //Author: Jitendra Parmar
    //###########################################################
	public function postCheckcategoryname(){
		$PostData = Input::all();
		if(isset($PostData) && !empty($PostData))
		{
			$CategoryName = $PostData['category_name'];
			$CategoryID = isset($PostData['category_id']) ? $PostData['category_id'] : '';
			$Result = DB::table('job_categories')
					->select('*')
					->where('category_name',$CategoryName)
                                        ->where('status',1);
			if(!empty($CategoryID)){
				$Result = $Result->where('category_id','!=',$CategoryID);
			}
			$Result = $Result->get();
			if($Result){
				$Staus = "false";
			}
			else{
				$Staus = "true";
			}
			return $Staus;
		}
	}
        
        public function postChecksubcategoryname(){
            
		$PostData = Input::all();
		if(isset($PostData) && !empty($PostData))
		{
			$CategoryName = $PostData['sub_category_name'];
			//$CategoryID = isset($PostData['sub_category_id']) ? $PostData['sub_category_id'] : '';
			$Result = DB::table('job_sub_categories')
					->select('*')
					->where('sub_category_name',$CategoryName)
                                        ->where('status',1);
//			if(!empty($CategoryID)){
//				$Result = $Result->where('sub_category_id','!=',$CategoryID);
//			}
			$Result = $Result->get();
			if($Result){
				$Staus = "false";
			}
			else{
				$Staus = "true";
			}
			return $Staus;
		}
	}
        
        
	
	/*##########################################################################
     * Function : postUpdatecommision()
     * Purpose  : To update unideal commision
     * @author : Jitendra Parmar
     * #########################################################################
    */
	public function postUpdatecommision(){
		$PostData = Input::all();
		$response = array();
		if(isset($PostData) && !empty($PostData)){
			$UpdateData['setting_value'] = $PostData['commission_charge'];
			$UpdateData['updated_on'] = gmdate("Y-m-d H:i:s");
			$SettingID = $PostData['setting_id'];
			$CommissionData = Manage::UpdateCommision($UpdateData,$SettingID);
			if($CommissionData){
				$response['success'] = true;
				$response['message'] = SUCCESS_COMMISSION_EDIT;
			}
			else
			{
				$response['success'] = false;
				$response['message'] = ERROR_COMMISSION_EDIT;
			}
			return Response::json($response);
		}	
	}
	
	/*##########################################################################
     * Function : getTearmsandcondition()
     * Purpose  : To get tearmsand condition data
     * @author : Jitendra Parmar
     * #########################################################################
    */
	public function getTearmsandcondition(){
		$response = array();
		$response['success'] = false;
		$TearmsandConditionData = Manage::TearmsandConditionData();
		if($TearmsandConditionData){
			$response['success'] = true;
			$ResultData['terms_and_conditions'] = $TearmsandConditionData['terms_and_conditions'];
			$response['data'] = $ResultData;
		}
		return Response::json($response);
	}
	/*##########################################################################
     * Function : getContactus()
     * Purpose  : To get Contact us data
     * @author : hardika satasiya
     * #########################################################################
    */
	public function getContactus(){
		$response = array();
		$response['success'] = false;
		$ContactUs = Manage::Contactus();
               
		if($ContactUs){
			$response['success'] = true;
			$ResultData['contact_us'] = $ContactUs['terms_and_conditions'];
			$response['data'] = $ResultData;
		}
		return Response::json($response);
	}
	
	/*##########################################################################
     * Function : getJobposting()
     * Purpose  : To get terms and condition for jobposting data
     * @author : hardika satasiya
     * #########################################################################
    */
	public function getJobposting(){
		$response = array();
		$response['success'] = false;
		$question = Manage::Question();
               
		if($question){
			$response['success'] = true;
			$ResultData['question'] = $question['terms_and_conditions'];
			$response['data'] = $ResultData;
		}
		return Response::json($response);
	}
	
        public function getJobapplying(){
		$response = array();
		$response['success'] = false;
		$question = Manage::Answer();
               
		if($question){
			$response['success'] = true;
			$ResultData['answer'] = $question['terms_and_conditions'];
			$response['data'] = $ResultData;
		}
		return Response::json($response);
	}
	
	/*##########################################################################
     * Function : postUpdatecommision()
     * Purpose  : To update terms and condition
     * @author : Jitendra Parmar
     * #########################################################################
    */
	public function postUpdatetermscondition(){
		$PostData = Input::all();
		$response = array();
		if(isset($PostData) && !empty($PostData)){
			
			$UpdateData['terms_and_conditions'] = $PostData['terms_and_conditions'];
			$UpdateData['updated_on'] = gmdate("Y-m-d H:i:s");
		
			$CommissionData = Manage::UpdateTermsCondition($UpdateData);
			if($CommissionData){
				$response['success'] = true;
				$response['message'] = SUCCESS_TERMSCONDITION_EDIT;
			}
			else
			{
				$response['success'] = false;
				$response['message'] = ERROR_TERMSCONDITION_EDIT;
			}
			return Response::json($response);
		}	
	}
	/*##########################################################################
     * Function : postUpdatejobposting()
     * Purpose  : To update terms and condition
     * @author : Jitendra Parmar
     * #########################################################################
    */
	public function postUpdatejobposting(){
		$PostData = Input::all();
		$response = array();
		if(isset($PostData) && !empty($PostData)){
			
			$UpdateData['terms_and_conditions'] = $PostData['Question'];
			$UpdateData['updated_on'] = gmdate("Y-m-d H:i:s");
		
			$jobpostingData = Manage::Updatejobposting($UpdateData);
			if($jobpostingData){
				$response['success'] = true;
				$response['message'] = SUCCESS_DISCLAIMER_EDIT;
			}
			else
			{
				$response['success'] = false;
				$response['message'] = ERROR_JOBPOSTING_EDIT;
			}
			return Response::json($response);
		}	
	}
	/*##########################################################################
     * Function : postUpdatestaticanswer()
     * Purpose  : To update terms and condition
     * @author : Jitendra Parmar
     * #########################################################################
    */
	public function postUpdatejobapplying(){
		$PostData = Input::all();
		$response = array();
		if(isset($PostData) && !empty($PostData)){
			
			$UpdateData['terms_and_conditions'] = $PostData['answer'];
			$UpdateData['updated_on'] = gmdate("Y-m-d H:i:s");
		
			$jobappyingData = Manage::Updatejobapplying($UpdateData);
			if($jobappyingData){
				$response['success'] = true;
				$response['message'] = SUCCESS_JOBAPPLYING_EDIT;
			}
			else
			{
				$response['success'] = false;
				$response['message'] = ERROR_JOBAPPLYING_EDIT;
			}
			return Response::json($response);
		}	
	}
    /*##########################################################################
     * Function : postUpdatecommision()
     * Purpose  : To update terms and condition
     * @author : Jitendra Parmar
     * #########################################################################
    */
	public function postUpdatecontactus(){
		$PostData = Input::all();
               
		$response = array();
		if(isset($PostData) && !empty($PostData)){
			
			$UpdateData['terms_and_conditions'] = $PostData['contact_us'];
			$UpdateData['updated_on'] = gmdate("Y-m-d H:i:s");
                     
			$ContactusData = Manage::UpdateContactus($UpdateData);
			if($ContactusData){
				$response['success'] = true;
				$response['message'] = SUCCESS_CONTACTUS_TEXT_EDIT;
			}
			else
			{
				$response['success'] = false;
				$response['message'] = ERROR_CONTACTUS_TEXT_EDIT;
			}
			return Response::json($response);
		}	
	}
	
	/*##########################################################################
     * Function : getCategoryadditionalfilds()
     * Purpose  : To get category additional filds data
     * @author : Jitendra Parmar
     * #########################################################################
    */
	public function getCategoryadditionalfields(){
		$response = array();
		$response['success'] = false;
		$Data = Manage::CategoryAdditionalFieldsData();
		if($Data){
			foreach($Data as $row){
				$response['success'] = true;
				$ResultData['category_additional_field_id'] = $row['category_additional_field_id'];
				$ResultData['field_name'] = $row['field_name'];
				$ResultData['field_type'] = $row['field_type'];
				$ResultData['status'] = $row['status'];
				$FinalData[] = $ResultData;
			}
			$response['data'] = $FinalData;
		}
		return Response::json($response);
	}
	
	/*##########################################################################
     * Function : postAddCategoryadditionalfields()
     * Purpose  : To add category additional fields
     * @author : Jitendra Parmar
     * #########################################################################
    */
	public function postAddcategoryadditionalfields(){
		$PostData = Input::all();
		$response = array();
		if(isset($PostData) && !empty($PostData)){
			$PostData['created_on'] = gmdate('Y-m-d H:i:s');
			$Data = Manage::AddCategoryAdditionalFields($PostData);
			if($Data['status']){
				$response['success'] = true;
				$response['message'] = SUCCESS_ADDITIONAL_CATEGORY_ADD;
				$response['data'] = $Data['data'];
			}
			else
			{
				$response['success'] = false;
				$response['message'] = ERROR_ADDITIONAL_CATEGORY_ADD;
			}
			return Response::json($response);
		}	
	}
	
	/*##########################################################################
     * Function : postAddpromocode()
     * Purpose  : To Add promocode
     * @author : Jitendra Parmar
     * #########################################################################
    */
	public function postAddpromocode(){
		$PostData = Input::all();
		$response = array();
		if(isset($PostData) && !empty($PostData)){
			$PostData['start_date'] = gmdate('Y-m-d H:i:s',strtotime($PostData['start_date']));
			$PostData['end_date'] = gmdate('Y-m-d H:i:s',strtotime($PostData['end_date']));
			$PostData['created_on'] = gmdate('Y-m-d H:i:s');
			$PostData['total_usage'] = 0;
			$CommissionData = Manage::AddPromoCode($PostData);
			if($CommissionData){
				$response['success'] = true;
				$response['message'] = SUCCESS_PROMOCODE_ADD;
			}
			else
			{
				$response['success'] = false;
				$response['message'] = ERROR_PROMOCODE_ADD;
			}
			return Response::json($response);
		}	
	}
	
	//###########################################################
    //Function : postIspromocodeexist
    //purpose : Check promocode exist or not. Allow aonly if enddate is less then current date
    //Author: Jitendra Parmar
    //###########################################################
	public function postIspromocodeexist(){
		$PostData = Input::all();
		if(isset($PostData) && !empty($PostData))
		{
			$Code = $PostData['code'];
			$CurrentDate = gmdate('Y-m-d H:i:s');
			$Result = DB::table('promo_code')
					->select('*')
					->where('code',$Code)
					->Where('end_date','>',$CurrentDate)
					->get();
			if($Result){
				$Staus = "false";
			}
			else{
				$Staus = "true";
			}
			return $Staus;
		}
	}
	
	//###########################################################
    //Function : getPromocodelist
    //purpose : Fetch all promocode list for datatable
    //Author: Jitendra Parmar
    //###########################################################
    public function getPromocodelist() {
        $data = DB::table('promo_code')
                ->select('promo_code_id','code',DB::Raw('CONCAT(reward," HK$") as reward'), DB::raw('DATE_FORMAT(start_date,"%d/%b/%y") as start_date'),DB::raw('DATE_FORMAT(end_date,"%d/%b/%y") as end_date'),DB::Raw('IF( promo_status = "1","Single","Multiple") as promo_status'),'usage_limit','total_usage',DB::Raw('CONCAT("1.25"," HK$") as rewards_utilized'))->orderBy('created_on','ASC');
                    
        return Datatables::of($data)->edit_column('total_usage', '<a href="javascript:;" class="total_usage" data-id="">{{$total_usage}}</a>')
		 ->make(true);
    }
	
    /*##########################################################################
     * Function : postUpdatepayment()
     * Purpose  : To update service fee advance payment / Job budget advance paymnet
     * @author : Jitendra Parmar
     * #########################################################################
    */
	public function postUpdatereferralamount(){
		$PostData = Input::all();
	
		$response = array();
		if(isset($PostData) && !empty($PostData)){
			$SettingID = $PostData['setting_id'];
			$UpdateData['setting_value'] = $PostData['setting_value'];
			if(isset($PostData['setting_status'])){
				$UpdateData['setting_status'] = 0;
			}else{
				$UpdateData['setting_status'] = 1;
			}
			$UpdateData['updated_on'] = gmdate("Y-m-d H:i:s");
			$UpdateStatus = Manage::UpdateRefferalAmount($UpdateData,$SettingID);
			if($UpdateStatus){
				$response['success'] = true;
				$response['message'] = SUCCESS_REFERRAL_AMOUNT_EDIT;
			}
			else
			{
				$response['success'] = false;
				$response['message'] = ERROR_REFERRAL_AMOUNT_EDIT;
			}
			return Response::json($response);
		}	
	}
	
	//###########################################################
    //Function : getReferralcodehistory
    //purpose : Fetch all referral code history
    //Author: Jitendra Parmar
    //###########################################################
    public function getReferralcodehistory() {
		
		$Data = DB::table('referral_code_history as rch')
                ->select('rch.id',DB::raw('(SELECT name from front_users where id=rch.from_user_id) as refered_by'),
				 DB::raw('(SELECT name from front_users where id=rch.to_user_id) as used_by'),
				 'rch.referral_amount',DB::raw('(SELECT count(referral_code) from referral_code_history where referral_code = rch.referral_code) as code_referred'));
		return Datatables::of($Data)->make(true);
    }
}
