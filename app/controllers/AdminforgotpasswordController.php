<?php

class AdminforgotpasswordController extends BaseController {

    protected $layout = 'layout.managemaster';

    /* Function called when login form to be shown */

    public function getIndex($forgot_token, $email) {
        $data['forgot_token'] = $forgot_token;
        $data['emailid'] = $email;
      
        $result_forgot = Adminlogin::checkuseravailable($forgot_token, $email);
       
        if ($result_forgot == true) { //just got verified
            // print_r($result_forgot);
       //  die;
            return View::make('adminsetnewpassword')->with('email', $email);
        } else {
            return View::make('adminurlexpired')->with('url_for', 'password reset');
        }
    }

}
