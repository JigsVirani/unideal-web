<?php

class HomeController extends BaseController {
    

    protected $layout = 'layout.frontmaster';

    //###########################################################
    //Function : getIndex
    //purpose : To load front end site
    //input : void
    //outpur : load front end view
    //###########################################################
    public function getIndex() {
        //$data['testlist'] = Message::getCountallunread();
        return View::make('frontsite.home');
    }
    
    public function getTermscondition(){
    
          $data = array();
          $getTermsAndCondition = DB::table('terms_and_conditions')->select('terms_and_conditions')->where('id', 1)->first();
        
          $data['data'] = $getTermsAndCondition->terms_and_conditions;
        
          return View::make('frontsite.terms', $data);
      
        
    }

    //###########################################################
    //Function : postPreregister
    //purpose : To save pre register data
    //input : email
    //output : error/scuccess message
    //###########################################################
    public function postPreregister() {
        //initilise data
        $ReturnData = array();
        $PostData = Input::all();

        //proceed admin insertion
        if (isset($PostData) && !empty($PostData)) {

            //define validator
            $Validator = Validator::make(array(
                        'email_address' => $PostData['email_address']
                            ), array(
                        'email_address' => 'required'
            ));

            if ($Validator->fails()) { //if validator is failed
                $ReturnData['status'] = '0';
                $ReturnData['message'] = INVALID_PARAMS;
            } else {
                //proceed for staff add
                $AddUser = Home::AddPreRegister($PostData);
                if (isset($AddUser) && $AddUser > 0) {
                    
                    //add status to post data for mailchimp
                    $PostData['status'] = "subscribed";                    
                    //add user to mail chimp
                    $this->syncMailchimp($PostData);

                    $ReturnData['status'] = '1';
                    $ReturnData['message'] = SUCCESS_REGISTER;
                } else {
                    $ReturnData['status'] = '0';
                    $ReturnData['message'] = GENERAL_ERROR;
                }
            }
        } else {
            $ReturnData['status'] = '0';
            $ReturnData['message'] = INVALID_PARAMS;
        }

        //return data
        return $ReturnData;
    }

    //###########################################################
    //Function : postContact
    //purpose : To send contact us email
    //input : email,name,message
    //output : error/scuccess message
    //###########################################################
    public function postContact() {

        //initilise data
        $ReturnData = array();
        $PostData = Input::all();

        //proceed admin insertion
        if (isset($PostData) && !empty($PostData)) {

            //define validator
            $Validator = Validator::make(array(
                        'contact_name' => $PostData['contact_name'],
                        'contact_email' => $PostData['contact_email'],
                        'contact_message' => $PostData['contact_message']
                            ), array(
                        'contact_name' => 'required',
                        'contact_email' => 'required',
                        'contact_message' => 'required'
            ));

            if ($Validator->fails()) { //if validator is failed
                $ReturnData['status'] = '0';
                $ReturnData['message'] = INVALID_PARAMS;
            } else {
                //proceed for staff add
                $AddContact = Home::AddContact($PostData);
                if (isset($AddContact) && $AddContact > 0) {
                    //define array for multiple "to"
                    $AdminEmails = [ADMIN_EMAIL, ADMIN_EMAIL_2, ADMIN_EMAIL_3];
                    //send email to user
                    Mail::send('emails.contactus', array('name' => $PostData['contact_name'], 'email' => $PostData['contact_email'], 'contact_message' => $PostData['contact_message']), function($message)
                            use ($PostData, $AdminEmails) {
                        $message->to($AdminEmails, $PostData['contact_name'])->subject('UNIDeal App : Inquiry')->bcc('bhargav@creolestudios.com');
                    });

                    $ReturnData['status'] = '1';
                    $ReturnData['message'] = SUCCESS_CONTACT;
                } else {
                    $ReturnData['status'] = '0';
                    $ReturnData['message'] = GENERAL_ERROR;
                }
            }
        } else {
            $ReturnData['status'] = '0';
            $ReturnData['message'] = INVALID_PARAMS;
        }

        //return data
        return $ReturnData;
    }

    //###########################################################
    //Function : postCheckemail
    //purpose : To check email is already exist or not
    //input : email
    //output : error/scuccess message
    //###########################################################
    public function postCheckemail() {

        //initilise data
        $ReturnData = array();
        $PostData = Input::all();

        //proceed admin insertion
        if (isset($PostData) && !empty($PostData)) {

            $EmailExist = Home::CheckEmailExist($PostData);

            if (isset($EmailExist) && !empty($EmailExist)) {
                return "false";
            } else {
                return "true";
            }
        }
    }

    //###########################################################
    //Function : syncMailchimp
    //purpose : To add user to mail chimp
    //input : email
    //output : error/scuccess message
    //###########################################################
    public function syncMailchimp($UserData) {
        $apiKey = 'bd54f0b917aa54521983af09ba8334e4-us12';
        $listId = '5dd7cfdd6d';

        $memberId = md5(strtolower($UserData['email_address']));
        $dataCenter = substr($apiKey, strpos($apiKey, '-') + 1);
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;

        $json = json_encode([
            'email_address' => $UserData['email_address'],
            'status' => $UserData['status'], // "subscribed","unsubscribed","cleaned","pending"            
        ]);

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return $httpCode;
    }

}
