<?php

/**
 * This file demonstrates add/delete card in stripe, add/update/delete bank details.
 * @author Bhargav Bhanderi <bhargav@creolestudios.com>
 * @version 1.0
 * @package : Add/Update cards,Add/Update/Delete Bank Details
 * @copyright  Creole Studios 2015
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 */
class ApipaymentController extends BaseController {

    protected $layout = 'layout.managemaster';

    //###########################################################
    //Function : getIndex
    //purpose : for index action
    //input : void
    //output : error/scuccess message
    //###########################################################
    public function getIndex() {
      App::setLocale('en');
        echo trans('messages.welcome');
        exit;
    }

   //###########################################################
    //Function : postAddbank
    //purpose : To add bank details
    //input : account details
    //output : error/scuccess message
    //###########################################################
    public function postAddbank() {

        //global declaration
        $ResponseData = array();
        //get data from request and process
        $PostData = Input::all();
        if (isset($PostData) && !empty($PostData)) {
             if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $BankValidator = Validator::make(array(
                        'user_id' => Input::get('user_id'),
                        'bank_name' => Input::get('bank_name'),
                        'account_number' => Input::get('account_number'),
                        'swift_code' => Input::get('swift_code'),
                        'language'=>Input::get('language'),
                        'version'=>Input::get('version'),
                        'os_type'=>Input::get('os_type')
                            ), array(
                        'user_id' => 'required',
                        'bank_name' => 'required',
                        'account_number' => 'required|min:6|max:16',
                        'swift_code' => 'required|min:8|max:11',
                        'language' => 'required',
                        'version' => 'required',
                        'os_type' => 'required'
            ));

            if ($BankValidator->fails()) {
                $ResponseData['success'] = STATUS_FALSE;
                $ResponseData['data'] = new stdClass();
                $ResponseData['message'] = $BankValidator->messages()->first();
            } else {
                
                $checkStatus = Apistatuscode::getstatusofuserid($PostData['user_id']);
                if($checkStatus['status_code'] != STATUS_CODE_200){
                    
                        $ResponseData['success'] = STATUS_FALSE;
                        $ResponseData['status_code'] = $checkStatus['status_code'];
                        $ResponseData['message'] = $checkStatus['message'];
                        $ResponseData['data'] = new stdClass();
                }else{
                    ## function to add account
                    $AddAccount = Apipayment::AddBankAccount($PostData);
                    if (isset($AddAccount) && $AddAccount['status'] == '1') {

                        $ResponseData['success'] = '1';
                        $ResponseData['status_code'] = STATUS_CODE_200;
                        $ResponseData['message'] = trans('messages.SUCCESS_BANK_ADDED');
                        $ResponseData['data'] = $AddAccount['bank_details'];
                    } else {
                        $ResponseData['success'] = STATUS_FALSE;
                        $ResponseData['status_code'] = STATUS_CODE_201;
                        $ResponseData['message'] = trans('messages.GENERAL_ERROR');
                        $ResponseData['data'] = new stdClass();
                    }
                }
            }
        } else {
            //print error response
            $ResponseData['success'] = STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }
        return Response::json($ResponseData);
    }

    //###########################################################
    //Function : postBanklist
    //purpose : To get bank list
    //input : user id
    //output : error/scuccess message
    //###########################################################
    public function postBanklist() {

        //global declaration
        $ResponseData = array();
        //get data from request and process
        $PostData = Input::all();
        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $BankValidator = Validator::make(array(
                        'user_id' => Input::get('user_id'),
                        'language'=> Input::get('language'),
                        'version'=>Input::get('version'),
                        'os_type'=>Input::get('os_type')
                            ), array(
                        'user_id' => 'required',
                        'language' => 'required',
                        'version' => 'required',
                        'os_type' => 'required'
            ));

            if ($BankValidator->fails()) {
                $ResponseData['success'] = STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['data'] = array();
                $ResponseData['message'] = $BankValidator->messages()->first();
            } else {
                
                $checkStatus = Apistatuscode::getstatusofuserid($PostData['user_id']);
                
                if($checkStatus['status_code'] != STATUS_CODE_200){
                    
                                $ResponseData['success'] = STATUS_FALSE;
                                $ResponseData['status_code'] = $checkStatus['status_code'];
                                $ResponseData['message'] = $checkStatus['message'];
                                $ResponseData['data'] = array();
                
                    }else{
                            ## function to add account
                            $GetAccounts = Apipayment::BankAccounts($PostData['user_id']);
                            if (isset($GetAccounts) && $GetAccounts['status'] == '1') {

                                $ResponseData['success'] = '1';
                                $ResponseData['message'] = trans('messages.GENERAL_SUCCESS');
                                $ResponseData['status_code'] = STATUS_CODE_200;
                                $ResponseData['data'] = $GetAccounts['bank_details'];
                            } else {
                                $ResponseData['success'] = STATUS_FALSE;
                                $ResponseData['status_code'] = STATUS_CODE_201;
                                $ResponseData['message'] = trans('messages.BANK_ACCOUNT_LIST_EMPTY');
                                $ResponseData['data'] = array();
                            }
                
                }
            }
        } else {
            //print error response
            $ResponseData['success'] = STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = array();
        }
        return Response::json($ResponseData);
    }

    //###########################################################
    //Function : postDeletebank
    //purpose : To delete bank account
    //input : bank account id
    //output : error/scuccess message
    //###########################################################
    public function postDeletebank() {
        //global declaration
        $ResponseData = array();
        //get data from request and process
        $PostData = Input::all();
        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $BankValidator = Validator::make(array(
                        'bank_details_id' => Input::get('bank_details_id'),
                        'user_id' => Input::get('user_id'),
                        'language' => Input::get('language'),
                        'os_type' => Input::get('os_type'),
                        'version' => Input::get('version'),
                        ), array(
                        'bank_details_id' => 'required',
                        'user_id' => 'required',
                        'language' => 'required',
                        'os_type' => 'required',
                        'version' => 'required'
            ));

            if ($BankValidator->fails()) {
                $ResponseData['success'] = STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['data'] = new stdClass();
                $ResponseData['message'] = $BankValidator->messages()->first();
            } else {
                
                $checkStatus = Apistatuscode::getstatusofuserid($PostData['user_id']);
                if($checkStatus['status_code'] != STATUS_CODE_200){
                     
                        $ResponseData['success'] = STATUS_FALSE;
                        $ResponseData['status_code'] = $checkStatus['status_code'];
                        $ResponseData['message'] = $checkStatus['message'];
                        $ResponseData['data'] = new stdClass();
                    
                }else {
                    
                    ## function to add account.
                    $DeleteAccounts = Apipayment::DeleteBankAccount($PostData);
                    if (isset($DeleteAccounts) && $DeleteAccounts['status'] == '1') {

                        $ResponseData['success'] = STATUS_TRUE;
                        $ResponseData['status_code'] = STATUS_CODE_200;
                        $ResponseData['message'] = trans('messages.SUCCESS_BANK_DELETED');
                        $ResponseData['data'] = new stdClass();
                    } else {
                        $ResponseData['success'] = STATUS_FALSE;
                        $ResponseData['status_code'] = STATUS_CODE_201;
                        $ResponseData['message'] = trans('messages.GENERAL_ERROR');
                        $ResponseData['data'] = new stdClass();
                    }
                }
            }
        } else {
            //print error response.
            $ResponseData['success'] = STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }
        return Response::json($ResponseData);
    }

    //###########################################################
    //Function : postSetdefaultbank
    //purpose : To set default bank account
    //input : bank account id
    //output : error/scuccess message
    //###########################################################
    public function postSetdefaultbank() {
        //global declaration.
        $ResponseData = array();
        //get data from request and process
        $PostData = Input::all();
        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $BankValidator = Validator::make(array(
                        'bank_details_id' => Input::get('bank_details_id'),
                        'user_id'=> Input::get('user_id'),
                        'language'=> Input::get('language'),
                        'os_type'=>Input::get('os_type'),
                        'version'=>Input::get('version')
                            ), array(
                        'bank_details_id' => 'required',
                        'user_id' => 'required',
                        'language' => 'required',
                        'os_type' => 'required',
                        'version' => 'required'
            ));

            if ($BankValidator->fails()) {
                
                    $ResponseData['success'] = STATUS_FALSE;
                    $ResponseData['status_code'] = STATUS_CODE_201;
                    $ResponseData['message'] = $BankValidator->messages()->first();
                    $ResponseData['data'] = new stdClass();
                
            } else {
                
                $checkStatus = Apistatuscode::getstatusofuserid($PostData['user_id']);
                if($checkStatus['status_code'] != STATUS_CODE_200){
                     
                        $ResponseData['success'] = STATUS_FALSE;
                        $ResponseData['status_code'] = $checkStatus['status_code'];
                        $ResponseData['message'] = $checkStatus['message'];
                        $ResponseData['data'] = new stdClass();
                    
                }else{
                 
                    ## function to add account.
                    $RemoveOldOne = Apiusers::UpdateUserDefaultCard($PostData['user_id']);
                    $Removedefault = Apiusers::UpdateSingleColumnWithTableName('front_user_bank_details', 'is_default', '0', 'bank_details_id', $PostData['bank_details_id']);
                   
                    $SetDefault = Apiusers::UpdateSingleColumnWithTableName('front_user_bank_details', 'is_default', '1', 'bank_details_id', $PostData['bank_details_id']);
                  
                    if (isset($SetDefault) && $SetDefault['status'] == '1') {

                        $ResponseData['success'] = STATUS_TRUE;
                        $ResponseData['status_code'] = STATUS_CODE_200;
                        $ResponseData['message'] =  trans('messages.GENERAL_SUCCESS');
                        $ResponseData['data'] = new stdClass();
                        
                    } else {
                        
                        $ResponseData['success'] = STATUS_FALSE;
                        $ResponseData['status_code'] = STATUS_CODE_201;
                        $ResponseData['message'] = trans('messages.GENERAL_NO_CHANGES') ;
                        $ResponseData['data'] = new stdClass();
                        
                    }
                }
            }
        } else {
            //print error response
            $ResponseData['success'] = STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] =  trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }
        return Response::json($ResponseData);
    }

   
	
	//###########################################################
    //Function : postWithdrawamount
    //purpose : 
	//Author: Himanshu Upadhyay
    //###########################################################
	public function postWithdrawamount()
	{
		$data['success'] = true;
		$data['message'] = "";
		$data['data'] = new stdClass();
		
		$all_data = Input::all();
		$validator = Validator::make(array(
                    'user_id'=> Input::get('user_id'),
                    'email'=> Input::get('email_address')
                     ), array(
				'user_id' => 'required',
				'email' => 'required|email'
				
			));
		if ($validator->fails()) 
		{
			$data['success'] = false;
            $ResponseData['data'] = new stdClass();
			$data['message'] = $validator->messages()->first();
			return Response::json($data);
		}
		
		$checkexists = Apipayment::checkuserexists(Input::get('user_id')); 
		if($checkexists['success']!=FALSE)
		{
			
			$anyexisting  =	Apipayment::checkexisitingwithdrawrequest(Input::get('user_id'));
			if($anyexisting== false)
			{
				$bank_data = Apipayment::fetchbankdata(Input::get('user_id'));
				if($bank_data != false)
				{
					$result = Apipayment::postWithdrawamount($all_data,$bank_data['bank_details_id']);
					if($result['success'] != false)
					{
						$data['message'] = WITHDRAWL_CONFIRM;
						$data['data'] = $checkexists['userdata'];
					}
					else
					{
						$data['message'] = NO_BALANCE;
						$data['success'] = false;

					}
				}
				else
				{
					$data['message'] = BANK_ACC_DEFAULT_NOT_EXIST;
					$data['success'] = false;
				}
			}
			else //already entry exisits
			{
				$data['message'] = REQUEST_ALREADY_EXISTS;
				$data['success'] = false;
			}
		}
		else
		{
			$data['success'] = $checkexists['success'];
			$data['message'] = $checkexists['message'];
		}
		
        return Response::json($data);
	}
	
	//###########################################################
    //Function : getWithdraw
    //purpose : 
	//Author: Himanshu Upadhyay
    //###########################################################
	public function getWithdraw($user_id,$amount,$bank_id,$withdraw_id)
	{
		$bank_data = Apipayment::fetchbankdata(base64_decode($user_id));
		if($bank_data != false)
		{
			/*$data_temp = array(
							'withdraw_user_id'	=> base64_decode($user_id),
							'withdraw_amount'	=> base64_decode($amount),
							'withdraw_bank_id'	=> base64_decode($bank_id)
						);*/

			$has_amount = Apipayment::fetchwithdrawalamount(base64_decode($user_id));
			if($has_amount == 1)
			{
				$withdraw_id = base64_decode($withdraw_id);
				
				$checkiflastwithdraw = Apipayment::checkiflastwithdrawrequest($withdraw_id,base64_decode($user_id));
				if($checkiflastwithdraw == 1)
				{
					$result1 = DB::table('fund_withdraw_request')->where(array('withdraw_id'=>$withdraw_id))->update(array('user_email_ack'=>1));

					# entry for transaction,prepare array
					/*$TransactionArray = array('user_id' => base64_decode($user_id), 'transaction_type' => '3', 'transaction_amount' => base64_decode($amount), 'transaction_reference' => '', 'payment_method' => '1', 'color_code' => '3', 'created_on' => date('Y-m-d H:i:s'), 'transaction_description' => 'Funds withdrawal request processed!');
					Apitransaction::AddTransaction($TransactionArray);*/

					$return_value = Apipayment::checkuserwithdraw(base64_decode($user_id));

					return View::make('fundwithdrawalsuccess')->With(array('total_earnings' => base64_decode($amount), 'bank_name' => $bank_data['bank_name'], 'account_number' => $bank_data['account_number'] ));
				}
				else
				{
					return View::make('withdrawexpired');
				}
			}
			else
			{
				return View::make('alreadywithdrawan')->With('total_earnings',base64_decode($amount));;
			}
		}
		else
		{
			print_r('The bank account to which you want to withdraw the amount, does not exists!');die;
		}
    }

}
