<?php

class AdminloginController extends BaseController {

    protected $layout = 'layout.managemaster';

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Function : getIndex
     * purpose : Function called when login form to be shown
     * input : void
     * outpur : rediect to login form.
     * * * * * * * * * * * * * * * * * * * * * * * * *  */

    public function getIndex() {
        if (Auth::manage_user()->check()) {
            return Redirect::to(MANAGE_TEXT . 'dashboard/');
        } else {
            return View::make('manage.LoginForm');
        }
    }

    //###########################################################
    //Function : postLogin
    //purpose : For admin panel login
    //input : email,password
    //output : error/scuccess message
    //###########################################################

    public function postLogin() {

        //initilise data
        $ReturnData = array();
        $PostData = Input::all();
        $IsLoggedIn = 0;

        //proceed for login
        if (isset($PostData) && !empty($PostData)) {

            //define validator
            $validator = Validator::make(array(
                        'password' => $PostData['password'],
                        'email' => $PostData['email'],
                            ), array(
                        'password' => 'required',
                        'email' => 'required|email'
            ));

            if (Auth::manage_user()->check()) {
                $IsLoggedIn = 1;
            }
            //check validator
            if ($validator->fails()) {

                $ReturnData['status'] = '0';
                $ReturnData['message'] = INVALID_CREDENTIALS_PARAMS;
            } else if ($IsLoggedIn == '1') {
                $ReturnData['status'] = '1';
            } else {
                $credentials = [
                    "email_address" => Input::get("email"),
                    "password" => Input::get("password")
                ];
                $remember = (Input::has('remember')) ? true : false;

                if (Auth::manage_user()->attempt($credentials, $remember)) {
                    $ReturnData['status'] = '1';
                } else {
                    $ReturnData['status'] = '0';
                    $ReturnData['message'] = LOGIN_ERROR;
                }
            }
        } else {
            $ReturnData['status'] = '0';
            $ReturnData['message'] = INVALID_PARAMS;
        }

        //return data
        return $ReturnData;
    }

    //###########################################################
    //Function : getLogout
    //purpose : To logout admin
    //input : void
    //outpur : success and redirect to login
    //###########################################################
    public function getLogout() {

        Auth::manage_user()->logout();
        Session::flush();
         Cache::flush();
        return Redirect::to(MANAGE_TEXT . 'login');
    }

    //###########################################################
    //Function : postForgotpassword
    //purpose : To send mail for forgot password link and procced
    //input : email
    //outpur : success/error
    //###########################################################
    public function postForgotpassword($data = 'null') {

        //initilise data
        $ReturnData = array();
        $PostData = Input::all();
        
        if (isset($PostData) && !empty($PostData)) {
            //define validator
            $validator = Validator::make(array(
                        'email' => $PostData['email_fp']
                            ), array(
                        'email' => 'required|exists:manage_users,email_address'
            ));
            //check for validator
            if ($validator->fails()) {

                $ReturnData['status'] = '0';
                $ReturnData['message'] = INVALID_PARAMS;
            } else {
                //
                $ResultForgot = Adminlogin::postForgotlink($PostData);

                if ($ResultForgot) {
                    $ReturnData['status'] = '1';
                    $ReturnData['message'] = FORGOT_PASSWORD_SUCCESS;
                } else {
                    $ReturnData['status'] = '0';
                    $ReturnData['message'] = GENERAL_ERROR;
                }
            }
        } else {
            $ReturnData['status'] = '0';
            $ReturnData['message'] = INVALID_PARAMS;
        }

        //return data
        return $ReturnData;
    }

    //###########################################################
    //Function : postResetpassword
    //purpose : To reset password after request of forgot password
    //Author: Hitarthi Panchal 
    //###########################################################
    public function postResetpassword() {
        $validator = Validator::make(array(
                    'email' => Input::get('user'),
                    'password' => Input::get('password'),
                    'password_confirmation' => Input::get('password_confirmation'),
                        ), array(
                    'email' => 'required',
                    'password' => 'required|min:6|confirmed',
                    'password_confirmation' => 'required|min:6',
        ));

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to(MANAGE_TEXT . 'login')->with('admin_login_message', $messages);
        } else {
            $update_data['password'] = Input::get('password');
            $update_data['email'] = base64_decode(Input::get('user'));
            $result_forgot = Adminlogin::setpassword($update_data);            
            if ($result_forgot != false) {
                return View::make('adminpasswordchanged')->with('url_for', 'password reset');
            }
        }
    }

}
