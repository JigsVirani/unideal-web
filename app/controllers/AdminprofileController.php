<?php

class AdminprofileController extends BaseController {

    protected $layout = 'layout.managemaster';

    public function postUpdatepersonalinfo()
	{
		$returndata = array();
		$result = Users::postUpdatepersonalinfo(Input::all());
			if ($result) {
				$returndata['success'] = true;
				$returndata['message'] = SUCCESS_ADMIN_PROFILE_UPDATE;
			}
			else{
				$returndata['success'] = false;
				$returndata['message'] = ERROR_ADMIN_PROFILE_UPDATE;
			}
		return Response::json($returndata);
	}
	
	public function anyAdminprofile()
	{
		$admin_id = Auth::manage_user()->id();
		$UserDetails = Users::admindetails($admin_id);
        if($UserDetails['success'] == 1){
            $returndata['success'] = true;
			$returndata['details'] = $UserDetails['data'];
        }
        return Response::json($returndata);
	}
    
    public function anyAdminprofilepic()
	{
		$admin_id = Auth::manage_user()->id();
        
		$UserDetails = Users::getadminpic($admin_id);
        if($UserDetails){
            $returndata['success'] = true;
			$returndata['details'] = $UserDetails;
        }
        return Response::json($returndata);
	}
    
    
    

    public function postChangepassword() {
		$credentials_login = [
			"id" => (Auth::manage_user()->get()->id),
			"password" => Input::get("old_password"),
		];
		$id = Auth::manage_user()->get()->id;
		if (Auth::manage_user()->validate($credentials_login)) {
			$result_forgot = Users::postSetnewpassword(Input::all(), $id);
			if ($result_forgot) {
				$returndata['success'] = true;
				$returndata['message'] = SUCCESS_ADMIN_CHANGE_PASSWORD;
			} else {
				$returndata['success'] = false;
				$returndata['message'] = ERROR_ADMIN_CHANGE_PASSWORD;
			}
		} else {
			$returndata['success'] = false;
			$returndata['message'] = ERROR_INVALID_CURRENT_PASSWORD;
		}
		return Response::json($returndata);
    }
    
    //###########################################################
    //Function : postDeleteprofile
    //purpose : Change password for admin.
    //Author: Hitarthi Panchal 
    //###########################################################
    public function postDeleteprofile() {

        //initilise data
        $ReturnData = array();
        $PostData = Input::all();

        if (isset($PostData) && !empty($PostData)) {
            //define validator
            $Validator = Validator::make(array(
                        'id' => $PostData['id']
                            ), array(
                        'id' => 'required'
            ));
            if ($Validator->fails()) { //if validator is failed
                $ReturnData['success'] = false;
                $ReturnData['message'] = "Invalid parameters.";
            } else {
                //proceed to delete trend
                $DeleteProfile = Profile::Deleteprofile($PostData);
                if ($DeleteProfile) {
                    $ReturnData['success'] = true;
                    $ReturnData['message'] = "Profile has been deleted.";
                } else {
                    $ReturnData['success'] = false;
                    $ReturnData['message'] = 'Something went wrong.';
                }
            }
        } else {
            $ReturnData['success'] = false;
            $ReturnData['message'] = "Invalid parameters.";
        }
        return Response::json($ReturnData);
    }
}
