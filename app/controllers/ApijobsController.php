<?php

class ApijobsController extends BaseController {

    protected $layout = 'layout.managemaster';

    //###########################################################
    //Function : getIndex
    //purpose : for index action
    //input : void
    //output : error/scuccess message
    //###########################################################
    public function getIndex() {
      App::setLocale('en');
        echo trans('messages.welcome');
        exit;
    }
    
    //###########################################################
    //Function : postAddjobs
    //purpose : for post new jobs 
    //input : void
    //output : add new job 
    //###########################################################
    public function postNewjob() {
        $ResponseData = array();
        $ImageData = array();
        //get data from request and process
        $PostData = Input::all();
        if (isset($PostData) && !empty($PostData)) {
             if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $jobValidator = Validator::make(array(
                        'version'=> Input::get('version'),
                        'language'=> Input::get('language'),
                        'os_type'=> Input::get('os_type'),
                        'user_id' => Input::get('user_id'),
                        'category_id' => Input::get('category_id'),
                        'agent_fees'=> Input::get('agent_fees'),
                        'consignment_size' => Input::get('consignment_size'),
                        'job_end_on' => Input::get('job_end_on'),
                        'job_title' => Input::get('job_title'),
                        'job_details' => Input::get('job_details')
                            ), array(
                         'user_id' => 'required',
                         'version' => 'required',
                         'language' => 'required',
                         'os_type' => 'required',
                        'agent_fees'=>'required',
                        'category_id' => 'required',
                        'consignment_size' => 'required|numeric',
                        'job_end_on' =>'required',
                        'job_title' => 'required',
                        'job_details' => 'required'
                        
            ));
          if ($jobValidator->fails()) {
                $ResponseData['success'] = STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $jobValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
            }
            else {
//                 if (Input::hasFile('files')) {
//                    //get image object and process upload
//                    $PostImages = Input::file('files');
//                 
//                    ## loop through data and stroe in array
//                    if (isset($PostImages) && !empty($PostImages)) {
//                       
//                        foreach ($PostImages as $ImageKey => $ImageVal) {
//                            $ImageThumbTemp = 'image_' . time() . '.' . strtolower($ImageVal->getClientOriginalExtension());
//                            Image::make($ImageVal)->save(NEW_JOBS_IMAGES . $ImageThumbTemp);
//                            ## store data in array
//                            $ImageData[] = $ImageThumbTemp;
//                        }
//                        if (isset($ImageData) && !empty($ImageData)) {
//                            ## decode data to json.
//                            $store_string = '{';
//                            $total_length = count($ImageData);
//                            foreach($ImageData as $keys => $vals){
//                                  if($keys == 0){
//                                      $store_string .= $vals;
//                                  }else{
//                                       $store_string .= ', '.$vals;
//                                  }
//                              }
//                               $store_string .= '}';
//                            $PostData['files'] = $store_string;
//                        }
//                    }
//                } else {
//                    $PostData['files'] = '';
//                }
                
                ## function to add account
                $Addjob = Apijobs::Newjob($PostData);
                
                if (isset($Addjob) && $Addjob['status'] == '1') {

                    $ResponseData['success'] = STATUS_TRUE;
                    $ResponseData['status_code'] = STATUS_CODE_200;
                    $ResponseData['message'] = trans('messages.SUCCESS_JOB_POSTED');
                    $ResponseData['data'] = $Addjob['job_details'];
                } else {
                    
                     $ResponseData['success'] = STATUS_FALSE;
                     $ResponseData['status_code'] = STATUS_CODE_201;
                     $ResponseData['message'] = trans('messages.GENERAL_ERROR');
                     $ResponseData['data'] = new stdClass();
                }
            }
        }   
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }
    
    public function postNewjobimages() {
        
       //global declaration.
       $ResponseData = array();
       $ImageData = array();
       $ResponseData['data'] = new stdClass();
       //get data from request and process.
       $PostData = Input::all();

       if (isset($PostData) && !empty($PostData)) {
           
           ## define validator.
            $PostImagesValidator = Validator::make(array(
                        'version'=> Input::get('version'),
                        'language'=> Input::get('language'),
                        'os_type'=> Input::get('os_type'),
                        'user_id' => Input::get('user_id'),
                        'job_id' => Input::get('job_id'),
                        'job_files' => Input::file('job_files')
                        ), array(
                         'user_id' => 'required',
                         'version' => 'required',
                         'language' => 'required',
                         'job_files' => 'required',
                         'job_id' => 'required',
                         'os_type' => 'required'));
           
           if ($PostImagesValidator->fails()) {
               
                $ResponseData['success'] = STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $PostImagesValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
               
           } else {
               
               ## process file upload.
               if (Input::hasFile('job_files')) {
                   //get image object and process upload.
                   $PostImages = Input::file('job_files');
                   ## loop through data and stroe in array.
                   if (isset($PostImages) && !empty($PostImages)) {
                       foreach ($PostImages as $ImageKey => $ImageVal) {
                           $VideoThumbTemp = 'image_' . time() . '.' . strtolower($ImageVal->getClientOriginalExtension());
                           Image::make($ImageVal)->save(DIR_JOBS . $VideoThumbTemp);
                           
                           ## create a thumb images.
                            $ImageValThumb = Image::make($ImageVal->getRealPath());
                            $ImageValThumb->resize(200, 200, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save(DIR_THUMBS_JOBS.$VideoThumbTemp);
                            ## store data in array.
                           $ImageData[] = $VideoThumbTemp;
                           sleep(1);
                       }
                       if (isset($ImageData) && !empty($ImageData)) {
                           ## decode data to json.
                            $PostData['job_files'] = $ImageData;
                         //  $PostData['post_images_size'] = json_encode($ImageData);
                       }
                   }
               } else {
                   $PostData['job_files'] = '';
               }
              
               $AddPostText = Apijobs::AddPostImages($PostData);
               if (isset($AddPostText) && $AddPostText['status'] == '1') {

                    $ResponseData['success'] = STATUS_TRUE;
                    $ResponseData['status_code'] = STATUS_CODE_200;
                    $ResponseData['message'] = trans('messages.GENERAL_SUCCESS');
                    $ResponseData['data'] = new stdClass();
                   
               } else {
                   
                    $ResponseData['success'] = STATUS_FALSE;
                    $ResponseData['status_code'] = STATUS_CODE_201;
                    $ResponseData['message'] = trans('messages.GENERAL_ERROR');
                    $ResponseData['data'] = new stdClass();
               }
           }
       } else {
           //print error response.
            $ResponseData['success'] = STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.GENERAL_ERROR');
            $ResponseData['data'] = new stdClass();
       }

       return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
   }
    
    //###########################################################
    //Function : getGetjobscategories
    //purpose : for post new jobs 
    //input : void
    //output : add new job 
    //###########################################################
    public function postAlljobscategories(){
    //global declaration
        $ResponseData = array();
        $JobData = array();
       
        $GetAlljobCategory = Apijobs::GetAllJobCategories();
        if (isset($GetAlljobCategory) && $GetAlljobCategory['status'] == '1') {
            $ResponseData['success'] = '1';
            $ResponseData['message'] = GENERAL_SUCCESS;
            $ResponseData['data'] = $GetAlljobCategory['jobcategory_data'];
        } else {
            $ResponseData['success'] = '0';
            $ResponseData['message'] = GENERAL_NO_DATA;
            $ResponseData['data'] = new stdClass();
        }

        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }
    
    //###########################################################
    //Function : postListjobs
    //purpose : for post new jobs 
    //input : void
    //output : add new job 
    //Hardika Satasiya 15 Nov 2016.
    //###########################################################
    public function postListjobs(){
        
    //global declaration.
        $ResponseData = array();
        $PostData=Input::all();
         if (isset($PostData) && !empty($PostData)) {
             if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $jobValidator = Validator::make(array(
                        'user_id' => Input::get('user_id'),
                        'language' => Input::get('language'),
                        'os_type' => Input::get('os_type'),
                        'version' => Input::get('version'),
                        'job_status' => Input::get('job_status'),
                        'user_type' => Input::get('user_type')
                            ), array(
                        'user_id' => 'required',
                        'language' => 'required',
                        'os_type' => 'required',
                        'version' =>'required',
                        'job_status' => 'required',
                        'user_type' => 'required'
             ));
             if ($jobValidator->fails()) {
                 
                $ResponseData['success'] = STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $jobValidator->messages()->first();
                $ResponseData['data'] = array();
            }else{
                $Addjob= Apijobs::GetAlljobs($PostData);
               
                if (isset($Addjob) && $Addjob['status'] == STATUS_TRUE) {
                    $ResponseData['success'] = STATUS_TRUE;
                    $ResponseData['status_code'] = STATUS_CODE_200;
                    $ResponseData['message'] = GENERAL_SUCCESS;
                    $ResponseData['data'] = $Addjob['joblist_data'];
                } else {
                    $ResponseData['success'] = STATUS_FALSE;
                    $ResponseData['status_code'] = STATUS_CODE_201;
                    $ResponseData['message'] = GENERAL_NO_DATA;
                    $ResponseData['data'] = array();
                }
            }
         }
       return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }
    
    
    ## Jigs Virani 14 Nov 2016.
    ## To get job details for job.
    public function postJobdetails(){
   
        //global declaration.
        $ResponseData = array();
        $PostData=Input::all();
         if (isset($PostData) && !empty($PostData)) {
             if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $jobValidator = Validator::make(array(
                        'user_id' => Input::get('user_id'),
                        'language' => Input::get('language'),
                        'os_type' => Input::get('os_type'),
                        'version' => Input::get('version'),
                        'job_id' => Input::get('job_id'),
                        'user_type' => Input::get('user_type'),
                         ), array(
                        'user_id' => 'required',
                        'language' => 'required',
                        'os_type' => 'required|numeric',
                        'version' =>'required',
                        'user_type' =>'required',
                        'job_id' => 'required'));
             if ($jobValidator->fails()) {
                 
                $ResponseData['success'] = STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $jobValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
            }else {
                ## get job details.
                $Addjob= Apijobs::GetJobDetailsjobs($PostData);
              
                if (isset($Addjob) && $Addjob['status'] == STATUS_TRUE) {
                    $ResponseData['success'] = STATUS_TRUE;
                    $ResponseData['status_code'] = STATUS_CODE_200;
                    $ResponseData['message'] = GENERAL_SUCCESS;
                    $ResponseData['data'] = $Addjob['data'];
                } else {
                    $ResponseData['success'] = STATUS_FALSE;
                    $ResponseData['status_code'] = STATUS_CODE_201;
                    $ResponseData['message'] = GENERAL_NO_DATA;
                    $ResponseData['data'] = new stdClass();
                }
            }
         }
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
      }
      
    //###########################################################
    //Function : postListapplicants
    //purpose : for list of applicants as per job 
    //input : void
    //output : listing of all the agents 
    //Hardika Satasiya 16 Nov 2016 
    //###########################################################
    public function postListapplicants(){
    //global declaration.
        $ResponseData = array();
        $PostData=Input::all();
         if (isset($PostData) && !empty($PostData)) {
             if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $applicantValidator = Validator::make(array(
                        'user_id' => Input::get('user_id'),
                        'language' => Input::get('language'),
                        'os_type' => Input::get('os_type'),
                        'version' => Input::get('version'),
                        'job_id' => Input::get('job_id'),
                        'job_status' => Input::get('job_status'),
                         ), array(
                        'user_id' => 'required',
                        'language' => 'required',
                        'os_type' => 'required',
                        'version' =>'required',
                        'job_status' =>'required',
                        'job_id' => 'required'));
             if ($applicantValidator->fails()) {
                $ResponseData['success'] = STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $applicantValidator->messages()->first();
                $ResponseData['data'] = array();
            }else{
                ## get Applicants details.
                $GetAllapplicants = Apijobs::Getapplicantslist($PostData);
                if (isset($GetAllapplicants) && $GetAllapplicants['status'] == '1') {
                    $ResponseData['success'] = STATUS_TRUE;
                    $ResponseData['status_code'] = STATUS_CODE_200;
                    $ResponseData['message'] = GENERAL_SUCCESS;
                    $ResponseData['data'] = $GetAllapplicants['jobapplicants_data'];
                } else {
                    $ResponseData['success'] = STATUS_FALSE;
                    $ResponseData['status_code'] = STATUS_CODE_201;
                    $ResponseData['message'] = GENERAL_NO_DATA;
                    $ResponseData['data'] = array();
                }
            }
        }
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }
    
      //###########################################################
    //Function : postApplicantaction
    //purpose : for applicants request approval
    //input : void
    //output : to change request status cancel or approved 
    //Hardika Satasiya 16 Nov 2016
    //###########################################################
    public function postApplicantaction(){
    //global declaration
        $ResponseData = array();
        $PostData=Input::all();
         if (isset($PostData) && !empty($PostData)) {
             if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $applicantValidator = Validator::make(array(
                        'user_id' => Input::get('user_id'), // requester id.
                        'language' => Input::get('language'),
                        'os_type' => Input::get('os_type'),
                        'version' => Input::get('version'),
                        'job_id' => Input::get('job_id'),
                        'applicant_id' => Input::get('applicant_id'), // agent id.
                        'job_status' => Input::get('job_status'),
                        'applicants_action'=>  Input::get('applicants_action'),
                        ), array(
                        'user_id' => 'required',
                        'language' => 'required',
                        'os_type' => 'required',
                        'version' =>'required',
                        'job_status' =>'required',
                        'applicant_id' =>'required',
                        'applicants_action' =>'required',
                        'job_id' => 'required'));
             if ($applicantValidator->fails()) {
                $ResponseData['success'] = STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $applicantValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
            }else{
                ## get Applicants details.
                $GetAllapplicants = Apijobs::ApplicantAction($PostData);
                 
                if (isset($GetAllapplicants) && $GetAllapplicants['status'] == '1') {
                    $ResponseData['success'] = STATUS_TRUE;
                    $ResponseData['status_code'] = STATUS_CODE_200;
                    $ResponseData['message'] = $GetAllapplicants['message'];
                   // $ResponseData['message'] = GENERAL_SUCCESS;
                    $ResponseData['data'] = new stdClass();;
                } else {
                    $ResponseData['success'] = STATUS_FALSE;
                    $ResponseData['status_code'] = STATUS_CODE_201;
                    $ResponseData['message'] = $GetAllapplicants['message'];
                   // $ResponseData['message'] = GENERAL_NO_CHANGES;
                    $ResponseData['data'] = new stdClass();
                }
            }
        }
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }
      //###########################################################
    //Function : postUpdatejobstatus
    //purpose : for post new jobs 
    //input : void
    //output : update job status
    //Hardika Satasiya 16 Nov 2016
    //###########################################################
    public function postUpdatejobstatus(){
        
     //global declaration.
        $ResponseData = array();
        $PostData=Input::all();
         if (isset($PostData) && !empty($PostData)) {
             if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $SettingsValidator = Validator::make(array(
                        'user_id' => Input::get('user_id'),
                        'language' => Input::get('language'),
                        'os_type' => Input::get('os_type'),
                        'version' => Input::get('version'),
                        'job_id' => Input::get('job_id'),
                        'user_type' => Input::get('user_type'),
                       // 'reviews' => Input::get('reviews'),
                        'request_job_status' => Input::get('request_job_status')
                        ), array(
                        'user_id' => 'required',
                        'language' => 'required',
                        'os_type' => 'required',
                        'version' =>'required',
                        'user_type' =>'required',
                       // 'reviews' =>'required',
                        'request_job_status' =>'required',
                        'job_id' => 'required'));
            
             if ($SettingsValidator->fails()) {
                $ResponseData['success'] = STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $SettingsValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
            }else{
                 
                ## get Applicants details.
                $GetAllapplicants = Apijobs::UpdateJobstatus($PostData);
               
                if (isset($GetAllapplicants) && $GetAllapplicants['status'] == '1') {
                    $ResponseData['success'] = STATUS_TRUE;
                    $ResponseData['status_code'] = STATUS_CODE_200;
                    $ResponseData['message'] = $GetAllapplicants['message'];
                    $ResponseData['data'] =  new stdClass();
                } else {
                    $ResponseData['success'] = STATUS_FALSE;
                    $ResponseData['status_code'] = STATUS_CODE_201;
                    $ResponseData['message'] = $GetAllapplicants['message'];
                    $ResponseData['data'] =  new stdClass();
                }
            }
        }
       return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }
    
    ## Jigs Virani 16 Nov 2016
    ## to get ratings of qutioner and agent.
    
    public function postRatings(){
    
        //global declaration
        $ResponseData = array();
        $PostData = Input::all();
        
         if (isset($PostData) && !empty($PostData)) {
             if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $SettingsValidator = Validator::make(array(
                        'user_id' => Input::get('user_id'),
                        'language' => Input::get('language'),
                        'os_type' => Input::get('os_type'),
                        'version' => Input::get('version'),
                        'user_type' => Input::get('user_type'),
                        ), array(
                        'user_id' => 'required',
                        'language' => 'required',
                        'os_type' => 'required',
                        'version' =>'required',
                        'user_type' =>'required'
                       ));
            
             if ($SettingsValidator->fails()) {
                $ResponseData['success'] = STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $SettingsValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
            }else{
                ## get Applicants details.
                $GetAllRatings = Apijobs::GetRatings($PostData);
                if (isset($GetAllRatings) && $GetAllRatings['status'] == '1') {
                    $ResponseData['success'] = STATUS_TRUE;
                    $ResponseData['status_code'] = STATUS_CODE_200;
                    $ResponseData['message'] = GENERAL_SUCCESS;
                    $ResponseData['data'] =  $GetAllRatings['data'];
                } else {
                    $ResponseData['success'] = STATUS_FALSE;
                    $ResponseData['status_code'] = STATUS_CODE_201;
                    $ResponseData['message'] = GENERAL_NO_DATA;
                    $ResponseData['data'] =  new stdClass();
                }
            }
        }
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
        
    }
    
    ## Jigs Virani 18 Nov 2016.
    ## to get job profile of qutioner and agent.
    
    public function postProfiledetails(){
    
    
        //global declaration.
        $ResponseData = array();
        $PostData = Input::all();
        
         if (isset($PostData) && !empty($PostData)) {
             if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $SettingsValidator = Validator::make(array(
                        'user_id' => Input::get('user_id'),
                        'language' => Input::get('language'),
                        'os_type' => Input::get('os_type'),
                        'version' => Input::get('version'),
                        'user_type' => Input::get('user_type'),
                        ), array(
                        'user_id' => 'required',
                        'language' => 'required',
                        'os_type' => 'required',
                        'version' =>'required',
                        'user_type' =>'required'
                       ));
            
             if ($SettingsValidator->fails()) {
                $ResponseData['success'] = STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $SettingsValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
            }else{
                ## get Applicants details.
                $GetAllRatings = Apijobs::GetProfilesofagentrequester($PostData);
                if (isset($GetAllRatings) && $GetAllRatings['status'] == '1') {
                    $ResponseData['success'] = STATUS_TRUE;
                    $ResponseData['status_code'] = STATUS_CODE_200;
                    $ResponseData['message'] = GENERAL_SUCCESS;
                    $ResponseData['data'] =  $GetAllRatings['data'];
                } else {
                    $ResponseData['success'] = STATUS_FALSE;
                    $ResponseData['status_code'] = STATUS_CODE_201;
                    $ResponseData['message'] = GENERAL_NO_DATA;
                    $ResponseData['data'] =  new stdClass();
                }
            }
        }
       return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }
    
    public function postSendproposal(){
    
    
        //global declaration
        $ResponseData = array();
        $PostData = Input::all();
        
         if (isset($PostData) && !empty($PostData)) {
             if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $SendValidator = Validator::make(array(
                        'user_id' => Input::get('user_id'),
                        'job_id' => Input::get('job_id'),
                        'language' => Input::get('language'),
                        'os_type' => Input::get('os_type'),
                        'version' => Input::get('version'),
                        'proposal_details' => Input::get('proposal_details'),
                        'user_type' => Input::get('user_type'),
                        ), array(
                        'user_id' => 'required',
                        'job_id' => 'required',
                        'language' => 'required',
                        'os_type' => 'required',
                        'version' =>'required',
                        'proposal_details' =>'required',
                        'user_type' =>'required'));
            
             if ($SendValidator->fails()) {
                $ResponseData['success'] = STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $SendValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
            }else{
                 
                 $checkJobId = Apijobs::checkJobStatus($PostData['job_id']);
                
                 if($checkJobId){
                        ## get Applicants details.
                        $SendProposal = Apijobs::SendProposal($PostData);

                        if (isset($SendProposal) && $SendProposal['status'] == '1') {
                            $ResponseData['success'] = STATUS_TRUE;
                            $ResponseData['status_code'] = STATUS_CODE_200;
                            $ResponseData['message'] = $SendProposal['message'];
                            $ResponseData['data'] =  $SendProposal['data'];
                        } else {
                            $ResponseData['success'] = STATUS_FALSE;
                            $ResponseData['status_code'] = STATUS_CODE_201;
                            $ResponseData['message'] = $SendProposal['message'];;
                            $ResponseData['data'] =  new stdClass();
                        }
                 }else{
                            $ResponseData['success'] = STATUS_FALSE;
                            $ResponseData['status_code'] = STATUS_CODE_201;
                            $ResponseData['message'] = trans('messages.JOB_DOES_NOT_EXIST');
                            $ResponseData['data'] =  new stdClass();
                 }
                
            }
        }
        
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    
    }
    
    public function postListagentreviews(){
    
        //global declaration.
        $ResponseData = array();
        $PostData = Input::all();
        
         if (isset($PostData) && !empty($PostData)) {
             if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $SettingsValidator = Validator::make(array(
                        'user_id' => Input::get('user_id'),
                        'language' => Input::get('language'),
                        'os_type' => Input::get('os_type'),
                        'version' => Input::get('version'),
                        'user_type' => Input::get('user_type'),
                        ), array(
                        'user_id' => 'required',
                        'language' => 'required',
                        'os_type' => 'required',
                        'version' =>'required',
                        'user_type' =>'required'
                       ));
            
             if ($SettingsValidator->fails()) {
                $ResponseData['success'] = STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $SettingsValidator->messages()->first();
                $ResponseData['data'] =  array();
            } else {
                 
                ## get Applicants details.
                $GetAllRatings = Apijobs::Listagentreviews($PostData);
                if (isset($GetAllRatings) && $GetAllRatings['status'] == '1') {
                    $ResponseData['success'] = STATUS_TRUE;
                    $ResponseData['status_code'] = STATUS_CODE_200;
                    $ResponseData['message'] = GENERAL_SUCCESS;
                    $ResponseData['data'] =  $GetAllRatings['data'];
                } else {
                    $ResponseData['success'] = STATUS_FALSE;
                    $ResponseData['status_code'] = STATUS_CODE_201;
                    $ResponseData['message'] = GENERAL_NO_DATA;
                    $ResponseData['data'] =  array();
                }
            }
        }
        
       return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
        
    }
    
    public function postAgentjobreviews(){
   
        //global declaration.
        $ResponseData = array();
        $PostData = Input::all();
        
         if (isset($PostData) && !empty($PostData)) {
             if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $SettingsValidator = Validator::make(array(
                        'user_id' => Input::get('user_id'),
                        'job_id' => Input::get('job_id'),
                        'language' => Input::get('language'),
                        'os_type' => Input::get('os_type'),
                        'version' => Input::get('version'),
                        'reviews' => Input::get('reviews'),
                        'user_type' => Input::get('user_type'),
                        ), array(
                        'user_id' => 'required',
                        'job_id' => 'required',
                        'language' => 'required',
                        'os_type' => 'required',
                        'version' =>'required',
                        'reviews' =>'required',
                        'user_type' =>'required'
                       ));
            
             if ($SettingsValidator->fails()) {
                $ResponseData['success'] = STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $SettingsValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
            }else {
                
                ## get Applicants details.
                $GetAllRatings = Apijobs::Agentjobreviews($PostData);
                if (isset($GetAllRatings) && $GetAllRatings['status'] == '1') {
                    $ResponseData['success'] = STATUS_TRUE;
                    $ResponseData['status_code'] = STATUS_CODE_200;
                    $ResponseData['message'] = $GetAllRatings['message'];
                    $ResponseData['data'] = new stdClass();
                } else {
                    $ResponseData['success'] = STATUS_FALSE;
                    $ResponseData['status_code'] = STATUS_CODE_201;
                    $ResponseData['message'] = $GetAllRatings['message'];
                    $ResponseData['data'] =  new stdClass();
                }
            }
        }
        
       return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
        
    }
    
}