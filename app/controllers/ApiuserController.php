<?php

/**
 * This file demonstrates User registration,login,forgot password,delete & reactive account.
 * @author Bhargav Bhanderi <bhargav@creolestudios.com>
 * @version 1.0
 * @package : Login,Register,Forgot Password,Re active & Delete account
 * @copyright  Creole Studios 2015
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 */
class ApiuserController extends BaseController {

    protected $layout = 'layout.managemaster';

    //###########################################################
    //Function : getIndex
    //purpose : for index action
    //input : void
    //output : error/scuccess message
    //###########################################################
    public function getIndex() {
        App::setLocale('en');
        echo trans('messages.welcome');
        exit;
    }

    //###########################################################
    //Function : postRegister
    //purpose : To register user
    //input : all user data
    //output : error/scuccess message
    //###########################################################
    public function postRegister() {//global declaration
       
        $ResponseData = array();
        //get data from request and process
        $PostData = Input::all();
        
        if (isset($PostData) && !empty($PostData)) {
            //set language
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            //define validator
            $RegisterValidator = Validator::make(array(
                        'name' => Input::get('name'),
                        'email_address' => Input::get('email_address'),
                        'password' => Input::get('password'),
                        'phone_number' => Input::get('phone_number'),
                        'os_type' => Input::get('os_type'),
                        'version' => Input::get('version'),
                        'language' => Input::get('language'),
                        'gender' => Input::get('gender'),
                        'user_type' => Input::get('user_type')
                            ), array(
                        'name' => 'required',
                        'email_address' => 'required|email',
                        'password' => 'required|min:6',
                        'phone_number' => 'required',
                        'os_type' => 'required',
                        'version' => 'required',
                        'gender' => 'required',
                        'user_type' => 'required'
            ));
            if ($RegisterValidator->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $RegisterValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
            } else {
                //process image upload
                //check if email already exist                
                $ValidatorEmailExist = Validator::make(array(
                            'email_address' => Input::get('email_address'),
                                ), array(
                            'email_address' => 'unique:front_users',
                ));
                if ($ValidatorEmailExist->fails()) { // if already in database, check for is deleted or not
                    $ValidatorDeactiveExist = Validator::make(array(
                                'email_address' => Input::get('email_address'),
                                    ), array(
                                'email_address' => 'unique:front_users,email_address,NULL,id,user_delete,1',
                    ));

                    //if validator failes then reactive the deleted user.
                    if ($ValidatorDeactiveExist->fails()) {  //user is deleted then reactivate him.
                        //process for image upload.
                        if (Input::hasFile('profile_pic')) {

                            //get image object and process upload.
                            $Image = Input::file('profile_pic');

                            //upload file to s3 bucket.
                            $f_name = time() . str_random(5);
                            $ImageNameTemp = $f_name . '.' . strtolower($Image->getClientOriginalExtension());
                             $ImageNameThumbTemp = $f_name . '.jpg';
                            Image::make($Image)->save(DIR_PROFILE_PIC . $ImageNameTemp);

                            Image::make($Image)->resize(60, 60)->save(DIR_PROFILE_PIC . "thumb_" . $ImageNameThumbTemp);

                            $PostData['profile_pic'] = $ImageNameTemp;
                        } else {
                            $PostData['profile_pic'] = '';
                        }
                        $ResultReActive = Apiusers::ReactiveUser($PostData);
                        if ($ResultReActive != false) {
                            $ResponseData['success']  = STATUS_TRUE;
                            $ResponseData['status_code'] = STATUS_CODE_200;
                            $ResponseData['message'] = trans('messages.REGISTER_SUCCESS');
                            $ResponseData['data'] = $ResultReActive;
                        } else {
                            $ResponseData['success'] =  STATUS_FALSE;
                            $ResponseData['status_code'] = STATUS_CODE_201;
                            $ResponseData['message'] = trans('messages.GENERAL_ERROR');
                            $ResponseData['data'] = new stdClass();
                        }
                    } else {
                        
                        $checkStatus = Apistatuscode::getstatusofuser(Input::get('email_address'));
                        $ResponseData['success'] =  STATUS_FALSE;
                        if($checkStatus['status_code'] == STATUS_CODE_303){
                             $ResponseData['status_code'] = STATUS_CODE_303;
                             $ResponseData['message'] = trans('messages.LOGIN_FAIL_ACCOUNT_NOT_VERIFIED');
                        }else {
                             $ResponseData['status_code'] = STATUS_CODE_201;
                             $ResponseData['message'] = $ValidatorEmailExist->messages()->first();
                        }
                        $ResponseData['data'] = new stdClass();
                    }
                } else {
                    
                    //proceed for register.
                    //process for image upload.
                    if (Input::hasFile('profile_pic')) {

                        //get image object and process upload
                        $Image = Input::file('profile_pic');

                        //upload file to s3 bucket
                        $f_name = time() . str_random(5);
                        $ImageNameTemp = $f_name . '.' . strtolower($Image->getClientOriginalExtension());
                        Image::make($Image)->save(DIR_PROFILE_PIC . $ImageNameTemp);

                        Image::make($Image)->resize(60, 60)->save(DIR_PROFILE_PIC . "thumb_" . $ImageNameTemp);

                        $PostData['profile_pic'] = $ImageNameTemp;
                    } else {
                        $PostData['profile_pic'] = '';
                    }
                    $ResultRegister = Apiusers::Register($PostData);
                    if (isset($ResultRegister) && $ResultRegister['status'] == 1) {
                        $ResponseData['success']  = STATUS_TRUE;
                         $ResponseData['status_code'] = STATUS_CODE_200;
                        $ResponseData['message'] = trans('messages.REGISTER_SUCCESS');
                        $ResponseData['data'] = new stdClass();
                    } else {
                        $ResponseData['success'] =  STATUS_FALSE;
                        $ResponseData['status_code'] = STATUS_CODE_201;
                        $ResponseData['message'] = trans('messages.GENERAL_ERROR');
                        $ResponseData['data'] = new stdClass();
                    }
                }
            }
        } else {
            
            //print error response.
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }
        //print response.
         return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }

    //###########################################################
    //Function : getVerifyaccount
    //purpose : To verify account
    //input : all user data
    //output : error/scuccess message
    //###########################################################
    public function getVerifyaccount($verification_token, $email) {

        $VerifyAccount = Apiusers::checkuserverification($verification_token, base64_decode($email));

        if (isset($VerifyAccount) && $VerifyAccount['status'] == 1) { //just got verified
            return View::make('accountverified');
        } else if ($VerifyAccount['status'] == 2) {
            return View::make('accountalreadyverified');
        } else if ($VerifyAccount['status'] == 3) {
            return View::make('urlexpired')->with('url_for', 'account activation');
        } else {
            return View::make('newemailalreadyexist');
        }
    }

    //###########################################################
    //Function : postLogin
    //purpose : To login user
    //input : email,password OR facebook ID
    //output : error/scuccess message
    //###########################################################
    public function postLoginfb() {

        //global declaration
        $ResponseData['success'] =  STATUS_FALSE;
        $ResponseData = array();

        //get data from request and process
        $PostData = Input::all();

        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            //make validator for facebook
            $ValidateFacebook = Validator::make(array(
                        'facebook_id' => Input::get('facebook_id'),
                        'name' => Input::get('name'),
                        'os_type' => Input::get('os_type'),
                        'version' => Input::get('version'),
                        'language' =>Input::get('language')
                            ), array(
                        'facebook_id' => 'required',
                        'name' => 'required',
                        'os_type' => 'required',
                        'version' => 'required',
                        'language' => 'required'
            ));
            if ($ValidateFacebook->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $ValidateFacebook->messages()->first();
                $ResponseData['data'] = new stdClass();
            } else {
                //process for image upload

                $CheckFBLogin = Apiusers::CheckFbUserExist($PostData);
                if ($CheckFBLogin['status'] == 1) {

                    if (isset($PostData['device_token']) && $PostData['device_token'] != '') {
                        Apiusers::ProcessToken($PostData['device_token'], $CheckFBLogin['user_data']['user_id'], $PostData['os_type']);
                    }
                    Apiusers::ProcessFirstLoginStatus($CheckFBLogin['user_data']['user_id']);
                    $ResponseData['success']  = STATUS_TRUE;
                    $ResponseData['status_code'] = STATUS_CODE_200;
                    $ResponseData['message'] = $CheckFBLogin['message'];
                    $ResponseData['data'] = $CheckFBLogin['user_data'];
                } else {
                    $ResponseData['success'] =  STATUS_FALSE;
                    $ResponseData['status_code']  = $CheckFBLogin['status_code'];
                    $ResponseData['message'] = $CheckFBLogin['message'];
                    $ResponseData['data'] = new stdClass();
                }
            }
        } else {
            //print error response
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }

        //print response.
         return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }

    //###########################################################
    //Function : postLogingoogle
    //purpose : To login  user via google account
    //input : email,password OR facebook ID
    //output : error/scuccess message
    //###########################################################
    public function postLogingoogle() {

        //global declaration
        $ResponseData['success'] =  STATUS_FALSE;
        $ResponseData = array();

        //get data from request and process
        $PostData = Input::all();

        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            //make validator for facebook
            $ValidateGoogle = Validator::make(array(
                        'google_id' => Input::get('google_id'),
                        'name' => Input::get('name'),
                        'os_type' => Input::get('os_type'),
                        'version' => Input::get('version'),
                        'language' =>Input::get('language')
                            ), array(
                        'google_id' => 'required',
                        'name' => 'required',
                        'os_type' => 'required',
                        'version' => 'required',
                        'language' => 'required'
            ));

            if ($ValidateGoogle->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $ValidateGoogle->messages()->first();
                $ResponseData['data'] = new stdClass();
            } else {
                //process for image upload.
                $CheckGoogleLogin = Apiusers::CheckGoogleUserExist($PostData);
               
                if ($CheckGoogleLogin['status'] == 1) {

                    if (isset($PostData['device_token']) && $PostData['device_token'] != '') {
                        Apiusers::ProcessToken($PostData['device_token'], $CheckGoogleLogin['user_data']['user_id'], $PostData['os_type']);
                    }
                    Apiusers::ProcessFirstLoginStatus($CheckGoogleLogin['user_data']['user_id']);
                    ##update second difference.
                    $ResponseData['success']  = STATUS_TRUE;
                    $ResponseData['status_code'] = STATUS_CODE_200;
                    $ResponseData['message'] = $CheckGoogleLogin['message'];
                    $ResponseData['data'] = $CheckGoogleLogin['user_data'];
                } else {
                    $ResponseData['success'] = $CheckGoogleLogin['status'];
                    $ResponseData['status_code']  = $CheckGoogleLogin['status_code'];
                    $ResponseData['message'] = $CheckGoogleLogin['message'];
                    $ResponseData['data'] = new stdClass();
                }
            }
        } else {
            //print error response
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }
        //print response.
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }

    //###########################################################
    //Function : postLogin
    //purpose : To login user
    //input : email,password OR facebook ID
    //output : error/scuccess message
    //###########################################################
    public function postLogin() {

        //global declaration        
        $ResponseData = array();

        //get data from request and process
        $PostData = Input::all();
        if (isset($PostData) && !empty($PostData)) {
            
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            
            $LoginValidator = Validator::make(array(
                        'email_address' =>  Input::get('email_address'),
                        'password' => Input::get('password'),
                        'os_type' => Input::get('os_type'),
                        'version' => Input::get('version'),
                        'language' =>Input::get('language')
                            ), array(
                        'email_address' => 'required|email',
                        'password' => 'required',
                        'os_type' => 'required',
                        'version' => 'required',
                        'language' => 'required'
            ));
            if ($LoginValidator->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $LoginValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
            } else {
                
                //CHECK IF USER EXISTS
                $LogInUser = Apiusers::CheckUserExist($PostData);
              
                if ($LogInUser['status'] == 1) {
                    $credentials = [
                        "email_address" => Input::get('email_address'),
                        "password" => Input::get('password'),
                    ];
                    if (Auth::front_user()->validate($credentials)) {
                        //update token end.
                        //process token.
                        if (isset($PostData['device_token']) && $PostData['device_token'] != '') {
                            Apiusers::ProcessToken($PostData['device_token'], $LogInUser['user_data']['user_id'], $PostData['os_type']);
                        }
                        Apiusers::ProcessFirstLoginStatus($LogInUser['user_data']['user_id']);
                        $ResponseData['success']  = STATUS_TRUE;
                        $ResponseData['status_code'] = $LogInUser['status_code'];
                        $ResponseData['message'] = trans('messages.LOGIN_SUCCESS');
                        $ResponseData['data'] = $LogInUser['user_data'];
                    } else {
                        $ResponseData['success'] =  STATUS_FALSE;
                        $ResponseData['status_code'] = STATUS_CODE_201;
                        $ResponseData['message'] = trans('messages.LOGIN_FAIL_INCORRECT_PASSWORD');
                        $ResponseData['data'] = new stdClass();
                    }
                } else {
                    $ResponseData['success'] = $LogInUser['status'];
                    $ResponseData['status_code'] = $LogInUser['status_code'];
                    $ResponseData['message'] = $LogInUser['message'];
                    $ResponseData['data'] = new stdClass();
                }
            }
        } else {
            //print error response.
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }

        //print response.
         return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }

    //###########################################################
    //Function : postForgotpassword
    //purpose : For forgot password request
    //input : email
    //output : error/scuccess message
    //###########################################################
    public function postForgotpassword() {

        //global declaration        
        $ResponseData = array();

        //get data from request and process
        $PostData = Input::all();
        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            $ForgotPasswordValidation = Validator::make(array(
                        'email_address' => Input::get('email_address'),
                            ), array(
                        'email_address' => 'required|email',
            ));

            if ($ForgotPasswordValidation->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $validator->messages()->first();
                $ResponseData['data'] = new stdClass();
            } else {
                $userStatus = Apistatuscode::getstatusofuser($PostData['email_address']);
              
                if($userStatus['status_code'] == STATUS_CODE_303){
                    
                        $ResponseData['success'] =  STATUS_FALSE;
                        $ResponseData['status_code'] = STATUS_CODE_303;
                        $ResponseData['message'] = trans('messages.LOGIN_FAIL_ACCOUNT_NOT_VERIFIED');
                    
                }else{
                    
                    $ForgotPassword = Apiusers::Forgotpassword($PostData);
                    if (isset($ForgotPassword) && $ForgotPassword['status'] == 1) {
                        $ResponseData['success']  = STATUS_TRUE;
                        $ResponseData['status_code'] = STATUS_CODE_200;
                        $ResponseData['message'] = $ForgotPassword['message'];
                    } else {
                        $ResponseData['success'] =  STATUS_FALSE;
                        $ResponseData['status_code'] = STATUS_CODE_201;
                        $ResponseData['message'] = $ForgotPassword['message'];
                        $ResponseData['data'] = new stdClass();
                    }
                
                }
            }
        } else {
            //print error response
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }
        //print response
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }

    //###########################################################
    //Function : getResetpassword
    //purpose : To request mail for forgot password
    //input : email
    //output : error/scuccess message
    //###########################################################
    public function getResetpassword($reset_token, $email) {  // Validate reset password token
        $ForgotPassword = Apiusers::CheckForgotToken($reset_token, base64_decode($email));
        if ($ForgotPassword['status'] == 1) { //just got verified
            return View::make('change_password')->with('email', $email);
        } else {
            return View::make('urlexpired')->with('url_for', 'password reset');
        }
    }

    //###########################################################
    //Function : postResetpassword
    //purpose : To reset password
    //input : email
    //output : error/scuccess message
    //###########################################################
    public function postResetpassword() {

        //get data from request and process
        $PostData = Input::all();

        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            //define validator
            $ValidateResetPassword = Validator::make(array(
                        'email' => base64_decode(Input::get('email_address')),
                        'newpassword' => Input::get('newpassword'),
                        'confirmedpassword' => Input::get('confirmedpassword'),
                            ), array(
                        'email' => 'required',
                        'newpassword' => 'required|min:6',
                        'confirmedpassword' => 'required|min:6',
            ));

            if ($ValidateResetPassword->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['message'] = $ValidateResetPassword->messages()->first();
                $ResponseData['data'] = new stdClass();
            } else {
                $ResultForgot = Apiusers::ResetPassword($PostData);
                if ($ResultForgot != false) {
                    return View::make('passwordchanged')->with('url_for', 'password reset');
                }
            }
        }

        return Response::json($data);
    }

    //###########################################################
    //Function : postChangepassword
    //purpose : To change password
    //input : user id,new password
    //output : error/scuccess message
    //###########################################################
    public function postChangepassword() {

        //global declaration        
        $ResponseData = array();

        //get data from request and process
        $PostData = Input::all();

        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            //define validator
            $ChangePasswordValidator = Validator::make(array(
                        'user_id' => Input::get('user_id'),
                        'current_password' => Input::get('current_password'),
                        'password' => Input::get('password'),
                            ), array(
                        'user_id' => 'required',
                        'current_password' => 'required',
                        'password' => 'required|min:6'
            ));

            if ($ChangePasswordValidator->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $ChangePasswordValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
            } else {
                $UserCredentials = [
                    "id" => Input::get('user_id'),
                    "password" => Input::get('current_password'),
                ];
                //Check user credentials
                if (Auth::front_user()->validate($UserCredentials)) {
                    $CheckSamePassword = [
                        "id" => Input::get('user_id'),
                        "password" => Input::get('password'),
                    ];
                    //Check if new password is match with already existing password i.e. current password
                    if (Auth::front_user()->validate($CheckSamePassword)) {
                         $ResponseData['success'] =  STATUS_FALSE;
                         $ResponseData['status_code'] = STATUS_CODE_201;
                         $ResponseData['message'] = trans('messages.SAME_PASSWORD');
                         $ResponseData['data'] = new stdClass();
                    } else {
                        
                         $checkStatus = Apistatuscode::getstatusofuserid($PostData['user_id']);
                           
                            if($checkStatus['status_code'] != STATUS_CODE_200){
                            
                                $ResponseData['success'] =  STATUS_FALSE;
                                $ResponseData['status_code'] = $checkStatus['status_code'];
                                $ResponseData['message'] = $checkStatus['message'];
                                $ResponseData['data'] = new stdClass();
                            
                            }else {
                                    
                                    //If new password is not same as current password then update 
                                    $result = Apiusers::Changepassword($PostData);
                                    if ($result != false) {
                                            $ResponseData['success']  = STATUS_TRUE;
                                            $ResponseData['status_code'] = STATUS_CODE_200;
                                            $ResponseData['message'] = trans('messages.PASSWORD_SUCCESS_CHANGE');
                                            $ResponseData['data'] = new stdClass();


                                    } else {
                                        $ResponseData['success'] =  STATUS_FALSE;
                                        $ResponseData['status_code'] = STATUS_CODE_201;
                                        $ResponseData['message'] = trans('messages.PASSWORD_FAIL');
                                        $ResponseData['data'] = new stdClass();
                                    }
                            }
                        
                    }
                } else {
                    $ResponseData['success'] =  STATUS_FALSE;
                    $ResponseData['status_code'] = STATUS_CODE_201;
                    $ResponseData['message'] = trans('messages.INVALID_CURRENT_PASSWORD');
                    $ResponseData['data'] = new stdClass();
                }
            }
        } else {
            //print error response.
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }
         return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }

    //###########################################################
    //Function : postProfile
    //purpose : To change password
    //input : user id,new password
    //output : error/scuccess message
    //###########################################################
    public function postProfile() {

        //global declaration        
        $ResponseData = array();

        //get data from request and process
        $PostData = Input::all();

        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            $ProfileValidator = Validator::make(array(
                        'user_id' => Input::get('user_id')
                            ), array(
                        'user_id' => 'required'
            ));

            if ($ProfileValidator->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $ProfileValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
            } else {
                
                 $checkStatus = Apistatuscode::getstatusofuserid($PostData['user_id']);
                           
                if($checkStatus['status_code'] == STATUS_CODE_200){
                    
                     $UserDetails = Apiusers::GetProfile($PostData['user_id']);
                    if (isset($UserDetails) && $UserDetails['status'] == 1) {
                        $ResponseData['success']  = STATUS_TRUE;
                        $ResponseData['status_code'] = STATUS_CODE_200;
                        $ResponseData['message'] = trans('messages.GENERAL_SUCCESS');
                        $ResponseData['data'] = $UserDetails['user_data'];
                    } else {
                        $ResponseData['success'] =  STATUS_FALSE;
                        $ResponseData['status_code'] = STATUS_CODE_201;
                        $ResponseData['message'] = trans('messages.NO_USER_DATA');
                        $ResponseData['data'] = new stdClass();
                    }
                }else{
                        $ResponseData['success'] =  STATUS_FALSE;
                        $ResponseData['status_code'] = $checkStatus['status_code'];
                        $ResponseData['message'] = $checkStatus['message'];
                        $ResponseData['data'] = new stdClass();
                }
            }
        } else {
            //print error response.
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }
         return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }

    //###########################################################
    //Function : postUpdate
    //purpose : To update profile of user
    //input : all user data
    //output : error/scuccess message
    //###########################################################
    public function postUpdate() {

        //global declaration        
        $ResponseData = array();
        $ProfileMessage = "";
        //get data from request and process
        $PostData = Input::all();
        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            //define validator
            $UpdateProfileValidator = Validator::make(array(
                        'language'=>Input::get('language'),
                        'version'=>Input::get('version'),
                        'os_type'=>Input::get('os_type')
                            ), array(
                        'language'=>'required',
                        'version'=>'required',
                        'os_type'=>'required',
            ));
            if ($UpdateProfileValidator->fails()) {
                 $ResponseData['success'] =  STATUS_FALSE;
                 $ResponseData['status_code'] =  STATUS_CODE_201;
                 $ResponseData['message'] = $UpdateProfileValidator->messages()->first();
                 $ResponseData['data'] = new stdClass();
            } else {
               //if(isset($PostData['from_social_network'])){
                //proceed for image uploade.
                    if (Input::hasFile('profile_pic')) {
                        //get image object and process upload
                        $Image = Input::file('profile_pic');
                        //upload file to s3 bucket
                        $ImageNameTemp = time() . '.' . strtolower($Image->getClientOriginalExtension());
                        Image::make($Image)->save(DIR_PROFILE_PIC . $ImageNameTemp);
                        $PostData['profile_pic'] = $ImageNameTemp;
                        } else {
                        $PostData['profile_pic'] = '';
                    }
                     if (Input::hasFile('document')) {
                        //get image object and process upload
                        $Document = Input::file('document');
                        //upload file to s3 bucket
                        $DocNameTemp = time() . '.' . strtolower($Document->getClientOriginalExtension());
                         Input::file('document')->move(DIR_PROFILE_DOC, $DocNameTemp);
                       // Image::make($Document)->save(DIR_PROFILE_DOC . $DocNameTemp);
                        $PostData['document'] = $DocNameTemp;
                        } else {
                        $PostData['document'] = '';
                    }
                //}
                $UpdateProfile = Apiusers::UpdateProfile($PostData);
                if (isset($UpdateProfile) && $UpdateProfile['status'] == 1) {
                    $ResponseData['success']  = STATUS_TRUE;
                    $ResponseData['status_code'] =  STATUS_CODE_200;
                    $ResponseData['message'] = trans('messages.SUCCESS_PROFILE_UPDATE');
                    $ResponseData['data'] = $UpdateProfile['user_data'];
                } else {
                    $ResponseData['success'] =  STATUS_FALSE;
                     $ResponseData['status_code'] =  STATUS_CODE_201;
                    $ResponseData['message'] = trans('messages.GENERAL_NO_CHANGES');
                    $ResponseData['data'] = new stdClass();
                }
            }
        } else {
            //print error response
            $ResponseData['success'] =  STATUS_FALSE;
             $ResponseData['status_code'] =  STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }
         return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }

    //###########################################################
    //Function : postRetrivesettings
    //purpose : To get user settings
    //input : user id
    //output : setting details
    //###########################################################

//    public function postRetrivesettings() {
//        //global declaration        
//        $ResponseData = array();
//
//        //get data from request and process
//        $PostData = Input::all();
//
//        if (isset($PostData) && !empty($PostData)) {
//            if (isset($PostData['language']) && $PostData['language'] != '') {
//                App::setLocale($PostData['language']);
//            }
//            $SettingValidator = Validator::make(array(
//                        'user_id' => Input::get('user_id')
//                            ), array(
//                        'user_id' => 'required'
//            ));
//
//            if ($SettingValidator->fails()) {
//                $ResponseData['success'] =  STATUS_FALSE;
//                
//                $ResponseData['message'] = $SettingValidator->messages()->first();
//                $ResponseData['data'] = new stdClass();
//            } else {
//                $UserDetails = Apiusers::GetSettings($PostData['user_id']);
//                if (isset($UserDetails) && $UserDetails['status'] == 1) {
//                    $ResponseData['success']  = STATUS_TRUE;
//                    $ResponseData['message'] = trans('messages.GENERAL_SUCCESS');
//                    $ResponseData['data'] = $UserDetails['user_data'];
//                } else {
//                    $ResponseData['success'] =  STATUS_FALSE;
//                    $ResponseData['message'] = trans('messages.NO_USER_DATA');
//                    $ResponseData['data'] = new stdClass();
//                }
//            }
//        } else {
//            //print error response
//            $ResponseData['success'] =  STATUS_FALSE;
//            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
//            $ResponseData['data'] = new stdClass();
//        }
//        return Response::json($ResponseData);
//    }

    //###########################################################
    //Function : postUpdatesettings
    //purpose : To update user settings
    //input : user id, notification and email settings
    //output : error/scuccess message
    //###########################################################
    public function postUpdatesettings() {

        //global declaration        
        $ResponseData = array();

        //get data from request and process
        $PostData = Input::all();

        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
//            $SettingsValidator = Validator::make(array(
//                        'user_id' => Input::get('user_id'),
//                        'langauge'=> Input::get('langauge'),
//                        'jobstatus_notification' => Input::get('jobstatus_notification'),
//                        'transaction_notification' => Input::get('transaction_notification'),
//                        'newjob_notification' => Input::get('newjob_notification')
//                            ), array(
//                        'user_id' => 'required',
//                        'jobstatus_notification' => 'required',
//                        'transaction_notification' => 'required',
//                        'newjob_notification' => 'required'
//            ));
//            if ($SettingsValidator->fails()) {
//                $ResponseData['success'] =  STATUS_FALSE;
//                $ResponseData['status_code'] = STATUS_CODE_201;
//                $ResponseData['message'] = $SettingsValidator->messages()->first();
//                $ResponseData['data'] = new stdClass();
//            } else {
                $UpdateProfileValidator = Validator::make(array(
                        //'language'=>Input::get('language'),
                        'version'=>Input::get('version'),
                        'os_type'=>Input::get('os_type')
                            ), array(
                       // 'language'=>'required',
                        'version'=>'required',
                        'os_type'=>'required',
            ));
            if ($UpdateProfileValidator->fails()) {
                 $ResponseData['success'] =  STATUS_FALSE;
                 $ResponseData['status_code'] =  STATUS_CODE_201;
                 $ResponseData['message'] = $UpdateProfileValidator->messages()->first();
                 $ResponseData['data'] = new stdClass();
            } else {
                
                $checkStatus = Apistatuscode::getstatusofuserid($PostData['user_id']);
                
                if($checkStatus['status_code'] == STATUS_CODE_200){
                    
                    //update user location.
                    $UpdateSettings = Apiusers::UpdateSettings($PostData);

                    if (isset($UpdateSettings) && $UpdateSettings['status'] == 1) {
                        //print error response.
                        $ResponseData['success']  = STATUS_TRUE;
                         $ResponseData['status_code'] = STATUS_CODE_200;
                        $ResponseData['message'] = trans('messages.SUCCESS_SETTINGS_UPDATE');
                        $ResponseData['data'] = new stdClass();
                    } else {
                        //print error response.
                        $ResponseData['success'] =  STATUS_FALSE;
                        $ResponseData['status_code'] = STATUS_CODE_201;
                        $ResponseData['message'] = trans('messages.GENERAL_NO_CHANGES');
                        $ResponseData['data'] = new stdClass();
                    }   
                } else {
                    //print error response.
                    $ResponseData['success'] =  STATUS_FALSE;
                    $ResponseData['status_code'] = $checkStatus['status_code'];
                    $ResponseData['message'] = $checkStatus['message'];
                    $ResponseData['data'] = new stdClass();
                }
            }
        } else {
            //print error response.
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }
         return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }

    //###########################################################
    //Function : getNewemailaccount
    //purpose : To udpate new email address
    //input : user id, email
    //output : error/scuccess message
    //###########################################################
    public function getNewemailaccount($user_id = '', $email_address) {

        //update password
        $ResultUpdate = Apiusers::UpdateUserEmail(base64_decode($user_id), base64_decode($email_address));
        if (isset($ResultUpdate) && $ResultUpdate['status'] == 1) { //just got verified
            return View::make('newemailverified');
        } else {
            return View::make('newemailnotverified');
        }
    }

    //###########################################################
    //Function : postResendmail
    //purpose : To resend activation email
    //input : email
    //output : error/scuccess message
    //###########################################################
    public function postResendmail() {

        //global declaration        
        $ResponseData = array();

        //get data from request and process
        $PostData = Input::all();
        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            $ValidatorEmail = Validator::make(array(
                        'email_address' => Input::get('email_address')
                            ), array(
                        'email_address' => 'required|email'
            ));
            if ($ValidatorEmail->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $ValidatorEmail->messages()->first();
                $ResponseData['data'] = new stdClass();
            } else { 
                $ResendEmail = Apiusers::ResendEmail($PostData['email_address']);
               
                if (isset($ResendEmail) && $ResendEmail['status'] == 1) {
                    $ResponseData['success']  = STATUS_TRUE;
                    $ResponseData['status_code'] = STATUS_CODE_200;
                    $ResponseData['message'] = trans('messages.VERIFICATION_LINK_SENT');
                    $ResponseData['data'] = new stdClass();
                } else {
                    $ResponseData['success'] =  STATUS_FALSE;
                    $ResponseData['status_code'] = STATUS_CODE_201;
                    $ResponseData['message'] = $ResendEmail['message'];
                    $ResponseData['data'] = new stdClass();
                }
            }
        } else {
            //print error response.
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }
         return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }

    //###########################################################
    //Function : postLogout
    //purpose : To logout user
    //input : email
    //output : error/scuccess message
    //###########################################################
    public function postLogout() {

        //global declaration        
        $ResponseData = array();

        //get data from request and process
        $PostData = Input::all();
        //proceed for logout
       
        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            $LogoutValidator = Validator::make(array(
                        'user_id' => Input::get('user_id'),
                        'language'=> Input::get('language'),
                        'version'=> Input::get('version'),
                        'os_type'=> Input::get('os_type')
                        ), array(
                        'user_id' => 'required',
                        'language' => 'required',
                        'version' => 'required',
                        'os_type' => 'required'
            ));

            if ($LogoutValidator->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $LogoutValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
            } else {
                
                $checkStatus = Apistatuscode::getstatusofuserid($PostData['user_id']);
                           
                if($checkStatus['status_code'] == STATUS_CODE_200){
                         
                    $LogoutUser = Apiusers::Logout($PostData);
                    if (isset($LogoutUser) && $LogoutUser['status'] == 1) {
                            //print error response.
                            $ResponseData['success']  = STATUS_TRUE;
                            $ResponseData['status_code'] = STATUS_CODE_200;
                            $ResponseData['message'] = trans('messages.LOGOUT');
                            $ResponseData['data'] = new stdClass();
                        } else {
                            //print error response.
                            $ResponseData['success']  = STATUS_TRUE;
                            $ResponseData['status_code'] = STATUS_CODE_200;
                            $ResponseData['message'] = trans('messages.LOGOUT');
                            $ResponseData['data'] = new stdClass();
                        }
                } else {
                    
                            $ResponseData['success'] =  STATUS_FALSE;
                            $ResponseData['status_code'] = $checkStatus['status_code'];
                            $ResponseData['message'] = $checkStatus['message'];
                            $ResponseData['data'] = new stdClass();
                        
                }
            }
        } else {
            //print error response
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }
    
     //###########################################################
    //Function : postLogout
    //purpose : To logout user
    //input : email
    //output : error/scuccess message
    //###########################################################
    public function postSwitchuser() {
        
        //global declaration.        
        $ResponseData = array();

        //get data from request and process.
        $PostData = Input::all();
        //proceed for logout.
       
        if (isset($PostData) && !empty($PostData)) {
            
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            $switchUserValidator = Validator::make(array(
                        'user_id' => Input::get('user_id'),
                        'new_user_type' => Input::get('new_user_type'),
                        'language'=>Input::get('language'),
                        'version'=>Input::get('version'),
                        'os_type'=>Input::get('os_type')
                            ), array(
                        'user_id' => 'required',
                        'new_user_type' => 'required',
                        'language' => 'required',
                        'version' => 'required',
                        'os_type' => 'required'
            ));
        
            if ($switchUserValidator->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['status_code'] =  STATUS_CODE_201;
                $ResponseData['message'] = $switchUserValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
            } else {
                
                $checkStatus = Apistatuscode::getstatusofuserid($PostData['user_id']);
                if($checkStatus['status_code'] != STATUS_CODE_200){
                     
                        $ResponseData['success'] = STATUS_FALSE;
                        $ResponseData['status_code'] = $checkStatus['status_code'];
                        $ResponseData['message'] = $checkStatus['message'];
                        $ResponseData['data'] = new stdClass();
                    
                } else{
                    
                      $SwitchUser = Apiusers::SwitchUser($PostData);
                      if (isset($SwitchUser) && $SwitchUser['status'] == 1) {
                        //print error response.
                        $ResponseData['success']  = STATUS_TRUE;
                        $ResponseData['status_code'] =  STATUS_CODE_200;
                        $ResponseData['message'] = trans('messages.GENERAL_SUCCESS');
                        $ResponseData['data'] = new stdClass();
                        } else {
                            //print error response.
                            $ResponseData['success'] =  STATUS_FALSE;
                            $ResponseData['status_code'] =  STATUS_CODE_200;
                            $ResponseData['message'] = trans('messages.GENERAL_SUCCESS');
                            $ResponseData['data'] = new stdClass();
                        }
                }
            }
        } else {
            //print error response
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['status_code'] =  STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }
    //###########################################################
    //Function : postDelete
    //purpose : To delete user account
    //input : user id
    //output : error/scuccess message
    //###########################################################
    public function postDelete() {

        //global declaration        
        $ResponseData = array();

        //get data from request and process
        $PostData = Input::all();
        //proceed for logout
        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            $DeleteValidator = Validator::make(array(
                        'user_id' => Input::get('user_id'),
                        'language'=>Input::get('language'),
                        'version'=>Input::get('version'),
                        'os_type'=>Input::get('os_type')
                            ), array(
                        'user_id' => 'required',
                        'language' => 'required',
                        'version' => 'required',
                        'os_type' => 'required'
            ));

            if ($DeleteValidator->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $DeleteValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
            } else {
                
                $checkStatus = Apistatuscode::getstatusofuserid($PostData['user_id']);
                if($checkStatus['status_code'] == STATUS_CODE_200){
                     $DeleteAccount = Apiusers::DeleteAccount($PostData['user_id']);
                     $ResponseData['success']  = STATUS_TRUE;
                    $ResponseData['status_code'] = STATUS_CODE_200;
                     $ResponseData['message'] = trans('messages.DELETE_SUCCESS');
                     $ResponseData['data'] = new stdClass();
                }else{
                
                        $ResponseData['status_code'] = $checkStatus['status_code'];
                        $ResponseData['success'] =  STATUS_FALSE;
                        $ResponseData['message'] = $checkStatus['message'];
                        $ResponseData['data'] = new stdClass();
                    
                }
            }
        } else {
            //print error response
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }

    //###########################################################
    //Function : postCheckmail
    //purpose : To check mail is exist or not
    //input : email
    //output : error/scuccess message
    //###########################################################
    public function postCheckmail() {

        //global declaration        
        $ResponseData = array();

        //get data from request and process
        $PostData = Input::all();
        //proceed for logout
        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            $EmailValidator = Validator::make(array(
                        'email_address' => Input::get('email_address')
                            ), array(
                        'email_address' => 'required|unique:front_users',
            ));

            if ($EmailValidator->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['message'] = $EmailValidator->messages()->first();
                $ResponseData['data'] = new stdClass();
            } else {
                $ResponseData['success']  = STATUS_TRUE;
                $ResponseData['message'] = trans('messages.EMAIL_NOT_AVAILABLE');
                $ResponseData['data'] = new stdClass();
            }
        } else {
            //print error response
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['message'] = trans('messages.INVALID_PARAMS');
            $ResponseData['data'] = new stdClass();
        }
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }

    //###########################################################
    //Function : postFacebookstatus
    //purpose : To check status for facebook
    //input : email
    //output : error/scuccess message
    //###########################################################
    public function postFacebookstatus() {

        //global declaration        
        $ResponseData = array();

        //get data from request and process
        $PostData = Input::all();

        //proceed for logout
        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $validator = Validator::make(array(
                        'facebook_id' => Input::get('facebook_id'),
                        'version'=> Input::get('version'),
                        'language'=> Input::get('language'),
                        'os_type'=> Input::get('os_type'),
                            ), array(
                        'facebook_id' => 'required',
                        'version' => 'required',
                        'language' => 'required',
                        'os_type' => 'required'
            ));

            if ($validator->fails()) {
                 $ResponseData['success'] =  STATUS_FALSE;
                 $ResponseData['status_code'] = STATUS_CODE_201;
                 $ResponseData['message'] = $validator->messages()->first();
                 $ResponseData['data'] = new stdClass();
            } else {
                
                $CheckStatus = Apiusers::CheckFacebookuserExist($PostData['facebook_id']);
                   
                if ($CheckStatus['status'] == 1) {
                    $ResponseData['success'] = $CheckStatus['status'];
                    $ResponseData['status_code'] = STATUS_CODE_200;
                    $ResponseData['message'] = $CheckStatus['message'];
                    $ResponseData['data'] = $CheckStatus['data']['user_data'];
                } else {
                    $ResponseData['data'] = new stdClass();
                    $ResponseData['status_code'] = STATUS_CODE_201;
                    $ResponseData['success'] = $CheckStatus['status'];
                    $ResponseData['message'] = $CheckStatus['message'];
                }
            }
        } else {
            //print error response.
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = INVALID_PARAMS;
            $ResponseData['data'] = new stdClass();
        }
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }
    
    
    //###########################################################
    //Function : postFacebookstatus
    //purpose : To check status for facebook
    //input : email
    //output : error/scuccess message
    //###########################################################
    public function postGooglestatus() {

        //global declaration        
        $ResponseData = array();

        //get data from request and process
        $PostData = Input::all();

        //proceed for logout
        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $validator = Validator::make(array(
                        'google_id' => Input::get('google_id'),
                        'version'=> Input::get('version'),
                        'language'=> Input::get('language'),
                        'os_type'=> Input::get('os_type'),
                            ), array(
                        'google_id' => 'required',
                        'version' => 'required',
                        'language' => 'required',
                        'os_type' => 'required'
            ));

            if ($validator->fails()) {
                 $ResponseData['success'] =  STATUS_FALSE;
                 $ResponseData['status_code'] = STATUS_CODE_201;
                 $ResponseData['message'] = $validator->messages()->first();
                 $ResponseData['data'] = new stdClass();
            } else {
                
                $CheckStatus = Apiusers::CheckGoogleuserIdExist($PostData['google_id']);

                if ($CheckStatus['status'] == 1) {
                    $ResponseData['success'] = $CheckStatus['status'];
                    $ResponseData['status_code'] = STATUS_CODE_200;
                    $ResponseData['message'] = $CheckStatus['message'];
                    $ResponseData['data'] = $CheckStatus['data']['user_data'];
                } else {
                    $ResponseData['data'] = new stdClass();
                    $ResponseData['status_code'] = STATUS_CODE_201;
                    $ResponseData['success'] = $CheckStatus['status'];
                    $ResponseData['message'] = $CheckStatus['message'];
                }
            }
        } else {
            //print error response.
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = INVALID_PARAMS;
            $ResponseData['data'] = new stdClass();
        }
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }
    
    ## jigs virani 09/11
    ## To update token.
    public function postUpdatetoken(){
   
        //global declaration        
        $ResponseData = array();
        
        //get data from request and process
        $PostData = Input::all();

        //proceed for logout
        if (isset($PostData) && !empty($PostData)) {
            if (isset($PostData['language']) && $PostData['language'] != '') {
                App::setLocale($PostData['language']);
            }
            ## define validator
            $validator = Validator::make(array(
                        'user_id' => Input::get('user_id'),
                        'version'=> Input::get('version'),
                        'new_device_token'=> Input::get('new_device_token'),
                        'language'=> Input::get('language'),
                        'os_type'=> Input::get('os_type'),
                            ), array(
                        'user_id' => 'required',
                         'version' => 'required',
                        'language' => 'required',
                        'new_device_token'=> 'required',
                        'os_type' => 'required'));

            if ($validator->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['status_code'] = STATUS_CODE_201;
                $ResponseData['message'] = $validator->messages()->first();
                $ResponseData['data'] = new stdClass();
            } else {
                
                     $checkStatus = Apistatuscode::getstatusofuserid($PostData['user_id']);
                           
                            if($checkStatus['status_code'] != STATUS_CODE_200){
                            
                                $ResponseData['success'] =  STATUS_FALSE;
                                $ResponseData['status_code'] = $checkStatus['status_code'];
                                $ResponseData['message'] = $checkStatus['message'];
                                $ResponseData['data'] = new stdClass();
                            
                            }else {
                                   $CheckStatus = Apiusers::UpdateUserTokens($PostData);
                                   $ResponseData['success'] =  STATUS_TRUE;
                                   $ResponseData['status_code'] = STATUS_CODE_200;
                                   $ResponseData['message'] = trans('messages.GENERAL_SUCCESS');
                                   $ResponseData['data'] = new stdClass();
                            }
            }
        } else {
            //print error response.
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = INVALID_PARAMS;
            $ResponseData['data'] = new stdClass();
        }
       return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }
    
    public function postCheckoldpassword(){
        
         $ResponseData['success'] = STATUS_FALSE;
         $checkoldpassword = Apiusers::checkoldpasswords(Input::all());
           
            if($checkoldpassword == STATUS_TRUE){
                $ResponseData['success'] = STATUS_TRUE;
            }
       // print_r($ResponseData);die;
         return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
  
    
    }
    
    public function postSaveapilog(){
    
        $ResponseData['success'] = STATUS_TRUE;
        $params = Input::all();
        $savelog = Apilog::saveapilog($params);
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    
    }
}
