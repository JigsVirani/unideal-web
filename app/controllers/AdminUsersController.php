<?php

class AdminusersController extends BaseController {

    protected $layout = 'layout.managemaster';
	
	 ## Jigs Virani 18th Oct 2016
     ## To get all the users.
	public function anyAllusersdata()
	{ 
		$users = DB::table('front_users')
			->select('id', 'name','from_social_network',
			DB::raw('CASE WHEN from_social_network = "1" THEN "Facebook" WHEN from_social_network = "2" THEN "Google" ELSE "Email" END as from_social_network'),
			db::raw('IF(from_social_network = "1", "N/A", email_address) as email_address'), DB::raw('DATE_FORMAT(last_login,"%d %b %Y  %H:%i") as last_login'), 
			DB::raw('CASE WHEN user_type = "1" THEN "Requester" WHEN user_type = "2" THEN "Agent" ELSE "Both" END as user_type'),'my_referral_code', 'status', 'phone_number')
                        ->orderBy('id', 'DESC');               
            if(Input::get('roles_ids') !=''){
               $users->where('user_type', (int) Input::get('roles_ids'));
            }
        
    //   echo $users->toSql();die;
		return Datatables::of($users)
		->edit_column('name', '<a href="manage/usersdetail/{{base64_encode($id)}} ">{{$name}}</a>')
        ->make(true);
	}
    
    
   ## Jigs Virani 18th Oct 2016
   ## To export user data as xlsx 
    
    public function getGeneratecsv() {
        
        //global declaration
        $ResponseData = array();
        $UpdateArray = array();
        //get post data
        $PostData = Input::all();
        
            //call function to get user data
            $ResultUserList = Users::UserlistingCSV();
           
            //define array.
            $OutPutHeaderArray = array('UId', 'Name', 'Email', 'Role', 'Phone', 'Referal Code','status','Last Login');
            //call excel library for file generation.
            Excel::create('UserList-' . date('dSMY'), function($excel) use($OutPutHeaderArray, $ResultUserList) {
                $excel->sheet('Sheetname', function($sheet) use($OutPutHeaderArray, $ResultUserList) {

                    //append header
                    $sheet->rows(array($OutPutHeaderArray));
                    //check for data availability
                    if (isset($ResultUserList) && !empty($ResultUserList)) {
                        //loop through data and create csv
                        foreach ($ResultUserList as $UserKey => $UserVal) {
                            $sheet->appendRow(array($UserVal['id'], $UserVal['name'], $UserVal['email_address'], $UserVal['roles'], $UserVal['phone_number'], $UserVal['my_referral_code'],$UserVal['status'], $UserVal['last_login']));
                        }
                    }
                });
            })->export('xlsx');
            exit;
        
    }
    
    ## Jigs Virani 18th Oct 2016
   ## To export user data as pdf 
    
    public function getGeneratepdf() {
        
        //global declaration
        $ResponseData = array();
        $UpdateArray = array();
        //get post data
        $PostData = Input::all();
        
            //call function to get user data
        $ResultUserList = Users::UserlistingCSV();
          
        //for new html.
       $html ='<div class="container" style=" font-size: 14px; background-color:#fff;">
                    <br>
                    <table cellpadding="0" cellspacing="0" width="100%" align="center" id="mainTable" style=" background:rgb(255,255,255);">
                    <tr>
                          <td>
                              <table cellpadding="0" cellspacing="0" width="100%">

                                    <tr>

                                        <td>
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td>
                                                    <br><br>
                                                    <img src="' . ASSETS_IMAGE . '/logo-big.png" style="display:block; line-height:300px; margin: 0 auto;" width="117px" border="0" ></td>
                                                    <td><br><br>
                                                    <span style="text-transform:uppercase;font-size:14px"></span> </td>
                                                    <td>&nbsp; </td>
                                                </tr>
                                            </table>

                                         </td>

                                    </tr>
                                </table>
                             </td>
                        </tr>
                        </table>
                         <br>
                         <br>
                          <br>
                           
                            <h2 style="text-align: center;">UNIDeal Users List</h2>
                       <table cellpadding="5" cellspacing="5" width="100%">
                          <thead class="thead-inverse">
                            <tr>
                              <th>ID#</th>
                              <th>NAME</th>
                              <th>EMAIL</th>
                              <th>ROLE</th>
                              <th>PHONE</th>
                              <th>REFERAL CODE</th>
                              <th>STATUS</th>
                              <th>LAST LOGIN</th>
                            </tr>
                          </thead>
                          <tbody>';
                           foreach ($ResultUserList as $UserKey => $UserVal) {
                                
                                     $html .= '<tr>
                                          <th scope="row">'. $UserVal['id']. '</th>
                                          <td style="text-align: center;">'.  $UserVal['name'] .'</td>
                                          <td style="text-align: center;">'.  $UserVal['email_address'] .'</td>
                                          <td style="text-align: center;">'. $UserVal['roles'] .'</td>
                                          <td style="text-align: center;">'. $UserVal['phone_number'] .'</td>
                                          <td style="text-align: center;">'. $UserVal['my_referral_code'] .'</td>
                                          <td style="text-align: center;">'. $UserVal['status'] .'</td>
                                          <td style="text-align: center;">'. $UserVal['last_login'] .'</td>
                                        </tr>';

                                   
                           }

                            
            $html .=  '</tbody></tbody>
                        </table>
                </div>';
         // echo $html;die;
        $filename = "UNIDeal_users_" . date('dSMY') . ".pdf";
        $pdf = App::make('dompdf');
        $pdf->loadHTML($html);
        $final_path = ADMINPDF_USERS_PATH . $filename;
        
        return $pdf->download($filename);
    
    }
    
    //###########################################################
    //Function : getUserdetails
    //purpose : Fetch user detail
    //Author: Jitendra Parmar
    //###########################################################
    public function getUserdetails(){
        $returndata = array();
        $returndata['success'] = false;
        $Id = Input::get('id');
        $UserDetails = Users::UserDetail($Id);
        
        if($UserDetails['success'] == 1){
            $returndata['success'] = true;
			$returndata['details'] = $UserDetails['data'];
        }
        return Response::json($returndata);
    }
    
    
      public function postUserdeactivate(){
    
         $params = Input::all();
         $result = Users::userdeactivate($params);
         return Response::json($result);
    
    }
	
	//###########################################################
    //Function : getAllquestioner
    //purpose : Fetch questioner job data
    //Author: Jitendra Parmar
    //###########################################################
    public function getAllquestioner($id='') {
		
        $Data = DB::table('jobs as jb')
                ->select('job_id','job_title','category_name','jb.job_status',DB::raw('DATE_FORMAT(job_end_on,"%d %b %Y %h:%i %p") as end_date'))
				->leftJoin('job_categories as jbc', 'jbc.category_id', '=', 'jb.category_id')
				->where('jb.user_id',$id);
        
		 return Datatables::of($Data)
		->edit_column('job_title', '<a href="manage/jobinformation/{{base64_encode($job_id)}} ">{{$job_title}}</a>')
        
		->make(true);
    }
	
	//###########################################################
    //Function : getAllagent
    //purpose : Fetch agent job data
    //Author: Jitendra Parmar
    //###########################################################
    public function getAllagent($id='') {
		
       $Data = DB::table('job_invite as ji')
                ->select('ji.job_id','job_title','category_name','jb.job_status',DB::raw('DATE_FORMAT(job_end_on,"%d %b %Y %h:%i %p") as end_date'))
				->leftJoin('jobs as jb', 'ji.job_id', '=', 'jb.job_id')
				->leftJoin('job_categories as jbc', 'jbc.category_id', '=', 'jb.category_id')
				->where('ji.user_id','=',$id);
        return Datatables::of($Data)
		->edit_column('job_title', '<a href="manage/jobinformation/{{base64_encode($job_id)}} ">{{$job_title}}</a>')
        
		->make(true);
		//return Datatables::of($Data)
       // ->make(true);
    }
}