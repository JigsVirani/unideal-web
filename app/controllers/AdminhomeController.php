<?php

class AdminhomeController extends BaseController {

    protected $layout = 'layout.managemaster';

    //###########################################################
    //Function : getIndex
    //purpose : To load dashboard
    //input : email,password
    //output : error/scuccess message
    //###########################################################
    public function getIndex() {

        $data = array();
       
		$data['profile_pic'] = Users::getadminprofile();

		
       // $this->layout->content = View::make('manage', $data);
       
         return View::make('layout.managemaster', $data);
    }
	
	public function postFetchmonthlydata()
	{
		$ResponseData['lost_members'] = Dashboard::getlostmembers(Input::all());
		$ResponseData['active_members'] = Dashboard::getAllactivemembers(Input::all());
		$ResponseData['inactive_members'] = Dashboard::getAllinactivemembers(Input::all());
		return Response::json($ResponseData);
	}

    public function getLateststates() {

        $ResponseData['total_users'] = Dashboard::getTotalnewusers();
        $ResponseData['new_users'] = Dashboard::getLastmonthsusers();
        $ResponseData['new_jobs'] = Dashboard::getNewjobs();
        $ResponseData['total_revenue'] = Dashboard::getTotalrevenue();

        return Response::json($ResponseData);

    }

}
