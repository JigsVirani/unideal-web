<?php

class ApichatController extends BaseController {

    protected $layout = 'layout.managemaster';

    //###########################################################
    //Function : getIndex
    //purpose : for index action
    //input : void
    //output : error/scuccess message
    //###########################################################
    
    
    public function postUploadattachment(){
    
        
        //global declaration.
       $ResponseData = array();
       $ImageData = array();
       $ResponseData['data'] = array();
        
       //get data from request and process.
       $PostData = Input::all();

       if (isset($PostData) && !empty($PostData)) {
          
                        //process for image upload.
                        if (Input::hasFile('file_path')) {

                            //get image object and process upload.
                            $Image = Input::file('file_path');

                            //upload file to s3 bucket.
                            $f_name = time() . str_random(5);
                            $ImageNameTemp = $f_name . '.' . strtolower($Image->getClientOriginalExtension());
                            
                            Image::make($Image)->save(DIR_MESSAGE_PIC . $ImageNameTemp);
                            $saveUrl = DIR_MESSAGE_PIC_URL.$ImageNameTemp;
                            $data = getimagesize($saveUrl);
                            $width = $data[0];
                            $height = $data[1];

                            //print error response.
                                $ResponseData['success'] = STATUS_TRUE;
                                $ResponseData['status_code'] = STATUS_CODE_200;
                                //$ResponseData['message'] = trans('messages.GENERAL_ERROR');
                                $ResponseData['data']['image_url'] = $saveUrl;
                                $ResponseData['data']['height'] = $height;
                                $ResponseData['data']['width'] = $width;
                                
                            
                            
                        } else {
                            
                                //print error response.
                                $ResponseData['success'] = STATUS_FALSE;
                                $ResponseData['status_code'] = STATUS_CODE_201;
                                $ResponseData['message'] = COULD_NOT_UPLOAD;
                                $ResponseData['data'] = new stdClass();
                           
                            
                    }
       } else
       {
           
            //print error response.
            $ResponseData['success'] = STATUS_FALSE;
            $ResponseData['status_code'] = STATUS_CODE_201;
            $ResponseData['message'] = COULD_NOT_UPLOAD;
            $ResponseData['data'] = new stdClass();
       
       }
        
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    
    }
}