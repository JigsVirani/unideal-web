<?php

class LoginController extends BaseController {

    //###########################################################
    //Function : postRegister
    //purpose : Register new user(consumer/driver) in PortDrop.
    //Author: Hitarthi Panchal | Margi Patel
    //###########################################################
    public function postRegister() {
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();

        $user_type = 1;
        //checking if app_type parameter is present or not!
        $validator_exits = Validator::make(array(
                    'app_type' => Input::get('app_type'),
                        ), array(
                    'app_type' => 'required',
        ));
        if ($validator_exits->fails()) {
            $data['success'] = false;
            $data['message'] = $validator_exits->messages()->first();
            return Response::json($data);
        }

        $app_type = Input::get('app_type'); // app_type = (1: Consumer / 2: Driver)

        $file = Input::file('profile_photo');

        if ($app_type == 1) {
            $validator = Validator::make(array(
                        'full_name' => Input::get('full_name'),
                        'email' => Input::get('email'),
                        'password' => Input::get('password'),
                        'profile_photo' => $file,
                        'app_type' => Input::get('app_type'),
                        'os_type' => Input::get('os_type')
                            ), array(
                        'full_name' => 'required',
                        'email' => 'required|email',
                        'password' => 'required|min:6',
                        'profile_photo' => 'mimes:jpeg,jpg,png,gif|required|max:10000', // max 10000kb
                        'app_type' => 'required',
                        'os_type' => 'required',
            ));
        } else {
            $user_type = 2;
            $validator = Validator::make(array(
                        'full_name' => Input::get('full_name'),
                        'email' => Input::get('email'),
                        'password' => Input::get('password'),
                        'profile_photo' => $file,
                        'address' => Input::get('address'),
                        'phone_number' => Input::get('phone_number'),
                        'vehicle_type' => Input::get('vehicle_type'),
                        'website' => Input::get('website'),
                        'app_type' => Input::get('app_type'),
                        'os_type' => Input::get('os_type')
                            ), array(
                        'full_name' => 'required',
                        'email' => 'required|email',
                        'password' => 'required|min:6',
                        'profile_photo' => 'mimes:jpeg,jpg,png,gif|required|max:10000', // max 10000kb
                        'address' => 'required',
                        'phone_number' => 'required',
                        'vehicle_type' => 'required',
                        'website' => 'required',
                        'app_type' => 'required',
                        'os_type' => 'required'
            ));
        }

        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            //check if already exists
            $validator_exits = Validator::make(array(
                        'email' => Input::get('email'),
                            ), array(
                        'email' => 'unique:users',
            ));

            if ($validator_exits->fails()) { // if already in database
                $validator_deactive_exits = Validator::make(array(
                            'email' => Input::get('email'),
                                ), array(
                            'email' => 'unique:users,email,NULL,id,user_status,3',
                ));

                if ($validator_deactive_exits->fails()) {  //user is deleted then reactivate him
                    /* PREPARING DATA ARRAY TO REACTIVATE THE DELETED USER */
                    $all_data = Input::all();
                    if (isset($all_data['vehicle_type']) && !empty($all_data['vehicle_type'])) {
                        $user_type = 2;
                    } elseif (!isset($all_data['vehicle_type'])) {
                        $user_type = 1;
                    }

                    /* Upload vehicle photo */
                    if (Input::hasFile('vehicle_photo')) {
                        $vehicle_photo = Input::file('vehicle_photo');
                        $name = time() . '_' . $vehicle_photo->getClientOriginalName();
                        $vehicle_photo = $vehicle_photo->move('uploads/vehicle_photos/', $name);
                        $input['file'] = URL::to('/') . '/uploads/vehicle_photos/' . $name;
                        $all_data['vehicle_photo'] = $name;
                        $all_data['vehicle_photo_url'] = $input['file'];
                    } else {
                        $all_data['vehicle_photo'] = '';
                    }
                    if (Input::get('vehicle_licence') == '')
                        $all_data['vehicle_licence'] = '';

                    /* Upload profile photo */
                    if (Input::hasFile('profile_photo')) {
                        $profile_photo = Input::file('profile_photo');
                        $ext = $profile_photo->getClientOriginalExtension();
                        $new_imagename = SELF::postUploadimage(Input::all());
                        $photo_name = $new_imagename['time'] . '.' . $ext;
                        $profile_photo = $profile_photo->move('uploads/profile_photos/', $photo_name);
                        $input['file'] = URL::to('/') . '/uploads/profile_photos/' . $photo_name;
                        $all_data['profile_photo'] = $photo_name;
                        $all_data['profile_photo_url'] = $input['file'];
                    }
                    /* DATA ARRAY PREPARED AND PASSED AS ARGUMENT IN 'reactivateuser' FUNCTION */

                    $result = Login::reactivateuser($all_data, $user_type);
                    if ($result != false) {
                        $data['success'] = true;
                        $data['message'] = REGSUCCESS;
                        $data['data'] = $result;
                    } else {
                        $data['success'] = false;
                        $data['message'] = REGFAIL;
                    }
                } else {
                    $data['success'] = false;
                    $data['message'] = $validator_exits->messages()->first();
                }
            } else { //if doesnt exist
                $all_data = Input::all();

                /* Upload vehicle photo */
                if (Input::hasFile('vehicle_photo')) {
                    $vehicle_photo = Input::file('vehicle_photo');
                    $name = time() . '_' . $vehicle_photo->getClientOriginalName();
                    $vehicle_photo = $vehicle_photo->move('uploads/vehicle_photos/', $name);
                    $input['file'] = URL::to('/') . '/uploads/vehicle_photos/' . $name;
                    $all_data['vehicle_photo'] = $name;
                    $all_data['vehicle_photo_url'] = $input['file'];
                } else {
                    $all_data['vehicle_photo'] = '';
                }
                if (Input::get('vehicle_licence') == '')
                    $all_data['vehicle_licence'] = '';

                /* Upload profile photo */
                if (Input::hasFile('profile_photo')) {
                    $profile_photo = Input::file('profile_photo');
                    $ext = $profile_photo->getClientOriginalExtension();
                    $new_imagename = SELF::postUploadimage(Input::all());
                    $photo_name = $new_imagename['time'] . '.' . $ext;
                    $profile_photo = $profile_photo->move('uploads/profile_photos/', $photo_name);
                    $input['file'] = URL::to('/') . '/uploads/profile_photos/' . $photo_name;
                    $all_data['profile_photo'] = $photo_name;
                    $all_data['profile_photo_url'] = $input['file'];
                }

                $result = Login::postRegister($all_data, $user_type);

                $result['profile_photo'] = $all_data['profile_photo_url'];  //	Adding photo with full url
                if (isset($result['vehicle_photo']) && !empty($result['vehicle_photo'])) {
                    $result['vehicle_photo'] = $all_data['vehicle_photo_url'];  //	Adding photo with full url
                }

                if ($result != false) {
                    $data['success'] = true;
                    $data['message'] = REGSUCCESS;
                    $data['data'] = $result;
                } else {
                    $data['success'] = false;
                    $data['message'] = REGFAIL;
                }
            }
        }
        return Response::json($data);
    }

    //###########################################################
    //Function : postVehiclelist
    //purpose : Vehicle list.
    //Author: Himanshu Upadhyay
    //###########################################################
    public function postVehiclelist() {
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();

        $validator = Validator::make(array(
                    'with_description' => Input::get('with_description'),
                    'app_version' => Input::get('app_version'),
                    'os_type' => Input::get('os_type')
                        ), array(
                    'with_description' => 'required',
                    'app_version' => 'required',
                    'os_type' => 'required'
        ));
        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            $data['data'] = Login::postVehiclelist();
        }
        return Response::json($data);
    }

    //###########################################################
    //Function : postLoginuser
    //purpose : Login registerd consumer/driver in PortDrop.
    //Author: Hitarthi Panchal | Margi Patel
    //###########################################################
    public function postLoginuser() {
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();
        $validator = Validator::make(array(
                    'from_facebook' => Input::get('from_facebook'),
                    'app_type' => Input::get('app_type')
                        ), array(
                    'from_facebook' => 'required',
                    'app_type' => 'required'
        ));
        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            if (Input::get('from_facebook') == 1) { // WHEN COMES FROM FB
                $validator = Validator::make(array(
                            'facebook_id' => Input::get('facebook_id'),
                            'app_type' => Input::get('app_type')
                                ), array(
                            'facebook_id' => 'required',
                            'app_type' => 'required'
                ));
                if ($validator->fails()) {
                    $data['success'] = false;
                    $data['message'] = $validator->messages()->first();
                } else {
                    $checkfbuserexists = Login::checkfbuserexists(Input::all());
                    if ($checkfbuserexists['success'] == true) {
                        $remember_token = str_random(20);
                        $data_temp = array(
                            'user_id' => $checkfbuserexists['userdata']['user_id'],
                            'remember_token' => $remember_token
                        );
                        $result1 = DB::table('user_token')->insert($data_temp);
                        $checkfbuserexists['userdata']['logintoken'] = $remember_token;
                        $checkfbuserexists['userdata']['profile_photo'] = URL::to('/') . '/uploads/profile_photos/' . $checkfbuserexists['userdata']['profile_photo'];

                        $data['data'] = $checkfbuserexists['userdata'];
                        $data['message'] = LOGINSUCCESS;
                    } else {
                        $data['success'] = false;
                        $data['message'] = $checkfbuserexists['message'];
                    }
                }
            } else { // WHEN NORMAL LOGIN
                $validator = Validator::make(array(
                            'email' => Input::get('email'),
                            'password' => Input::get('password'),
                            'app_type' => Input::get('app_type')
                                ), array(
                            'email' => 'required|email',
                            'password' => 'required',
                            'app_type' => 'required'
                ));
                if ($validator->fails()) {
                    $data['success'] = false;
                    $data['message'] = $validator->messages()->first();
                } else {
                    //CHECK IF USER EXISTS
                    $checkexists = Login::checkuserexists(Input::all());
                    if ($checkexists['success'] == true) {
                        $credentials = [
                            "email" => Input::get("email"),
                            "password" => Input::get("password"),
                        ];
                        if (Auth::front_user()->validate($credentials)) {
                            //manage device token STARTS
                            $device_token = '';
                            $device_token = Input::get('device_token');
                            if ($device_token != '') {
                                Login::postUpdatedevicetoken($device_token, $checkexists['userdata']['user_id']);
                            }
                            $checkexists['userdata']['logintoken'] = $device_token;
                            $checkexists['userdata']['profile_photo'] = URL::to('/') . '/uploads/profile_photos/' . $checkexists['userdata']['profile_photo'];
                            //manage token ENDS

                            $data['message'] = LOGINSUCCESS;
                            $data['data'] = $checkexists['userdata'];
                        } else {
                            $data['success'] = false;
                            $data['message'] = LOGINFAIL2;
                        }
                    } else {
                        if (isset($checkexists['data']['user_status']) && $checkexists['data']['user_status'] == 0) {
                            $data['success'] = true;
                            $data['message'] = $checkexists['message'];
                            $data['data'] = $checkexists['data'];
                        } else {
                            $data['success'] = false;
                            $data['message'] = $checkexists['message'];
                            $data['data'] = $checkexists['data'];
                        }
                    }
                }
            }
        }
        return Response::json($data);
    }

    //###########################################################
    //Function : postMyprofile
    //purpose :To get user's own profile details(who is login in device).
    //Author: Himanshu Upadhyay
    //###########################################################
    public function postMyprofile() { //	To fetch user's profile details on My Profile page of app
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();
        $validator = Validator::make(array(
                    'user_id' => Input::get('user_id'),
                        ), array(
                    'user_id' => 'required'
        ));
        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            //CHECK IF USER EXISTS
            $checkexists = Login::fetchuserprofile(Input::all());
            if ($checkexists['success'] == true) {

                $checkexists['userdata']['profile_photo'] = URL::to('/') . '/uploads/profile_photos/' . $checkexists['userdata']['profile_photo'];

                $data['data'] = $checkexists['userdata'];
            } else {
                $data['success'] = true;
                $data['message'] = $checkexists['message'];
                $data['data'] = $checkexists['data'];
            }
        }

        return Response::json($data);
    }

    //###########################################################
    //Function : postResendactivationcode
    //purpose : To receive new activation url through email to activate new account!
    //Author: Himanshu Upadhyay
    //###########################################################
    public function postResendactivationcode() {
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();

        $validator = Validator::make(array(
                    'email' => Input::get('email')
                        ), array(
                    'email' => 'required|email'
        ));
        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            $mail_sent = Login::resendactivation(Input::all());
            if ($mail_sent['success'] == true) {
                $data['message'] = PASSWORDRESETLINK;
            } else {
                $data['success'] = false;
                $data['message'] = $mail_sent['message'];
            }
        }
        return Response::json($data);
    }

    //###########################################################
    //Function : postForgotpassword
    //purpose : To receive email with reset password url when user forgets his password.
    //Author: Himanshu Upadhyay
    //###########################################################
    public function postForgotpassword() {
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();

        $validator = Validator::make(array(
                    'email' => Input::get('email'),
                    'app_type' => Input::get('app_type')
                        ), array(
                    'email' => 'required|email',
                    'app_type' => 'required'
        ));

        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            $return_result = Login::postForgotpassword(Input::all());
            if ($return_result['success'] != false) {
                $data['message'] = PASSWORDRESETLINK;
            } else {
                $data['success'] = false;
                $data['message'] = $return_result['message'];
            }
        }
        return Response::json($data);
    }

    //###########################################################
    //Function : postUpdatemyprofile
    //purpose : To update user's own profile details
    //Author: Himanshu Upadhyay
    //###########################################################
    public function postUpdatemyprofile() {
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();

        $user_type = Input::get('user_type');

        if ($user_type == 1) { // If Consumer
            $validator = Validator::make(array(
                        'full_name' => Input::get('full_name'),
                        'user_id' => Input::get('user_id'),
                        'email' => Input::get('email'),
                        'current_email' => Input::get('current_email'),
                        'phone_number' => Input::get('phone_number'),
                        'address' => Input::get('address'),
                        'website' => Input::get('website')
                            ), array(
                        'full_name' => 'required',
                        'user_id' => 'required',
                        'email' => 'required|email|unique:users,email,' . Input::get('user_id'),
                        'current_email' => 'required|unique:users,email,' . Input::get('user_id'),
                        'phone_number' => 'required',
                        'address' => 'required',
                        'website' => 'required'
            ));
        } elseif ($user_type == 2) { // If Driver
            $validator = Validator::make(array(
                        'full_name' => Input::get('full_name'),
                        'user_id' => Input::get('user_id'),
                        'email' => Input::get('email'),
                        'current_email' => Input::get('current_email'),
                        'phone_number' => Input::get('phone_number'),
                        'address' => Input::get('address'),
                        'vehicle_type' => Input::get('vehicle_type')
                            ), array(
                        'full_name' => 'required',
                        'user_id' => 'required',
                        'email' => 'required|email|unique:users,email,' . Input::get('user_id'),
                        'current_email' => 'required|unique:users,email,' . Input::get('user_id'),
                        'phone_number' => 'required',
                        'address' => 'required',
                        'vehicle_type' => 'required'
            ));
        }

        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            $all_data = Input::all();
            if ($user_type == 2) {
                if (Input::hasFile('vehicle_photo')) /* Upload vehicle photo */ {
                    if (isset($all_data['current_vehicle_photo']) && !empty($all_data['current_vehicle_photo'])) {
                        $pathToFile = ('uploads/vehicle_photos/' . $all_data['current_vehicle_photo']);
                        File::delete($pathToFile);
                    }
                    $vehicle_photo = Input::file('vehicle_photo');
                    $name = time() . '_' . $vehicle_photo->getClientOriginalName();
                    $vehicle_photo = $vehicle_photo->move('uploads/vehicle_photos/', $name);
                    $input['file'] = URL::to('/') . '/uploads/vehicle_photos/' . $name;
                    $all_data['vehicle_photo'] = $name;
                }

                if (Input::hasFile('profile_photo')) /* Upload profile photo */ {
                    if (isset($all_data['current_profile_photo']) && !empty($all_data['current_profile_photo'])) {
                        $pathToFile = ('uploads/profile_photos/' . $all_data['current_profile_photo']);
                        File::delete($pathToFile);
                        $pathToFile_thumbs = ('uploads/profile_photos/thumbs/' . $all_data['current_profile_photo']);
                        File::delete($pathToFile_thumbs);
                    }
                    $profile_photo = Input::file('profile_photo');
                    $ext = $profile_photo->getClientOriginalExtension();
                    $new_imagename = SELF::postUploadimage(Input::all());
                    $photo_name = $new_imagename['time'] . '.' . $ext;
                    $profile_photo = $profile_photo->move('uploads/profile_photos/', $photo_name);
                    $all_data['profile_photo'] = $photo_name;
                }

                if (Input::get('vehicle_licence') == '')
                    $all_data['vehicle_licence'] = '';
            }
            else {
                if (Input::hasFile('profile_photo')) /* Upload profile photo */ {
                    if (isset($all_data['current_profile_photo']) && !empty($all_data['current_profile_photo'])) {
                        $pathToFile = ('uploads/profile_photos/' . $all_data['current_profile_photo']);
                        File::delete($pathToFile);
                        $pathToFile_thumbs = ('uploads/profile_photos/thumbs/' . $all_data['current_profile_photo']);
                        File::delete($pathToFile_thumbs);
                    }
                    $profile_photo = Input::file('profile_photo');
                    $ext = $profile_photo->getClientOriginalExtension();
                    $new_imagename = SELF::postUploadimage(Input::all());
                    $photo_name = $new_imagename['time'] . '.' . $ext;
                    $profile_photo = $profile_photo->move('uploads/profile_photos/', $photo_name);
                    $all_data['profile_photo'] = $photo_name;
                }
            }

            $new_data = Login::postUpdateprofile($all_data);
            if ($new_data['success'] != false) {
                if (Input::get('email') == Input::get('current_email')) { // if same email address
                    $data['message'] = PROFILESUCCESS;
                    if (isset($new_data['data']['userdata']['profile_photo']) && !empty($new_data['data']['userdata']['profile_photo']))
                        $new_data['data']['userdata']['profile_photo'] = URL::to('/') . '/uploads/profile_photos/' . $new_data['data']['userdata']['profile_photo'];
                    if (isset($new_data['data']['userdata']['vehicle_photo']) && !empty($new_data['data']['userdata']['vehicle_photo']))
                        $new_data['data']['userdata']['vehicle_photo'] = URL::to('/') . '/uploads/vehicle_photos/' . $new_data['data']['userdata']['vehicle_photo'];

                    $data['data'] = $new_data['data']['userdata'];
                }
                else {
                    if (isset($new_data['data']['userdata']['profile_photo']) && !empty($new_data['data']['userdata']['profile_photo']))
                        $new_data['data']['userdata']['profile_photo'] = URL::to('/') . '/uploads/profile_photos/' . $new_data['data']['userdata']['profile_photo'];
                    if (isset($new_data['data']['userdata']['vehicle_photo']) && !empty($new_data['data']['userdata']['vehicle_photo']))
                        $new_data['data']['userdata']['vehicle_photo'] = URL::to('/') . '/uploads/vehicle_photos/' . $new_data['data']['userdata']['vehicle_photo'];

                    $data['message'] = PROFILESUCCESS1;
                    $data['data'] = $new_data['data']['userdata'];
                }
            }
            else {
                $data['success'] = false;
                $data['message'] = PROFILEFAIL;
            }
        }
        return Response::json($data);
    }

    //###########################################################
    //Function : postUploadimage
    //purpose : Upload image for profile with resize and quality change.
    //Author: Hitarthi Panchal 
    //###########################################################
    public function postUploadimage() {
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();

        $user_type = Input::get('user_type');

        $validator = Validator::make(array(
                    'app_version' => Input::get('app_version')
                        ), array(
                    'app_version' => 'required',
        ));


        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            $all_data = Input::all();

            if (Input::hasFile('profile_photo')) /* Upload profile photo */ {
                $profile_photo = Input::file('profile_photo');
                $image = getimagesize($profile_photo);
                $width = $image[0]; // $image[0] is the width
                $height = $image[1]; // $image[1] is the height
                if ($width < 150 || $height < 150) {
                    $width = $image[0] / 2;
                    $height = $image[1] / 2;
                    $quality = 100;
                }
                if (($width > 150 || $height > 150) && ($width < 500 || $height < 500)) {
                    $width = $image[0] / 3;
                    $height = $image[1] / 3;
                    $quality = 100;
                }
                if (($width > 500 || $height > 500) && ($width < 1500 || $height < 1500)) {
                    $width = $image[0] / 5;
                    $height = $image[1] / 5;
                    $quality = 100;
                }
                if (($width > 1500 || $height > 1500) && ($width < 2500 || $height < 2500)) {
                    $width = $image[0] / 10;
                    $height = $image[1] / 10;
                    $quality = 100;
                }
                if (($width > 2500 || $height > 2500) && ($width < 3500 || $height < 3500)) {
                    $width = $image[0] / 15;
                    $height = $image[1] / 15;
                    $quality = 100;
                }
                $time = time();

                $ext = $profile_photo->getClientOriginalExtension();
                $photo_name = $time . '.' . $ext;
                $profile_photo = Image::make($profile_photo)->resize($width, $height);
                $profile_photo = $profile_photo->save('uploads/profile_photos/thumbs/' . $photo_name, $quality);
                //$input['file'] = URL::to('/').'/uploads/profile_photos/'.$photo_name;
                $return_array['new_name'] = $photo_name;
                $return_array['time'] = $time;
            }
        }
        return $return_array;
    }

    //###########################################################
    //Function : postUpdatepassword
    //purpose : To update user password.
    //Author: Himanshu Upadhyay
    //###########################################################
    public function postUpdatepassword() {
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();

        $validator = Validator::make(array(
                    'user_id' => Input::get('user_id'),
                    'current_password' => Input::get('current_password'),
                    'password' => Input::get('password'),
                        ), array(
                    'user_id' => 'required',
                    'current_password' => 'required',
                    'password' => 'required|min:6',
        ));

        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            $credentials = [
                "id" => Input::get("user_id"),
                "password" => Input::get("password"),
            ];
            if (Auth::front_user()->validate($credentials)) {
                $data['success'] = false;
                $data['message'] = PASSWORDFAIL1;
            } else {
                //check if current password is correct
                $credentials_login = [
                    "id" => Input::get("user_id"),
                    "password" => Input::get("current_password"),
                ];
                if (Auth::front_user()->validate($credentials_login)) {
                    $result = Login::postUpdatepassword(Input::all());
                    if ($result != false) {
                        $data['message'] = PASSWORDSUCCESS;
                    } else {
                        $data['success'] = false;
                        $data['message'] = PASSWORDFAIL;
                    }
                } else {
                    $data['success'] = false;
                    $data['message'] = PASSWORDFAIL2;
                }
            }
        }
        return Response::json($data);
    }

    //###########################################################
    //Function : postLogout
    //purpose : Logout with PortDrop.
    //Author: Himanshu Upadhyay
    //###########################################################
    public function postLogout() { 
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();
        $validator = Validator::make(array(
                    'user_id' => Input::get('user_id'),
//					'device_token' => Input::get('device_token')
                        ), array(
                    'user_id' => 'required',
//					'device_token' => 'required'
        ));

        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            $remember_token = Input::get('device_token');
            if (isset($remember_token) && !empty($remember_token)) {
                $result1 = DB::table('user_token')->where(array('device_token' => $remember_token))->delete();
            }
            $data['message'] = LOGOUTSUCCESS;
        }
        return Response::json($data);
    }

    //###########################################################
    //Function : postUpdatetoken
    //purpose : When user login in PortDrop then update token.
    //Author: Himanshu Upadhyay
    //###########################################################
    public function postUpdatetoken() {
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();
        $validator = Validator::make(array(
                    'user_id' => Input::get('user_id'),
                    'device_token' => Input::get('device_token'),
                    'os_type' => Input::get('os_type')
                        ), array(
                    'user_id' => 'required',
                    'device_token' => 'required',
                    'os_type' => 'required'
        ));

        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            $device_token = Input::get('device_token');
            $user_id = Input::get('user_id');
            if (!empty($user_id) && !empty($device_token)) {
                Login::postUpdatedevicetoken($device_token, $user_id);
            }
            $data['message'] = DEVICE_TOKEN_UPDATED;
        }
        return Response::json($data);
    }

    //###########################################################
    //Function : getVerifyaccount
    //purpose : Verify account by sending activation link in email.
    //Author: Himanshu Upadhyay
    //###########################################################
    public function getVerifyaccount($verification_token, $email) {

        $verification_result = Login::checkuserverification($verification_token, base64_decode($email));
        if ($verification_result == 1) { //just got verified
            return View::make('accountverified');
        } else if ($verification_result == 2) {
            return View::make('accountalreadyverified');
        } else if ($verification_result == 3) {
            return View::make('urlexpired')->with('url_for', 'account activation');
        }
    }

    //###########################################################
    //Function : getResetpassword
    //purpose : Reset password by sending link via mail.
    //Author: Himanshu Upadhyay
    //###########################################################
    public function getResetpassword($reset_token, $email) {  // Validate reset password token
        $result_forgot = Login::checkresettoken($reset_token, base64_decode($email));
        if ($result_forgot['success'] == true) { //just got verified
            return View::make('setnewpasswords')->with('email', $email);
        } else {
            return View::make('urlexpired')->with('url_for', 'password reset');
        }
    }

    //###########################################################
    //Function : postResetpassword
    //purpose : After get email user reset password by clicking on link.
    //Author: Himanshu Upadhyay
    //###########################################################	
    public function postResetpassword() {
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();

        $validator = Validator::make(array(
                    'email' => Input::get('user'),
                    'password' => Input::get('password'),
                    'password_confirmation' => Input::get('password_confirmation'),
                        ), array(
                    'email' => 'required',
                    'password' => 'required|min:6|confirmed',
                    'password_confirmation' => 'required|min:6',
        ));

        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            $update_data['password'] = Input::get('password');
            $update_data['email'] = base64_decode(Input::get('user'));

            $result_forgot = Login::setpassword($update_data);
            if ($result_forgot != false) {
                return View::make('passwordchanged')->with('url_for', 'password reset');
            }
        }
        return Response::json($data);
    }

    //###########################################################
    //Function : getNewemailaccount
    //purpose : When user wish to change email id then verify that email.
    //Author: Himanshu Upadhyay
    //###########################################################	
    public function getNewemailaccount($verification_token, $email) {

        $result_forgot = Login::updateuseremail(base64_decode($verification_token), base64_decode($email));
        if ($result_forgot == 1) { //just got verified
            return View::make('newemailverified');
        } else {
            return View::make('newemailnotverified');
        }
    }

    //###########################################################
    //Function : postFetchusersetting
    //purpose : Fetch setting of user account.
    //Author: Himanshu Upadhyay
    //###########################################################	
    public function postFetchusersetting() {
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();

        $all_data = Input::all();
        $result = Login::postretriveusersetting($all_data);
        if ($result != false) {
            $data['message'] = ACCOUNT_SETTINGS_DETAILS;
            $data['data'] = $result;
        } else {
            $data['success'] = false;
            $data['message'] = DEFAULT_ERROR_MESSAGE;
        }
        return Response::json($data);
    }

    //###########################################################
    //Function : postUpdateusersetting
    //purpose : Change setting of user's for notify,email and newsletters.
    //Author: Hitarthi Panchal
    //###########################################################	
    public function postUpdateusersetting() {
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();

        $validator = Validator::make(array(
                    'user_id' => Input::get('user_id'),
                    'receive_notification' => Input::get('receive_notification'),
                    'receive_email_notification' => Input::get('receive_email_notification'),
                    'receive_newsletters' => Input::get('receive_newsletters'),
                    'os_type' => Input::get('os_type'),
                    'app_version' => Input::get('app_version'),
                        ), array(
                    'user_id' => 'required',
                    'os_type' => 'required',
                    'app_version' => 'required',
        ));

        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            $result = Login::postNotification(Input::all());
            if (is_array($result)) {
                $data['message'] = USER_SETTINGS_UPDATED;
                $data['data'] = $result;
            } elseif ($result == 1) {
                $data['success'] = false;
                $data['message'] = NOTHING_TO_UPDATE;
            } else {
                $data['success'] = false;
                $data['message'] = DEFAULT_ERROR_MESSAGE;
            }
        }
        return Response::json($data);
    }

    //###########################################################
    //Function : postClientprofile
    //purpose : Any user want to see another user's profile.
    //Author: Himanshu Upadhyay
    //###########################################################
    public function postClientprofile() {
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();

        $validator = Validator::make(array(
                    'client_id' => Input::get('client_id'),
                    'app_type' => Input::get('app_type'),
                    'os_type' => Input::get('os_type'),
                    'app_version' => Input::get('app_version'),
                        ), array(
                    'client_id' => 'required',
                    'app_type' => 'required',
                    'os_type' => 'required',
                    'app_version' => 'required',
        ));

        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            $all_data = Input::all();
            $result = Login::postClientprofile($all_data);
            if ($result != false) {
                $data['message'] = CLIENT_PROFILE_DETAILS;
                $data['data'] = $result;
            } else {
                $data['success'] = false;
                $data['message'] = USERFAIL;
            }
        }
        return Response::json($data);
    }

    //###########################################################
    //Function : postFetchnewjobstatus
    //purpose : Fetch job status.
    //Author: Hitarthi Panchal
    //###########################################################
    public function postFetchnewjobstatus() {
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();

        $validator = Validator::make(array(
                    'job_id' => Input::get('job_id'),
                    'os_type' => Input::get('os_type'),
                    'app_version' => Input::get('app_version'),
                        ), array(
                    'job_id' => 'required',
                    'os_type' => 'required',
                    'app_version' => 'required',
        ));

        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            $all_data = Input::all();
            $result = Login::Jobstatus($all_data);
            if ($result != false) {
                $data['message'] = 'Job status';
                $data['data'] = $result;
            } else {
                $data['success'] = false;
                $data['message'] = NO_JOB;
            }
        }
        return Response::json($data);
    }

    //###########################################################
    //Function : postTest
    //purpose : Notification test(dummy).
    //Author: Himanshu Upadhyay
    //###########################################################
    public function postTest() {
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();

        $validator = Validator::make(array(
                    'noti_type' => Input::get('noti_type')
                        ), array(
                    'noti_type' => 'required'
        ));

        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            $all_data = Input::all();
            $noti_type = Input::get('noti_type');
            $noti_data = array();
            if ($noti_type == 1) {
                $noti_data['type'] = 1;
                $noti_data['recepient_id'] = 36;
                $token_info = Login::Getusertokeninfo($noti_data['recepient_id']); //print_r($token_info);die;
                $noti_data['file_name'] = 'ck_development_consumer';
                $noti_data['device_token'] = $token_info['device_token'];
                $noti_data['message'] = 'palak has applied on Job ID #24.';
                $noti_data['job_id'] = 24;
                Login::postIosnotification($noti_data);
            } elseif ($noti_type == 2) {
                $noti_data['type'] = 2;
                $noti_data['recepient_id'] = 28;
                $token_info = Login::Getusertokeninfo($noti_data['recepient_id']); //print_r($token_info);die;
                $noti_data['file_name'] = 'ck_development_driver';
                $noti_data['device_token'] = $token_info['device_token'];
                $noti_data['message'] = 'Himanshu approved application for Job ID #22.';
                $noti_data['job_id'] = 22;
                Login::postIosnotification($noti_data);
            } elseif ($noti_type == 3) {
                $noti_data['type'] = 3;
                $noti_data['recepient_id'] = 36;
                $token_info = Login::Getusertokeninfo($noti_data['recepient_id']); //print_r($token_info);die;
                $noti_data['file_name'] = 'ck_development_consumer';
                $noti_data['device_token'] = $token_info['device_token'];
                $noti_data['message'] = 'palak has activated Job ID #23.';
                $noti_data['job_id'] = 23;
                Login::postIosnotification($noti_data);
            } elseif ($noti_type == 4) {
                $noti_data['type'] = 4;
                $noti_data['recepient_id'] = 36;
                $token_info = Login::Getusertokeninfo($noti_data['recepient_id']); //print_r($token_info);die;
                $noti_data['file_name'] = 'ck_development_consumer';
//				$noti_data['device_token'] = '7e338ecbe170b79f05a46f92f50a7baca5834d82137f693d93dd247e2fb459d1';
                $noti_data['device_token'] = $token_info['device_token'];
                $noti_data['message'] = 'palak picked up the packets for Job ID #30.';
                $noti_data['job_id'] = 30;
                Login::postIosnotification($noti_data);
            } elseif ($noti_type == 5) {
                $noti_data['type'] = 5;
                $noti_data['recepient_id'] = 36;
                $token_info = Login::Getusertokeninfo($noti_data['recepient_id']); //print_r($token_info);die;
                $noti_data['file_name'] = 'ck_development_consumer';
//				$noti_data['device_token'] = '7e338ecbe170b79f05a46f92f50a7baca5834d82137f693d93dd247e2fb459d1';
                $noti_data['device_token'] = $token_info['device_token'];
                $noti_data['message'] = 'palak has completed Job ID #58.';
                $noti_data['job_id'] = 30;
                Login::postIosnotification($noti_data);
            }
            $data['success'] = true;
        }
        return Response::json($data);
    }

    //###########################################################
    //Function : postTest
    //purpose : Notification test android(dummy).
    //Author: Hitarthi Panchal
    //###########################################################
    public function postTestandroid() {
        $data['success'] = true;
        $data['message'] = "";
        $data['data'] = new stdClass();

        $validator = Validator::make(array(
                    'noti_type' => Input::get('noti_type')
                        ), array(
                    'noti_type' => 'required'
        ));

        if ($validator->fails()) {
            $data['success'] = false;
            $data['message'] = $validator->messages()->first();
        } else {
            $all_data = Input::all();
            $noti_type = Input::get('noti_type');
            $noti_data = array();
            if ($noti_type == 1) {
                $noti_data['type'] = 1;
                $noti_data['recepient_id'] = 24;
                $token_info = Login::Getusertokeninfo($noti_data['recepient_id']); //print_r($token_info);die;
                //$noti_data['file_name'] = 'ck_development_consumer';
                $noti_data['device_token'] = $token_info['device_token'];
                $noti_data['message'] = 'Palak has applied on Job ID #24.';
                $noti_data['job_id'] = 24;
                $noti_data['title'] = 'PortDrop Consumer';
                Login::postAndroidnotification($noti_data);
            } elseif ($noti_type == 2) {
                $noti_data['type'] = 2;
                $noti_data['recepient_id'] = 28;
                $token_info = Login::Getusertokeninfo($noti_data['recepient_id']); //print_r($token_info);die;
                //$noti_data['file_name'] = 'ck_development_driver';
                $noti_data['device_token'] = $token_info['device_token'];
                $noti_data['message'] = 'Himanshu approved application for Job ID #22.';
                $noti_data['job_id'] = 22;
                $noti_data['title'] = 'PortDrop Driver';
                Login::postAndroidnotification($noti_data);
            } elseif ($noti_type == 3) {
                $noti_data['type'] = 3;
                $noti_data['recepient_id'] = 24;
                $token_info = Login::Getusertokeninfo($noti_data['recepient_id']); //print_r($token_info);die;
                //$noti_data['file_name'] = 'ck_development_consumer';
                $noti_data['device_token'] = $token_info['device_token'];
                $noti_data['message'] = 'Palak has activated Job ID #23.';
                $noti_data['job_id'] = 23;
                $noti_data['title'] = 'PortDrop Consumer';
                Login::postAndroidnotification($noti_data);
            } elseif ($noti_type == 4) {
                $noti_data['type'] = 4;
                $noti_data['recepient_id'] = 24;
                $token_info = Login::Getusertokeninfo($noti_data['recepient_id']); //print_r($token_info);die;
                //$noti_data['file_name'] = 'ck_development_consumer';
                $noti_data['device_token'] = $token_info['device_token'];
                $noti_data['message'] = 'palak picked up the packets for Job ID #30.';
                $noti_data['job_id'] = 30;
                $noti_data['title'] = 'PortDrop Consumer';
                Login::postAndroidnotification($noti_data);
            } elseif ($noti_type == 5) {
                $noti_data['type'] = 5;
                $noti_data['recepient_id'] = 24;
                $token_info = Login::Getusertokeninfo($noti_data['recepient_id']); //print_r($token_info);die;
                //$noti_data['file_name'] = 'ck_development_consumer';
                $noti_data['device_token'] = $token_info['device_token'];
                $noti_data['message'] = 'palak has completed Job ID #58.';
                $noti_data['job_id'] = 30;
                $noti_data['title'] = 'PortDrop Consumer';
                Login::postAndroidnotification($noti_data);
            }
            $data['success'] = true;
        }
        return Response::json($data);
    }

}

?>
