<?php

class ProfileController extends BaseController {

    public function postAllprofileinfo()
	{	
		$data['success'] = true;
		$data['message'] = "";
		$data['data'] = new stdClass();
		
		$data['data'] = Profile::postAllprofileinfo();
		
		return Response::json($data);
		
	}
}