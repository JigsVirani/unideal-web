<html lang="en"><!--<![endif]--><!-- BEGIN HEAD --><head>
        <meta charset="utf-8">
        <title>UNIDeal || 404</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css">

        {{ HTML::style('assets/lib/bootstrap/css/bootstrap.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/font-awesome/css/font-awesome.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/simple-line-icons/simple-line-icons.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/uniform/css/uniform.default.css',array('rel'=>'stylesheet')) }}  
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        {{ HTML::style('assets/css/components.min.css', array('rel' => 'stylesheet')) }}
        {{ HTML::style('assets/css/plugins.min.css', array('rel' => 'stylesheet')) }}
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        {{ HTML::style('assets/css/error.min.css',array('rel'=>'stylesheet')) }}
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="<?= url('/') . "/" ?>favicon.png"/>
    </head>
    <!-- END HEAD -->

    <body class=" page-404-full-page" cz-shortcut-listen="true">
        <div class="row">
            <div class="col-md-12 page-404">
                <div class="number font-red"> 404 </div>
                <div class="details">
                    <h3>Oops! You're lost.</h3>
                    <p> We can not find the page you're looking for.</p>
                    <p>
                        <a href="{{ URL::to(MANAGE_TEXT.'dashboard')}}" class="btn red btn-outline"> Return home </a>                        
                    </p>
                </div>
            </div>
        </div>
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->

        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->


    </body>
</html>