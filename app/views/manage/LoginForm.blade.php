<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>UNIDeal | Admin Panel</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        {{ HTML::style('assets/lib/font-awesome/css/font-awesome.min.css',array('rel'=>'stylesheet')) }}    
        {{ HTML::style('assets/lib/simple-line-icons/simple-line-icons.min.css',array('rel'=>'stylesheet')) }}          
        {{ HTML::style('assets/lib/bootstrap/css/bootstrap.min.css',array('rel'=>'stylesheet')) }} 
        {{ HTML::style('assets/lib/uniform/css/uniform.default.css',array('rel'=>'stylesheet')) }}
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        {{ HTML::style('assets/lib/select2/css/select2.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/lib/select2/css/select2-bootstrap.min.css',array('rel'=>'stylesheet')) }}
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME GLOBAL STYLES -->
        {{ HTML::style('assets/css/components.min.css', array('rel' => 'stylesheet')) }}
        {{ HTML::style('assets/css/plugins.min.css', array('rel' => 'stylesheet')) }}
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        {{ HTML::style('assets/css/login3.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/lib/bootstrap-toastr/toastr.min.css',array('rel'=>'stylesheet')) }}
        <!-- END PAGE LEVEL STYLES -->

        <!--{{ HTML::style('assets/admin/css/custom.css',array('rel'=>'stylesheet')) }}-->  

        <link rel="shortcut icon" href="<?= url('/') . "/" ?>favicon.png"/>
    </head>
    <body class="login" >        
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGO -->
        <div class="logo">
            <img src="<?php echo ASSETS_IMAGE . 'logo.png' ?>" alt="UNIDeal" class="" style="max-width: 200px;"/>
        </div>        
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form form_login" action="" method="post">
                <h3 class="form-title">Administrative Panel</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter email address and password. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text"  placeholder="Email" name="email" /> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password"  placeholder="Password" name="password" /> </div>
                </div>
                <div class="form-actions">
                    <label class="checkbox">
                        <input type="checkbox" name="remember" value="" /> Remember me </label>
                    <button type="submit" class="btn green pull-right btn_custom_login"><i class="fa fa-lock"></i> Login </button>
                </div>                
                <div class="forget-password">
                    <a href="javascript:;" id="forget-password"><h4>Forgot your password ?</h4></a>                    
                </div>                
            </form>
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" action="" method="post">
                <h3>Forget Password ?</h3>
                <p> Enter your email address below to reset your password. </p>
                <div class="form-group">
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email_fp" id="email_fp" /> </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn grey-mint"><i class="fa fa-arrow-left"></i> Back </button>
                    <button type="submit" class="btn green pull-right btn_custom_login" id="btn_forgot_password"><i class="fa fa-check"></i> Submit </button>
                </div>
            </form>
        </div>
        <!-- END FORGOT PASSWORD FORM -->
        <div class="copyright"> <?= date('Y') ?> &copy; UNIDeal. </div>
        <!--[if lt IE 9]>
        <script src="../assets/lib/respond.min.js"></script>
        <script src="../assets/lib/excanvas.min.js"></script> 
        <![endif]-->

        <!-- BEGIN CORE PLUGINS -->
        <script>var BASEURL = '<?php echo url() . '/manage/'; ?>'</script>
        {{ HTML::script('assets/lib/jquery.min.js') }}
        {{ HTML::script('assets/lib/bootstrap/js/bootstrap.min.js') }}
        {{ HTML::script('assets/lib/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}
        {{ HTML::script('assets/lib/uniform/jquery.uniform.min.js') }}
        {{ HTML::script('assets/lib/jquery.cokie.min.js') }}
        {{ HTML::script('assets/lib/uniform/jquery.uniform.min.js') }}        

        <!-- END CORE PLUGINS -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        {{ HTML::script('assets/lib/jquery-validation/js/jquery.validate.min.js') }}
        {{ HTML::script('assets/lib/jquery-validation/js/additional-methods.min.js') }}
        {{ HTML::script('assets/lib/select2/js/select2.full.min.js') }}
        {{ HTML::script('assets/lib/bootstrap-toastr/toastr.min.js') }}
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        {{ HTML::script('assets/js/app.js') }}
        {{ HTML::script('assets/js/login.js') }}
        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
            jQuery(document).ready(function() {

                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-top-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "600",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "slideDown",
                    "hideMethod": "slideUp"
                }
            });
        </script>

    </body>
</html>
