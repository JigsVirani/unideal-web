<!DOCTYPE html>
<html lang="en">
    <head>
        <title ng-bind="title"> UNIDeal | Password Change </title>

        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        {{ HTML::style('assets/lib/font-awesome/css/font-awesome.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/simple-line-icons/simple-line-icons.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/bootstrap/css/bootstrap.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/uniform/css/uniform.default.css',array('rel'=>'stylesheet')) }}
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        {{ HTML::style('assets/lib/select2/css/select2.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/lib/select2/css/select2-bootstrap.min.css',array('rel'=>'stylesheet')) }}
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME GLOBAL STYLES -->
        {{ HTML::style('assets/css/components.min.css', array('rel' => 'stylesheet')) }}
        {{ HTML::style('assets/css/plugins.min.css', array('rel' => 'stylesheet')) }}
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        {{ HTML::style('assets/css/login3.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/lib/bootstrap-toastr/toastr.min.css',array('rel'=>'stylesheet')) }} 

        <link rel="shortcut icon" href="<?= url('/') . "/" ?>favicon.ico"/>
    </head>
    <body class="login page-header-fixed page-sidebar-fixed page-container-bg-solid page-sidebar-closed-hide-logo " >
        <div class="copyright">
<!--            <h2 style="color:#FFFFFF">UNIDeal</h2>-->
            <img src="<?= ASSETS_IMAGE_FRONT . 'monkey.png' ?>" alt="UNIDeal" width="150" border="0" style="padding:0px;">          
            <div class="">
                <div class="col-md-12 col-sm-6 col-xs-12 text-center" ><h4 class="h4-verify"  style="COLOR:#FFFFFF;">Your password has been changed! You can login with your new password now.</h4></div>
            </div>
        </div>

        <div class="whiter text-center">
            <h1>&nbsp;</h1>
        </div>
        <div class="menu-toggler sidebar-toggler">
        </div>


        {{ HTML::script('assets/lib/jquery.min.js') }}
        {{ HTML::script('assets/lib/bootstrap/js/bootstrap.min.js') }}
        {{ HTML::script('assets/lib/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}
        {{ HTML::script('assets/lib/uniform/jquery.uniform.min.js') }}
        {{ HTML::script('assets/lib/jquery.cokie.min.js') }}
        {{ HTML::script('assets/lib/uniform/jquery.uniform.min.js') }}        

        <!-- END CORE PLUGINS -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        {{ HTML::script('assets/lib/select2/js/select2.full.min.js') }}
        {{ HTML::script('assets/lib/bootstrap-toastr/toastr.min.js') }}

        <script>
            $(document).ready(function()
            {
                //Metronic.init();

            });
        </script>
    </body>
</html>
