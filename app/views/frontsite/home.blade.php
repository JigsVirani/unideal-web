<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>UNIDeal | Admin Panel</title>
        {{ HTML::script('assets/js/frontsite/jquery.min.js') }}
        {{ HTML::style('assets/css/frontsite/custom-font.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/css/frontsite/font-awesome.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/css/frontsite/icon_moon.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/lib/bootstrap-toastr/toastr.min.css',array('rel'=>'stylesheet')) }}
        <!-- Bootstrap -->
        {{ HTML::style('assets/css/frontsite/bootstrap.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/css/frontsite/style.css',array('rel'=>'stylesheet')) }}
        <link rel="shortcut icon" type="image/x-icon" href="favicon.png">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    </head>

    <body>

        <section class="remo-sec-1">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 ">
                        <div class="list-proces-remo">
                            <div class="logo">
                                <img src="<?= ASSETS_IMAGE_FRONT ?>logo.png" />
                            </div>
                            <div class="list-process">
                                <ul class="list-unstyled Segoe-UI-Light">
                                    <li><span>-</span> Set weekly goals</li>
                                    <li><span>-</span> Get paid if you complete those goals</li>
                                    <li><span>-</span> Pay for it if you don’t</li>
                                    <li><span>-</span> Share your workouts with friends</li>
                                </ul>
                                <a class="btn btn-green marginT20" id="join">Sign Up Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 app-img">
                        <div class="pull-right app-img-inner">
                            <img src="<?= ASSETS_IMAGE_FRONT ?>hand-app.png" />

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="remo-sec-2">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-md-10 col-md-offset-1">
                        <div class="row">
                            <div class="col-sm-9 col-md-6  col-md-offset-2  ">
                                <p class="marginT15">The secret to getting ahead is getting started.
                                </p>
                                <span>- Mark Twain</span>
                            </div>
                            <div class="col-sm-3 img">
                                <img class="pull-left" src="<?= ASSETS_IMAGE_FRONT ?>monkey.png" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="remo-sec-3">
            <div class="container">
                <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel" data-slide-to="1"></li>
                        <li data-target="#carousel" data-slide-to="2"></li>
                        <li data-target="#carousel" data-slide-to="3"></li>
                        <li data-target="#carousel" data-slide-to="4"></li>
                    </ol>
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="active item">
                            <div class="row">
                                <div class=" col-sm-3 col-md-4">
                                </div>
                                <div class="col-sm-6 col-md-4 mobile-screen">
                                    <img src="<?= ASSETS_IMAGE_FRONT ?>screen1.jpg" />
                                    <div class="text place1">
                                        <div class="icon text-right">
                                            <i class="icon-calendar">
                                            </i>
                                        </div>
                                        <div class="desc marginT15">
                                            <p class="text-right">You decide which days you are committed to going to the gym.</p>
                                        </div>
                                    </div>
                                    <div class="text place2">
                                        <div class="icon text-left">
                                            <i class="icon-coin">
                                            </i>
                                        </div>
                                        <div class="desc marginT15">
                                            <p class="text-left">You decide how much is enough to keep you motivated.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class=" col-sm-3 col-md-4">
                                </div>
                            </div>

                        </div>
                        <div class="item">
                            <div class="row">
                                <div class=" col-sm-3 col-md-4">
                                </div>
                                <div class="col-sm-6 col-md-4 mobile-screen">
                                    <img src="<?= ASSETS_IMAGE_FRONT ?>screen2.jpg" />
                                    <div class="text place3">
                                        <div class="icon text-right">
                                            <i class="icon-users">
                                            </i>
                                        </div>
                                        <div class="desc marginT15">
                                            <p class="text-right">See which friends you have on ReMo who work out at the same gym.</p>
                                        </div>
                                    </div>
                                    <div class="text place4">
                                        <div class="icon text-left">
                                            <i class="icon-search" style="font-weight: 600;">
                                            </i>
                                        </div>
                                        <div class="desc marginT15">
                                            <p class="text-left">Type the name of your gym or use the Current Location option to bring up a list of gyms in your area.</p>
                                        </div>
                                    </div>

                                    <div class="text place5">
                                        <div class="icon text-left">
                                            <i class="icon-dumbbell1">
                                            </i>
                                        </div>
                                        <div class="desc marginT15">
                                            <p class="text-left">Select up to 10 gyms you plan on attending to fulfill your goals.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class=" col-sm-3 col-md-4">
                                </div>
                            </div>

                        </div>
                        <div class="item">
                            <div class="row">
                                <div class=" col-sm-3 col-md-4">
                                </div>
                                <div class="col-sm-6 col-md-4 mobile-screen">
                                    <img src="<?= ASSETS_IMAGE_FRONT ?>screen3.jpg" />
                                    <div class="text place6">
                                        <div class="icon text-right">
                                            <i class="icon-like">
                                            </i>
                                        </div>
                                        <div class="desc marginT15">
                                            <p class="text-right">ReMo tracks which days you went to the gym and which days you did not</p>
                                        </div>
                                    </div>
                                    <div class="text place7">
                                        <div class="icon text-right">
                                            <i class="icon-placeholder">
                                            </i>
                                        </div>
                                        <div class="desc marginT15">
                                            <p class="text-right">Using the GPS on your phone, ReMo automatically clocks you in an out of your workouts!</p>
                                        </div>
                                    </div>
                                    <div class="text place8">
                                        <div class="icon text-left">
                                            <i class="icon-alarm-clock">
                                            </i>
                                        </div>
                                        <div class="desc marginT15">
                                            <p class="text-left">
                                                You must be at the Gym for a minimum of 30 minutes to get credit for your workout
                                            </p>
                                        </div>
                                    </div>

                                </div>
                                <div class=" col-sm-3 col-md-4">
                                </div>
                            </div>

                        </div>
                        <div class="item">
                            <div class="row">
                                <div class=" col-sm-3 col-md-4">
                                </div>
                                <div class="col-sm-6 col-md-4 mobile-screen">
                                    <img src="<?= ASSETS_IMAGE_FRONT ?>screen4.jpg" />
                                    <div class="text place9">
                                        <div class="icon text-right">
                                            <i class="icon-money-bag">
                                            </i>
                                        </div>
                                        <div class="desc marginT15">
                                            <p class="text-right">Earn money for consistently going to the gym</p>
                                        </div>
                                    </div>
                                    <div class="text place10">
                                        <div class="icon text-right">
                                            <i class="icon-money">
                                            </i>
                                        </div>
                                        <div class="desc marginT15">
                                            <p class="text-right">Each week you complete your goals, ReMo rewards you with cash!</p>
                                        </div>
                                    </div>
                                    <div class="text place11">
                                        <div class="icon text-left">
                                            <i class="icon-dislike">
                                            </i>
                                        </div>
                                        <div class="desc marginT15">
                                            <p class="text-left">
                                                Pay each time you skip a scheduled workout
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class=" col-sm-3 col-md-4">
                                </div>
                            </div>

                        </div>
                        <div class="item">
                            <div class="row">
                                <div class=" col-sm-3 col-md-4">
                                </div>
                                <div class="col-sm-6 col-md-4 mobile-screen">
                                    <img src="<?= ASSETS_IMAGE_FRONT ?>screen5.jpg" />
                                    <div class="text place12">
                                        <div class="icon text-right" style="font-size:35px;">
                                            <i class="icon-profits">
                                            </i>
                                        </div>
                                        <div class="desc marginT15">
                                            <p class="text-right">Click here to learn how to get more points by increasing your workout schedule,referring friends to ReMo, and sharing on Social Media!</p>
                                        </div>
                                    </div>
                                    <div class="text place13">
                                        <div class="icon text-left">
                                            <i class="fa fa-trophy">
                                            </i>
                                            <span>$500.00 above</span>
                                        </div>
                                        <div class="desc marginT15">
                                            <p class="text-left">Once a month ReMo gives out a Major Motivation Reward to the person who consistently went to the gym the most!</p>
                                        </div>
                                    </div>
                                    <div class="text place14">
                                        <div class="icon text-left">
                                            <i class="icon-appicon" style="font-size:40px;">
                                            </i>
                                        </div>
                                        <div class="desc marginT15">
                                            <p class="text-left">This could be you!</p>
                                        </div>
                                    </div>

                                </div>
                                <div class=" col-sm-3 col-md-4">
                                </div>
                            </div>

                        </div>

                    </div>
                    <!-- Carousel nav -->
                    <a class="carousel-control left visible-xs" href="#carousel" data-slide="prev"><img src="<?= ASSETS_IMAGE_FRONT ?>back.png" /></a>
                    <a class="carousel-control right visible-xs" href="#carousel" data-slide="next"><img src="<?= ASSETS_IMAGE_FRONT ?>next.png" /></a>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="remo-sec-4">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-md-10 col-md-offset-1">
                        <div class="remo-mail col-md-offset-1">
                            <p class=" ">Sign Up now and be one of the first to sign up for UNIDeal! </p>
                        </div>
                        <div class="col-md-offset-1 email-form">
                            <form role="form" action="#" class="form-inline" method="post" id="pre_register_form" name="pre_register_form">
                                <div class="form-group col-sm-8 col-xs-12">
                                    <input type="email" class="form-control" placeholder="Enter your email address here" id="email_address" name="email_address">                                    
                                </div>                                
                                <button type="submit" class="btn btn-green col-sm-3 col-xs-12 btn_submit_sign_up">Submit</button>                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <section class="remo-sec-5">
            <div class="container">
                <div class="row">
                    <div class="col-sm-11 col-sm-offset-1">
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="pull-right opertunity-img ">
                                    <div id="carousel-opertunuti" class="carousel slide carousel-fade" data-ride="carousel">
                                        <!-- Carousel items -->
                                        <div class="carousel-inner">
                                            <div class="active item"> <img src="<?= ASSETS_IMAGE_FRONT ?>opertinuti1.jpg" />
                                            </div>
                                            <div class="item"><img src="<?= ASSETS_IMAGE_FRONT ?>opertinuti.jpg" />
                                            </div>

                                        </div>
                                        <!-- Carousel nav -->
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="opertunity">
                                    <h3 class="">Get in Shape, Get Paid, and Share it with your Friends!</h3>
                                    <p>
                                        Be a part of the ReMo community and talk with other gym-goers around the world! Post on the community feed, Facebook, Instagram, or Twitter and gain additional points to win the Monthly Motivation Reward.
                                    </p>
                                    <div class="email-form">
                                        <form role="form" action="#" class="form-inline" method="post" id="pre_register_form_1" name="pre_register_form_1">
                                            <div class="form-group col-xs-12 col-sm-0">
                                                <input type="email" class="form-control" placeholder="Enter your email address here" name="email_address" id="email_address_1">
                                            </div>
                                            <button type="submit" class="btn btn-green btn_send"><i class="fa fa-send"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="contact-form">
                            <h3>Send us a Message</h3>
                            <form role="form" class="marginT20" name="contact_form" id="contact_form">
                                <div class="group form-group">
                                    <input type="text" class="form-control" name="contact_name" id="contact_name" autocomplete="off">
                                    <label for="contact_name">Your Name</label>
                                </div>

                                <div class="group form-group">
                                    <input type="text" class="form-control" id="contact_email" name="contact_email" autocomplete="off">
                                    <label for="contact_email">Your Email</label>
                                </div>
                                <div class="group form-group">
                                    <textarea class="form-control"  style=" resize: vertical; " name="contact_message" id="contact_message"></textarea>
                                    <label>Your Message</label>
                                </div>
                                <button type="submit" class="btn btn-green btn_contact">Send</button>
                            </form>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="address col-sm-offset-4">
                            <h3>Contact us</h3>
                            <address class="marginT20">
                                <span class="title">Email</span><br/>
                                <a href="mailto:aschwarzenegger@remo.team" class="custom_footer_email">aschwarzenegger@remo.team</a><br/>
                                <span class="title">Address</span><br/>
                                Address 1990 NW Boca Raton Blvd.<br/>
                                Boca Raton, FL 33432<br/><br/>
                                <span>06637-7396</span>
                            </address>
                            <div class="social-icon marginT40">
                                <ul class="list-unstyled list-inline">
                                    <li class="facebook"> <a href="https://www.facebook.com/ReMoApp/" target="_blank"><i class="fa fa-facebook"></i></a> </li>
                                    <li class="twitter"><a href="https://www.facebook.com/ReMoApp/" target="_blank"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li class="Instagram"><a href="https://www.instagram.com/remo.fitness/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-center marginT20 copyright ">
                        <a>&COPY; copyright <?= date('Y') ?></a>
                    </div>
                </div>
            </div>

        </footer>
        <!-- child of the body tag -->
        <span id="top-link-block" class="hidden">
            <a href="#top" class="well well-sm" onclick="$('html,body').animate({scrollTop: 0}, 'slow');
                    return false;">
                <i class="glyphicon glyphicon-chevron-up"></i> 
            </a>
        </span><!-- /top-link-block -->
    </body>    
    {{ HTML::script('assets/js/frontsite/bootstrap.js') }}
    {{ HTML::script('assets/js/frontsite/script.js') }}
    {{ HTML::script('assets/lib/jquery-validation/js/jquery.validate.min.js') }}    
    {{ HTML::script('assets/lib/bootstrap-toastr/toastr.min.js') }}
    <script>
        $(document).ready(function()
        {
            //define toster
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-full-width",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "slideDown",
                "hideMethod": "slideUp"
            }
            //form 1
            $('#pre_register_form').validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    email_address: {
                        required: true,
                        email: true,
                        remote: {
                            url: "<?php echo action('HomeController@postCheckemail'); ?>",
                            type: "post",
                            data: {
                                email_address: function() {
                                    return $("#email_address").val();
                                }
                            }
                        }
                    }
                },
                messages: {
                    email_address: {
                        required: "Please tell us your email address.",
                        email: "Invalid email address.",
                        remote: "Email address already exist."
                    }
                },
                invalidHandler: function(event, validator) { //display error alert on form submit   

                },
                highlight: function(element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function(label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                errorPlacement: function(error, element) {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                },
                submitHandler: function(form) {
                    //define form data
                    var fd = new FormData();
                    //append data
                    $.each($('#pre_register_form').serializeArray(), function(i, obj) {
                        fd.append(obj.name, obj.value)
                    })

                    $.ajax({
                        url: '<?php echo action('HomeController@postPreregister'); ?>',
                        type: "post",
                        processData: false,
                        contentType: false,
                        data: fd,
                        beforeSend: function() {
                            $('.btn_submit_sign_up').attr('disabled', true);
                            $('.btn_submit_sign_up').text('Please Wait...');
                        },
                        success: function(res) {
                            if (res.status == '1')// in case genre added successfully
                            {
                                toastr.success(res.message, 'UNIDeal');
                                return false;
                            } else { // in case error occue
                                toastr.warning(res.message, 'UNIDeal');
                                return false;
                            }
                        },
                        error: function(e) {
                            //called when there is an error
                            toastr.error('Oops! Something just went wrong. Please try again later.', 'UNIDeal');
                            return false;
                        },
                        complete: function() {
                            $('#pre_register_form')[0].reset();
                            $('.btn_submit_sign_up').attr('disabled', false);
                            $('.btn_submit_sign_up').text('Submit');
                        }
                    }, "json");
                    return false;
                }
            });
            //form 2

            $('#pre_register_form_1').validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    email_address: {
                        required: true,
                        email: true,
                        remote: {
                            url: "<?php echo action('HomeController@postCheckemail'); ?>",
                            type: "post",
                            data: {
                                email_address: function() {
                                    return $("#email_address_1").val();
                                }
                            }
                        }
                    }
                },
                messages: {
                    email_address: {
                        required: "Please tell us your email address.",
                        email: "Invalid email address.",
                        remote: "Email address already exist."
                    }
                },
                invalidHandler: function(event, validator) { //display error alert on form submit   

                },
                highlight: function(element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function(label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                errorPlacement: function(error, element) {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                },
                submitHandler: function(form) {
                    //define form data
                    var fd = new FormData();
                    //append data
                    $.each($('#pre_register_form_1').serializeArray(), function(i, obj) {
                        fd.append(obj.name, obj.value)
                    })

                    $.ajax({
                        url: '<?php echo action('HomeController@postPreregister'); ?>',
                        type: "post",
                        processData: false,
                        contentType: false,
                        data: fd,
                        beforeSend: function() {
                            $('.btn_send').attr('disabled', true);
                        },
                        success: function(res) {
                            if (res.status == '1')// in case genre added successfully
                            {
                                toastr.success(res.message, 'UNIDeal');
                                return false;
                            } else { // in case error occue
                                toastr.warning(res.message, 'UNIDeal');
                                return false;
                            }
                        },
                        error: function(e) {
                            //called when there is an error
                            toastr.error('Oops! Something just went wrong. Please try again later.', 'UNIDeal');
                            return false;
                        },
                        complete: function() {
                            $('#pre_register_form_1')[0].reset();
                            $('.btn_send').attr('disabled', false);
                        }
                    }, "json");
                    return false;
                }
            });

            //contact form
            $('#contact_form').validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    contact_name: {
                        required: true
                    },
                    contact_email: {
                        required: true,
                        email: true
                    },
                    contact_message: {
                        required: true
                    },
                },
                messages: {
                    contact_name: {
                        required: "Name is required."
                    },
                    contact_email: {
                        required: "Email address is required.",
                        email: "Invalid email address."
                    },
                    contact_message: {
                        required: "Message is required."
                    },
                },
                invalidHandler: function(event, validator) { //display error alert on form submit   

                },
                highlight: function(element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function(label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                errorPlacement: function(error, element) {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                },
                submitHandler: function(form) {
                    //define form data
                    var fd = new FormData();
                    //append data
                    $.each($('#contact_form').serializeArray(), function(i, obj) {
                        fd.append(obj.name, obj.value)
                    })

                    $.ajax({
                        url: '<?php echo action('HomeController@postContact'); ?>',
                        type: "post",
                        processData: false,
                        contentType: false,
                        data: fd,
                        beforeSend: function() {
                            $('.btn_contact').attr('disabled', true);
                            $('.btn_contact').text('Please Wait...');
                        },
                        success: function(res) {
                            if (res.status == '1')// in case genre added successfully
                            {
                                toastr.success(res.message, 'UNIDeal');
                                return false;
                            } else { // in case error occue
                                toastr.warning(res.message, 'UNIDeal');
                                return false;
                            }
                        },
                        error: function(e) {
                            toastr.error('Oops! Something just went wrong. Please try again later.', 'UNIDeal');
                            //called when there is an error
                            return false;
                        },
                        complete: function() {
                            $('#contact_form')[0].reset();
                            $('.btn_contact').attr('disabled', false);
                            $('.btn_contact').text('Send');
                        }
                    }, "json");
                    return false;
                }
            });
        });
    </script>
    <script> (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-80512348-1', 'auto');
        ga('send', 'pageview');
    </script>
</html>