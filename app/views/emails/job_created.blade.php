
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#2d383a" c-style="bgImage" object="drag-module" style="background-color: rgb(48, 48, 48);">
    <tbody>
        <tr mc:repeatable="">
            <td align="center" style="-webkit-background-size: cover;  background-color: #2d383a; background-size: cover; background-position: 50% 50%; background-repeat: no-repeat;" c-style="bgImage" id="not1">
                <div mc:hideable="">

                    <!-- Mobile Wrapper -->
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                        <tbody>
                            <tr>
                                <td width="100%" align="center">

                                    <div class="sortable_inner ui-sortable">

                                        <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small">
                                            <tbody>
                                                <tr>
                                                    <td width="700" valign="middle">

                                                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                            <tbody><tr>
                                                                    <td width="100%" height="50"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small">
                                            <tbody><tr>
                                                    <td width="700" valign="middle" align="center">

                                                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                            <tbody><tr>
                                                                    <td width="100%" height="50"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <table width="392" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                        <tbody><tr>
                                <td align="center" width="20" valign="middle"></td>
                                <td align="center" width="700" valign="middle">

                                    <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                        <tbody><tr>
                                                <td align="center" width="700" valign="middle" bgcolor="#1dc8e9" c-style="blueBG" style="background-color: #2d383a;">

                                                    <div class="sortable_inner ui-sortable">

                                                        <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#1dc8e9" c-style="blueBG" object="drag-module-small" style="background-color: rgb(255,255,255); border-radius: 50px 50px 0px 0px;">
                                                            <tbody><tr>
                                                                    <td width="700" valign="middle" align="center">

                                                                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                            <tbody><tr>
                                                                                    <td width="100%" height="30"></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>							
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <!-- Start Top -->
                                                        <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#1dc8e9" c-style="blueBG" object="drag-module-small" style="background-color: rgb(255,255,255);">
                                                            <tbody><tr>
                                                                    <td width="700" valign="middle" align="center">

                                                                        <!-- Header Text --> 
                                                                        <table width="348" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                            <tbody><tr>
                                                                                    <td align="center" valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 40px; color: #29ab87; line-height: 44px; font-weight: 100;padding: 10px" t-style="whiteText" class="fullCenter" mc:edit="1" object="text-editable">
                                                                                        <!--[if !mso]><!--><span style="font-family: 'proxima_novathin', Helvetica; font-weight: normal;"><!--<![endif]-->
                                                                                            <singleline>
                                                                                                <img src="<?= ASSETS_IMAGE_FRONT . 'monkey.PNG' ?>" alt="UNIDeal" width="100" border="0" style="padding:0px;max-width: 200px">
                                                                                            </singleline>
                                                                                            <!--[if !mso]><!--></span><!--<![endif]-->
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="100%" height="0"></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table><!-- End Top -->


                                                        <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#1dc8e9" c-style="blueBG" object="drag-module-small" style="background-color: rgb(255,255,255);">
                                                            <tbody><tr>
                                                                    <td width="700" valign="middle" align="center">
                                                                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                            <tbody><tr>
                                                                                    <td width="100%" height="25"></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>							
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td align="center" width="20" valign="middle"></td>
                            </tr>
                        </tbody>
                    </table>

                    <table width="392" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                        <tbody><tr>
                                <td align="center" width="20" valign="middle"></td>
                                <td align="center" width="700" valign="middle">

                                    <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                        <tbody><tr>
                                                <td align="center" width="700" valign="middle" bgcolor="#ffffff" c-style="whiteBG" <div class="sortable_inner ui-sortable" style="background-color: #29ab87;    border-radius: 0px 0px 50px 50px;">
                                                        <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="whiteBG" object="drag-module-small" style="background-color: #29ab87;">
                                                            <tbody><tr>
                                                                    <td width="700" valign="middle" align="center">

                                                                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                            <tbody><tr>
                                                                                    <td width="100%" height="20"></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>							
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <!-- Start Second -->
                                                        <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="whiteBG" object="drag-module-small" style="background-color: #29ab87;">
                                                            <tbody><tr>
                                                                    <td width="700" valign="middle" align="center">

                                                                        <!-- Header Text --> 
                                                                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                            <tbody><tr>
                                                                                    <td align="center" valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 39px; color: rgb(255,255,255); line-height: 44px; font-weight: 100; text-align: center;" t-style="headline" class="fullCenter" mc:edit="3" object="text-editable">
                                                                                        <!--[if !mso]><!--><span style="font-family: 'proxima_novathin', Helvetica; font-weight: normal;"><!--<![endif]-->
                                                                                            <singleline>
                                                                                                Dear {{ $name }}
                                                                                            </singleline>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="whiteBG" object="drag-module-small" style="background-color: #29ab87;">
                                                            <tbody><tr>
                                                                    <td width="700" valign="middle" align="center">

                                                                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                            <tbody><tr>
                                                                                    <td width="100%" height="30"></td>
                                                                                </tr>
                                                                            </tbody></table>							
                                                                    </td>
                                                                </tr>
                                                            </tbody></table>

                                                        <!-- Start Second -->
                                                        <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="whiteBG" object="drag-module-small" style="background-color: #29ab87;">
                                                            <tbody><tr>
                                                                    <td width="700" valign="middle" align="center">
                                                                        <!-- Header Text --> 
                                                                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                            <tbody><tr>
                                                                                    <td valign="middle" width="100%" style="text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: rgb(255,255,255); line-height: 24px;" t-style="textColor" class="fullCenter" mc:edit="4" object="text-editable">
                                                                                        <!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]-->
                                                                                            <singleline>
                                                                                                <table>
                                                                                                    <tr><td width="120" ><b> Job Title </b> </td>
                                                                                                        <td>: {{ $job_title }}</td>
                                                                                                    </tr>
                                                                                                    <tr><td width="120" valign="top"><b> Job Description </b> </td>
                                                                                                        <td>:  {{ $job_details }} </td>
                                                                                                    </tr>
                                                                                                    <tr><td width="120"><b> Agent Fees </b> </td>
                                                                                                        <td>:  {{ $agent_commision }} </td>
                                                                                                    </tr>
                                                                                                    <tr><td width="130"><b> Consignment Size </b> </td>
                                                                                                        <td>:  {{ $consignment_size }} </td>
                                                                                                    </tr>
                                                                                                    <tr><td width="120"><b> Job End Date </b></td>
                                                                                                        <td>:  {{ $job_end_on }} </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                <br/><br/>
                                                                                                UNIDeal Team
                                                                                            </singleline><!--[if !mso]><!--></span><!--<![endif]-->
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="whiteBG" object="drag-module-small" style="background-color: #29ab87;">
                                                            <tbody><tr>
                                                                    <td width="700" valign="middle" align="center">

                                                                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                            <tbody><tr>
                                                                                    <td width="100%" height="25"></td>
                                                                                </tr>
                                                                            </tbody></table>							
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="whiteBG" object="drag-module-small" style="background-color: #29ab87;">
                                                            <tbody><tr>
                                                                    <td width="700" valign="middle" align="center">

                                                                        <table width="200" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; " class="fullCenter">
                                                                            <tbody>
                                                                               
                                                                <tr>
                                                                    <td width="100%" height="10">
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>							
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="whiteBG" object="drag-module-small" style="background-color: #29ab87; border-radius: 0px 0px 50px 50px; ">
                                        <tbody><tr>
                                                <td width="700" valign="middle" align="center">

                                                    <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                        <tbody><tr>
                                                                <td width="100%" height="30"></td>
                                                            </tr>
                                                        </tbody></table>							
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- End Second -->
                                </td>
                            </tr>
                        </tbody>
                    </table>
            </td>
            <td align="center" width="20" valign="middle"></td>
        </tr>
    </tbody></table>

<!-- Mobile Wrapper -->
<table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
    <tbody><tr>
            <td width="700" align="center">

                <div class="sortable_inner ui-sortable">

                    <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small">
                        <tbody><tr>
                                <td width="700" valign="middle" align="center">

                                    <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                        <tbody><tr>
                                                <td width="100%" height="40"></td>
                                            </tr>
                                        </tbody></table>							
                                </td>
                            </tr>
                        </tbody></table>

                    <!-- CopyRight -->
                    <table width="700" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="mobile-full-width" style="text-align: center; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FFF;" align="right">
                                <div style="padding-top:4px; margin-bottom: 10px;">
                                    <a href="javascript:;" style="color:#FFF; text-decoration:none;">Contact Us</a>  |  <a href="javascript:;" style="color:#FFF; text-decoration:none;">User Support</a>  |  <a href="javascript:;" style="color:#FFF; text-decoration:none;"> FAQ </a>
                                </div>
                                <a href="javascript:;" style="padding:0 10px;"><img src="<?= ASSETS_IMAGE_FRONT . 'facebook.png' ?>" height="30" width="30" style="line-height:10px;" /></a>                                
                                <a href="javascript:;" style="padding:0 10px;"><img src="<?= ASSETS_IMAGE_FRONT . 'twitter.png' ?>" height="30" width="30" style="line-height:10px;" /></a>
                                <a href="javascript:;" style="padding:0 10px;"><img src="<?= ASSETS_IMAGE_FRONT . 'insta_logo.png' ?>" height="30" width="30" style="line-height:10px;" /></a>
                            </td>
                        </tr>
                    </table>
                    <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
                        <tbody><tr>
                                <td align="center" valign="middle" width="700" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 13px; color: rgb(149, 149, 149); line-height: 24px;" t-style="copyright" class="fullCenter" mc:edit="8" object="text-editable">

                                    <!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]-->
                                        {{ date('Y') }} &copy; UNIDeal <!--[if !mso]><!--></span><!--<![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table><!-- End CopyRight -->

                    <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small">
                        <tbody><tr>
                                <td align="center" width="700" valign="middle">

                                    <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                        <tbody><tr>
                                                <td width="100%" height="60"></td>
                                            </tr>
                                            <tr>
                                                <td width="100%" height="1" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>							
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
    </tbody>
</table>
</div>
</td>
</tr>
</tbody></table>