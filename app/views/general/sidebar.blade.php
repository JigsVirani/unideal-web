<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse" style="background-color: #0a1421 !important;">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed text-capitalize" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px" ng-controller="MyCtrl">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper hide">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler"> </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            <li class="sidebar-search-wrapper">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                <form class="sidebar-search" action="page_general_search_3.html" method="POST" style="display: none;">
                    <a href="javascript:;" class="remove">
                        <i class="icon-close"></i>
                    </a>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <a href="javascript:;" class="btn submit">
                                <i class="icon-magnifier"></i>
                            </a>
                        </span>
                    </div>
                </form>
                <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>
			
            <li ng-class="{active:isActive('/manage/dashboard')}" ng-click="hidesub()">
                <a href="{{ URL::to(MANAGE_TEXT.'dashboard')}}" class="nav-link nav-toggle">
                    <i class="fa fa-dashboard"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
				</a>
            </li>
            <li class="take-active" ng-class="{'active': isActive('/manage/users')}" ng-click="hidesub()">
                <a href="{{ URL::to(MANAGE_TEXT.'users')}}" class="nav-link nav-toggle">
                    <i class="icon-all-users"></i>
                    <span class="title">Users</span>
                    <span  ng-class="{'selected': isActive('/manage/users')}"></span>
				</a>
            </li>
            <li ng-class="{'active': isActive('/manage/jobs')}" ng-click="hidesub()">
                <a href="{{ URL::to(MANAGE_TEXT.'jobs')}}" class="nav-link nav-toggle">
                    <i class="fa fa-briefcase"></i>
                    <span class="title">Jobs</span>
                    <span  ng-class="{'selected': isActive('/manage/jobs')}"></span>
				</a>
            </li>
            <li ng-class="mainmenu" class="">
                <a href="javascript:void(0);" class="nav-link nav-toggle" ng-click="submenu()">
                    <i class="fa fa-cogs"></i>
                    <span class="title">Manage</span>
                    <span ng-class="arrowopen"></span>
                </a>
                <ul class="sub-menu" style="display: block;" ng-show="showsub">
                    <li ng-class="{'active': isActive('/manage/jobcategory')}" ng-click="activemainmenu()">
					  <a href="{{ URL::to(MANAGE_TEXT.'jobcategory')}}" class="nav-link nav-toggle">
                            <i class="icon-category"></i>
                            <span class="title">Job Categories</span>
                            <span ng-class="{'selected': isActive('/manage/jobcategory')}" ></span>
                        </a>
                    </li>
                   <li ng-class="{'active': isActive('/manage/advancepayment')}" ng-click="activemainmenu()">
					  <a href="{{ URL::to(MANAGE_TEXT.'advancepayment')}}" class="nav-link nav-toggle">
                            <i class="icon-payment"></i>
                            <span class="title">Advance Payment</span>
                            <span  ng-class="{'selected': isActive('/manage/advancepayment')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/commission')}" ng-click="activemainmenu()">
					  <a href="{{ URL::to(MANAGE_TEXT.'commission')}}" class="nav-link nav-toggle">
                            <i class="icon-Commition"></i>
                            <span class="title">Commission Charges</span>
                            <span  ng-class="{'selected': isActive('/manage/commission')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/promocode')}" ng-click="activemainmenu()">
					  <a href="{{ URL::to(MANAGE_TEXT.'promocode')}}" class="nav-link nav-toggle">
                            <i class="icon-coupan"></i>
                            <span class="title">Promo Code</span>
                            <span  ng-class="{'selected': isActive('/manage/promocode')}"></span>
                        </a>
                    </li>
                     <li ng-class="{'active': isActive('/manage/termscondition')}" ng-click="activemainmenu()">
					  <a href="{{ URL::to(MANAGE_TEXT.'termscondition')}}" class="nav-link nav-toggle">
                            <i class="icon-terms"></i>
                            <span class="title">Terms &amp; Conditions</span>
                            <span  ng-class="{'selected': isActive('/manage/termscondition')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/publishnotification')}" ng-click="activemainmenu()">
					  <a href="{{ URL::to(MANAGE_TEXT.'publishnotification')}}" class="nav-link nav-toggle">
                            <i class="icon-notifications"></i>
                            <span class="title">Push Notification</span>
                            <span  ng-class="{'selected': isActive('/manage/publishnotification')}"></span>
                        </a>
                    </li>
                </ul>
            </li>            
<!--            <li ng-class="{'active': isActive('/manage/')}" ng-click="hidesub()">
                <a href="{{ URL::to(MANAGE_TEXT.'')}}" class="nav-link nav-toggle">
                    <i class="icon-report"></i>
                    <span class="title">Reports</span>
                   <span  ng-class="{'selected': isActive('/manage/publishnotification')}"></span>

                </a>
            </li>
           <li ng-class="{'active': isActive('/manage/')}" ng-click="hidesub()">
                <a href="{{ URL::to(MANAGE_TEXT.'')}}" class="nav-link nav-toggle">
                    <i class="icon-analytics"></i>
                    <span class="title">Analytics</span>
                  <span  ng-class="{'selected': isActive('/manage/analytics')}"></span>
                </a>
            </li>            -->
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->