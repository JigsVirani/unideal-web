<!DOCTYPE html>
<html lang="en">
    <head>
        <title ng-bind="title"> UNIDeal | Reset Password </title>

        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        {{ HTML::style('assets/lib/font-awesome/css/font-awesome.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/simple-line-icons/simple-line-icons.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/bootstrap/css/bootstrap.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/uniform/css/uniform.default.css',array('rel'=>'stylesheet')) }}
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        {{ HTML::style('assets/lib/select2/css/select2.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/lib/select2/css/select2-bootstrap.min.css',array('rel'=>'stylesheet')) }}
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME GLOBAL STYLES -->
        {{ HTML::style('assets/css/components.css', array('rel' => 'stylesheet')) }}
        {{ HTML::style('assets/css/plugins.css', array('rel' => 'stylesheet')) }}
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        {{ HTML::style('assets/css/login3.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/lib/bootstrap-toastr/toastr.min.css',array('rel'=>'stylesheet')) }}
        <!-- END PAGE LEVEL STYLES -->

        <!--{{ HTML::style('assets/admin/css/custom.css',array('rel'=>'stylesheet')) }}-->  

        <link rel="shortcut icon" href="<?= url('/') . "/" ?>favicon.png"/>
        <style>
            .error{
                border-color: red;
            }
            label.error{
                color:red;
            }
        </style>
        <link rel="shortcut icon" href="<?= url('/') . "/" ?>favicon.ico"/>
    </head>
    <body class="login" >
        <div class="logo">
            <img src="<?php echo ASSETS_IMAGE_FRONT . 'logo.png' ?>" alt="UNIDeal" class="" style="width: 250px;"/>
        </div>
        <div class="content">
            <form class="login-form" action="<?php echo URL::to('/') . '/manage/login/resetpassword' ?>" method="POST" id="resetpasswordform">
                <input type="hidden" name="user" value="{{$email}}">
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Password</label>                                            
                    <input type="password" name="password" id="newPassword" class="form-control" placeholder="New password" autofocus>                    
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Confirm Password</label>                        
                    <input type="password" name="password_confirmation" id="confirmPassword" class="form-control" placeholder="ReType Password">                    
                </div>
                <div class="form-actions" style="border-bottom: none;">
                    <label class="checkbox"></label>
                    <button type="submit" class="btn green pull-right"> Reset Password </button>
                </div>

            </form>
        </div>
        <div class="copyright"> <?= date('Y') ?> &copy; UNIDeal. </div>
    </body>


    <script>var BASEURL = '<?php echo url() . '/manage/'; ?>'</script>
    {{ HTML::script('assets/lib/jquery.min.js') }}
    {{ HTML::script('assets/lib/bootstrap/js/bootstrap.min.js') }}
    {{ HTML::script('assets/lib/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}
    {{ HTML::script('assets/lib/uniform/jquery.uniform.min.js') }}
    {{ HTML::script('assets/lib/jquery.cokie.min.js') }}
    {{ HTML::script('assets/lib/uniform/jquery.uniform.min.js') }}        

    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    {{ HTML::script('assets/lib/jquery-validation/js/jquery.validate.min.js') }}
    {{ HTML::script('assets/lib/jquery-validation/js/additional-methods.min.js') }}
    {{ HTML::script('assets/lib/select2/js/select2.full.min.js') }}
    {{ HTML::script('assets/lib/bootstrap-toastr/toastr.min.js') }}
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    {{ HTML::script('assets/js/app.js') }}    

    <script>
        $(document).ready(function()
        {
            $("#resetpasswordform").validate({
                rules: {
                    password: {
                        required: true,
                        minlength: 6
                    },
                    password_confirmation: {
                        required: true,
                        minlength: 6,
                        equalTo: "#newPassword"
                    }
                },
                messages: {
                    password: {
                        required: "Password is required.",
                        minlength: "Your password must be at least 6 characters long"
                    },
                    password_confirmation: {
                        required: "Re-Type your password.   ",
                        minlength: "Your password must be at least 6 characters long",
                        equalTo: "Please enter the same password as above"
                    }
                }
            });

        });
    </script>
</body>
</html>
