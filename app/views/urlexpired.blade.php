<!DOCTYPE html>
<html lang="en">
    <head>
        <title ng-bind="title"> UNIDeal | URL Expired </title>

        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>

        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>

        {{ HTML::style('assets/lib/bootstrap/css/bootstrap.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/font-awesome/css/font-awesome.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/simple-line-icons/simple-line-icons.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/uniform/css/uniform.default.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/css/login3.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/css/components.min.css',array('rel'=>'stylesheet','id'=>'style_components')) }}  
        {{ HTML::style('assets/css/plugins.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/css/layout.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/css/themes/darkblue.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/css/custom.css',array('rel'=>'stylesheet')) }} 

        <link rel="shortcut icon" href="<?= url('/') . "/" ?>favicon.png"/>
    </head>
    <body class="login page-header-fixed page-sidebar-fixed page-container-bg-solid page-sidebar-closed-hide-logo " >
        <div class="copyright">            
             <img src="<?= ASSETS_IMAGE_FRONT . 'monkey.png' ?>" alt="UNIDeal" width="150" border="0" style="padding:0px;">
            <div class="">
                <div class="col-md-12 col-sm-6 col-xs-12 text-center" ><h4 class="h4-verify"  style="COLOR:#FFFFFF;">Sorry!  Your {{$url_for}} link has been expired.</h4></div>
            </div>
        </div>
        <div class="whiter text-center">
            <h1>&nbsp;</h1>
        </div>
        <div class="menu-toggler sidebar-toggler">
        </div>
        {{ HTML::script('assets/lib/jquery.min.js') }}
        {{ HTML::script('assets/lib/jquery-migrate.min.js') }}
        {{ HTML::script('assets/lib/bootstrap/js/bootstrap.min.js') }}
        {{ HTML::script('assets/lib/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}
        {{ HTML::script('assets/lib/jquery-slimscroll/jquery.slimscroll.min.js') }}
        {{ HTML::script('assets/lib/jquery.blockui.min.js') }}
        {{ HTML::script('assets/lib/jquery.cokie.min.js') }}
        {{ HTML::script('assets/lib/uniform/jquery.uniform.min.js') }}      

        <script>
            $(document).ready(function()
            {
                //Metronic.init();

            });
        </script>
    </body>
</html>
