<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>UNIDeal | Reset Password</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        {{ HTML::style('assets/lib/font-awesome/css/font-awesome.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/simple-line-icons/simple-line-icons.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/bootstrap/css/bootstrap.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/uniform/css/uniform.default.css',array('rel'=>'stylesheet')) }}
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        {{ HTML::style('assets/lib/select2/css/select2.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/lib/select2/css/select2-bootstrap.min.css',array('rel'=>'stylesheet')) }}
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME GLOBAL STYLES -->
        {{ HTML::style('assets/css/components.min.css', array('rel' => 'stylesheet')) }}
        {{ HTML::style('assets/css/plugins.min.css', array('rel' => 'stylesheet')) }}
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        {{ HTML::style('assets/css/login3.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/lib/bootstrap-toastr/toastr.min.css',array('rel'=>'stylesheet')) }}
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="icon" type="image/png" href="<?= ASSETS_IMAGE ?>favicon.png"> 
        <style type="text/css">
            .login .content .resetpassword-form h3 {
                color: #4db3a5;
                text-align: center;
                font-size: 28px;
                font-weight: 400 !important;
            }
        </style>
    </head>
    <!-- END HEAD -->

    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo spadelogo">
        </div>
        <!-- END LOGO -->
        <div class="menu-toggler sidebar-toggler">
        </div>
        <!-- BEGIN FORGOT PASSWORD FORM -->
        <div class="logo">
            <a href="{{ URL::to('advertiser') }}">
                <img src="<?php echo ASSETS_IMAGE_FRONT . 'logo.png' ?>" alt="UNIDeal" class="" style="height: 100px !important;"/>
            </a>
        </div>
        <div class="content">

            <!-- BEGIN LOGO -->

            <!-- END LOGO -->

            {{ Form::open(array('url' =>'api/users/resetpassword','class' => 'resetpassword-form','method'=>'post')) }}

            <input type="hidden" name="email_address" id="user_hidden_email" value="{{$email}}">
            <!--                <h3 class="form-title">User</h3>-->
            <h3 class="form-title">Reset Password</h3>		     
            @if(Session::has('forgot_message'))
            <div class="alert alert-danger">{{ Session::get('forgot_message') }}</div>
            @endif

            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">New Password</label>
                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <input class="form-control placeholder-no-fix" id="newpassword" type="password" value="" autocomplete="off" placeholder="New Password" name="newpassword"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Confirm Password</label>
                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <input class="form-control placeholder-no-fix" type="password" value="" autocomplete="off" placeholder="Confirm Password" name="confirmedpassword"/>
                </div>
            </div>
            <div class="form-actions text-center">
                <label class="checkbox">
                    <button type="submit" class="btn spadegreen pull-right">
                        Reset Password
                    </button>
            </div>		
        </form>
    </div>
    <div class="copyright"> <?= date('Y') ?> &copy; UNIDeal. </div>
    <!-- END LOGIN -->


    <!-- BEGIN CORE PLUGINS -->
    <script>var BASEURL = '<?php echo url() . '/manage/'; ?>'</script>
    {{ HTML::script('assets/lib/jquery.min.js') }}
    {{ HTML::script('assets/lib/bootstrap/js/bootstrap.min.js') }}
    {{ HTML::script('assets/lib/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}
    {{ HTML::script('assets/lib/uniform/jquery.uniform.min.js') }}
    {{ HTML::script('assets/lib/jquery.cokie.min.js') }}
    {{ HTML::script('assets/lib/uniform/jquery.uniform.min.js') }}        

    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    {{ HTML::script('assets/lib/jquery-validation/js/jquery.validate.min.js') }}
    {{ HTML::script('assets/lib/jquery-validation/js/additional-methods.min.js') }}
    {{ HTML::script('assets/lib/select2/js/select2.full.min.js') }}
    {{ HTML::script('assets/lib/bootstrap-toastr/toastr.min.js') }}
    <!-- END PAGE LEVEL SCRIPTS -->

    <script type="text/javascript">
        $(document).ready(function(e) {
         var user_hidden_email = $("#user_hidden_email").val();
            $.validator.addMethod("checkoldpassword",
               function (value, element) {
                   var result;
                   var id = $('#hidden_id').val();
                   $.ajax({
                       type: "POST",
                       async: false,
                       url: '<?= action('ApiuserController@postCheckoldpassword') ?>', // script to validate in server side
                       data: {password: value, user_hidden_email: user_hidden_email},
                       success: function (data) {
                           result = (data.success == false) ? true : false;

                       }
                   });
                   // return true if email is exist in database
                   return result;
               },
               "Your new password is same as old one!!! Please try new."
               );

            $('.resetpassword-form').validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    newpassword: {
                        required: true
                      //  checkoldpassword: true
                    },
                    confirmedpassword: {
                        equalTo: "#newpassword",
                        required: true
                    },
                },
                messages: {
                    newpassword: {
                        required: "New Password is required."
                    },
                    confirmedpassword: {
                        equalTo: "Confirm password does not match the new password.",
                        required: "Confirm Password is required."
                    }
                },
                invalidHandler: function(event, validator) { //display error alert on form submit   
                    $('.alert-error', $('.login-form')).show();
                },
                highlight: function(element) { // hightlight error inputs
                    $(element)
                            .closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                success: function(label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },
                errorPlacement: function(error, element) {
                    error.insertAfter(element.closest('.input-icon'));
                }
            });

        });
    </script>
</body>
</html>

