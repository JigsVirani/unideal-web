<?php

return [
    'welcome' => '欢迎来到我们的应用',
    'REGISTER_SUCCESS' => ' 感谢您与注册不理想. 帐户验证链接对你的Email发送.',
    'INVALID_PARAMS' => '参数无效.',
    'SAME_PASSWORD'=> '你不能设置当前密码作为新密码',
    'LOGIN_FAIL_NO_ACCOUNT'=> '该帐户不存在。请注册继续',
    'LOGIN_FAIL_ACCOUNT_INACTIVE' => '您的帐户已关闭请联系管理员',
    'LOGIN_FAIL_ACCOUNT_NOT_VERIFIED' => '您的帐户尚未得到验证.请检查电子邮件进行验证.',
    'LOGIN_ERROR' => '电子邮箱地址或密码无效.',
    'LOGIN_SUCCESS' => '验证成功完成.',
    'LOGIN_FAIL_INCORRECT_PASSWORD' => '密码不正确.',
    'LOGOUT' => '您已成功注销.',
    'LOGOUT_FAILURE' => '你不能注销.',
    'NO_USER_DATA'=> '无可用数据.',
    'GENERAL_SUCCESS' => '完成!',
    'GENERAL_ERROR' => '哎呀！有些事情出了问题.请稍后再试.',
    'GENERAL_NO_CHANGES'=>'您所做的任何更改.',
    'PASSWORD_FAIL' => '密码没有更新.再试一次！',
    'INVALID_CURRENT_PASSWORD' => '当前密码不正确',
    'PASSWORD_SUCCESS_CHANGE'=> '密码已成功更新',
    'SUCCESS_PROFILE_UPDATE' => '个人资料已更新.',
    'SUCCESS_PROFILE_UPDATE_EMAIL' => '资料更新成功！电子邮件验证链接被作为电子邮件发送给您.',
    'FORGOT_PASSWORD_SUCCESS' => '一封电子邮件已经发送给您。请检查您的收件箱.',
    'EMAIL_NOT_EXIST' => '电子邮件没有与帐户匹配.',
    'FACEBBOK_ALREADY_EXIST' => 'Facebook的ID是存在的.',
    'FACEBOOK_NOT_EXIST' =>'Facebook的ID不存在.',
    'GOOGLE_ALREADY_EXIST' =>'Google 是存在的',
    'GOOGLE_NOT_EXIST' =>'Google 不存在.',
    'DELETE_SUCCESS' => '您的帐户已被删除.',
    'SUCCESS_SETTINGS_UPDATE' => '设置已被更新.',
    'EMAIL_NOT_AVAILABLE' => '电子邮件地址不可用.',
    'SUCCESS_BANK_ADDED' => '您的银行详细信息已保存.',
    'SUCCESS_BANK_DELETED' => '银行信息已被删除.',
    'GENERAL_NO_DATA'=> '无可用数据.',
    'VERIFICATION_LINK_SENT' => '帐户验证链接对你的电子邮件被发送',
    'ACCOUNT_ALREADY_VERIFIED' => '您的帐户已经过验证.请登录继续.',
    'SUCCESS_JOB_POSTED' => '你的工作细节已经公布.',
    'BANK_ACCOUNT_LIST_EMPTY' => '银行帐户列表为空.',
    'JOB_AWARDED_SUCCESSFULLY' => '您已成功授予工作。',
    'JOB_ALREADY_APPLIED' => '工作已经授予。',
    'JOB_DECLINED_PROPOSAL' => '您已成功拒绝代理。',
    'APPLIED_SUCCESS' => '您已成功应用作业。',
    'ALREADY_SEND_PROPOSAL' => '您已将提案发送给此工作。',
    'JOB_STATUS_ALREADY_APPLIED' =>'您已应用此状态。',
    'JOB_STATUS_PAUSED' => '您已暂停此工作。',
    'JOB_STATUS_RESUME' => '您已恢复此工作。',
    'JOB_STATUS_COMPLETED' => '您已完成此工作。',
    'JOB_DOES_NOT_EXIST' => '糟糕！没有工作。',
    'NOT_AUTHORIZED_TO_COMEPLETE' => ' 您无权完成此工作。',
    'JOB_NOT_COMPLETED_YET'=> '作业尚未完成。',
    'NOT_AUTHORIZED_TO_RESUME' => '您无权恢复此工作。',
    'JOB_STATUS_DISPUT' => '您的工作已由管理员提出异议。请联系UNIDeal管理员。',
    
];
?>