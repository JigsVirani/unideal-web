<?php

/**
 * This file demonstrates User registration,login,forgot password,delete & reactive account.
 * @author Jigs Virani <jigs@creolestudios.com>
 * @version 1.0
 * @package : Login,Register,Forgot Password,Re active & Delete account
 * @copyright  Creole Studios 2015
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 */
class Apijobs extends Eloquent {
    
    #checkjobsfilesexist
    #GetAllJobCategories
    #GetJobAgentDetails
    #checkJobAppliedStatus
    #checkJobProposalStatus
    #checkJobStatus
    #CheckAgentJobStatus
    
    ## Check jobs file exist.
    public static function checkjobsfilesexist($image) {
        
        $filename = URL_JOBS_THUMBS_IMAGES . $image;
        $physicalfilename = DIR_THUMBS_JOBS . $image;

        if (file_exists($physicalfilename) && $image != '') {
            $filename = $filename;
        } else {
            $filename = DIR_UPLOADS . 'default_file.png';
        }
        return $filename;
    }
    
    //#################################################################
    // Name : profilethumbexists
    // Purpose : To check for profile image thumb
    // In Params : all user data
    // Out params : Re active user and send verification email
    //#################################################################
    public static function GetAllJobCategories() {
        
        //global declaraton
        $ReturnData = array();

        $QueryUser = DB::table('job_categories as jc')
                ->select('jc.category_id', 'jc.category_name', 'jc.status', 'jc.created_on')
                ->where('jc.status', 1)
                ->get();

        $jobcategoryData = json_decode(json_encode($QueryUser), true);
        if ($QueryUser) {
            $ReturnData['status'] = '1';
            $ReturnData['jobcategory_data'] = $jobcategoryData;
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }    

  
    
    //#################################################################
    // Name : Newjob
    // Purpose : To add bank account in table
    // In Params : account details
    // Out params : status
    //#################################################################
    public static function Newjob($Params) {

        //global declaration
        $ReturnData = array();
        
        extract($Params);
        
        //prepare array for insertion.
        $InsertArray = array(
            'user_id'=> (int) $user_id,
            'category_id'=> (int) $category_id,
            'sub_category' => isset($sub_category_id)? $sub_category_id : '',
            'deliveryplace' => isset($deliveryplace) ? $deliveryplace : '',
            'job_title'=> $job_title,
            'job_details'=> $job_details,
            'agent_commision'=> (float)$agent_fees,
            'consignment_size'=> (float) $consignment_size,
            'questionar_review' => 0,
            'agent_review' => 0,
            'applicant' => 0,
            'is_dispute' => 0,
            'dispute_count' =>0,
            'job_status'=> 1,
            'status'=> 1,
            'job_start_on'=> date('Y-m-d H:i:s'),
            'job_end_on'=> $job_end_on,
            'created_on' => date('Y-m-d H:i:s')
        );
        
        if (isset($promotional_code) && $promotional_code != '') {
            
          $UserQuery = DB::table('promo_code')
                        ->select('promo_code_id','code','reward')
                        ->where('code', $promotional_code)
                        ->take(1)->first();
          
          $InsertArray['promotional_code'] = $promotional_code;
          $insertPromoHistory = array('user_id'=> $user_id,
                                      'promotional_code'=>$promotional_code);
         if(isset($UserQuery->reward)){
             $InsertArray['agent_commision'] =  $InsertArray['agent_commision'] - $UserQuery->reward;
         }else{
             $InsertArray['agent_commision'] = $InsertArray['agent_commision'];
         }
         
            
        }
        
        ## get advanced unideal service fees.
        $getAdvancedFees = Apiconfig::GetAllMasterSettings(SERVICE_FEES_ADVANCE);
        if(isset($getAdvancedFees) && $getAdvancedFees !=''){
             $InsertArray['service_fees_advanced'] =  (float) $getAdvancedFees['setting_value'];
        }
        ##get job budget advanced fees.
        $getAdvancedBudget = Apiconfig::GetAllMasterSettings(JOB_BUDGET_ADVANCED);
    
        if(isset($getAdvancedBudget) && $getAdvancedBudget !=''){
             $InsertArray['advanced_charge'] =  (float) $getAdvancedBudget['setting_value'];
        }
       
       // jobbudget_advance_payment
        ##get commision charges from agent.
        $getChargesFromAgent = Apiconfig::GetAllMasterSettings(COMMISION_CHARGE);
        if(isset($getChargesFromAgent) && $getChargesFromAgent !=''){
             $InsertArray['commision_charge'] = (float) $getChargesFromAgent['setting_value'];
        }
        
        if(isset($sub_category) && $sub_category !=''){
            $InsertArray['sub_category'] = $sub_category;
        }   
        if(isset($quantity) && $quantity !=''){
             $InsertArray['quantity'] = $quantity;
        }
        
        #3 add data to table.
        $Addjob = DB::table('jobs')->insertGetId($InsertArray);
        
        ## return data.
        if ($Addjob) {
            
            ## add account id to insert array for return.
            unset($InsertArray['created_on']);
            $InsertArray['job_id'] = $Addjob;
            $ReturnData['status'] = '1';
            $ReturnData['job_details'] = $InsertArray;
            $UserQuery = DB::table('front_users')->select('id','name','email_address')->where('id',$user_id )->where('user_type',1)->first();
            if($UserQuery){
                $Questionerjob =  array_merge($InsertArray,array('name'=>$UserQuery->name,'email_address'=>$UserQuery->email_address));
                $template = 'emails.job_created';
                $subject = 'UNIDeal Team : Job Created Successfully.';
                $sendmail = Apiemail::email($Questionerjob,$template,$subject);
                ##send push notifictions.
                ## send the push notification for related agent.
                $toSendThePushToAgent = Apiusers::sendMessagesToAgentForExpertise($category_id, $Addjob);
                
//                if(isset($os_type) && $os_type == OS_TYPE_IOS){
//                    $GetTokensList = Apiusers::GetUsersDeviceTokens($user_id, OS_TYPE_IOS);
//                   
//                    if($GetTokensList){
//                        $message = array('message' => 'You have successfully created new jobs.','user_id'=> $user_id, 'user_type' => 1, 'job_id' => $Addjob);
//                        foreach($GetTokensList as $toke){
//                           
//                            $sendPushIos = Apiusers::SendIospushnotifications($message, $toke['device_token']);
//                             // print_r($toke['device_token']);die;
//                         }
//                    }
//                }
            }
        } else {
            $ReturnData['status'] = '0';
        }
        
        ## return data.
        return $ReturnData;
    }
   
    
    //#################################################################
    // Name : GetAlljobs
    // Purpose : List of all the jobs 
    // In Params : user_id,Job_status data
    // Out params : require details as per job type
    // Hardika Satasiya 15 nov 2016
    //#################################################################
    public static function GetAlljobs($Params) {
        
        //global declaraton
        $ReturnData = array();
          
        ##get open jobs.
        if($Params['job_status'] == JOB_STATUS_OPEN){
            $getOpenJobs = Apijobs::getAllOpenJobs($Params);
            if(isset($getOpenJobs['status']) && $getOpenJobs['status'] == STATUS_TRUE){
                 $ReturnData['status'] = STATUS_TRUE;
                 $ReturnData['joblist_data'] = $getOpenJobs['data'];
            }
            else{
                $ReturnData['status'] = STATUS_FALSE;
            }
        }
        ##get applied jobs.
        else if($Params['job_status'] == JOB_STATUS_APPLIED){
            $getAppliedjobs = Apijobs::getAllAppliedJobs($Params['user_id'],$Params['user_type']);
             if(isset($getAppliedjobs['status']) && $getAppliedjobs['status'] == STATUS_TRUE){
                 $ReturnData['status'] = STATUS_TRUE;
                 $ReturnData['joblist_data'] = $getAppliedjobs['data'];
            }
            else{
                $ReturnData['status'] = STATUS_FALSE;
            }
        }
        ## get inprocess jobs.
        else if($Params['job_status'] == JOB_STATUS_INPROCESS){
            $getinprocessjobs = Apijobs::getAllInprocessJobs($Params['user_id'],$Params['user_type']);
            
             if(isset($getinprocessjobs['status']) && $getinprocessjobs['status'] == STATUS_TRUE){
                 $ReturnData['status'] = STATUS_TRUE;
                 $ReturnData['joblist_data'] = $getinprocessjobs['data'];
            }
            else{
                $ReturnData['status'] = STATUS_FALSE;
            }
        }
        ## get Completed jobs.
     else if($Params['job_status'] == JOB_STATUS_COMPLETED){
            $getCompletedJobs = Apijobs::getAllCompletedJobs($Params['user_id'],$Params['user_type']);
             if(isset($getCompletedJobs['status']) && $getCompletedJobs['status'] == STATUS_TRUE){
                 $ReturnData['status'] = STATUS_TRUE;
                 $ReturnData['joblist_data'] = $getCompletedJobs['data'];
            }
            else{
                $ReturnData['status'] = STATUS_FALSE;
            }
        }
        ## get Paused jobs.
       else if($Params['job_status'] == JOB_STATUS_PAUSED){
            $getPausedJobs = Apijobs::getAllPausedJobs($Params['user_id'],$Params['user_type']);
             if(isset($getPausedJobs['status']) && $getPausedJobs['status'] == STATUS_TRUE){
                 $ReturnData['status'] = STATUS_TRUE;
                 $ReturnData['joblist_data'] = $getPausedJobs['data'];
            }
            else{
                $ReturnData['status'] = STATUS_FALSE;
            }
        }
        ## get cancelled jobs.
       else if($Params['job_status'] == JOB_STATUS_CANCELLED){
            $getCancelledJobs = Apijobs::getAllCancelledJobs($Params['user_id'],$Params['user_type']);
             if(isset($getCancelledJobs['status']) && $getCancelledJobs['status'] == STATUS_TRUE){
                 $ReturnData['status'] = STATUS_TRUE;
                 $ReturnData['joblist_data'] = $getCancelledJobs['data'];
            }
            else{
                $ReturnData['status'] = STATUS_FALSE;
            }
        }
        else{
            $ReturnData['status'] = STATUS_FALSE;
         }
     
        return $ReturnData;
    }
    //#################################################################
    // Name : getAllOpenJobs
    // Purpose : List of all the open jobs 
    // In Params : user_id,user_type data
    // Out params : require details of open type jobs
    // Hardika Satasiya 15 nov 2016
    //#################################################################
    
    public static function getAllOpenJobs($Params){
        
         $ReturnData = array();
      
         $Querylistjobs = DB::table('jobs as j')
                ->select('j.job_id','j.user_id', 'j.job_title', 'j.job_status', 'j.agent_commision', 'fu.name',
                          'j.job_end_on',
                          'j.applicant','j.category_id','jc.category_name')
                ->join('job_categories as jc', 'j.category_id', '=', 'jc.category_id')
                ->join('front_users as fu', 'j.user_id', '=', 'fu.id')
                ->where(function($Querylistjobs) use ($Params) {
                        if ($Params['user_type'] ==  1):
                            $Querylistjobs->where(array('j.status'=> 1,'j.user_id'=> $Params['user_id'], 'j.job_status'=> JOB_STATUS_OPEN));
                        endif;
                       if ($Params['user_type'] ==  2):
                            $Querylistjobs->where(array('j.status'=> 1,'j.job_status'=> JOB_STATUS_OPEN));
                            $Querylistjobs->where('j.user_id','!=', $Params['user_id']);
                            
                           // $Querylistjobs->where('ji.status','!=', 3);
                           // $Querylistjobs->where('ji.status','!=', 2);
                       endif;
                        if ($Params['user_type'] ==  2):
                            
                             ## if both are appied.
                            if(isset($Params['job_expiry_date']) && $Params['job_expiry_date'] !='' && isset($Params['job_posting_date']) && $Params['job_posting_date'] !=''){
                                         $Querylistjobs->whereRaw('DATE_FORMAT(j.created_on, "%Y-%m-%d") >= "'.$Params['job_posting_date'].'"');
                                         $Querylistjobs->whereRaw('DATE_FORMAT(j.job_end_on, "%Y-%m-%d") <= "'.$Params['job_expiry_date'].'"');
                                    
                            }else {
                                     ## if only posting date.
                                    if(isset($Params['job_posting_date']) && $Params['job_posting_date'] !='' ){
                                         $Querylistjobs->whereRaw('DATE_FORMAT(j.created_on, "%Y-%m-%d") >= "'.$Params['job_posting_date'].'"');
                                    }
                                    ## if only expire date.
                                    if(isset($Params['job_expiry_date']) && $Params['job_expiry_date'] !=''){
                                         $Querylistjobs->whereRaw('DATE_FORMAT(j.job_end_on, "%Y-%m-%d") >= "'.$Params['job_expiry_date'].'"');
                                    }
                            }
                           
                            if(isset($Params['category_id']) && $Params['category_id'] !=''){
                                 $Querylistjobs->where('j.category_id', $Params['category_id']);
                            }
                            if(isset($Params['job_fee_min']) && $Params['job_fee_min'] !=''){
                                 $Querylistjobs->where('j.agent_commision' ,'>=', $Params['job_fee_min']);
                            }
                            if(isset($Params['job_fee_max']) && $Params['job_fee_max'] !=''){
                                 $Querylistjobs->where('j.agent_commision' ,'<=', $Params['job_fee_max']);
                            }
                            if(isset($Params['job_search']) && $Params['job_search'] !=''){
                                 $Querylistjobs->where('j.job_title', 'LIKE', '%'.$Params['job_search'].'%');
                            }
                            $Querylistjobs->whereRaw('j.job_id NOT IN ((Select job_id from job_invite where user_id = "'.$Params['user_id'].'"))');
                       endif;
                 })
        
//                if($Params['user_type'] == IS_AGENT){
//                     $Querylistjobs ->orderby('j.created_on', 'desc');
//                } else {
//                     $Querylistjobs->orderbyRaw('');
//                }
               
                ->orderby('j.created_on', 'desc')
                ->get();
        
              //print_r($Querylistjobs);die;
        
        $listjobData = json_decode(json_encode($Querylistjobs), true);
            
        if ($Querylistjobs) {
           
                foreach($listjobData as $jobsKey => $jobsVal){
                    
                     if(isset($Params['user_type']) && $Params['user_type'] == 2){
                        $jobsVal['user_name'] = $jobsVal['name'];
                          unset($jobsVal['name']);
                     }
                    if(isset($Params['user_type']) && $Params['user_type'] == 1){ 
                       // echo 'sd';die;
                        unset($jobsVal['name']);
                     }
                    
                    $listjobData[$jobsKey] = $jobsVal;
                 
                }
            
            
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['data'] = $listjobData;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
        return $ReturnData;
    }
     //#################################################################
    // Name : getAllAppliedJobs
    // Purpose : List of all the applied jobs 
    // In Params : user_id,user_type data
    // Out params : require details of Applied Jobs type jobs
    // Hardika Satasiya 15 nov 2016
    //#################################################################
    public static function getAllAppliedJobs($user_id, $user_type){
       
        $ReturnData = array();
        
      $Querylistjobs = DB::table('job_invite as ji')
                ->select('j.job_id','ji.user_id','fu.name as user_name', 'j.job_title', 'j.job_status', 'j.agent_commision','j.job_end_on','j.applicant','j.category_id','jc.category_name')
                ->leftJoin('jobs as j', 'j.job_id', '=', 'ji.job_id')
                ->leftJoin('job_categories as jc', 'j.category_id', '=', 'jc.category_id')
                ->leftJoin('front_users as fu', 'j.user_id', '=', 'fu.id')
                ->where(array('j.status'=> 1, 'ji.user_id' => $user_id, 'ji.status' => 1))
                ->where('j.job_status', JOB_STATUS_OPEN)
                ->orderby('ji.created_on', 'desc')
                ->get();
        
//        $Querylistjobs = DB::table('jobs as j')
//                ->select('j.job_id','j.user_id', 'j.job_title', 'j.job_status', 'j.agent_commision',DB::raw('DATE_FORMAT(j.job_end_on,"%d %b %Y") as job_end_on', 'job_end_on'),'j.category_id','j.applicant','j.category_id','jc.category_name')
//                ->join('job_categories as jc', 'j.category_id', '=', 'jc.category_id')
//                ->join('front_users as fu', 'j.user_id', '=', 'fu.id')
//                ->where(array('j.status'=> 1,'j.user_id'=> $user_id, 'j.job_status'=> JOB_STATUS_APPLIED))
//                ->get();
         
        $listjobData = json_decode(json_encode($Querylistjobs), true);
        
        if ($listjobData) {
            
            foreach($listjobData as $keys => $vals){
                   
                    $vals['job_status'] = (int) JOB_STATUS_APPLIED;
                    $listjobData[$keys] = $vals;
            }
            
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['data'] = $listjobData;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
         return $ReturnData;
    }
     //#################################################################
    // Name : getAllInprocessJobs
    // Purpose : List of all the In porcess jobs 
    // In Params : user_id,user_type data
    // Out params : require details of  In porcess type jobs
    //#################################################################
    public static function getAllInprocessJobs($user_id, $user_type){
        
        $ReturnData = array();
        
        $Querylistjobs = DB::table('jobs as j')
                ->select('j.job_id','j.user_id', 'j.job_title','j.agent_commision', 'j.job_status', 'j.agent_commision',
                         db::raw('(select name from front_users where id = j.user_id) as questionar_name'),
                         db::raw('(select name from front_users where id = ji.user_id) as agent_name'),
                        'j.job_end_on','j.category_id','j.applicant','jc.category_name','ji.user_id as agent_id',
                         'fu.name as agent_name')
                ->leftJoin('job_categories as jc', 'j.category_id', '=', 'jc.category_id')
                ->leftJoin('job_invite as ji', 'j.job_id', '=', DB::raw('ji.job_id and ji.status = 3'))
                ->leftJoin('front_users as fu', 'ji.user_id', '=', 'fu.id')
                ->where(function($Querylistjobs) use ($user_type,$user_id) {
                        if ($user_type ==  1):
                             $Querylistjobs->where(array('j.status'=> 1,'j.user_id'=> $user_id, 'j.job_status'=> JOB_STATUS_INPROCESS));
                        endif;
                       if ($user_type ==  2):
                             $Querylistjobs->where(array('j.status'=> 1,'ji.user_id'=> $user_id, 'j.job_status'=> JOB_STATUS_INPROCESS));
                       endif;
                  })
                ->orderby('j.updated_on', 'desc')
                ->get();

        $listjobData = json_decode(json_encode($Querylistjobs), true);
       
        if ($listjobData) {
            
            ## To get messages for inprocess jobs.
            foreach($listjobData as $keys => $vals){
                
                     if(isset($user_type) && $user_type == 2){
                        $unreadMessage = self::getjobsunreadmessages($vals['job_id'], $vals['agent_id']);
                        $vals['user_name'] = $vals['questionar_name'];
                        unset($vals['agent_name']);
                        unset($vals['questionar_name']);
                     }
                    if(isset($user_type) && $user_type == 1){
                        $unreadMessage = self::getjobsunreadmessages($vals['job_id'], $vals['user_id']);
                        $vals['user_name'] = $vals['agent_name'];
                        unset($vals['agent_name']);
                        unset($vals['questionar_name']);
                     }
                    
                     unset($vals['agent_id']);
                     $vals['messages'] = (int) $unreadMessage;
                     $listjobData[$keys] = $vals;
            }
            
            //$listjobData['message']= (int) 5;
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['data'] = $listjobData;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
         return $ReturnData;
    }
     //#################################################################
    // Name : getAllCompletedJobs
    // Purpose : List of all the Completed jobs 
    // In Params : user_id,user_type data
    // Out params : require details of  Completed type jobs
    // Hardika Satasiya 15 nov 2016
    //#################################################################
    public static function getAllCompletedJobs($user_id, $user_type) {
        
        $ReturnData = array();
        $Querylistjobs = DB::table('jobs as j')
                ->select('j.job_id','j.user_id', 'j.job_title', 'j.job_status','j.agent_commision',
                          db::raw('(select name from front_users where id = j.user_id) as questionar_name'),
                         db::raw('(select name from front_users where id = ji.user_id) as agent_name'),
                         'j.job_end_on','j.category_id',
                         'jc.category_name','j.agent_review','j.questionar_review')
                ->leftJoin('job_categories as jc', 'j.category_id', '=', 'jc.category_id')
                ->leftJoin('job_invite as ji', 'j.job_id', '=', DB::raw('ji.job_id and ji.status = 3'))
                ->leftJoin('front_users as fu', 'ji.user_id', '=', 'fu.id')
                ->where(function($Querylistjobs) use ($user_type,$user_id) {
                        if ($user_type ==  1):
                             $Querylistjobs->where(array('j.status'=> 1,'j.user_id'=> $user_id, 'j.job_status'=> JOB_STATUS_COMPLETED));
                        endif;
                       if ($user_type ==  2):
                             $Querylistjobs->where(array('j.status'=> 1,'ji.user_id'=> $user_id, 'j.job_status'=> JOB_STATUS_COMPLETED));
                       endif;
                  })
             ->orderby('j.created_on', 'desc')
                ->get();
         
        $listjobData = json_decode(json_encode($Querylistjobs), true);
        
        if ($Querylistjobs) {
            
             ## To get messages for inprocess jobs.
            foreach($listjobData as $keys => $vals){
                
                     if(isset($user_type) && $user_type == 2){
                        $vals['user_name'] = $vals['questionar_name'];
                        $vals['reviews'] = $vals['questionar_review'];
                        unset($vals['agent_name']);
                        unset($vals['questionar_review']);
                         unset($vals['agent_review']);
                        unset($vals['questionar_name']);
                     }
                    if(isset($user_type) && $user_type == 1){ 
                        $vals['user_name'] = $vals['agent_name'];
                        $vals['reviews'] = $vals['agent_review'];
                        unset($vals['agent_name']);
                        unset($vals['questionar_review']);
                        unset($vals['agent_review']);
                        unset($vals['questionar_name']);
                     }
                    
                    $listjobData[$keys] = $vals;
              
            }
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['data'] = $listjobData;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
         return $ReturnData;
    }
     //#################################################################
    // Name : getAllCompletedJobs
    // Purpose : List of all the Completed jobs 
    // In Params : user_id,user_type data
    // Out params : require details of  Completed type jobs
    // Hardika Satasiya 15 nov 2016
    //#################################################################
    public static function getAllPausedJobs($user_id, $user_type){
        
        $ReturnData = array();
        ##fetching records of all paused records 
        $Querylistjobs = DB::table('jobs as j')
                ->select('j.job_id','j.user_id', 'j.job_title', 'j.job_status','j.agent_commision',
                         'j.job_end_on','j.category_id',
                          db::raw('(select name from front_users where id = j.user_id) as questionar_name'),
                         db::raw('(select name from front_users where id = ji.user_id) as agent_name'),
                         'jc.category_name','fu.name as agent_name')
                ->leftJoin('job_categories as jc', 'j.category_id', '=', 'jc.category_id')
                ->leftJoin('job_invite as ji', 'j.job_id', '=', DB::raw('ji.job_id and ji.status = 3'))
                ->leftJoin('front_users as fu', 'ji.user_id', '=', 'fu.id')
                ->where(function($Querylistjobs) use ($user_type,$user_id) {
                        if ($user_type ==  1):
                             $Querylistjobs->where(array('j.status'=> 1,'j.user_id'=> $user_id, 'j.job_status'=> JOB_STATUS_PAUSED));
                        endif;
                       if ($user_type ==  2):
                             $Querylistjobs->where(array('j.status'=> 1,'ji.user_id'=> $user_id, 'j.job_status'=> JOB_STATUS_PAUSED));
                       endif;
                  })
                ->orderby('j.updated_on', 'desc')
                ->get();
         ## records in json array
        $listjobData = json_decode(json_encode($Querylistjobs), true);
        
        if ($Querylistjobs) {
            
             foreach($listjobData as $keys => $vals){
                
                     if(isset($user_type) && $user_type == 2){
                        $vals['user_name'] = $vals['questionar_name'];
                       
                        unset($vals['agent_name']);
                        unset($vals['questionar_name']);
                     }
                    if(isset($user_type) && $user_type == 1){ 
                      $vals['user_name'] = $vals['agent_name'];
                        unset($vals['agent_name']);
                        unset($vals['questionar_name']);
                     }
                    $listjobData[$keys] = $vals;
            }
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['data'] = $listjobData;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
         return $ReturnData;
    }
     //#################################################################
    // Name : getAllCompletedJobs
    // Purpose : List of all the Completed jobs 
    // In Params : user_id,user_type data
    // Out params : require details of  Completed type jobs
    // Hardika Satasiya 15 nov 2016
    //#################################################################
    public static function getAllCancelledJobs($user_id, $user_type){
        $ReturnData = array();
       $Querylistjobs = DB::table('jobs as j')
                ->select('j.job_id','j.user_id', 'j.job_title', 'j.job_status', 'j.agent_commision',
                           db::raw('(select name from front_users where id = j.user_id) as questionar_name'),
                         db::raw('(select name from front_users where id = ji.user_id) as agent_name'),
                         'j.cancelled_on','j.category_id',
                         'jc.category_name','fu.name as agent_name')
                ->leftJoin('job_categories as jc', 'j.category_id', '=', 'jc.category_id')
                ->leftJoin('job_invite as ji', 'j.job_id', '=', DB::raw('ji.job_id and ji.status = 3'))
                ->leftJoin('front_users as fu', 'ji.user_id', '=', 'fu.id')
                 ->where(function($Querylistjobs) use ($user_type,$user_id) {
                        if ($user_type ==  1):
                             $Querylistjobs->where(array('j.status'=> 1,'j.user_id'=> $user_id, 'j.job_status'=> JOB_STATUS_CANCELLED));
                        endif;
                       if ($user_type ==  2):
                             $Querylistjobs->where(array('j.status'=> 1,'ji.user_id'=> $user_id, 'j.job_status'=> JOB_STATUS_CANCELLED));
                       endif;
                  })
                ->orderby('j.cancelled_on', 'desc')
                ->get();
        $listjobData = json_decode(json_encode($Querylistjobs), true);
       //print_r($listjobData);die;
        if ($Querylistjobs) {
            
            foreach($listjobData as $keys => $vals){
                
                     if(isset($user_type) && $user_type == 2){
                        $vals['user_name'] = $vals['questionar_name'];
                       
                        unset($vals['agent_name']);
                        unset($vals['questionar_name']);
                     }
                    if(isset($user_type) && $user_type == 1){
                    
                      $vals['user_name'] = $vals['agent_name'];
                        unset($vals['agent_name']);
                        unset($vals['questionar_name']);
                     }
                    $listjobData[$keys] = $vals;
            }
            
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['data'] = $listjobData;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
         return $ReturnData;
    }
    
    ## jigs Virani 14 nov 2016
    ## to get job details.
    public static function GetJobDetailsjobs($Params){
    
        $ReturnData = array();
        
         $Querylistjobs = DB::table('jobs as j')
                ->select('j.job_id','j.user_id', 'j.job_title','j.is_dispute',DB::raw('IFNULL(j.deliveryplace, "") as deliveryplace'),DB::raw('IFNULL(jsc.sub_category_name, "") as sub_category_name'),
                         DB::raw('IFNULL(ji.is_questioner_confirm, 0) as is_questioner_confirm'),
                         DB::raw('IFNULL(ji.is_agent_confirm, 0) as is_agent_confirm'),
                         db::raw('IFNULL(j.user_id, "") as quser_id'),db::raw('IFNULL(ji.user_id,"") as agent_id'),'j.job_status','agent_review','questionar_review','files',
                         db::raw('(select name from front_users where id = j.user_id) as questionar_name'),
                         db::raw('(select name from front_users where id = ji.user_id) as agent_name'),
                         'j.job_status', 'j.agent_commision','j.consignment_size','j.job_details',
                         'job_end_on',
                         'j.job_start_on',
                         'jc.category_name','j.applicant','jc.category_name')
                ->leftJoin('job_categories as jc', 'j.category_id', '=', 'jc.category_id')
                ->leftJoin('job_sub_categories as jsc', 'j.category_id', '=', 'jsc.category_id')
                ->leftJoin('job_invite as ji', 'ji.job_id', '=', DB::raw('j.job_id and ji.status = 3'))
                ->where(array('j.status'=> 1,'j.job_id'=> $Params['job_id']))
                ->first();
        
        $listjobData = json_decode(json_encode($Querylistjobs), true);
        
        
        if ($listjobData) {
            
            if(isset($listjobData['files']) && $listjobData['files'] !=''){
                $files_string = str_replace('}','',str_replace('{','',$listjobData['files']));
                $files_array = explode(',',$files_string);
                    
                 foreach($files_array as $vals){
                        $JobsFiles[] = self::checkjobsfilesexist($vals);
                 }
                 $listjobData['files'] =  $JobsFiles;
            } else {
                 $listjobData['files'] =  array();
            }
            
             $listjobData['user_name'] = '';
             $getthreadId = DB::table('job_messages')->select('thread_id')
                                                    ->where('job_id', $Params['job_id'])
                                                   // ->where('sender_id', $Params['user_id'])
                                                    ->whereRaw('(receiver_id  = '. $Params['user_id'].' or  sender_id = '.$Params['user_id'].')')
                                                    ->first();
//            print_r($listjobData);die;
           
            if(isset($Params['user_type']) && $Params['user_type'] == 1){
                
                    $listjobData['user_name'] = $listjobData['questionar_name'];
                
                    if($listjobData['job_status'] != 1 && $listjobData['job_status'] != 2){
                         if(isset($getthreadId->thread_id) && $getthreadId->thread_id !=''){
                                $listjobData['msg_thread_id'] = $getthreadId->thread_id;
                         }
                        $getProfilePic =  Apiusers::GetProfile($listjobData['agent_id']);
                        $listjobData['msg_user_profile_url'] = $getProfilePic['user_data']['profile_pic'];
                        $listjobData['msg_user_id'] = $listjobData['agent_id'];
                        $listjobData['msg_user_name'] = $getProfilePic['user_data']['name'];
                        
                    }
            }
            
            if(isset($Params['user_type']) && $Params['user_type'] == 2){
                    
                    $getProfilePic =  Apiusers::GetProfile($listjobData['quser_id']);
                    $checkAgentJobStatus = self::CheckAgentJobStatus($Params['job_id'], $Params['user_id']);
                  
                   if($checkAgentJobStatus){
                             
                            if(isset($getthreadId->thread_id) && $getthreadId->thread_id !=''){
                                $listjobData['msg_thread_id'] = $getthreadId->thread_id;
                            }
                            
                            $listjobData['job_status'] = JOB_STATUS_APPLIED;
                            $listjobData['msg_user_profile_url'] = $getProfilePic['user_data']['profile_pic'];
                            $listjobData['msg_user_id'] = $listjobData['quser_id'];
                            $listjobData['msg_user_name'] = $getProfilePic['user_data']['name'];
                    }
                
                    $listjobData['user_name'] = $listjobData['questionar_name'];
                    if($listjobData['job_status'] != 1 && $listjobData['job_status'] != 2){
                        
                        if(isset($getthreadId->thread_id) && $getthreadId->thread_id !=''){
                                $listjobData['msg_thread_id'] = $getthreadId->thread_id;
                            }
                       
                        $listjobData['user_profile_picture'] = $getProfilePic['user_data']['profile_pic'];
                        $listjobData['msg_user_profile_url'] = $getProfilePic['user_data']['profile_pic'];
                        $listjobData['msg_user_id'] = $listjobData['quser_id'];
                        $listjobData['msg_user_name'] = $getProfilePic['user_data']['name'];
                        
                    }
            }
            
          //  $listjobData['user_id'] = $Params['user_id'];
       
            unset($listjobData['agent_name']);
            unset($listjobData['quser_id']);
            unset($listjobData['agent_id']);
            unset($listjobData['questionar_name']);
            $ReturnData['status'] = STATUS_TRUE;
            //$listjobData['jobapplicants_data'] = $applicantArray;
            $ReturnData['data'] = $listjobData;
            
            
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
          
        return $ReturnData;
        
    }
    
    ## jigs Virani 14 nov 2016.
    ## to get job details.
    public static function GetJobAgentDetails($Params){
        
        $ReturnData['success'] = STATUS_FALSE; 
        
        ## to get details of agent.
        if(isset($Params) && $Params !=''){
            
            $Querylistjobs = DB::table('job_invite as ji')
                ->select('fu.*')
                ->leftJoin('front_users as fu', 'fu.id', '=', 'ji.user_id')
                ->where(array('ji.status'=> 3,'ji.job_id'=> $Params))
                ->first();
            
            $listjobData = json_decode(json_encode($Querylistjobs), true);
            
            $ReturnData['success'] = STATUS_TRUE; 
            $ReturnData['data'] = $listjobData;
        }
        
        return $ReturnData; 
    }
    
    public static function GetJobApplicants($Params){
        
        $ReturnData['success'] = STATUS_FALSE; 
        
        ## to get details of agent.
        if(isset($Params) && $Params !=''){
            
            $Querylistjobs = DB::table('job_invite as ji')
                ->select('fu.name', 'profile_pic','agent_ratings', 'ji.status')
                ->leftJoin('front_users as fu', 'fu.id', '=', 'ji.user_id')
                ->where(array('ji.job_id'=> $Params))
                ->get();
            
            $listjobData = json_decode(json_encode($Querylistjobs), true);
          
            foreach($listjobData as $keys => $vals){
                  $listjobData[$keys]['agent_ratings'] = (int)  $listjobData[$keys]['agent_ratings'];
                if(!filter_var($listjobData[$keys]['profile_pic'], FILTER_VALIDATE_URL) === false){
                    $listjobData[$keys]['profile_pic'] = $listjobData[$keys]['profile_pic'];
                } else {
                     $listjobData[$keys]['profile_pic'] = Apiusers::profileimgexists($listjobData[$keys]['profile_pic']);
                }
            }
            
            $ReturnData['success'] = STATUS_TRUE; 
            $ReturnData['data'] = $listjobData;
        }
         return $ReturnData; 
    }
    
    ## Hardika Satasiya 16 nov 2016.
    ## To Get applicants list of all active jobs.
    
    public static function Getapplicantslist($params){
        
        $ReturnData = array();
        extract($params);
        ##fetching records of all paused records 
        $QueryListApplicants = DB::table('jobs as j')
                ->select('j.job_id','fu.name as user_name','ji.user_id as applicant_id','ji.status as applicant_status','fu.agent_ratings as reviews','fu.profile_pic')
                ->leftJoin('job_invite as ji', 'j.job_id', '=', 'ji.job_id')
                ->leftJoin('front_users as fu', 'ji.user_id', '=', 'fu.id')
                ->Where(array('j.job_id'=> $job_id,'j.job_status'=> $job_status))
                ->Where('ji.status', '!=', 2)
                ->get();
      
         ## records in json array
        $listjobData = json_decode(json_encode($QueryListApplicants), true);
        
         foreach($listjobData as $keys => $vals){
                  $listjobData[$keys]['reviews']  = (int) $listjobData[$keys]['reviews'];
                if(!filter_var($listjobData[$keys]['profile_pic'], FILTER_VALIDATE_URL) === false){
                    $listjobData[$keys]['profile_pic'] = $listjobData[$keys]['profile_pic'];
                } else {
                     $listjobData[$keys]['profile_pic'] = Apiusers::profileimgexists($listjobData[$keys]['profile_pic']);
                }
            }
        if ($QueryListApplicants) {
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['jobapplicants_data'] = $listjobData;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
        return $ReturnData;
    }
    
    ## Hardika Satasiya 16 nov 2016.
    ## To applicants request aprroval for jobs
    
    public static function ApplicantAction($params){
        
        $ReturnData = array();
        
        extract($params);
     
        if(isset($applicants_action) && $applicants_action != '')
        {
            if($applicants_action == AWARDED){
                
                ## check wether already applied or not.
                $checkJobStatusInvite = self::checkJobAppliedStatus($job_id,$applicant_id);
               
                if($checkJobStatusInvite == STATUS_FALSE){
                    
                    $UpdateArray = array('status' => 3);
                    $UpdateJobArray = array('job_status' => 3, 'updated_on' => date('Y-m-d H:i:s'));
                    $ResultUpdate = DB::table('job_invite')->where(array('job_id' => $job_id,'user_id'=> $applicant_id))->update($UpdateArray);
                  
                    ##if job has been awarded then remove all the messages from job messages except this user.
                    $deleteRequestApplicants = DB::table('job_messages')->where('job_id', $job_id)
                                                                    ->where('receiver_id','!=',$applicant_id)
                                                                    ->where('sender_id', '!=', $applicant_id)
                                                                    ->delete();
                                           
                    $UpdateJobStatus = DB::table('jobs')->where('job_id',$job_id)->update($UpdateJobArray);
                    $ReturnData['message'] = trans('messages.JOB_AWARDED_SUCCESSFULLY');
                    ##send push notifictions.
                    if(isset($os_type) && $os_type == OS_TYPE_IOS){
                        $GetTokensList = Apiusers::GetUsersDeviceTokens($applicant_id, OS_TYPE_IOS);

                        if($GetTokensList){
                            $message = array('message' => 'Congratulations!! Job "#'.$job_id.'" awarded to you.','user_id'=> $applicant_id, 'user_type' => 2, 'job_id' => $job_id);
                            foreach($GetTokensList as $toke){
                                    $sendPushIos = Apiusers::SendIospushnotifications($message, $toke['device_token']);
                             }
                        }
                    }
                } else {
                      $ResultUpdate = false;
                      $ReturnData['message'] = trans('messages.JOB_ALREADY_APPLIED');
                      $ReturnData['status'] = STATUS_FALSE;
                }
            }
            else if($applicants_action == DECLINE )
            {
                $UpdateArray = array('status' => 2);
                $ReturnData['message'] = trans('messages.JOB_DECLINED_PROPOSAL');
                $ResultUpdate = DB::table('job_invite')->where(array('job_id' => $job_id,'user_id'=> $applicant_id))->update($UpdateArray);
                 
                 $decrementCount = DB::table('jobs')->where(array('job_id' => $job_id))
                        ->update( array(
                        'applicant' => DB::raw( 'applicant - 1' )
                    ));
                    
                    ##send push notifictions.
                    if(isset($os_type) && $os_type == OS_TYPE_IOS){
                        $GetTokensList = Apiusers::GetUsersDeviceTokens($applicant_id, OS_TYPE_IOS);

                        if($GetTokensList){
                            $message = array('message' => 'You proposal on job #"'.$job_id.'" has been declined.','user_id'=> $applicant_id, 'user_type' => 2, 'job_id' => $job_id);
                            foreach($GetTokensList as $toke){
                                    $sendPushIos = Apiusers::SendIospushnotifications($message, $toke['device_token']);
                             }
                        }
                    }
            }
            
            if ($ResultUpdate) {
                
                $ReturnData['status'] = STATUS_TRUE;
                $ReturnData['applicantsupdate_status'] = $ResultUpdate;
                
            } else {
                $ReturnData['status'] = STATUS_FALSE;
            }
        } else{
            
             $ReturnData['message'] = GENERAL_NO_CHANGES;
             $ReturnData['status'] = STATUS_FALSE;
        
        }
        
     return $ReturnData;
    }
    
    ## Hardika Satasiya 16 nov 2016.
    ## To update job status as per progress.
    
    public static function UpdateJobstatus($params){
        
        $ReturnData = array();
        $UpdateArray = array();
        $ResultUpdate = STATUS_FALSE;
       
        $checkInviteFlag = STATUS_FALSE;
        $checkVoteFlag = STATUS_TRUE;
        extract($params);
        
        $queryconfirm =DB::table('jobs as j')
                 ->leftJoin('job_invite as ji', 'j.job_id', '=', 'ji.job_id')
                 ->select('ji.job_id','ji.is_agent_confirm','ji.is_questioner_confirm')
                 ->where(function($queryconfirm) use ($user_type,$user_id,$job_id,$request_job_status) {
                     if ($user_type ==  1):
                             $queryconfirm->where('j.user_id', $user_id);
                             $queryconfirm->where('j.job_id', $job_id);
                            if($request_job_status == JOB_STATUS_COMPLETED)
                             {
                                $queryconfirm->where('j.job_status', JOB_STATUS_INPROCESS);
                             } else
                             {
                                 $queryconfirm->where('j.job_status', $request_job_status);
                             }
                             //$queryconfirm->where('ji.is_questioner_confirm', 1);
                        endif;
                       if ($user_type ==  2):
                             $queryconfirm->where('ji.user_id', $user_id);
                             $queryconfirm->where('ji.job_id', $job_id);
                             if($request_job_status == JOB_STATUS_COMPLETED)
                             {
                                 $queryconfirm->where('j.job_status', JOB_STATUS_INPROCESS);
                             } else
                             {
                                 $queryconfirm->where('j.job_status', $request_job_status);
                             }
                            // $queryconfirm->where('ji.is_agent_confirm', 1);
                       endif;
                  })
                ->Where('ji.status', 3)
                ->first();
        
    
        ##first check already did that or not.
        if(isset($queryconfirm->is_questioner_confirm) && $queryconfirm->is_questioner_confirm == 1 && $user_type == 1){
          
            $checkVoteFlag = STATUS_FALSE;
            $ResultUpdate = STATUS_FALSE;
            $ReturnData['status'] = STATUS_FALSE;
            $ReturnData['message'] = trans('messages.JOB_STATUS_ALREADY_APPLIED');
        }
        
        if(isset($queryconfirm->is_agent_confirm) && $queryconfirm->is_agent_confirm == 1 && $user_type == 2  && $request_job_status) {
             $checkVoteFlag = STATUS_FALSE;
            $ResultUpdate = STATUS_FALSE;
             $ReturnData['status'] = STATUS_FALSE;
             $ReturnData['message'] = trans('messages.JOB_STATUS_ALREADY_APPLIED');
        }
        
          
        ############################################################################################
        
            ## change status to paused.
           if(isset($request_job_status) && $request_job_status == JOB_STATUS_PAUSED && $checkVoteFlag == STATUS_TRUE) {
             
                 $ResultUpdate = STATUS_FALSE;
                 $PausedJob = DB::table('jobs')->where('job_id', $job_id)->update(array('job_status' => JOB_STATUS_PAUSED, 'updated_on' => date('Y-m-d H:i:s')));
                
                if($PausedJob){
                            
                            ## Send notification to both user.
                            if ($user_type == 1) { 
                                $UpdateArray = array('is_questioner_confirm' => 1,'job_status'=> JOB_STATUS_PAUSED); 
                            }
                            if ($user_type == 2) {
                                $UpdateArray = array('is_agent_confirm' => 1 ,'job_status'=> JOB_STATUS_PAUSED); 
                            }

                           $ResultUpdate = DB::table('job_invite')
                                            ->where('job_id', $job_id)
                                            ->update($UpdateArray);
                            
                           $ResultUpdatePaused = DB::table('jobs')->where(array('job_id' => $job_id))
                                            ->update( array(
                                            'dispute_count' => DB::raw( 'dispute_count + 1' )
                            ));
                        
                 }
                
                 $getcount = DB::table('jobs')->select('dispute_count')->where('job_id',$job_id)->first();
               
                 if(isset($getcount->dispute_count) && $getcount->dispute_count >= 2){
                     
                      $ReturnData['message'] = trans('messages.JOB_STATUS_DISPUT');
                      $ReturnData['status'] = STATUS_TRUE;
                     
                      $confirmupdate = DB::table('job_invite')->where('job_id', $Params['job_id'])
                                                              ->update(array('is_questioner_confirm' => 0,
                                                              'is_agent_confirm' => 0));
                     
                      $isDispute = DB::table('jobs')
                                    ->where(array('job_id' => $Params['job_id']))
                                    ->update(array('is_dispute' => 1));
                     
                     
                 } else {
                    
                      $ReturnData['message'] = trans('messages.JOB_STATUS_PAUSED');
                      $ReturnData['status'] = STATUS_TRUE;
                 
                 }
           }
           
           ## change status to resume.
           if(isset($request_job_status) && $request_job_status == JOB_STATUS_RESUME && $checkVoteFlag == STATUS_TRUE)
           {
              
              $checkPausedStatus = self::checkJobPausedStatus($job_id, $user_id, $user_type);
              if($checkPausedStatus){
              
                        $ResultUpdate = STATUS_FALSE;
                        $ResumeJob = DB::table('jobs')->where('job_id', $job_id)->update(array('job_status' => JOB_STATUS_INPROCESS));
              
                           $UpdateArray = array('is_questioner_confirm' => 0, 
                                          'is_agent_confirm' => 0,
                                            
                                          'job_status'=> 0); 
                        
                          $ResultUpdate = DB::table('job_invite')
                                            ->where('job_id', $job_id)
                                            ->update($UpdateArray);
                          ##if resume job send the notification.
                  
                           $ReturnData['message'] = trans('messages.JOB_STATUS_RESUME');
                           $ReturnData['status'] = STATUS_TRUE;
              
              
              } else {
                  
                     ##if not then it ill not authorized.
                     $ReturnData['status'] = STATUS_FALSE;
                     $ReturnData['message'] = trans('messages.NOT_AUTHORIZED_TO_RESUME');
                     $ResultUpdate = STATUS_FALSE;
              
              
              }
           }
        
        ############################################################################################
      
       if(!empty($queryconfirm) && $checkVoteFlag == STATUS_TRUE && $request_job_status != JOB_STATUS_RESUME && $request_job_status != JOB_STATUS_PAUSED){
         
           
          // if($request_job_status != JOB_STATUS_RESUME && $request_job_status != JOB_STATUS_PAUSED)
           //{ 
              // print_r($queryconfirm);die; 
              
                ## if user is questioner then complete the job.
//                if ($user_type == 1 && $request_job_status == JOB_STATUS_COMPLETED)
//                { 
//               
//                      $whereSecondArray = array('j.job_id' => $job_id,'j.user_id'=> $user_id);
//                    
//                      if(isset($reviews) && $reviews !=''){
//                            $UpdateArray = array('j.questionar_review' => (float) $reviews,'j.job_end_on' => date('Y-m-d H:i:s'), 'j.job_status' => JOB_STATUS_COMPLETED);
//                      } else { 
//                            $UpdateArray = array('j.job_end_on' => date('Y-m-d H:i:s'), 'j.job_status' => JOB_STATUS_COMPLETED);
//                      }
//                     
//                      $ResultUpdate = DB::table('jobs as j')
//                                   ->where($whereSecondArray)
//                                   ->update($UpdateArray);
//                    
//                      $WhereStatusChange = array('job_id'=> $job_id, 'status' => 3);  
//                      $checkIt  = self::updateJobRequester($WhereStatusChange, 1);
//                    
//                      $CalculateAgentReviews = Apiusers::CalculateReviews($user_id, $job_id, $reviews, $user_type);
//                    
//                    $checkInviteFlag = true;
//                    $ReturnData['status'] = STATUS_TRUE;
//                    $ReturnData['message'] = trans('messages.JOB_STATUS_COMPLETED');
//                    
//                } else {
//                     
//                     $ReturnData['status'] = STATUS_FALSE;
//                     $ReturnData['message'] = trans('messages.NOT_AUTHORIZED_TO_COMEPLETE');
//                     $ResultUpdate = STATUS_FALSE;
//                    
//                }
               
               #######################################################
               # This code is commenet for future enhancement in update job status.
               # please do not change @jigs virani.
               #######################################################
               
                ## When first request is come.
               if(isset($queryconfirm->is_questioner_confirm) && $queryconfirm->is_questioner_confirm  == 0 && $queryconfirm->is_agent_confirm  == 0 )
                {
                  if ($user_type == 1) { 
                      $UpdateArray = array('is_questioner_confirm' => 1 , 'job_status'=> JOB_STATUS_COMPLETED); 
                    }
                  else if ($user_type == 2) {
                      $UpdateArray = array('is_agent_confirm' => 1 , 'job_status'=> JOB_STATUS_COMPLETED); 
                    }
                 
                  $ResultUpdate = DB::table('job_invite')
                                    ->where('job_id', $job_id)
                                    ->update($UpdateArray);
                   
                    if($ResultUpdate)
                    {
                            
                                         $whereSecondArray = array('j.job_id' => $job_id);
                    
                                          if(isset($reviews) && $reviews !='' && $user_type == IS_REQUESTER){
                                                $UpdateArray = array('j.questionar_review' => (float) $reviews,'j.job_end_on' => date('Y-m-d H:i:s'));
                                          } 
                                         if(isset($reviews) && $reviews !='' && $user_type == IS_AGENT) { 
                                                $UpdateArray = array('j.agent_review'=> (float) $reviews,'j.job_end_on' => date('Y-m-d H:i:s'));
                                          }

                                          $ResultReviewUpdate = DB::table('jobs as j')
                                                       ->where($whereSecondArray)
                                                       ->update($UpdateArray);
                        
                    }
                   
                    $checkInviteFlag = true;
                    $ResultUpdate = STATUS_TRUE;
                    $ReturnData['status'] = STATUS_TRUE;
                    $ReturnData['message'] = trans('messages.JOB_STATUS_COMPLETED');
                }

                #request coming from questioner.
                else if(isset($queryconfirm->is_questioner_confirm) && $queryconfirm->is_questioner_confirm == 1 && $queryconfirm->is_agent_confirm == 0){

                   $UpdateArray = array('ji.is_agent_confirm' => 1 );
                   
                   if(isset($request_job_status) && $request_job_status != '')
                    {
                        if($request_job_status == JOB_STATUS_COMPLETED )
                            {
                                $UpdateArray = array_merge($UpdateArray,array('j.job_status' => JOB_STATUS_COMPLETED)); 
                            }
                    }
                   
                    $whereSecondArray = array('j.job_id' => $job_id,'j.user_id'=>$user_id);
                    $whereThirdArray = array('j.job_id' => $job_id,'ji.user_id'=>$user_id);

                    $ResultUpdate = DB::table('jobs as j')
                                   ->leftJoin('job_invite as ji', 'j.job_id', '=', 'ji.job_id')
                                   ->where($whereSecondArray)
                                   ->orWhere($whereThirdArray)
                                   ->update($UpdateArray);
                    
                    if($ResultUpdate){
                                        $whereSecondArray = array('j.job_id' => $job_id);
                    
                                          if(isset($reviews) && $reviews !='' && $user_type == IS_REQUESTER){
                                                $UpdateArray = array('j.questionar_review' => (float) $reviews, 'j.job_end_on' => date('Y-m-d H:i:s'), 'j.job_status'=> JOB_STATUS_COMPLETED);
                                          } 
                                         if(isset($reviews) && $reviews !='' && $user_type == IS_AGENT) { 
                                                $UpdateArray = array('j.agent_review'=> (float) $reviews, 'j.job_end_on' => date('Y-m-d H:i:s'), 'j.job_status'=> JOB_STATUS_COMPLETED);
                                          }

                                          $ResultReviewUpdate = DB::table('jobs as j')
                                                       ->where($whereSecondArray)
                                                       ->update($UpdateArray);
                        
                                     $UpdateArray = array('is_questioner_confirm' => 0, 
                                                      'is_agent_confirm' => 0,
                                                      'job_status'=> 0); 

                                      $ResultUpdateReset = DB::table('job_invite')
                                                        ->where('job_id', $job_id)
                                                        ->update($UpdateArray);
                    }
                   

                    $checkInviteFlag = true;
                    $ResultUpdate = STATUS_TRUE;
                    $ReturnData['status'] = STATUS_TRUE;
                    $ReturnData['message'] = trans('messages.JOB_STATUS_COMPLETED');

                }
           
                # request coming from agent.
                else if(isset($queryconfirm->is_questioner_confirm) && $queryconfirm->is_questioner_confirm == 0 && $queryconfirm->is_agent_confirm == 1 ){

                        $UpdateArray = array('ji.is_questioner_confirm' => 1 );
                        
                        if(isset($request_job_status) && $request_job_status != '')
                        {
                            if($request_job_status == JOB_STATUS_COMPLETED )
                            {
                                $UpdateArray = array_merge($UpdateArray,array('j.job_status' => JOB_STATUS_COMPLETED)); 
                            }
                        }
                         $whereFourthArray = array('j.job_id' => $job_id,'j.user_id'=>$user_id);
                         $whereFifthArray = array('j.job_id' => $job_id,'ji.user_id'=>$user_id);

                        $ResultUpdate = DB::table('jobs as j')
                                       ->leftJoin('job_invite as ji', 'j.job_id', '=', 'ji.job_id')
                                       ->where($whereFourthArray)
                                       ->orWhere($whereFifthArray)
                                       ->update($UpdateArray);
                    
                        if($ResultUpdate)
                        {
                            
                                         $whereSecondArray = array('j.job_id' => $job_id);
                    
                                           if(isset($reviews) && $reviews !='' && $user_type == IS_REQUESTER){
                                                $UpdateArray = array('j.questionar_review' => (float) $reviews,'j.job_end_on' => date('Y-m-d H:i:s'), 'j.job_status'=> JOB_STATUS_COMPLETED);
                                          } 
                                         if(isset($reviews) && $reviews !='' && $user_type == IS_AGENT) { 
                                                $UpdateArray = array('j.agent_review'=> (float) $reviews,'j.job_end_on' => date('Y-m-d H:i:s'), 'j.job_status'=> JOB_STATUS_COMPLETED);
                                          }

                                          $ResultReviewUpdate = DB::table('jobs as j')
                                                       ->where($whereSecondArray)
                                                       ->update($UpdateArray);
                        
                                     $UpdateArray = array('is_questioner_confirm' => 0, 
                                                      'is_agent_confirm' => 0,
                                                      'job_status'=> 0); 

                                      $ResultUpdateReset = DB::table('job_invite')
                                                        ->where('job_id', $job_id)
                                                        ->update($UpdateArray);
                        }
                        
                        $checkInviteFlag = true;
                        $ResultUpdate = STATUS_TRUE;
                        $ReturnData['status'] = STATUS_TRUE;
                        $ReturnData['message'] = trans('messages.JOB_STATUS_COMPLETED');
                    }
//           } else {
//              
//                 $ResultUpdate = STATUS_FALSE;
//                 $ReturnData['message'] = trans('messages.GENERAL_ERROR');
//                 $ReturnData['status'] = STATUS_FALSE;
//           }
       } 
//           else {  
//                
//                $ResultUpdate = STATUS_FALSE;
//                $ReturnData['message'] = trans('messages.JOB_STATUS_ALREADY_APPLIED');
//                $ReturnData['status'] = STATUS_FALSE;
//       
//       }
        
        if($ResultUpdate) {
            if($checkInviteFlag){ 
                $ReturnData['message'] = trans('messages.JOB_STATUS_COMPLETED');
            }
            
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['update_Jobstatus'] = $ResultUpdate;
            
        }
        else {
            
            $ReturnData['message'] = GENERAL_ERROR;
            $ReturnData['status'] = STATUS_FALSE;
        }
        
        return $ReturnData;
    }
    
    ## To add new images in jobs files.
     public static function AddPostImages($Params){
    
        $ReturnData['status'] =  STATUS_FALSE;
        
        if(isset($Params) && count($Params) > 0){
            
            $updateString  = '{';
            
            foreach($Params['job_files'] as $keys=> $vals){
                
                if($keys == 0){
                 $updateString  .= $vals;
                } else{
                
                 $updateString  .= ','.$vals;
                }
            }
            
            $updateString .= '}';
            $updateArray = array('files' => $updateString);
            
            $upateJobsImages = DB::table('jobs')->where('job_id', $Params['job_id'])->update($updateArray);
            $ReturnData['status'] =  STATUS_TRUE;
        
        }
        return $ReturnData;
    }
    
    ## Jigs Virani 16 nov 2016
    ## To get ratings.
    public static function GetRatings($Params){
        
        $ReturnData['status'] =  STATUS_FALSE;
        $ReturnData['data'] = array();
        
        $profile_reviews = DB::table('front_users')->select('questioner_ratings', 'agent_ratings')->where('id',$Params['user_id'])->first();
        
      
        ## For questioner ratings.
        if(isset($Params['user_type']) && $Params['user_type'] == 1){
            
               $getAverageReview = DB::table('jobs')->select(DB::raw('FORMAT((SUM(agent_review)/COUNT(job_id) ), 1) questioner_ratings'))
                ->where('user_id', $Params['user_id'])
                ->where('job_status', JOB_STATUS_COMPLETED)
                ->first();
            
                $ReturnData['data']['average_ratings']  = (float) $getAverageReview->questioner_ratings;
                ##fetching records.
                $QueryListApplicants = DB::table('jobs as j')
                    ->select('j.job_title', 'j.job_id', 'j.agent_review as reviews',
                             'job_end_on',
                             db::raw('(select questioner_ratings from front_users where id = j.user_id) as average'),
                             db::raw('(select name from front_users where id = ji.user_id) as user_name')
                            )
                    ->leftJoin('job_invite as ji', 'j.job_id', '=', 'ji.job_id')
                    ->Where(array('j.user_id'=> $Params['user_id'],'j.job_status'=> JOB_STATUS_COMPLETED))
                    ->Where('ji.status', 3)
                    ->get();
            
        }
        
        ## For Agent ratings.
        if(isset($Params['user_type']) && $Params['user_type'] == 2){
            
            $getAverageReview = DB::table('jobs as j')
                ->select(DB::raw('FORMAT((SUM(j.questionar_review)/COUNT(j.job_id) ), 1) agent_ratings'))
                 ->leftJoin('job_invite as ji', 'j.job_id', '=', 'ji.job_id')
                ->where('ji.user_id', $Params['user_id'])
                ->where('j.job_status', JOB_STATUS_COMPLETED)
                ->first();
            
            $ReturnData['data']['average_ratings']  = (float) $getAverageReview->agent_ratings;
            ##fetching records.
            $QueryListApplicants = DB::table('jobs as j')
                    ->select('j.job_title', 'j.job_id', 'j.questionar_review as reviews',
                              'job_end_on',
                               db::raw('(select agent_ratings from front_users where id = ji.user_id) as average'),
                               db::raw('(select name from front_users where id = j.user_id) as user_name'))
                    ->leftJoin('job_invite as ji', 'j.job_id', '=', 'ji.job_id')
                    ->leftJoin('front_users as fu', 'ji.user_id', '=', 'fu.id')
                    ->Where(array('ji.user_id'=> $Params['user_id'],'j.job_status'=> JOB_STATUS_COMPLETED))
                    ->Where('ji.status', 3)
                    ->get();
        }
        
        ## records in json array.
        $listjobData = json_decode(json_encode($QueryListApplicants), true);
        
        if($profile_reviews){
           
                 if($listjobData){

                     foreach($listjobData as $keys => $vals){

                        $listjobData[$keys]['reviews'] = (float) $vals['reviews'];
                         unset( $listjobData[$keys]['average']);
                    }
                 }
            
              $ReturnData['data']['job_ratings'] = $listjobData;
              $ReturnData['status'] =  STATUS_TRUE;
        }
        
        return $ReturnData;
    }
    
    ## Jigs Virani 18 Nov 2016
    ## Get requester and agent jobs profiles.
    
    public static function GetProfilesofagentrequester($Params){
    
          
        $ReturnData['status'] =  STATUS_FALSE;
        $listjobData = array();
        
        
        ## Front user details.
        $GetUserProfile = DB::table('front_users')
                                ->select('name', DB::raw('IFNULL(expertise_id, 0) as category_id'), 'agent_ratings','profile_pic','questioner_ratings', db::raw('IFNULL(bio, "") as bio'), db::raw('IFNULL(bio_files,"") as user_doc'))
                                ->where('id',$Params['user_id'])
                                ->first();
       
        ## For questioner job profile.
        if(isset($Params['user_type']) && $Params['user_type'] == 1){
        
            ##fetching records.
            $QueryListApplicants = DB::table('jobs as j')
                ->select('j.job_title', 'j.job_id', 'j.questionar_review as reviews','jc.category_name',
                         'job_end_on','j.agent_commision',
                         db::raw('(select name from front_users where id = ji.user_id) as user_name')
                        )
                ->leftJoin('job_invite as ji', 'j.job_id', '=', 'ji.job_id')
                ->leftJoin('job_categories as jc', 'jc.category_id', '=', 'j.category_id')
                ->Where(array('j.user_id'=> $Params['user_id'],'j.job_status'=> JOB_STATUS_COMPLETED))
                ->Where('ji.status', 3)
                ->get();
            
        }
        
        ## For Agent profile.
        if(isset($Params['user_type']) && $Params['user_type'] == 2){
        
            ##fetching records.
            $QueryListApplicants = DB::table('job_invite as ji')
                    ->select('j.job_title', 'j.job_id', 'j.agent_review as reviews','jc.category_name',
                              'job_end_on','j.agent_commision',
                             db::raw('(select name from front_users where id = j.user_id) as user_name'))
                    ->leftJoin('jobs as j', 'j.job_id', '=', 'ji.job_id')
                    ->leftJoin('job_categories as jc', 'jc.category_id', '=', 'j.category_id')
                    ->leftJoin('front_users as fu', 'ji.user_id', '=', 'fu.id')
                    ->Where(array('ji.user_id'=> $Params['user_id'],'j.job_status'=> JOB_STATUS_COMPLETED))
                    ->Where('ji.status', 3)
                    ->get();
            
        }
        
         ## records in json array.
         $listjobData = json_decode(json_encode($QueryListApplicants), true);
         $GetUserProfile = json_decode(json_encode($GetUserProfile), true);
        
        if($GetUserProfile){
            
                if($listjobData){
                        $GetUserProfile['completed_jobs'] = count($listjobData);
                }else {
                        $GetUserProfile['completed_jobs'] = (int) 0;
                }
               // print_r($listjobData);die;
                if(!filter_var($GetUserProfile['profile_pic'], FILTER_VALIDATE_URL) === false){
                        $GetUserProfile['profile_pic'] = $GetUserProfile['profile_pic'];
                } else {
                       $GetUserProfile['profile_pic'] = Apiusers::profileimgexists($GetUserProfile['profile_pic']);
                }
            
              ## if user is requester then.
              if(isset($Params['user_type']) && $Params['user_type'] == 1){
                  
                  $GetUserProfile['reviews'] = (int) $GetUserProfile['questioner_ratings'];
                  unset($GetUserProfile['user_doc']);
                  unset($GetUserProfile['expertise_id']);
                  unset($GetUserProfile['questioner_ratings']);
                  unset($GetUserProfile['agent_ratings']);
                  
              }
              
              ## if user is agent then.
              if(isset($Params['user_type']) && $Params['user_type'] == 2){
                 
                  $GetUserProfile['reviews'] = (int) $GetUserProfile['agent_ratings'];
                  
                  unset($GetUserProfile['agent_ratings']);
                  unset($GetUserProfile['questioner_ratings']);
                
                    if($GetUserProfile['user_doc'] !=''){
                        $GetUserProfile['user_doc'] = Apiusers::profiledocumentsexists($GetUserProfile['user_doc']);
                    }
              }
               
                unset($GetUserProfile['agent_ratings']);
                unset($GetUserProfile['questioner_ratings']);
            
             $ReturnData['status'] =  STATUS_TRUE;
        }
            
        
            $ReturnData['data']['user_profile'] = $GetUserProfile;
            $ReturnData['data']['user_jobs'] = $listjobData;
            return $ReturnData;
    
    }
    
    ## check already applied or not.
    public static function checkJobAppliedStatus($job_id,$applicant_id){
    
         $QueryListApplicants = DB::table('job_invite as ji')
                    ->select('ji.job_id')->where('ji.job_id', $job_id)->where('ji.user_id', $applicant_id)->where('ji.status',3)->first();
        if($QueryListApplicants){
            return STATUS_TRUE;
        }else{
            return STATUS_FALSE;
        }
    }
    
    public static function checkJobProposalStatus($job_id,$applicant_id){
    
         $QueryListApplicants = DB::table('job_invite as ji')
                    ->select('ji.job_id')->where('ji.job_id', $job_id)->where('ji.user_id', $applicant_id)->where('ji.status',1)->first();
        if($QueryListApplicants){
            return STATUS_TRUE;
        }else{
            return STATUS_FALSE;
        }
    }
    
    
    ## Jigs Virani 18 Nov 2016.
    ## To send proposal.
    public static function SendProposal($Params){
    
        $ReturnData['status'] =  STATUS_FALSE;
        
            $checkSendProposal = self::checkJobProposalStatus($Params['job_id'], $Params['user_id']);
        
            if($checkSendProposal == STATUS_FALSE){

                $InsertproposalArray = array('user_id' => $Params['user_id'],
                                        'is_questioner_confirm' => 0,
                                         'is_agent_confirm' => 0,
                                         'proposal_details'=> $Params['proposal_details'],
                                         'job_id' => $Params['job_id'],
                                         'created_on' => date('Y-m-d H:i:s'),
                                         'status'=> 1);

            $SendProposalId = DB::table('job_invite')->insertGetId($InsertproposalArray);

            ## To insert data.
            if($SendProposalId){
                
                $getRequesterId = db::table('jobs')->select('user_id')->where('job_id', $Params['job_id'])->first();
                               
                    $thread_id = str_random(7);
                    //Insert into job messages.
                    $jobMesaages = array('job_id'=> $Params['job_id'], 
                                         'sender_id'=> $Params['user_id'], 
                                         'receiver_id'=> isset($getRequesterId->user_id) ? $getRequesterId->user_id : 0, 
                                         'thread_id'=> $thread_id, 
                                         'message_type'=> 0,
                                         'user_type' => IS_AGENT,
                                         'text'=> $Params['proposal_details'],
                                         'read_flag'=> 0,
                                         'created_date'=> date('Y-m-d H:i:s'));
                
                   $SendMessage = DB::table('job_messages')->insertGetId($jobMesaages);

                    DB::table('jobs')->where(array('job_id' => $Params['job_id']))
                        ->update( array(
                        'applicant' => DB::raw( 'applicant + 1' )
                    ));
                    
                $getprofile = Apiusers::GetProfile($getRequesterId->user_id);
              ///  print_r($getprofile);
//                $data = '';
                
                $data = array('msg_user_profile_url'=> $getprofile['user_data']['profile_pic'],
                              'msg_user_id'=> $getprofile['user_data']['user_id'],
                              'msg_thread_id'=> $thread_id,
                              'msg_user_name'=> $getprofile['user_data']['name']);
                
                 $ReturnData['message'] = trans('messages.APPLIED_SUCCESS');
                 $ReturnData['data'] = $data;
                 $ReturnData['status'] =  STATUS_TRUE;
            }else{
                 $ReturnData['message'] =   trans('messages.GENERAL_ERROR');
                 $ReturnData['status'] =  STATUS_FALSE;
            }
                
        }else{
                
             $ReturnData['message'] =  trans('messages.ALREADY_SEND_PROPOSAL');
             $ReturnData['status'] =  STATUS_FALSE;
        }
        
        return $ReturnData;
    }
    
    public static function checkJobStatus($job_id){
        
        $checkJobId = DB::table('jobs')->select('job_id')->where('status', 1)->where('job_id', $job_id)->first();
        if($checkJobId){
            return STATUS_TRUE;
        }else{
            return STATUS_FALSE;
        }
    }
    
    public static function Listagentreviews($Params){
    
           $ReturnData = array();
            
            $getReviewListObj = DB::table('jobs as j')
                    ->select('j.job_title','j.job_id','ji.user_id',
                             db::raw('(select name from front_users where id = j.user_id) as user_name'))
                    ->leftJoin('job_invite as ji', 'j.job_id', '=', 'ji.job_id')
                    ->leftJoin('front_users as fu', 'ji.user_id', '=', 'fu.id')
                    ->Where(array('ji.user_id'=> $Params['user_id'],'j.job_status'=> JOB_STATUS_COMPLETED))
                    ->Where('j.agent_review', 0)
                    ->get();
            
        $getReviewListArray = json_decode(json_encode($getReviewListObj), true);
            if($getReviewListArray){
                 $ReturnData['data'] = $getReviewListArray;
                 $ReturnData['status'] = STATUS_TRUE;
            } else{
                $ReturnData['status'] = STATUS_FALSE;
            }
        
        return $ReturnData;
    
    }
    
    public static function Agentjobreviews($Params){
    
         $ReturnData = array();
         $ReturnData['status'] = STATUS_FALSE;
        extract($Params);
        
        ##check if job is completed from requester side.
        $queryconfirm =DB::table('jobs as j')
                ->leftJoin('job_invite as ji', 'j.job_id', '=', 'ji.job_id')
                 ->select('ji.job_id','ji.is_agent_confirm','ji.is_questioner_confirm')
                ->where('ji.user_id', $user_id)
                ->where('j.job_id', $job_id)
                ->where('ji.is_questioner_confirm', 1)
//                ->where(function($queryconfirm) use ($user_type,$user_id,$job_id) {
//                   
//                             $queryconfirm->where('ji.user_id', $user_id);
//                             $queryconfirm->where('j.job_id', $job_id);
//                             $queryconfirm->where('ji.is_questioner_confirm', 1);
//                      
//                })
                ->Where('ji.status', 3)
                ->first();
      //rint_r($queryconfirm);die;
        ##first check already did that or not.
        if(isset($queryconfirm->is_questioner_confirm) && $queryconfirm->is_questioner_confirm == 1){
          
            $UpdateArray = array('agent_review'=> (float) $reviews);
            $updateStatus = DB::table('jobs')->where('job_id',$job_id)->update($UpdateArray);
            
            if($updateStatus){
                
                  ## calculate average reviews.
                  $CalculateRequesterReviews = Apiusers::CalculateReviews($user_id,$job_id, $reviews, $user_type);
                
                 $ReturnData['status'] = STATUS_TRUE;
                 $ReturnData['message'] = trans('messages.JOB_STATUS_COMPLETED');
                
            }else {
                
                 $ReturnData['status'] = STATUS_TRUE;
                 $ReturnData['message'] = trans('messages.JOB_STATUS_COMPLETED');
            }
        } else{
            
            $ReturnData['status'] = STATUS_FALSE;
            $ReturnData['message'] = trans('messages.JOB_NOT_COMPLETED_YET');
        }
        
     return $ReturnData;
    
    }
    
    ##Jigs virani 23 Nov 2016
    ## To update job requester status.
    public static function updateJobRequester($array, $user_type){
        
        if($user_type == 1){
            $updateArray = array('is_questioner_confirm' => 1);
        }else{
             $updateArray = array('is_agent_confirm' => 1);
        }
       
        DB::table('job_invite')
            ->where('job_id', '=', $array['job_id'])
            ->where('status', '=', $array['status'])
            ->update($updateArray);
        
        return true;
    
    }
    
    ## Jigs virani 23 Nov 2016
    ##  To check agent status on perticular job 
    public static function CheckAgentJobStatus($job_id, $user_id){
    
        $checkStatus = DB::table('job_invite as ji')
                                ->select('*')
                                ->leftJoin('jobs AS j', 'j.job_id', '=', 'ji.job_id')
                                ->where('j.job_status', JOB_STATUS_OPEN)
                                 ->where('ji.user_id', $user_id)
                                ->where('ji.job_id', $job_id)
                                ->where('ji.status', 1)
                                ->where('j.status', 1)
                                ->first();
        
        if($checkStatus){
            return true;
        }else{
            return false;
        }
    }
    
    ## To changes in job paused status.
    public static function checkJobPausedStatus($job_id, $user_id, $user_type){
    
        if($user_type == 1){
                $wherearray = array('is_questioner_confirm'=> 1);
        }else{
                $wherearray = array('is_agent_confirm' => 1);
        }

        $checkPaused = DB::table('job_invite')
                            ->where('job_id', $job_id)
                            ->where('status', 3)
                            ->where('job_status', JOB_STATUS_PAUSED)
                            ->where($wherearray)
                            ->first();
        
        if($checkPaused){
            return true;
        }else{
            return false;
        }
        
    }
    
    ## To get unread messages.
    public static function getjobsunreadmessages($job_id,$user_id){
       
        $getCountOfUnreadMessage = DB::table('job_messages')
                                ->select(DB::raw('count(message_id) as total_unread'))
                                ->where('receiver_id', $user_id)
                                ->where('job_id', $job_id)
                                ->whereRaw('read_flag != 2')
                                ->first();
        
        
        
        if($getCountOfUnreadMessage){
                return $getCountOfUnreadMessage->total_unread;
        } else{
                return 0;
        }
    }
}
