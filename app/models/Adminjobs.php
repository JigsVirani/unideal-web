<?php

## Jigs Virani 19 th Oct 2016
## Admin panel jobs model.

class Adminjobs extends Eloquent {
    
    ## Jigs Virani
    ## To get all the categories of jobs.
    public static function getalljobscategories() {
        
        $cate_obj = DB::table('job_categories')->select('*')
                ->where('status', 1)
                ->get();

        $cate_array = json_decode(json_encode($cate_obj), true);

        if ($cate_array) {
            return $cate_array;
        } else {
            return false;
        }
    
    }
    
    public static function countdispute() {
        
        $cate_obj = DB::table('jobs')->select('*')
                ->where('is_dispute', 1)
                ->where('status', 1)
                ->count();

        $cate_array = json_decode(json_encode($cate_obj), true);

        if ($cate_array) {
            return $cate_array;
        } else {
            return false;
        }
    
    }
    
    ## Jigs Virani
    ## To get all the categories of jobs.
    public static function generatecsv($categories_id, $job_status) {
    
          $users_obj = DB::table('jobs AS jbs')->select('jbs.job_id', 'jbs.job_title','jc.category_name',
                                                  DB::raw('CONCAT(DATE_FORMAT(jbs.job_start_on,"%y-%m-%d"), DATE_FORMAT(jbs.job_start_on,"%r")) as job_start_on'), DB::raw('CONCAT(jbs.consignment_size," HK$") as consignment_size'), 
                                                  DB::raw('CASE WHEN jbs.job_status =  1 THEN "Open" WHEN jbs.job_status = 3 THEN "In Process" WHEN jbs.job_status = 5 THEN "Paused" WHEN jbs.job_status = 4 THEN "Complete" WHEN jbs.job_status = 7 THEN "Cancelled" END as job_status'))
                 ->leftJoin('job_categories AS jc', 'jbs.category_id', '=', 'jc.category_id')
                  ->where(function($query) use ($categories_id, $job_status) {
                            if ($categories_id != '' && $categories_id != '0'):
                                 $query->where('jbs.category_id','=',$categories_id);
                            endif;
                        if ($job_status != '' && $job_status != '0'):
                                 $query->where('jbs.job_status','=',$job_status);
                            endif;
                        })->get();
        
        if($users_obj){
                $users_array = json_decode(json_encode($users_obj), true);
        }else{ $users_array= false; }
        
       
        return $users_array;
    
    
    }
    
    ## Jigs Virani
    ## To get job details.
    
    public static function getjobsdetails($job_id) {
        
        $returndata = array();
        $returndata['success'] = false; 
        $job_id = base64_decode($job_id);
        
        if(isset($job_id) && $job_id !='') {
            
             $jobs_obj = DB::table('jobs AS jbs')
                                ->select('jbs.job_id', 'jbs.job_title','jc.category_name','jbs.job_details','jbinvite.user_id as agent_id','jbs.is_dispute',
                                DB::raw('jbs.job_status as job_class'),
                                DB::raw('IF(jbs.sub_category = "0", "N/A",jbs.sub_category) as sub_category'),
                                DB::raw('DATE_FORMAT(jbs.job_start_on, "%d %b %Y %h:%i %p") as job_start_on'),
                                DB::raw('DATE_FORMAT(jbs.job_end_on, "%d %b %Y %h:%i %p") as job_end_on'), 'jbs.files', 
                                DB::raw('(select name from front_users where id = jbs.user_id) as questionar'),
                                DB::raw('(select id from front_users where id = jbs.user_id) as questionar_id'),
                                DB::raw('(select name from front_users where id = jbinvite.user_id) as agent_name'),
                                DB::raw('(select profile_pic from front_users where id = jbinvite.user_id) as profile_image_agent'),
                                DB::raw('(select profile_pic from front_users where id = jbs.user_id) as profile_pic_requester'),
                                DB::raw('CONCAT(jbs.consignment_size," HK$") as consignment_size'), 
                                'jbs.agent_commision','jbs.job_status as main_status',
                                //DB::raw('DATE_FORMAT(jbs.job_end_on,"%y-%m-%d") as job_start_on'), 
                                DB::raw(' CASE WHEN jbs.job_status =  1 THEN "Open" WHEN jbs.job_status = 3 THEN "In Process" WHEN jbs.job_status = 5 THEN "Paused" WHEN jbs.job_status = 4 THEN "Complete" WHEN jbs.job_status = 7 THEN "Cancelled" END as job_status')
                                ,'jbs.applicant')
                                ->leftJoin('job_categories AS jc', 'jbs.category_id', '=', 'jc.category_id')
                                ->leftJoin('job_invite AS jbinvite', 'jbs.job_id', '=', 'jbinvite.job_id')
                                ->where('jbs.job_id',$job_id)
                                ->first();
                                if($jobs_obj)
                                {
                                    $jobs_obj->profile_image_agent = Users::ImageExist($jobs_obj->profile_image_agent);
                                    $jobs_obj->profile_pic_requester = Users::ImageExist($jobs_obj->profile_pic_requester);
                                }
                        $jobs_array = json_decode(json_encode($jobs_obj), true);
                if($jobs_array)
                 {    
                        $images = array();
                        if(isset($jobs_array['files']) && $jobs_array['files'] !=''){
                                
                                $files_array = explode(",", str_replace("}","",str_replace("{","",$jobs_array['files'])));
                                foreach($files_array as $keys => $vals){
                                       $images[]  = URL_JOBS_THUMBS_IMAGES.$vals;
                                }
                            
                               // print_r($images);die;
                                $jobs_array['images'] = $images;
                        }
                        $returndata['success'] = true; 
                        $returndata['data'] = $jobs_array;
                 }
        }
        return $returndata;
    }
}