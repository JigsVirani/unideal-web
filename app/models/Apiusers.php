<?php

/**
 * This file demonstrates User registration,login,forgot password,delete & reactive account.
 * @author Bhargav Bhanderi <bhargav@creolestudios.com>
 * @version 1.0
 * @package : Login,Register,Forgot Password,Re active & Delete account
 * @copyright  Creole Studios 2015
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 */
class Apiusers extends Eloquent {
    
    
    
    ###common utility functions
    #profileimgexists
    #profiledocumentsexists
    #profilethumbexists
    #ProcessToken
    #GetProfile
    #ProcessFirstLoginStatus
    ##SendIospushnotifications // To send push notification for ios.
    ##SendAndroidpushnotifications // To send push notification for android.
    
    //#################################################################
    // Name : profileimgexists
    // Purpose : To check for profile image
    // In Params : all user data
    // Out params : Re active user and send verification email
    //#################################################################
    public static function profileimgexists($image) {
        $filename = DIR_PROFILE_PIC_URL . $image;
        $physicalfilename = DIR_PROFILE_PIC . $image;

        if (file_exists($physicalfilename) && $image != '') {
            $filename = $filename;
        } else {
            $filename = DIR_UPLOADS . 'default_user.png';
        }
        return $filename;
    }

    //#################################################################
    // Name : profileimgexists
    // Purpose : To check for profile image
    // In Params : all user data
    // Out params : Re active user and send verification email
    //#################################################################
    public static function profiledocumentsexists($docs) {

        $filename = DIR_PROFILE_DOC_URL . $docs;
        $physicalfilename = DIR_PROFILE_DOC . $docs;

        if (file_exists($physicalfilename) && $docs != '') {
            $filename = $filename;
        } else {
            $filename = '';
        }
        return $filename;
    }

    //#################################################################
    // Name : profilethumbexists
    // Purpose : To check for profile image thumb
    // In Params : all user data
    // Out params : Re active user and send verification email
    //#################################################################
    public static function profilethumbexists($image) {
        $filename = DIR_PROFILE_PIC_URL . "thumb_" . $image;
        $physicalfilename = DIR_PROFILE_PIC . "thumb_" . $image;

        if (file_exists($physicalfilename) && $image != '') {
            $filename = $filename;
        } else {
            $filename = DIR_UPLOADS . 'default_user.png';
        }
        return $filename;
    }

    //#################################################################
    // Name : ReactiveUser
    // Purpose : To reactive deleted user at register time
    // In Params : all user data
    // Out params : Re active user and send verification email
    //#################################################################
    public static function ReactiveUser($Params) {

        //global declaration
        $ReturnData = array();

        //extrat params
        extract($Params);
        //generate new referral code for profile
        $MyReferralCode = str_random(5);
        //prepare array for insert
        $UpdateArray = array(
            'name' => $name,
            'email_address' => $email_address,
            'password' => Hash::make($password),
            'gender' => $gender,
            'os_type' => $os_type,
            'my_referral_code' => $MyReferralCode,
            'from_social_network' => 0,
            'facebook_id' => '',
            'language' => $language,
            'jobstatus_notification' => '1',
            'transaction_notification' => '1',
            'newjob_notification' => '1',
            'created_on' => date('Y-m-d H:i:s'),
            'user_type' => $user_type,
            'status' => 0, //unverified	
            'user_delete' => 0,
            'version' => $version//update his deleted status
        );

        if (isset($profile_pic) && $profile_pic != '') {
            $UpdateArray = array_merge($UpdateArray, array('profile_pic' => $profile_pic));
        }

        //set referral code for user in case provided        
        if (isset($referral_code) && $referral_code != '') {
            $UpdateArray = array_merge($UpdateArray, array('referral_code' => $referral_code));
        }

        $ResultUpdate = DB::table('front_users')->where(array('email_address' => $email_address))->update($UpdateArray);

        //START VERIFICICATION CODE
        $VerificationToken = str_random(25);

        $Parameters = array('verification_token' => $VerificationToken, 'email_id' => base64_encode($email_address), 'user_id' => '');
        $URL = action('ApiuserController@getVerifyaccount', $Parameters);

        $UpdateTokenArray = array('verification_token' => $VerificationToken);
        $ResultToken = DB::table('front_users')->where('email_address', $email_address)->update($UpdateTokenArray);

        $data['username'] = $name;
        $data['link'] = $URL;

        Mail::send('emails.verifyaccount', $data, function($message) use ($Params) {
            $message->from('noreply@unideal.team', 'UNIDeal Team');
            $message->to($Params['email_address'], $Params['name'])->subject('UNIDeal Team : Account Verification');
        });
        //END VERIFICICATION CODE
        unset($UpdateArray['password']);
        //process for image
        $UpdateArray['profile_pic'] = self::profileimgexists($profile_pic);

        //get user id of activated user by email
        $UserIdQuery = DB::table('front_users')
                ->select('id')
                ->where('email_address', $email_address)
                ->first();
        $ResultUserId = json_decode(json_encode($UserIdQuery), true);
        $UpdateArray['user_id'] = $ResultUserId['id'];
        //Home::postInsertactivity($result, 0, REG_TEXT);

        if ($ResultUpdate) {
            return $UpdateArray;
        } else {
            return false;
        }
    }

    //#################################################################
    // Name : Register
    // Purpose : To register user
    // In Params : all user data
    // Out params : Re active user and send verification email
    //#################################################################
    public static function Register($Params) {
        //global declaration
        $ReturnData = array();
        //extrat params
        extract($Params);
        //generate new referral code for profile
        $MyReferralCode = str_random(8);
        //prepare array for insert
        $InsertArray = array(
            'name' => $name,
            'email_address' => $email_address,
            'password' => Hash::make($password),
            'gender' => $gender,
            'os_type' => $os_type,
            'my_referral_code' => $MyReferralCode,
            'from_social_network' => 3,
            'facebook_id' => '',
            'googleplus_id' => '',
            'language' => $language,
            'jobstatus_notification' => '1',
            'transaction_notification' => '1',
            'newjob_notification' => '1',
            'questioner_ratings' => 0,
            'agent_ratings' => 0,
            'created_on' => date('Y-m-d H:i:s'),
            'user_type' => $user_type,
            'status' => 2, //unverified	
            'user_delete' => 0,
            //'is_first_login' => 0,
            'version' => $version //update his deleted status
        );

        if (isset($profile_pic) && $profile_pic != '') {
            $InsertArray = array_merge($InsertArray, array('profile_pic' => $profile_pic));
        }
        if (isset($phone_number) && $phone_number != '') {
            $InsertArray = array_merge($InsertArray, array('phone_number' => $phone_number));
        }

        //set referral code for user in case provided        
        if (isset($referral_code) && $referral_code != '') {
            //merge array for referral code data
            $InsertArray = array_merge($InsertArray, array('referral_code' => $referral_code));
        }
        //insert data to table
        $ResultInsert = DB::table('front_users')->insertGetId($InsertArray);

        //if data inserted then only send email
        if (isset($ResultInsert) && $ResultInsert > 0) {
            unset($InsertArray['password']);
            $InsertArray['user_id'] = $ResultInsert;
            $InsertArray['profile_pic'] = self::profileimgexists($profile_pic);
            //proceed for referral code histroy
            if (isset($referral_code) && $referral_code != '') {

                //check master setting. referal code is enable or disable                
                $GetReferralSetting = Apicommon::GetMasterSettings(5);
                //if not disable then proceed
                if (isset($GetReferralSetting) && $GetReferralSetting['setting_status'] == '1') {

                    //find user for matching referral code                
                    $Checkreferral = DB::table('front_users as fu')
                            ->select('id', db::raw('(select device_token from front_user_token where user_id = fu.id order by created_on desc limit 1) as device_token'))
                            ->where('my_referral_code', $referral_code)
                            ->first();

                    if ($Checkreferral) { // if there is a connection 
                        //add data to referral code histrory and transaction table
                        $InsertArrayForHistory = array(
                            'from_user_id' => $Checkreferral->id,
                            'to_user_id' => $ResultInsert,
                            'referral_code' => $referral_code,
                            'referral_amount' => $GetReferralSetting['referral_amount'],
                            'created_on' => gmdate('Y-m-d H:i:s')
                        );
                        //prepare array for transaction table
                        $InsertArrayForTrasaction = array(
                            'user_id' => $Checkreferral->id,
                            'transaction_type' => 1,
                            'transaction_amount' => $GetReferralSetting['referral_amount'],
                            'transaction_description' => 'User joined through referral code.',
                            'color_code' => 2,
                            'created_on' => gmdate('Y-m-d H:i:s')
                        );
                        //add data to referral code history
                        $AddReferralHistory = Apicommon::CommonInsert('referral_code_history', $InsertArrayForHistory);
                        //add data to transaction table
                        $AddTransactionHistory = Apicommon::CommonInsert('transactions', $InsertArrayForTrasaction);
                    }
                }
            }

            //START VERIFICICATION CODE
            $VerificationToken = str_random(25);
            $Parameters = array('verification_token' => $VerificationToken, 'email_id' => base64_encode($email_address));
            $URL = action('ApiuserController@getVerifyaccount', $Parameters);
            $UpdateTokenArray = array('verification_token' => $VerificationToken);
            $ResultToken = DB::table('front_users')->where('email_address', $email_address)->update($UpdateTokenArray);

            $Data['username'] = $name;
            $Data['link'] = $URL;
            try {
                Mail::send('emails.verifyaccount', $Data, function($message) use ($Params) {
                    $message->from('noreply@unideal.team', 'UNIDeal Team');
                    $message->to($Params['email_address'], $Params['name'])->subject('UNIDeal Team : Account Verification');
                });
            } catch (Exception $e) {
                echo 'Message: ' . $e->getMessage();
                exit;
            }

            //END VERIFICICATION CODE
        }
        if ($ResultInsert) {
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['data'] = $InsertArray;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : checkuserverification
    // Purpose : To check user verification
    // In Params : all user data
    // Out params : Re active user and send verification email
    //#################################################################
    public static function checkuserverification($verification_token, $email) {
        //global declaration
        $ReturnData = array();

        //check for verification token
        $CheckToken = DB::table('front_users')
                ->select('id')
                ->where('verification_token', $verification_token)
                ->first();

        //if token is found then update status and set token to blank
        if ($CheckToken) {
            //if token is found then check for email is exist or not

            $CheckEmailExist = DB::table('front_users')
                    ->where(array('email_address' => $email))
                    ->where('status', 1)
                    ->where('user_delete', 0)
                    ->orderby('id', 'desc')
                    ->first();

            if ($CheckEmailExist) {
                $EmailExistArray = array(
                    'verification_token' => '',
                );
                $ResultEmailExist = DB::table('front_users')->where(array('email_address' => $email))->update($EmailExistArray);
                $ReturnData['status'] = '4'; //Email Address Exist with other user
            } else {

                $UpdateArray = array(
                    'status' => 1,
                    'verification_token' => ''
                );
                $ResultUpdate = DB::table('front_users')->where(array('email_address' => $email))->update($UpdateArray);
                if ($ResultUpdate) {
                    $ReturnData['status'] = STATUS_TRUE;
                } else {//already verified
                    $ReturnData['status'] = '2';
                }
            }
        } else {
            $ReturnData['status'] = '3';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : checkuserverification
    // Purpose : To check user verification
    // In Params : all user data
    // Out params : Re active user and send verification email
    //#################################################################
    public static function CheckFbUserExist($Params) {

        //global declaration
        $ReturnData = array();
        $CreatedOn = date('Y-m-d H:i:s');
        //extract params.
        extract($Params);

        $Updatelanguage = array('language' => $language);
        $language_update = DB::table('front_users')
                ->where('facebook_id', $facebook_id)
                ->update($Updatelanguage);

        ## Check if already exist or not from google.
        $CheckExistorNot = Apiusers::SocialUserCheck($facebook_id, $Params, 'facebook');

        $QueryUser = DB::table('front_users')
                        ->select(DB::Raw('id as user_id'), 'user_type', 'name', 'email_address', 'gender', 'phone_number', 'os_type', 'my_referral_code', 'from_social_network', db::raw('IFNULL(facebook_id, "") as facebook_id'), db::raw('IFNULL(googleplus_id,"") as google_id'), 'created_on', 'status', 'user_delete', 'profile_pic', 'referral_code', 'language', 'version', db::raw('IFNULL(jobstatus_notification,"") as jobstatus_notification'), db::raw('IFNULL(transaction_notification,"") as transaction_notification'), db::raw('IFNULL(newjob_notification,"") as newjob_notification'), db::raw('IFNULL(bio, "") as bio'), db::raw('IFNULL(expertise_id,"") as user_expertise'), db::raw('IFNULL(bio_files,"") as user_doc'))
                        ->where('facebook_id', $facebook_id)
                        ->take(1)->first();

        $ResultUser = json_decode(json_encode($QueryUser), true);


        if ($ResultUser) {
            if ($ResultUser['user_delete'] == 1) { // if user deleted
                $ReturnData['status'] = '-1';
                $ReturnData['status_code'] = STATUS_CODE_301;
                $ReturnData['message'] = trans('messages.LOGIN_FAIL_NO_ACCOUNT');
            } else if ($ResultUser['status'] == 0) { // if deactivated
                $ReturnData['status'] = '0';
                $ReturnData['status_code'] = STATUS_CODE_302;

                $ReturnData['message'] = trans('messages.LOGIN_FAIL_ACCOUNT_INACTIVE');
            } else if ($ResultUser['status'] == 2) { // if not verified
                $ReturnData['status'] = '2';
                $ReturnData['status_code'] = STATUS_CODE_303;
                $ReturnData['message'] = trans('messages.LOGIN_FAIL_ACCOUNT_NOT_VERIFIED');
            } else {
                $ReturnData['status'] = STATUS_TRUE;
                $ReturnData['status_code'] = STATUS_CODE_200;
                $ReturnData['message'] = trans('messages.LOGIN_SUCCESS');
                $ResultUser['user_expertise'] = (int) $ResultUser['user_expertise'];
                if (isset($ResultUser['profile_pic']) && $ResultUser['profile_pic'] != '') {
                    if (!filter_var($ResultUser['profile_pic'], FILTER_VALIDATE_URL) === false) {
                        $ResultUser['profile_pic'] = $ResultUser['profile_pic'];
                    } else {
                        $ResultUser['profile_pic'] = self::profileimgexists($ResultUser['profile_pic']);
                    }
                }
                if ($ResultUser['user_doc'] != '') {
                    $ResultUser['user_doc'] = self::profiledocumentsexists($ResultUser['user_doc']);
                }
                unset($ResultUser['status']);
                unset($ResultUser['user_delete']);
                $ReturnData['user_data'] = $ResultUser;
            }
        } else { //USER DOESTNT EXISTS
            // create the user now
            //generate new referral code for profile
            $MyReferralCode = str_random(5);

            $InsertArray = array(
                'name' => $name,
                'os_type' => $os_type,
                'from_social_network' => 1,
                'facebook_id' => $facebook_id,
                'status' => 1,
                'user_delete' => 0,
                'created_on' => $CreatedOn,
                'language' => $language,
                'version' => $version,
                'user_type' => 2,
                'questioner_ratings' => 0,
                'agent_ratings' => 0,
                'jobstatus_notification' => '1',
                'transaction_notification' => '1',
                'newjob_notification' => '1',
                'my_referral_code' => $MyReferralCode,
            );
            ## add optional parameter based on added in web service input
            if (isset($profile_pic) && $profile_pic != '') {
                $InsertArray = array_merge($InsertArray, array('profile_pic' => $profile_pic));
            }
            if (isset($gender) && $gender != '') {
                $InsertArray = array_merge($InsertArray, array('gender' => $gender));
            }

            if (isset($profile_pic) && $profile_pic != '') {
                $InsertArray = array_merge($InsertArray, array('profile_pic' => $profile_pic));
            }
            if (isset($email_address) && $email_address != '') {
                $ValidatorEmailExist = Validator::make(array(
                            'email_address' => $email_address,
                                ), array(
                            'email_address' => 'unique:front_users',
                ));
                if ($ValidatorEmailExist->fails()) {
                    $ReturnData['status'] = STATUS_FALSE;
                     $ReturnData['status_code'] = STATUS_CODE_201;
                    $ReturnData['message'] = $ValidatorEmailExist->messages()->first();
                } else {
                    //insert data into db
                    $InsertArray = array_merge($InsertArray, array('email_address' => $email_address));

                    $RegisterUser = DB::table('front_users')->insertGetId($InsertArray);
                    if (isset($RegisterUser) && $RegisterUser > 0) {

                        $GetUserProfile = self::GetProfile($RegisterUser);
                        $ReturnData['status'] = STATUS_TRUE;
                        $ReturnData['message'] = trans('messages.LOGIN_SUCCESS');
                        $ReturnData['user_data'] = $GetUserProfile['user_data'];
                    } else {
                        $ReturnData['status'] = STATUS_FALSE;
                        $ReturnData['message'] = trans('messages.GENERAL_ERROR');
                    }
                }
            } else {

                $InsertArray = array_merge($InsertArray, array('email_address' => ''));
                $RegisterUser = DB::table('front_users')->insertGetId($InsertArray);
                if (isset($RegisterUser) && $RegisterUser > 0) {
                    //get user details
                    $GetUserProfile = self::GetProfile($RegisterUser);
                    $ReturnData['status'] = STATUS_TRUE;
                    $ReturnData['message'] = trans('messages.LOGIN_SUCCESS');
                    $ReturnData['user_data'] = $GetUserProfile['user_data'];
                } else {
                    $ReturnData['status'] = STATUS_FALSE;
                    $ReturnData['message'] = trans('messages.GENERAL_ERROR');
                }
            }
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : CheckgoogleUserExist
    // Purpose : To check google user verification.
    // In Params : all user data.
    // Out params : Re active user and send verification email.
    //#################################################################
    public static function CheckGoogleUserExist($Params) {

        //global declaration
        $ReturnData = array();
        $CreatedOn = date('Y-m-d H:i:s');
        //extract params.
        extract($Params);

        $Updatelanguage = array('language' => $language);
        $language_update = DB::table('front_users')
                ->where('googleplus_id', $google_id)
                ->update($Updatelanguage);

        ## Check if already exist or not from google.
        $CheckExistorNot = Apiusers::SocialUserCheck($google_id, $Params, 'google');

        $QueryUser = DB::table('front_users')
                        ->select(DB::Raw('id as user_id'), 'user_type', db::raw('IFNULL(bio, "") as bio'), db::raw('IFNULL(expertise_id,"") as user_expertise'), db::raw('IFNULL(bio_files,"") as user_doc'), 'name', 'email_address', 'gender', 'phone_number', 'os_type', 'my_referral_code', 'from_social_network', db::raw('IFNULL(facebook_id, "") as facebook_id'), db::raw('IFNULL(googleplus_id,"") as google_id'), 'created_on', 'status', 'user_delete', 'profile_pic', 'referral_code', 'language', 'version', db::raw('IFNULL(jobstatus_notification,"") as jobstatus_notification'), db::raw('IFNULL(transaction_notification,"") as transaction_notification'), db::raw('IFNULL(newjob_notification,"") as newjob_notification'))
                        ->where('googleplus_id', $google_id)
                        ->where('user_delete', 0)
                        ->take(1)->first();

        $ResultUser = json_decode(json_encode($QueryUser), true);

        if ($ResultUser) {
            if ($ResultUser['user_delete'] == 1) { // if user deleted
                $ReturnData['status'] = '0';
                $ReturnData['status_code'] = STATUS_CODE_301;
                $ReturnData['message'] = trans('messages.LOGIN_FAIL_NO_ACCOUNT');
            } else if ($ResultUser['status'] == 0) { // if deactivated
                $ReturnData['status'] = '0';
                $ReturnData['status_code'] = STATUS_CODE_302;
                $ReturnData['message'] = trans('messages.LOGIN_FAIL_ACCOUNT_INACTIVE');
            } else if ($ResultUser['status'] == 2) { // if not verified
                $ReturnData['status'] = '0';
                $ReturnData['status_code'] = STATUS_CODE_303;
                $ReturnData['message'] = trans('messages.LOGIN_FAIL_ACCOUNT_NOT_VERIFIED');
            } else {
                $ReturnData['status'] = STATUS_TRUE;
                $ReturnData['status_code'] = STATUS_CODE_200;
                $ReturnData['message'] = trans('messages.LOGIN_SUCCESS');
                 $ResultUser['user_expertise'] = (int) $ResultUser['user_expertise'];

                if (isset($ResultUser['profile_pic']) && $ResultUser['profile_pic'] != '') {
                    if (!filter_var($ResultUser['profile_pic'], FILTER_VALIDATE_URL) === false) {
                        $ResultUser['profile_pic'] = $ResultUser['profile_pic'];
                    } else {
                        $ResultUser['profile_pic'] = self::profileimgexists($ResultUser['profile_pic']);
                    }
                }
                 if ($ResultUser['user_doc'] != '') {
                    $ResultUser['user_doc'] = self::profiledocumentsexists($ResultUser['user_doc']);
                }

                unset($ResultUser['status']);
                unset($ResultUser['user_delete']);
                $ReturnData['user_data'] = $ResultUser;
            }
        } else { //USER DOESTNT EXISTS
            // create the user now
            //generate new referral code for profile
            $MyReferralCode = str_random(5);

            $InsertArray = array(
                'name' => $name,
                'os_type' => $os_type,
                'from_social_network' => 2,
                'googleplus_id' => $google_id,
                'status' => 1,
                'user_delete' => 0,
                'created_on' => $CreatedOn,
                'language' => $language,
                'version' => $version,
                'user_type' => 2,
                'questioner_ratings' => 0,
                'agent_ratings' => 0,
                'jobstatus_notification' => '1',
                'transaction_notification' => '1',
                'newjob_notification' => '1',
                'my_referral_code' => $MyReferralCode,
            );
            ## add optional parameter based on added in web service input
            if (isset($profile_pic) && $profile_pic != '') {
                $InsertArray = array_merge($InsertArray, array('profile_pic' => $profile_pic));
            }
            if (isset($gender) && $gender != '') {
                $InsertArray = array_merge($InsertArray, array('gender' => $gender));
            }
            if (isset($email_address) && $email_address != '') {
                $ValidatorEmailExist = Validator::make(array(
                            'email_address' => $email_address,
                                ), array(
                            'email_address' => 'unique:front_users',
                ));
                if ($ValidatorEmailExist->fails()) {
                    $ReturnData['status'] = 0;
                     $ReturnData['status_code'] = STATUS_CODE_201;
                    $ReturnData['message'] = $ValidatorEmailExist->messages()->first();
                } else {
                    //insert data into db.
                    $InsertArray = array_merge($InsertArray, array('email_address' => $email_address));

                    $RegisterUser = DB::table('front_users')->insertGetId($InsertArray);
                    if (isset($RegisterUser) && $RegisterUser > 0) {

                        $GetUserProfile = self::GetProfile($RegisterUser);
                        $ReturnData['status'] = STATUS_TRUE;
                        $ReturnData['message'] = trans('messages.LOGIN_SUCCESS');
                        //$ResultUser['profile_pic'] = self::profileimgexists($GetUserProfile['user_data']['profile_pic']);
                        $ReturnData['user_data'] = $GetUserProfile['user_data'];
                    } else {
                        $ReturnData['status'] = STATUS_FALSE;
                        $ReturnData['message'] = trans('messages.GENERAL_ERROR');
                    }
                }
            } else {
                $InsertArray = array_merge($InsertArray, array('email_address' => ''));
                $RegisterUser = DB::table('front_users')->insertGetId($InsertArray);
                if (isset($RegisterUser) && $RegisterUser > 0) {
                    //get user details
                    $GetUserProfile = self::GetProfile($RegisterUser);
                    $ReturnData['status'] = STATUS_TRUE;
                    $ReturnData['message'] = trans('messages.LOGIN_SUCCESS');
                    // $ResultUser['profile_pic'] = self::profileimgexists($GetUserProfile['user_data']['profile_pic']);
                    $ReturnData['user_data'] = $GetUserProfile['user_data'];
                } else {
                    $ReturnData['status'] = STATUS_FALSE;
                    $ReturnData['message'] = trans('messages.GENERAL_ERROR');
                }
                //Home::postInsertactivity($result, 0, REG_TEXT);
            }
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : CheckUserExist
    // Purpose : To login the user
    // In Params : email
    // Out params : status
    //#################################################################
    public static function CheckUserExist($Params) {

        //global declaration.
        $ReturnData = array();

        //extract params.
        extract($Params);

        $Updatelanguage = array('language' => $language);
        $language = DB::table('front_users')
                ->where('email_address', $email_address)
                ->update($Updatelanguage);
        //query to fetch data
        $UserQuery = DB::table('front_users')
                        ->select(DB::Raw('id as user_id'), 'name', 'user_type', db::raw('IFNULL(bio, "") as bio'), db::raw('IFNULL(expertise_id,"") as user_expertise'), db::raw('IFNULL(bio_files,"") as user_doc'), 'jobstatus_notification', 'transaction_notification', 'newjob_notification', 'email_address', 'gender', 'phone_number', 'os_type', 'my_referral_code', 'from_social_network', 'facebook_id', 'googleplus_id', 'created_on', 'status', 'user_delete', 'profile_pic', 'referral_code', 'user_type', 'language', 'version')
                        ->where('email_address', $email_address)
                        ->take(1)->first();

        $ResultLogin = json_decode(json_encode($UserQuery), true);
        if ($ResultLogin) {
            if ($ResultLogin['user_delete'] == 1) { // if user deleted.
                $ReturnData['status_code'] = STATUS_CODE_301;
                $ReturnData['status'] = STATUS_FALSE;
                $ReturnData['message'] = trans('messages.LOGIN_FAIL_NO_ACCOUNT');
            } else if ($ResultLogin['status'] == 0) { // if deactivated.
                $ReturnData['status_code'] = STATUS_CODE_302;
                $ReturnData['status'] = STATUS_FALSE;
                $ReturnData['message'] = trans('messages.LOGIN_FAIL_ACCOUNT_INACTIVE');
            } else if ($ResultLogin['status'] == 2) { // if not verified.
                $ReturnData['status_code'] = STATUS_CODE_303;
                $ReturnData['status'] = STATUS_FALSE;
                $ReturnData['message'] = trans('messages.LOGIN_FAIL_ACCOUNT_NOT_VERIFIED');
            } else {
                $ReturnData['status_code'] = STATUS_CODE_200;
                $ReturnData['status'] = STATUS_TRUE;
                $ReturnData['message'] = trans('messages.LOGIN_SUCCESS');

                if ($ResultLogin['from_social_network'] == 3) {
                    $ResultLogin['profile_pic'] = self::profileimgexists($ResultLogin['profile_pic']);
                } else {
                    $ResultLogin['profile_pic'] = $ResultLogin['profile_pic'];
                }
                if ($ResultLogin['user_doc'] != '') {
                    $ResultLogin['user_doc'] = self::profiledocumentsexists($ResultLogin['user_doc']);
                }
                $ResultLogin['user_expertise'] = (int)  $ResultLogin['user_expertise'];
                unset($ResultLogin['status']);
                unset($ResultLogin['user_delete']);
                $ReturnData['user_data'] = $ResultLogin;
            }
        } else {

            $ReturnData['status'] = STATUS_FALSE;
            $ReturnData['status_code'] = STATUS_CODE_301;
            $ReturnData['message'] = trans('messages.LOGIN_FAIL_NO_ACCOUNT');
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : ProcessToken
    // Purpose : To process token
    // In Params : token,user id
    // Out params : status
    //#################################################################
    public static function ProcessToken($DeviceToken, $UserId, $OsType) {

        //delete token from table
        $DeleteToken = DB::table('front_user_token')->where(array('device_token' => $DeviceToken))->delete();
        $DeleteTokenUser = DB::table('front_user_token')->where(array('user_id' => $UserId))->delete();
        //insert token
        $InsertTokenArray = array(
            'user_id' => $UserId,
            'device_token' => $DeviceToken,
            'os_type' => $OsType
        );
        $InsertToken = DB::table('front_user_token')->insert($InsertTokenArray);

        return 1;
    }

    //#################################################################
    // Name : postForgotpassword
    // Purpose : To send mail for forgot password
    // In Params : email
    // Out params : status
    //#################################################################
    public static function Forgotpassword($Params) {
        //global declaraton
        $ReturnData = array();

        //extract params
        extract($Params);

        //validate email address
        $ValidateEmail = DB::table('front_users')
                ->select('id', 'email_address', 'name')
                ->where('email_address', $email_address)
                ->where('user_delete', 0)
                ->first();

        $ValidateEmailResult = json_decode(json_encode($ValidateEmail), true);
        //if email exist then send email
        if ($ValidateEmail) {
            //START VERIFICICATION CODE
            $reset_token = str_random(25);

            $parameter = array('reset_password_token' => $reset_token, 'email_id' => base64_encode($email_address));
            $URL = action('ApiuserController@getResetpassword', $parameter);

            $UpdatePasswordToken = array('reset_password_token' => $reset_token);
            $update_result = DB::table('front_users')
                    ->where('email_address', $email_address)
                    ->update($UpdatePasswordToken);

            $resent_data['username'] = $ValidateEmailResult['name'];
            $resent_data['link'] = $URL;

            Mail::send('emails.user_forgotpassword', $resent_data, function($message) use ($Params) {
                $message->from('noreply@UNIDeal.team', 'UNIDeal Team');
                $message->to($Params['email_address'])->subject('UNIDeal Team: Reset your password');
            });

            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['message'] = trans('messages.FORGOT_PASSWORD_SUCCESS');
        } else {
            $ReturnData['status'] = STATUS_FALSE;
            $ReturnData['message'] = trans('messages.LOGIN_FAIL_NO_ACCOUNT');
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : CheckForgotToken
    // Purpose : To chekc forgot password token
    // In Params : email,token
    // Out params : status
    //#################################################################
    public static function CheckForgotToken($ResetToken, $email) {

        //global declaraton
        $ReturnData = array();

        $ChekcToken = DB::table('front_users')
                ->select('id')
                ->where('reset_password_token', $ResetToken)
                ->first();

        $ResetTokenResult = json_decode(json_encode($ChekcToken), true);
        //if token found
        if ($ChekcToken) {
            //reset token to expire the link
            $ResetArray = array(
                'reset_password_token' => ''
            );
            //$ResetToken = DB::table('front_users')->where(array('email_address' => $email))->update($ResetArray);
            $ReturnData['status'] = STATUS_TRUE;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : ResetPassword
    // Purpose : To reset user password
    // In Params : password,email
    // Out params : status
    //#################################################################
    public static function ResetPassword($Params) {
        //global declaraton
        $ReturnData = array();

        //extract params
        extract($Params);

        $ResetPassworArray = array(
            'password' => Hash::make($newpassword), 'reset_password_token' => ''
        );
        $PasswordReset = DB::table('front_users')->where(array('email_address' => base64_decode($email_address)))->update($ResetPassworArray);
        if ($PasswordReset) {
            $ReturnData['status'] = STATUS_TRUE;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : Changepassword
    // Purpose : To change current password
    // In Params : user id,new password
    // Out params : status
    //#################################################################
    public static function Changepassword($Params) {
        //global declaraton
        $ReturnData = array();

        //extract params
        extract($Params);

        $UpdateArray = array(
            'password' => Hash::make($password),
        );
        $ResultUpdatePassword = DB::table('front_users')->where(array('id' => $user_id))->update($UpdateArray);
        //if password is updated
        if ($ResultUpdatePassword) {

            $GetUserEmail = DB::table('front_users')
                    ->select('email_address', 'name')
                    ->where('id', $user_id)
                    ->first();

            if ($GetUserEmail) {

                $UserEmail = json_decode(json_encode($GetUserEmail), true);
                $EmailData['username'] = $UserEmail['name'];
                $EmailData['password_changedate'] = date('d-M-Y H:i a');

                Mail::send('emails.user_password_change', $EmailData, function($message) use ($EmailData, $UserEmail) {
                    $message->from('noreply@UNIDeal.team', 'UNIDeal Team');
                    $message->to($UserEmail['email_address'])->subject('UNIDeal Team: Your account password was updated');
                });
                return true;
            } else {
                return true;
            }
        }
    }

    //#################################################################
    // Name : GetProfile
    // Purpose : To get ueser profile
    // In Params : user id
    // Out params : status
    //#################################################################
    public static function GetProfile($UserId) {
        //global declaraton.
        $ReturnData = array();

        $QueryUser = DB::table('front_users')
                ->select(DB::Raw('id as user_id'), 'user_type', 'name', db::raw('IFNULL(email_address,"") as email_address'), 'gender', 'phone_number', 'os_type', 'my_referral_code', 'from_social_network', DB::raw('IFNULL(facebook_id, "") as facebook_id'), db::raw('IFNULL(googleplus_id, "") as google_id'), 'created_on', 'status', 'user_delete', 'profile_pic', 'referral_code', 'total_earnings', 'default_card', 'language', 'version', db::raw('IFNULL(jobstatus_notification,"") as jobstatus_notification'), db::raw('IFNULL(transaction_notification,"") as transaction_notification'), db::raw('IFNULL(newjob_notification,"") as newjob_notification'), db::raw('IFNULL(bio, "") as bio'), db::raw('IFNULL(expertise_id,"") as user_expertise'), db::raw('IFNULL(bio_files,"") as user_doc'))
                ->where('id', $UserId)
                //->where('status', 1)
                ->where('user_delete', 0)
                ->first();

        $UserData = json_decode(json_encode($QueryUser), true);
        
        if (!filter_var($UserData['profile_pic'], FILTER_VALIDATE_URL) === false) {
            $UserData['profile_pic'] = $UserData['profile_pic'];
        } else {
            $UserData['profile_pic'] = self::profileimgexists($UserData['profile_pic']);
        }

        if ($UserData['user_doc'] != '') {
            $UserData['user_doc'] = self::profiledocumentsexists($UserData['user_doc']);
        }

        if ($QueryUser) {
            $UserData['user_expertise'] =  (int) $UserData['user_expertise'];
            $ReturnData['status'] = STATUS_TRUE;
            unset($UserData['user_delete']);
            $ReturnData['user_data'] = $UserData;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : GetProfile
    // Purpose : To get ueser profile
    // In Params : user id
    // Out params : status
    //#################################################################
    public static function GetProfileByemail($EmailAddress) {
        //global declaraton
        $ReturnData = array();

        $QueryUser = DB::table('front_users')
                ->select(DB::Raw('id as user_id'), 'name', 'email_address', 'gender', 'phone_number', 'os_type', 'my_referral_code', 'from_social_network', 'facebook_id', 'googleplus_id', 'created_on', 'status', 'user_delete', 'profile_pic', 'referral_code', 'language', 'version')
                ->where('email_address', $EmailAddress)
                ->first();

        $UserData = json_decode(json_encode($QueryUser), true);
        $UserData['profile_pic'] = self::profileimgexists($UserData['profile_pic']);
        if ($QueryUser) {
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['user_data'] = $UserData;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : GetSettings
    // Purpose : To get ueser settings
    // In Params : user id
    // Out params : status
    //#################################################################
    public static function GetSettings($UserId) {
        //global declaraton
        $ReturnData = array();

        $QueryUser = DB::table('front_users')
                ->select('jobstatus_notification', 'transaction_notification', 'newjob_notification')
                ->where('id', $UserId)
                ->first();

        $UserData = json_decode(json_encode($QueryUser), true);
        if ($QueryUser) {
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['user_data'] = $UserData;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : UpdateProfile
    // Purpose : To update user profile
    // In Params : user id, All user data
    // Out params : status
    //#################################################################
    public static function UpdateProfile($Params) {

        //global declaraton
        $ReturnData = array();
        //extract parms
        extract($Params);
        // print_r($document);die;
        //prepare array for update
        $UpdateArray = array(
            'language' => $language,
            'version' => $version,
            'os_type' => $os_type
        );
        if (isset($new_password) && $new_password != '') {
            if (isset($old_password) && $old_password != '') {
                $QueryUser = DB::table('front_users')
                                ->select('id', 'email_address', 'password')
                                ->where('email_address', $email_address)->orWhere('id', '=', $user_id)->first();
                if ($QueryUser) {
                    if (Hash::check($old_password, $QueryUser->password)) {
                        $UpdateArray = array_merge($UpdateArray, array('password' => Hash::make($new_password)));
                    }
                } else {
                    $ReturnData['status'] = STATUS_FALSE;
                }
            } else {
                $ReturnData['status'] = STATUS_FALSE;
            }
        }
        if (isset($name) && $name != '') {
            $UpdateArray = array_merge($UpdateArray, array('name' => $name));
        }
        if (isset($gender) && $gender != '') {
            $UpdateArray = array_merge($UpdateArray, array('gender' => $gender));
        }
        if (isset($email_address) && $email_address != '') {
            $UpdateArray = array_merge($UpdateArray, array('email_address' => $email_address));
        }
        if (isset($profile_pic) && $profile_pic != '') {
            $UpdateArray = array_merge($UpdateArray, array('profile_pic' => $profile_pic));
        }

        if (isset($phone_number) && $phone_number != '') {
            $UpdateArray = array_merge($UpdateArray, array('phone_number' => $phone_number));
        }
        if (isset($bio) && $bio != '') {
            $UpdateArray = array_merge($UpdateArray, array('bio' => $bio));
        }
        if (isset($user_expertise) && $user_expertise != '') {
            $UpdateArray = array_merge($UpdateArray, array('expertise_id' => $user_expertise));
        }
        if (isset($document) && $document != '') {
            $UpdateArray = array_merge($UpdateArray, array('bio_files' => $document));
        }
        if (isset($user_type) && $user_type != '') {
            $UpdateArray = array_merge($UpdateArray, array('user_type' => $user_type));
        }
        if (isset($from_social_network) && $from_social_network != '') {
            $UpdateArray = array_merge($UpdateArray, array('from_social_network' => $from_social_network));
        }
        if (isset($expertize) && $expertize != '') {
            $UpdateArray = array_merge($UpdateArray, array('expertise_id' => $expertize));
        }
        //query to update data.
        $UpdateProfile = DB::table('front_users')->where(array('id' => $user_id))->update($UpdateArray);

        if ($UpdateProfile) {
            //fetch user data.
            $GetUserProfile = self::GetProfile($user_id);
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['user_data'] = $GetUserProfile['user_data'];
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
        //return data
        return $ReturnData;
    }

    //#################################################################
    // Name : UpdateLocation
    // Purpose : To udpate user locatin
    // In Params : user id,latitude,longitude
    // Out params : status
    //#################################################################
    public static function UpdateLocation($Params) {
        //global declaraton
        $ReturnData = array();
        //extract parms
        extract($Params);

        //query to update location
        $UpdateArray = array(
            'latitude' => $latitude,
            'longitude' => $longitude
        );

        $ResultUpdateLocation = DB::table('front_users')->where(array('id' => $user_id))->update($UpdateArray);
        //return data
        if ($ResultUpdateLocation) {
            $ReturnData['status'] = STATUS_TRUE;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
        //return data
        return $ReturnData;
    }

    //#################################################################
    // Name : UpdateSettings
    // Purpose : To udpate user settings
    // In Params : user id,notification_email,promotional_email
    // Out params : status
    //#################################################################
    public static function UpdateSettings($Params) {
        //global declaraton
        $ReturnData = array();
        //extract parms
        extract($Params);

        //query to update location.
        $UpdateArray = array(
            'jobstatus_notification' => isset($jobstatus_notification) ? $jobstatus_notification : '1',
            'transaction_notification' => isset($transaction_notification) ? $transaction_notification : '1',
            'newjob_notification' => isset($newjob_notification) ? $newjob_notification : '1',
        );
        if (isset($language) && $language != '') {
            $UpdateArray['language'] = $language;
        }

        //print_r($UpdateArray);die;
        $ResultUpdateSettings = DB::table('front_users')->where(array('id' => $user_id))->update($UpdateArray);
        //return data
        if ($ResultUpdateSettings) {
            $ReturnData['status'] = STATUS_TRUE;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
        //return data
        return $ReturnData;
    }

    //#################################################################
    // Name : UpdateUserEmail
    // Purpose : To udpate new email for user
    // In Params : new and current email
    // Out params : status
    //#################################################################
    public static function UpdateUserEmail($user_id, $new_email) {
        //global declaraton
        $ReturnData = array();
        //prepare array for update
        $UpdateEmail = array(
            'email_address' => $new_email
        );
        $ResultUpdate = DB::table('front_users')->where(array('id' => $user_id))->update($UpdateEmail);
        if ($ResultUpdate) {
            $ReturnData['status'] = STATUS_TRUE;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
        //return data
        return $ReturnData;
    }

    //#################################################################
    // Name : ResendEmail
    // Purpose : To resend activation email
    // In Params : email
    // Out params : status
    //#################################################################
    public static function ResendEmail($EmailAddress) {
        //global declaraton
        $ReturnData = array();
        $UserName = 'User';
        //generate verifiction 
        $verification_token = str_random(25);

        //if account already verified.

        $checkStatus = Apistatuscode::getstatusofuser($EmailAddress);

        if ($checkStatus['status_code'] == STATUS_CODE_200) {
            $ReturnData['status'] = STATUS_FALSE;
            $ReturnData['message'] = trans('messages.ACCOUNT_ALREADY_VERIFIED');
        } else {

            //update user status to not verified.
            $UpdateToken = array('verification_token' => $verification_token, 'status' => 2);
            $UpdateTokenResult = DB::table('front_users')
                    ->where('email_address', $EmailAddress)
                    ->update($UpdateToken);

            //if data is updated, send verification email.
            if ($UpdateTokenResult) {

                $Parameters = array('verification_token' => $verification_token, 'email_address' => base64_encode($EmailAddress));
                $url = action('ApiuserController@getVerifyaccount', $Parameters);

                $GetUserDetails = self::GetProfileByemail($EmailAddress);

                if (isset($GetUserDetails) && $GetUserDetails['status'] == '1') {
                    $UserName = $GetUserDetails['user_data']['name'];
                }

                $EmailData['username'] = $UserName;
                $EmailData['link'] = $url;
                try {
                    Mail::send('emails.verifyaccount', $EmailData, function($message) use ($EmailData, $EmailAddress) {
                        $message->from('noreply@unideal.team', 'UNIDeal Team');
                        $message->to($EmailAddress)->subject('UNIDeal Team : Account Verification');
                    });
                } catch (Exception $e) {
                    echo 'Message: ' . $e->getMessage();
                    exit;
                }
                $ReturnData['status'] = STATUS_TRUE;
            } else {
                $ReturnData['message'] = trans('messages.EMAIL_NOT_AVAILABLE');
                $ReturnData['status'] = STATUS_FALSE;
            }
        }

        //return data.
        return $ReturnData;
    }

    //#################################################################
    // Name : Logout
    // Purpose : To logout user
    // In Params : user id
    // Out params : status
    //#################################################################
    public static function Logout($Params) {
        $ReturnData = array();
        //extract parms
        extract($Params);
        //delete use token
        $DeleteTokenUser = DB::table('front_user_token')->where(array('user_id' => $user_id, 'device_token' => $device_token))->delete();
        if ($DeleteTokenUser) {
            $ReturnData['status'] = STATUS_TRUE;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : SwitchUser
    // Purpose : To switch user from questioner or agent
    // In Params : user id
    // Out params : status
    //#################################################################
    public static function SwitchUser($Params) {
        $ReturnData = array();
        //extract parms.
        extract($Params);
        //UPDATE last login as user
        if (isset($Params['new_user_type']) && $Params['new_user_type'] != '') {

            $updateArray = array('user_type' => $new_user_type);
            $updateSwitchUser = DB::table('front_users')->where(array('id' => $user_id))->update($updateArray);
        } else {
            $updateSwitchUser = false;
            $ReturnData['status'] = STATUS_FALSE;
        }


        if ($updateSwitchUser) {
            $ReturnData['status'] = STATUS_TRUE;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : DeleteAccount
    // Purpose : To delete user account
    // In Params : user id
    // Out params : status
    //#################################################################
    public static function DeleteAccount($UserId) {
        //global declaraton
        $ReturnData = array();

        //update user account to delete status and delete all user data
        //prepare array for update
        $DeleteAccountArray = array(
            'user_delete' => '1',
            'delete_date' => date('Y-m-d H:i:s')
        );
        $ResultDeleteAccount = DB::table('front_users')->where(array('id' => $UserId))->update($DeleteAccountArray);
        //if user deleted successfully then remove all user data
        if ($ResultDeleteAccount) {
            //delete use token
            $DeleteTokenUser = DB::table('front_user_token')->where(array('user_id' => $UserId))->delete();
        }

        return 1;
    }

    //#################################################################
    // Name : CheckFacebookuserExist
    // Purpose : To check facebook user exist or not
    // In Params : facebook id
    // Out params : all data
    //#################################################################
    public static function CheckFacebookuserExist($FacebookId) {
        //global declaraton
        $ReturnData = array();
        // define query
        $QueryStatus = DB::table('front_users')
                ->select(db::raw('id as user_id'))
                ->where('facebook_id', $FacebookId)
                ->where('user_delete', '=', 0)
                ->orderby('id', 'desc')
                ->first();
        $ResultFacebook = json_decode(json_encode($QueryStatus), true);

        if ($ResultFacebook) {
            $ReturnData['data'] = self::GetProfile($ResultFacebook['user_id']);
            ;
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['message'] = trans('messages.FACEBBOK_ALREADY_EXIST');
        } else { //USER DOESTNT EXISTS
            $ReturnData['data'] = 1;
            $ReturnData['message'] = trans('messages.FACEBOOK_NOT_EXIST');
            $ReturnData['status'] = STATUS_FALSE;
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : CheckGoogleuserIdExist
    // Purpose : To check google user exist or not
    // In Params : facebook id
    // Out params : all data
    //#################################################################
    public static function CheckGoogleuserIdExist($googleplus_id) {
        //global declaraton
        $ReturnData = array();
        // define query
        $QueryStatus = DB::table('front_users')
                ->select(db::raw('id as user_id'))
                ->where('googleplus_id', $googleplus_id)
                ->where('user_delete', '=', 0)
                ->orderby('id', 'desc')
                ->first();
        $ResultFacebook = json_decode(json_encode($QueryStatus), true);
        if ($ResultFacebook) {
            $ReturnData['data'] = self::GetProfile($ResultFacebook['user_id']);
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['message'] = trans('messages.GOOGLE_ALREADY_EXIST');
        } else { //USER DOESTNT EXISTS
            $ReturnData['data'] = array();
            $ReturnData['message'] = trans('messages.GOOGLE_NOT_EXIST');
            $ReturnData['status'] = STATUS_FALSE;
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : UpdateUser
    // Purpose : To udpate user app flag
    // In Params : field name & value
    // Out params : all data
    //#################################################################
    public static function UpdateUserSingleColumn($FieldName, $FieldValue, $UserId) {
        $UpdateArray = array($FieldName => $FieldValue);
        $Result = DB::table('front_users')->where('id', $UserId)->update($UpdateArray);

        return 1;
    }

    //#################################################################
    // Name : UpdateUserMultipleColumn
    // Purpose : To udpate user for multiple  columns
    // In Params : field name & value
    // Out params : all data
    //#################################################################
    public static function UpdateUserMultipleColumn($UpdateArray, $UserId) {
        //update query to update data
        $Result = DB::table('front_users')->where('id', $UserId)->update($UpdateArray);

        return 1;
    }

    public static function checkoldpasswords($data) {

        $ReturnData['success'] = STATUS_FALSE;

        if (isset($data) && count($data) > 0) {

            $credentials = [
                "email_address" => base64_decode($data['user_hidden_email']),
                "password" => $data['password'],
            ];
            if (Auth::front_user()->validate($credentials)) {
                $ReturnData['success'] = STATUS_TRUE;
            }
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : UpdateUser
    // Purpose : To udpate user app flag
    // In Params : field name & value
    // Out params : all data
    //#################################################################
    public static function UpdateSingleColumnWithTableName($TableName, $FieldName, $FieldValue, $UpdateParameter, $UpdateParameterVal) {

        //global declaraton
        $ReturnData = array();
        ## query to update data
        $UpdateArray = array($FieldName => $FieldValue);
        $ResultUpdate = DB::table($TableName)->where($UpdateParameter, $UpdateParameterVal)->update($UpdateArray);

        if ($ResultUpdate) {
            $ReturnData['status'] = STATUS_TRUE;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
        ## return data
        return $ReturnData;
    }

    public static function UpdateUserDefaultCard($userId) {

        //global declaraton.
        $ReturnData = array();
        ## query to update data
        $UpdateArray = array('is_default' => 0);
        $ResultUpdate = DB::table('front_user_bank_details')->where('user_id', $userId)->update($UpdateArray);

        return true;
    }

    public static function UpdateUserTokens($Params) {

        //global declaraton.
        $ReturnData = array();
        // define query.
        ##  check old device token if yes then replace it.
        if (isset($Params['old_token']) && $Params['old_token'] != '') {

            $whereArray = array('user_id' => $Params['user_id'], 'device_token' => $Params['old_token'], 'os_type' => $Params['os_type']);
            $checkOldToken = DB::table('front_user_token')->select('token_id')->where($whereArray)->first();
            if ($checkOldToken) {
                $updateTokens = array('device_token' => $Params['new_device_token'], 'os_type' => $Params['os_type']);
                $updateToken = DB::table('front_user_token')->where('token_id', $checkOldToken->token_id)->update($updateTokens);
            } else {

                ## otherwise insert a new one.
                $insertArray = array('user_id' => $Params['user_id'], 'os_type' => $Params['os_type'], 'device_token' => $Params['new_device_token']);
                $checkOldToken = DB::table('front_user_token')->insert($insertArray);
            }
        } else {
            ## otherwise insert a new one.
            $insertArray = array('user_id' => $Params['user_id'], 'os_type' => $Params['os_type'], 'device_token' => $Params['new_device_token']);
            $checkOldToken = DB::table('front_user_token')->insert($insertArray);
        }
        return true;
    }

    public static function SocialUserCheck($social_id, $data, $social_flag) {

        if ($social_flag == 'google') {

            $CheckExistornot = DB::table('front_users')
                    ->select(DB::Raw('id'))
                    ->where('googleplus_id', $social_id)
                    ->where('user_delete', 1)
                    ->take(1)
                    ->first();
        }
        if ($social_flag == 'facebook') {

            $CheckExistornot = DB::table('front_users')
                    ->select(DB::Raw('id'))
                    ->where('facebook_id', $social_id)
                    ->where('user_delete', 1)
                    ->take(1)
                    ->first();
        }

        $CheckExistornot = json_decode(json_encode($CheckExistornot), true);

        if ($CheckExistornot) {
            $googleUpdateArray = array('user_delete' => 0, 'status' => 1);
            $DeleteFirst = DB::table('front_users')->where(array('id' => $CheckExistornot['id']))->update($googleUpdateArray);
        }
    }
    
    ## check is first login and last login date.
    public static function ProcessFirstLoginStatus($UserId){
        
        $updateArray = array('last_login'=> date('Y-m-d H:i:s'));
        $checkLastLogin = DB::table('front_users')->where('id', $UserId)->update($updateArray);
        return true;
        
    }
    
    ##Jigs virani
    ## For send push notifications.
    public static function SendIospushnotifications($message, $deviceToken){
    
        
            $passphrase = IOS_PASSPHARSE;
            $Ckfile = DIR_IOS_PUSH.'UNIDealDevelopment.pem';
           
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $Ckfile);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            
            ## type notification type 1.
        
                $fp = stream_socket_client(GATEWAY_PUSH_APPLE, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

                if (!$fp)
                    exit("Failed to connect: $err $errstr" . PHP_EOL);
                
                $body['aps'] = array(
                        'alert' => $message['message'],
                        'sound' => 'default',
                        'user_type'=> $message['user_type'],
                        'user_id'=> $message['user_id'],
                        'job_id'=> $message['job_id']
                );
                
                $payload = json_encode($body);
                $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
                try {
                    //$result = fwrite($fp, $msg, strlen($msg));
                    $result = fwrite($fp, $msg, strlen($msg));
                } catch (Exception $ex) {
                    sleep(1); //sleep for 5 seconds
                    $result = fwrite($fp, $msg, strlen($msg));
                }
                 fclose($fp);
                   if (isset($result)) {
                        $data['success'] = true;
                        $data['message'] = 1;
                        $data['data'] = $body['aps'];
                        
                    } else {
                       $data['success'] = false;
                       $data['message'] = 0;
                    }
    }
    
    
    public static function GetUsersDeviceTokens($user_id, $device_type){
    
        $getTokens = DB::table('front_user_token')
                     ->select('device_token')
                     ->where('user_id', $user_id)
                     ->where('os_type', OS_TYPE_IOS)
                     ->get();
        
       $UserData = json_decode(json_encode($getTokens), true); 
        
        if($UserData){
            return $UserData;
        }else{
            return false;
        }
    }
    
    ##Jigs Virani.
    ## To send the messages for the similar job has been posted.
    public static function sendMessagesToAgentForExpertise($expertise_id, $job_id){
    
        $selectUsers = DB::table('front_users as fu')
                        ->join('front_user_token as fut', 'fut.user_id', '=', 'fu.id')
                        ->select('fu.name', 'fu.id as user_id', 'fut.device_token', 'fut.os_type')
                        ->where('fu.expertise_id', $expertise_id)
                        ->whereRaw('fut.device_token != ""')
                        ->get();
        
        $UserData = json_decode(json_encode($selectUsers), true);
      
        if($UserData){
        
            foreach($UserData as $vals){
            
                ##if android device then this.
                if($vals['os_type'] == OS_TYPE_IOS){
                    
                    ## To send the notification.
                    $message = 'You have new job (#"'. $job_id . '") in your expertise area.';
                    
                    $data = array('message'=> $message, 'user_type' => IS_AGENT, 'user_id' => $vals['user_id'], 'job_id'=> $job_id);
                    $SendIos = self::SendIospushnotifications($data, $vals['device_token']);
                
                }
                ## if ios device then.
                if($vals['os_type'] == OS_TYPE_ANDROID){
                
                }
            }
        }
        return true;
    
    }
    
    
    ## Jigs Virani 20 Nov 2016.
    ## For calculate review.
    
    public static function CalculateReviews($user_id,$job_id, $review, $user_type){
        
        $GetUser = DB::table('jobs as j')
                        ->select('j.user_id as r_id',
                                 'ji.user_id as a_id')
                        ->leftJoin('job_invite as ji', 'j.job_id', '=', 'ji.job_id')
                        ->Where('j.job_status', JOB_STATUS_COMPLETED)
                        ->Where('ji.status', 3)->first();
        
        ## if requester then give review for agent.
        if($user_type == IS_REQUESTER){
         
            $getTotalJobsCount = DB::table('job_invite as ji')
                                ->leftJoin('jobs as j', 'j.job_id', '=', 'ji.job_id')
                                ->select(DB::raw('count(ji.job_invite_id) as count'),DB::raw('(select agent_ratings from front_users where id = ji.user_id) as ratings_count'))
                                ->where('ji.user_id', $GetUser->a_id)
                                ->Where('j.job_status', JOB_STATUS_COMPLETED)
                                ->first();
           
            try {
                
                $tempRatings = $getTotalJobsCount->ratings_count + $review;
                $currentRatings = $getTotalJobsCount;
               
                $NewRatings = $tempRatings / $getTotalJobsCount->count;
                $updateRatings = DB::table('front_users')->where('id', $GetUser->a_id)->update(array('agent_ratings' => $NewRatings));
                
                
            } catch(Exception $e){
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
            
        }
        if($user_type == IS_AGENT){
        
            $getTotalJobsCount = DB::table('jobs as ji')
                                ->select(DB::raw('count(ji.job_id) as count'),DB::raw('(select questioner_ratings from front_users where id = ji.user_id) as ratings_count'))
                                ->where('ji.user_id', $GetUser->r_id)
                                 ->Where('ji.job_status', JOB_STATUS_COMPLETED)->first();
            try {
                
                $tempRatings = $getTotalJobsCount->ratings_count + $review;
                $currentRatings = $getTotalJobsCount;
                
                $NewRatings = $tempRatings / $getTotalJobsCount->count;
                $updateRatings = DB::table('front_users')->where('id', $GetUser->r_id)->update(array('questioner_ratings' => $NewRatings));
                
            } catch(Exception $e){
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
            
        }
        return true;
        
    }
    
}
