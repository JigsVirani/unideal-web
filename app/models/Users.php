<?php

class Users extends Eloquent {

	// check image exist or not and return image 
	public static function ImageExist($image) {
        
        $filename = DIR_PROFILE_PIC_URL . $image;
        $physicalfilename = DIR_PROFILE_PIC.$image;
        if (file_exists($physicalfilename) && $image != '') {
            $filename = $filename;
        } else {
            $filename = DIR_PROFILE_PIC_URL . 'no_image_user.png';
        }
        return $filename;
    }
	
    public static function UserlistingCSV() {
        
        $users_obj = DB::table('front_users')
                ->select('id', 'name', 'email_address', DB::raw('CASE WHEN user_type = "1" THEN "Requester" WHEN user_type = "2" THEN "Agent" ELSE "Both" END as roles'),
                         'phone_number', 'my_referral_code',
                        DB::raw('CASE WHEN status =  1 THEN "Active" WHEN status = 2 THEN "Unverify" WHEN status = 0 THEN "Deactive" END as status'), 'last_login')
                ->where('user_delete', '0')
                ->get();
        $users_array = json_decode(json_encode($users_obj), true);
        return $users_array;
    }
    
     public static function userdeactivate($params) {
     
     
            $return_array = array();
            $return_array['success'] =  false;
            
            if(isset($params['user_id']) &&  $params['user_id'] !=''){
                
                $check_to =  DB::table('front_users AS ef')->select('status')->where('id', $params['user_id'])->first();
                $check_to =  json_decode(json_encode($check_to), true);
       
                if(isset($check_to['status']) && $check_to['status'] ==  $params['user_status'] )
                {
                    $status = $check_to['status'] == 0 ? 1 : 0;
                    
                    $is_updated = DB::table('front_users AS ef')->where(array('id' =>  $params['user_id']))->update(array('status' => $status));
                    if($is_updated){  $return_array['data'] = $status; $return_array['success'] =  true; }

                }
            }
        
            return $return_array;
     }
	
	//###########################################################
    //Function : UserDetail
    //purpose : Fetch user detail
    //Author: Jitendra Parmar
    //###########################################################
	public static function UserDetail($Id) {
		$returndata = array();
        $returndata['success'] = false; 
        $Id = base64_decode($Id);
        
        if(isset($Id) && $Id !='') {
			$UserData = DB::table('front_users')
			->select('id','name', 'status', 'from_social_network',
                     DB::raw('CASE  WHEN from_social_network = 1 THEN "Facebook" WHEN from_social_network = 2 THEN "Google" WHEN from_social_network = 3 THEN "Email" END as login_type'),
                       'email_address','profile_pic','phone_number','questioner_ratings','agent_ratings','phone_number','my_referral_code',
				DB::raw('DATE_FORMAT(created_on,"%d %b %Y") as joined'),
				DB::raw('DATE_FORMAT(last_login,"%d %b %Y %H:%i %p") as last_active'))
			->where('id',$Id)
			->first();
		   if($UserData)
			{
				$returndata['success'] = true; 
				$UserData->profile_pic = Users::ImageExist($UserData->profile_pic);
				$returndata['data'] = json_decode(json_encode($UserData), true);
			}
		}
		return $returndata;
	}
	
	public static function admindetails($admin_id){
		$returndata = array();
        $returndata['success'] = false; 
        if(isset($admin_id) && $admin_id !='') {
			$UserData = DB::table('manage_users')
			->select('id','name','email_address','profile_image')
			->where('id',$admin_id)
			->first();
		   if($UserData)
			{
				$returndata['success'] = true; 
				$UserData->profile_image = Users::ImageExist($UserData->profile_image);
				$returndata['data'] = json_decode(json_encode($UserData), true);
			}
		}
		return $returndata;	
	}
	
	public static function postUpdatepersonalinfo($data)
	{
		$created_on  = date('Y-m-d H:i:s');	
		$destinationPath = ADMINPROFILE_IMAGE_PATH;
		if (Input::hasFile('profile_image')) {
				$filename = rand() . time() . str_replace(' ', '_', Input::file('profile_image')->getClientOriginalName());
				$x = (int) $data['x1'];
				$y = (int) $data['y1'];
				$w = (int) $data['w'];
				$h = (int) $data['h'];
				Image::make(Input::file('profile_image'))->crop($w, $h, $x, $y)->resize(400, 400, function ($constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				})->save($destinationPath . $filename);
			}
			else {
				$filename = '';
			}
			if($filename == '')
			{
				$data_temp = array(
					'name' => $data['name'],	
					'email_address' => $data['email_address'],
				);
			}
			else
			{
				$data_temp = array(
					'name' => $data['name'],	
					'email_address' => $data['email_address'],
					'profile_image' => $filename,
				);
			}
			$result = DB::table('manage_users')->where('id', $data['id'])->update($data_temp);
			return $result;
	}
	
	public static function postSetnewpassword($data ,$id) {
		$data_temp = array(
			'password' => Hash::make($data['new_password'])
		);
		$result = DB::table('manage_users')->where(array('id'=>$id))->update($data_temp);
		if($result)
		{
			return true;
		}
		else
		{
			return false;
		}
    }
    
    
    public static function getadminpic($admin_id){
        
        $result = DB::table('manage_users')->select('profile_image')->where(array('id' =>$admin_id))->first();
		if($result)
		{
           $result =  Users::ImageExist($result->profile_image);
			return $result;
		}
		else
		{
			return false;
		}
    
    }
    
    public static function sendpushios($type,$data,$device_token,$message,$from_user_data = array())
	{
		//insert notification in database
		if(isset($data['community_id']))
		{
			$community_id = $data['community_id'];
		}
		else
		{
			$community_id = 0;
		}
		$from_user_id = isset($from_user_data['from_user_id']) ? $from_user_data['from_user_id'] : 0 ;
		
		if($from_user_id != $data['user_id'])
		{
			$InsertArray = array(
				'user_id' => $data['user_id'],
				'from_user_id' => $from_user_id,
				'community_id' => $community_id,
				'notification_type' => $type,
				'goal_dates' =>isset($from_user_data['goal_dates']) ? $from_user_data['goal_dates'] : 0 ,
				'message' => $message,
				'created_on' => date('Y-m-d H:i:s')
			);
			## add data to community_post_like table
			$Addnotification = DB::table('notifications')->insertGetId($InsertArray);

			if($type == 7)
			{
				$message  = $from_user_data['name'].$message;

			}
			else if($type == 3)
			{
				$message  = str_replace("%fullname%",$from_user_data['name'],$message);
			}
			
			/*
			TYPE
			1- On winning monthly motivation prize.
			2- On winning weekly motivation prize.
			3- When a user joins the app who was invited by me. 
			4- When a user misses today's goal.
			- When user is begin charged for a missed workout:
				  5 - (weekly)You've been charged $40.00 for missing out a day on your weekly schedule.
				   6- (individual) You've been charged $30.00 for missing your today's workout. (make 'today' relative).
			7- Community activity on user's posts.
			8- Withdrawal request processed successfully.
			*/

			//for local :- ssl://gateway.sandbox.push.apple.com:2195
			//for live : - ssl://gateway.push.apple.com:2195

			$url =  url('/');
			if (strpos($url, 'unideal.team') !== false) // if live
			{
				$appleurl = "ssl://gateway.push.apple.com:2195";
				$Ckfile = base_path().'/pemdata/ck.pem';
			}
			else //local
			{
				$appleurl = "ssl://gateway.sandbox.push.apple.com:2195";
				$Ckfile = base_path().'/pemdata/ck.pem';
			}

		   $passphrase = "1234";

		   $ctx = stream_context_create();
		   stream_context_set_option($ctx, 'ssl', 'local_cert', $Ckfile);
		   stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		   $fp = stream_socket_client(
				   $appleurl, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

		   if (!$fp)
			   exit("Failed to connect: $err $errstr" . PHP_EOL);

		   //echo 'Connected to APNS' . PHP_EOL;
			// Create the payload body
		   $body['aps'] = array(
			   'alert' => $message,
			   'sound' => 'default',
			   'type' => $type,
			   'data' => $data
		   );
		   $payload = json_encode($body);
		   $msg = chr(0) . pack('n', 32) . pack('H*', $device_token) . pack('n', strlen($payload)) . $payload;
		   $result = fwrite($fp, $msg, strlen($msg));
		   fclose($fp);
		   // Close the connection to the server
		   //fclose($fp);

		   if (isset($result)) {
				return $body['aps']['data'];
		   } else {
			   return 0;
		   }
		}
		else
		{
			return 0;
		}
	}
    
}

