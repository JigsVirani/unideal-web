<?php
class Manage extends Eloquent {
	
	//#################################################################
    // Name : AddJobCategory
    // Purpose : To add new job category into database
    // In Params : categry_name,created_on
    // Out params : status
    //#################################################################
	public static function AddJobCategory($PostData) {
        //global declaration
        $ReturnData = array();
		$InsertQuery = DB::table('job_categories')->insert($PostData);
		if ($InsertQuery) {
            $ReturnData['status'] = true;
        } else {
            $ReturnData['status'] = false;
        }
        return $ReturnData;
    }
	
	//#################################################################
    // Name : JobCategoryDetail
    // Purpose : To get job category information by id
    // In Params : categry_id
    // Out params : status
    //#################################################################
	public static function JobCategoryDetail($CategoryID) {
        $CategoryData = DB::table('job_categories')
                ->select('*')
                ->where('category_id', $CategoryID)
                ->first();
        if ($CategoryData) {
			$CategoryData = json_decode(json_encode($CategoryData), true);
			return $CategoryData;
        }
    }
	
	//#################################################################
    // Name : UpdateUserMultipleColumn
    // Purpose : To udpate category
    // In Params : field name & value
    // Out params : all data
    //#################################################################
    public static function UpdateUserMultipleColumn($UpdateArray, $CategoryId) {
        //update query to update data
        $Result = DB::table('job_categories')->where('category_id', $CategoryId)->update($UpdateArray);
        return 1;
    }
    
    public static function UpdateUsersubcateColumn($UpdateArray, $CategoryId) {
        //update query to update data
        $Result = DB::table('job_sub_categories')->where('sub_category_id', $CategoryId)->update($UpdateArray);
        return 1;
    }
	
	//#################################################################
    // Name : AdvancePaymentData
    // Purpose : To get advance payment data
    // In Params : categry_id
    // Out params : status
    //#################################################################
	public static function AdvancePaymentData() {
        $PaymentData = DB::table('master_settings')
                ->select('*')
                ->get();
		if ($PaymentData) {
			$PaymentData = json_decode(json_encode($PaymentData), true);
			return $PaymentData;
        }
    }
	
	//#################################################################
    // Name : UpdatePaymnet
    // Purpose : To update service fee advance payment/ Job budget advance paymnet
    // In Params : setting_id & setting_value
    // Out params : all data
    //#################################################################
    public static function UpdatePaymnet($UpdateArray, $SettingId) {
        $Result = DB::table('master_settings')->where('setting_id', $SettingId)->update($UpdateArray);
        return 1;
    }
	
	//#################################################################
    // Name : UpdateCommision
    // Purpose : To update unideal commision
    // In Params : setting_id & setting_value
    // Out params : all data
    //#################################################################
    public static function UpdateCommision($UpdateArray, $SettingId) {
        $Result = DB::table('master_settings')->where('setting_id', $SettingId)->update($UpdateArray);
        return 1;
    }
	
	//#################################################################
    // Name : CategoryAdditionalFildsData
    // Purpose : To get category additional filds data
    // In Params : 
    // Out params : all data
    //#################################################################
	public static function CategoryAdditionalFieldsData() {
        $CategoryAdditionalFildsData = DB::table('category_additional_fields')
                ->select('category_additional_field_id','field_name','status',DB::raw('CASE WHEN field_type = 1 THEN "Text box" WHEN field_type = 2 THEN "Drop Down" WHEN field_type = 3 THEN "Check Box" WHEN field_type = 4 THEN "Button" END as field_type'))
                ->get();
		if ($CategoryAdditionalFildsData) {
			$CategoryAdditionalFildsData = json_decode(json_encode($CategoryAdditionalFildsData), true);
			return $CategoryAdditionalFildsData;
        }
    }
	
	//#################################################################
    // Name : AddCategoryAdditionalFields
    // Purpose : To Add additional category filds
    // In Params : field_name,field_type,status,created_on
    // Out params : all data
    //#################################################################
    public static function AddCategoryAdditionalFields($InsertArray) {
        $ReturnData = array();
		$InsertQuery = DB::table('category_additional_fields')->insertGetId($InsertArray);
		if ($InsertQuery) {
            $ReturnData['status'] = true;
            $InsertArray['category_additional_field_id'] = $InsertQuery;
            
				if($InsertArray['field_type'] == 1){
					$InsertArray['field_type'] = 'Text Box';
                    
				}
				if($InsertArray['field_type'] == 2){
					$InsertArray['field_type'] = 'Dropdown';
				}
				if($InsertArray['field_type'] == 3){
					$InsertArray['field_type'] = 'CheckBox';
				}
				if($InsertArray['field_type'] == 4){
					$InsertArray['field_type'] = 'Button';
				}
				unset($InsertArray['created_on']);
			$ReturnData['data'] = $InsertArray;
        } else {
            $ReturnData['status'] = false;
        }
        return $ReturnData;
    }
	
	
	//#################################################################
    // Name : AdvancePaymentData
    // Purpose : To get advance payment data
    // In Params : categry_id
    // Out params : status
    //#################################################################
	public static function TearmsandConditionData() {
        $Data = DB::table('terms_and_conditions')
                ->select('*')
                ->where('id',1)
                ->first();
		if ($Data) {
			$Data = json_decode(json_encode($Data), true);
			return $Data;
        }
        }
	
        public static function Contactus() {
        $Data = DB::table('terms_and_conditions')
                ->select('*')
                ->where('id',2)
                ->first();
		if ($Data) {
			$Data = json_decode(json_encode($Data), true);
			return $Data;
        }
    }
        public static function Question() {
        $Data = DB::table('terms_and_conditions')
                ->select('*')
                ->where('id',3)
                ->first();
		if ($Data) {
			$Data = json_decode(json_encode($Data), true);
			return $Data;
        }
    }
        public static function Answer() {
        $Data = DB::table('terms_and_conditions')
                ->select('*')
                ->where('id',4)
                ->first();
		if ($Data) {
			$Data = json_decode(json_encode($Data), true);
			return $Data;
        }
    }
	
    //#################################################################
    // Name : UpdateTermsCondition
    // Purpose : To terms and condition
    // In Params : terms_and_conditions,updated_on
    // Out params : all data
    //#################################################################
    public static function UpdateTermsCondition($UpdateArray) {
		
        $Result = DB::table('terms_and_conditions')->where('id',1)->update($UpdateArray);
        return 1;
    }
   
    public static function UpdateContactus($UpdateArray) {
		
        $Result = DB::table('terms_and_conditions')->where('id',2)->update($UpdateArray);
        return 1;
    }
    
    public static function Updatejobposting($UpdateArray) {
		
        $Result = DB::table('terms_and_conditions')->where('id',3)->update($UpdateArray);
        return 1;
    }
    public static function Updatejobapplying($UpdateArray) {
		
        $Result = DB::table('terms_and_conditions')->where('id',4)->update($UpdateArray);
        return 1;
    }
	
	//#################################################################
    // Name : UpdateRefferalAmount
    // Purpose : To update referral amount
    // In Params : setting_id,setting_value,setting_status
    // Out params : all data
    //#################################################################
    public static function UpdateRefferalAmount($UpdateArray, $SettingId) {
        $Result = DB::table('master_settings')->where('setting_id', $SettingId)->update($UpdateArray);
		return 1;
    }
	
	//#################################################################
    // Name : AddPromoCode
    // Purpose : To Add promo code
    // In Params : code,reward,start_date,expire_date,frequency,uses_total,created_on
    // Out params : all data
    //#################################################################
    public static function AddPromoCode($InsertArray) {
        $ReturnData = array();
		$InsertQuery = DB::table('promo_code')->insert($InsertArray);
		if ($InsertQuery) {
            $ReturnData['status'] = true;
        } else {
            $ReturnData['status'] = false;
        }
        return $ReturnData;
    }
    
    ## Hardika Satasiya 18 nov 2016
    ## to get job sub category details
    
    public static function SubCategoryDetail($sub_category_id) {
        
        $ReturnData = array();
        $subCategoryData =DB::table('job_sub_categories as js')
                ->select('js.sub_category_id','js.category_id','js.sub_category_name','js.sub_category_id as id','jc.category_name')
                ->leftjoin('job_categories as jc','jc.category_id','=','js.category_id')
                ->where('js.sub_category_id', $sub_category_id)
                ->first();
        if ($subCategoryData) {
                        $allcategory = self::AllMainCategory();
                        $ReturnData['category'] = $allcategory;
			$ReturnData['sub_category'] = json_decode(json_encode($subCategoryData), true);
			return $ReturnData;
        }
    }
    
    ## Hardika Satasiya 18 nov 2016
    ## to get job All Main Category details
    
    public static function AllMainCategory() {
        $CategoryData =DB::table('job_categories as js')
                ->select('js.category_id','js.category_name')
                ->where('status',1)
                ->get();
        if ($CategoryData) {
			$CategoryData = json_decode(json_encode($CategoryData), true);
			return $CategoryData;
        }
    }
    
    ## Hardika Satasiya 18 nov 2016
    ## to Add job Sub Category details
    public static function AddSubCategory($PostData) {
        
        //global declaration.
       
        $ReturnData = array();
       
		$InsertQuery = DB::table('job_sub_categories')->insert($PostData);
                 //print_r($PostData);die;
		if ($InsertQuery) {
            $ReturnData['status'] = true;
        } else {
            $ReturnData['status'] = false;
        }
        return $ReturnData;
    }
    ## Hardika Satasiya 18 nov 2016
    ## to Delete Job Category details
    
    public static function DeleteJobCategory($params){
    
        $ReturnData = array();
        $ReturnData['status'] = STATUS_FALSE;
        
        if(isset($params['category_id']) && $params['category_id'] !=''){
            if($params['category_option'] == 1){
                 $UpdateCate = DB::table('job_categories')->where('category_id', $params['category_id'])->update(array('status' => 0));
            }else {
                 $UpdateCate = DB::table('job_sub_categories')->where('sub_category_id', $params['category_id'])->update(array('status' => 0));
            }
           
            if($UpdateCate){
                    $ReturnData['status'] = STATUS_TRUE;
            }
        
        }
        return $ReturnData;
    
    }
    
}
