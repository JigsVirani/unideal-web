<?php

class Message extends Eloquent {
   
    public static function getunreadmessages($params)
	{ 
        //SELECT COUNT(id) FROM messages WHERE to_user = 1 AND read_flag = '0'
        $unread_count = 0;
        $unread_array = DB::table('job_messages')
                        ->select(DB::raw('COUNT(message_id) as unread'))
                        ->where('receiver_id', $params)
                        ->where('read_flag','0')
                        ->first();
        if($unread_array){  return json_decode(json_encode($unread_array), true); }
        else{ return 0;}
        }
	
	
    public static function messagesend($params)
        { 
  	$ReturnData = array();
        $InsertArray = array(
            'job_id'=>$params['job_id'],
            'sender_id'=>$params['sender_id'],
            'receiver_id'=>$params['receiver_id'],
            'message_type'=>$params['message_type'],
            'user_type'=>3,
            'text'=>$params['text'],
            'read_flag'=>1,
            'created_date' => date('Y-m-d H:i:s')
        );
        $threadid = DB::table('job_messages')
                            ->select('thread_id')
                            ->where('job_id', $params['job_id'])
                            ->first();
        if($threadid){
            $InsertArray =  array_merge($InsertArray,array('thread_id'=>$threadid->thread_id));
        }
           
        // $ReturnData['data'] = DB::table('job_messages')->insert($InsertArray);
        $Addid = DB::table('job_messages')->insertGetId($InsertArray);
        $InsertArray =  array_merge($InsertArray,array('message_id'=>$Addid));
        return json_decode(json_encode($InsertArray), true);
    }
    
    public static function getchathistory($params){
        
         $history_array1 = DB::table('job_messages as jm')
                        ->select('jm.message_id',
                                'jm.user_type',
                                'jm.image_url',
                                'jm.message_type',
                                'jm.text',
                                'jm.thread_id',
                                'jm.created_date',
                                'jm.sender_id',
                                'jm.job_id', 
                                'jm.sender_id',
                                'jm.receiver_id',
                                DB::raw('(select NAME FROM front_users WHERE id = jm.sender_id) AS username'),
                                DB::raw('(select profile_pic FROM front_users WHERE id = jm.sender_id) AS profile_pic'))
                                ->where('jm.job_id', $params['job_id'])
                                ->where(function($history_array1) use ($params) {
                                    if (isset($params['last_block']) &&  $params['last_block'] !=  ''):
                                        $history_array1->where('message_id', '<', $params['last_block']);
                                    endif;
                                })
                                ->orderBy('jm.message_id', 'desc')
                                ->take(10)
                                ->get();
                 
                if($history_array1){
//                   print_r(json_decode(json_encode($history_array1), true));die;
                   return json_decode(json_encode($history_array1), true);
                    
               }else{ return 0;}
        }
       
        
        
    

    

    public static function getimageurl($params){
    
      $destinationPath = 'uploads/messages/';
      $file_return_name = '';
      $returndata =  array();
        
        
        $filearray = "";
      
        if (Input::hasFile('image_file')) {

            // getting all of the post data
            $files = Input::file('image_file');
            
                $rules = array('image' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                $validator = Validator::make(array('image' => $files), $rules);
                if ($validator->passes()) {
                    $filename = rand() . time() . '.' . str_replace(' ', '_', $files->getClientOriginalExtension());
                    $filearray = $filename;
					Image::make($files)->resize(800, null, function ($constraint) {
						$constraint->aspectRatio();
						$constraint->upsize();
					})->save($destinationPath . $filename);   
                    
                     $returndata['success'] = true;
                     $returndata['return_image_url'] = url('/').'/'.$destinationPath.$filename;
                   
                }else{ 
            $returndata['success'] = false;
        }
            
        }else{ 
        $returndata['success'] = false;
        }
       
        return $returndata;
    }
    
    
}