<?php

/**
 * This file demonstrates User registration,login,forgot password,delete & reactive account.
 * @author Bhargav Bhanderi <bhargav@creolestudios.com>
 * @version 1.0
 * @package : Login,Register,Forgot Password,Re active & Delete account
 * @copyright  Creole Studios 2015
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 */
class Apiconfig extends Eloquent {

    
    //#################################################################
    // Name : profilethumbexists
    // Purpose : To check for profile image thumb
    // In Params : all user data
    // Out params : Re active user and send verification email
    //#################################################################
    public static function GetAllappconfig() {
       
        //global declaraton
        $ReturnData = array();
        
        $QueryCategory = DB::table('job_categories as jc')
                ->select('jc.category_id', 'jc.category_name')
                ->where('jc.status', 1)
                ->get();
        
         $QuerySubCategory = DB::table('job_categories as jc')
                ->leftjoin('job_sub_categories as jsc', 'jsc.category_id', '=', 'jc.category_id')
                ->select('jc.category_id', 'jsc.sub_category_id', 'sub_category_name')
                ->where('jc.status', 1)
                ->where('jsc.status', 1)
                ->get();
       
        $promocode_status =DB::table('master_settings')
                            ->select('setting_id','setting_status')
                            ->where('setting_id',PROMOCODE_ID)
                            ->first();
        
         $Queryminmax = DB::table('master_settings')
                            ->select('setting_id','setting_value')
                            ->where('setting_id',MAX_ID)
                            ->orwhere('setting_id',MIN_ID)
                            ->get();
        
	$UserData = DB::table('manage_users')
			->select('name','email_address as email','profile_image as profile_url')
			->first();
	if($UserData)
	{
		$returndata['success'] = true; 
		$UserData->profile_url = Users::ImageExist($UserData->profile_url);
		$adminprofile= json_decode(json_encode($UserData), true);
        }
	$min_max = json_decode(json_encode($Queryminmax), true);
        $jobcategoryData = json_decode(json_encode($QueryCategory), true);
        $promocodeData = json_decode(json_encode($promocode_status), true);
        $QuerySubCategory = json_decode(json_encode($QuerySubCategory), true);
       
        if($promocodeData['setting_status'] == 1){
             $ReturnData['data']['promocode_status']= STATUS_TRUE;
        } else{
            $ReturnData['data']['promocode_status']= STATUS_FALSE;
        }
        
       if ($QueryCategory ) {
            $ReturnData['status'] = '1';
            $ReturnData['status_code'] = STATUS_CODE_200;
            $ReturnData['data']['admin_email']  = ADMIN_EMAIL;
            $ReturnData['data']['terms_and_condition']= TERMSANDCONDITION_SCREEN_URL;
            $ReturnData['data']['help_screen_url']= HELP_SCREEN_URL;
            $ReturnData['data']['jobcategory_data'] = $jobcategoryData;
            $ReturnData['data']['job_sub_category_data'] = $QuerySubCategory;
             $ReturnData['data']['admin'] = $adminprofile;
           
            $ReturnData['data']['min_range_value'] = (int)  $min_max[0]['setting_value'];
            $ReturnData['data']['max_range_value'] = (int)  $min_max[1]['setting_value'];
           
        }else{
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }
    
    public static function GetAllhelpquestion() {
       
        //global declaraton
        $ReturnData = array();
        
        $QueryHelpquestion = DB::table('help_questions as hq')
                ->select('hq.question_id', 'hq.question','hq.answer','hq.status')
                ->where('hq.status', 0)
                ->get();
       
        if($QueryHelpquestion){
             $ReturnData['data']=$QueryHelpquestion;
             $ReturnData['status'] = '1';
        }
        else{
             $ReturnData['status'] = '0';
        }
       return $ReturnData;
    }
    
    public static function GetAllMasterSettings($setting_id) {
       
        //global declaraton.
        $ReturnData = array();
        
       $QuerySettingspquestion =  DB::table('master_settings')
                    
                            ->select('*')
                             ->where(function($QuerySettingspquestion) use ($setting_id) {
                                 
                                  if ($setting_id == SERVICE_FEES_ADVANCE):
                                    $QuerySettingspquestion->where('setting_id', $setting_id);
                                  endif;
                                  if ($setting_id == JOB_BUDGET_ADVANCED):
                                    $QuerySettingspquestion->where('setting_id', $setting_id);
                                  endif;
                                 if ($setting_id == COMMISION_CHARGE):
                                    $QuerySettingspquestion->where('setting_id', $setting_id);
                                  endif;
                                 
                             })->first();
        $settingsData = json_decode(json_encode($QuerySettingspquestion), true);
        
        if($settingsData){
             $ReturnData = $settingsData;
        }
        else{
             $ReturnData['status'] = false;
        }
       return $ReturnData;
    }
}