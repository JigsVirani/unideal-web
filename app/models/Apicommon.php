<?php

/**
 * This file contains all the common function for insert/update.
 * @author Bhargav Bhanderi <bhargav@creolestudios.com>
 * @version 1.0
 * @package : Login,Register,Forgot Password,Re active & Delete account
 * @copyright  Creole Studios 2015
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 */
class Apicommon extends Eloquent {

    //#################################################################
    // Name : CommonUpdate
    // Purpose : To update value for any table with any number of column + conditin
    // In Params : Table name, array of update fields, array of condition
    // Out params : all data
    //#################################################################
    public static function CommonUpdate($TableName, $UpdateArray, $UpdateCondition) {
        //global declaration
        $ReturnData = array();
        //update query
        $ResultUpdate = DB::table($TableName)->where($UpdateCondition)->update($UpdateArray);
        //prepare return data
        if ($ResultUpdate) {
            $ReturnData['status'] = 1;
        } else {
            $ReturnData['status'] = 0;
        }
        //return data
        return $ReturnData;
    }

    //#################################################################
    // Name : CommonInsert
    // Purpose : To insert data in any column
    // In Params : Table name, array of insert field
    // Out params : all data
    //#################################################################
    public static function CommonInsert($TableName, $InsertArray) {
        //global declaration
        $ReturnData = array();
        //insert query
        $ResultInsert = DB::table($TableName)->insertGetId($InsertArray);
        //prepare return data
        if (isset($ResultInsert) && $ResultInsert > 0) {
            $ReturnData['status'] = 1;
            $ReturnData['last_insert_id'] = $ResultInsert;
        } else {
            $ReturnData['status'] = 0;
        }
        //return data
        return $ReturnData;
    }

    //#################################################################
    // Name : GetMasterSettings
    // Purpose : To get master settings from 
    // In Params : Table name, array of insert field
    // Out params : all data
    //#################################################################
    public static function GetMasterSettings($SettingId) {
        //global declaration
        $ReturnData = array();
        //insert query
        $MasterSettingQuery = DB::table('master_settings')
                ->select('setting_id','setting_status','setting_value')
                ->where('setting_id', $SettingId)
                ->first();
        //prepare return data
        if ($MasterSettingQuery) {
            $ReturnData['status'] = 1;
            $ReturnData['setting_status'] = json_decode(json_encode($MasterSettingQuery), true);;
        } else {
            $ReturnData['status'] = 0;
        }
        //return data
        return $ReturnData;
    }

}
?>

