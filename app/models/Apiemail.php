<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Apiemail extends Eloquent {
    
     ## To send the email.
      public static function email($Params,$template,$sub) {
         
          try{
                Mail::queue($template, $Params, function($message) use ($Params,$sub) {
                    $message->from('noreply@unideal.team', 'UNIDeal Team');
                    $message->to($Params['email_address'], $Params['name'])->subject($sub);
                });
          
          } catch(Exception $e){
            
          }
      }
}
