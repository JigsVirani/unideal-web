<?php

class Dashboard extends Eloquent {
    ##Jigs Virani 20 Oct 2015
    ## To get total numbers of new users.

    public static function getTotalnewusers() {

        $returndata = array();

        //select  as total_count from front_users.
        $returndata['total_new_user'] = DB::table('front_users')
                ->select(DB::raw('count(id) as total_count'))
                ->where('user_delete', '0')
                ->first();

        $returndata['total_new_current_user'] = DB::table('front_users')
                ->select(DB::raw('count(id) as total_count'))
                ->where('user_delete', '0')
                ->whereraw('MONTH(created_on) = MONTH (NOW()) AND YEAR(NOW())')
                ->first();

        $last_moths_diff = DB::table('front_users')
                ->select(DB::raw('count(id) as total_count'))
                ->where('user_delete', '0')
                ->whereraw('MONTH(created_on) = MONTH (NOW()) - 1  AND YEAR(NOW())')
                ->first();

        $jobs_current_new = DB::table('jobs')
                ->select(DB::raw('count(job_id) as job_count'))
                ->where('status', '1')
                ->whereraw('MONTH(created_on) = MONTH (NOW()) AND YEAR(NOW())')
                ->first();

        //select  as total_count from front_users.
        $returndata['total_jobs'] = DB::table('jobs')
                ->select(DB::raw('count(job_id) as job_count'))
                ->where('status', '1')
                ->first();

        $returndata = json_decode(json_encode($returndata), true);

        if (isset($returndata['total_new_user']) && $last_moths_diff) {

            $firstval = $last_moths_diff->total_count;
            $secval = $returndata['total_new_user']['total_count'];
           if($firstval>0 && $secval>0){
              $percentage = 100 / ($firstval * $secval);
                if ($firstval < $secval) {
                $returndata['user_diff'] = intval($percentage) . '% Less From Last Month';
                } else {
                $returndata['user_diff'] = intval($percentage) . '% More From Last Month';
                }
            }
            else {
                $returndata['user_diff']='0% More From Last Month';
            }
        }
        if (isset($returndata['total_new_current_user']) && $last_moths_diff) {

            $firstval_months = $last_moths_diff->total_count;
            $secval_months = $returndata['total_new_current_user']['total_count'];
            if($firstval_months>0 && $secval_months>0){
                $percentage_months = 100 / ($firstval_months * $secval_months);
                if ($firstval_months < $secval_months) {
                    $returndata['user_months_diff'] = intval($percentage_months) . '% Less From Last Month';
                } else {
                    $returndata['user_months_diff'] = intval($percentage_months) . '% More From Last Month';
                }
            }else{
                    $returndata['user_months_diff']='0% More From Last Month';
            }
          
        }
        if (isset($returndata['total_jobs']) && $jobs_current_new) {

            $fistjobs = $jobs_current_new->job_count;
            $secjobs = $returndata['total_jobs']['job_count'];
             if($fistjobs>0 && $secjobs>0){
                 $percentage_jobs = 100 / ($fistjobs * $secjobs);
                if ($fistjobs < $secjobs) {
                    $returndata['new_jobs_diff'] = intval($percentage_jobs) . '% Less From Last Month';
                } else {
                    $returndata['new_jobs_diff'] = intval($percentage_jobs) . '% More From Last Month';
                }
            }
            else {
                    $returndata['new_jobs_diff']='0% More From Last Month';
            }
           
        }
        return $returndata;
    }

}

?>