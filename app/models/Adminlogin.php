<?php

class Adminlogin extends Eloquent {
    /* Update the table with random string. */

    public static function postForgotlink($data) {

        /* Generate random string with 25 charater. */
        $forgot_token = str_random(25);

        /* Get the email addresss. */
        $emailid = $data['email_fp'];

        /* Generate the link with parameter. */
        $paramerter = array('forgot_token' => $forgot_token, 'email' => base64_encode($emailid));
        $url = action('AdminforgotpasswordController@getIndex', $paramerter);
        
        /* Update the user with forgot token. */
        $data_temp = array('forgot_token' => $forgot_token);
        $result = DB::table('manage_users')->where('email_address', $emailid)->update($data_temp);

        if ($result) {

            /* Get the user information based on ther current user email address. */
            $where = array('email_address' => $emailid);
            $result = DB::table('manage_users')->select('name')->where($where)->first();

            /* Sent mail forgot link. */
            $data['username'] = $result->name;
            $data['link'] = $url;
            Mail::send('emails.forgotpassword', $data, function($message) use ($data) {
                $message->from('noreply@unideal.com', 'UNIDeal Administrator');
                $message->to($data['email_fp'])->subject('UNIDeal : Forgot password');                
            });

            return $result;
        } else {
            return false;
        }
    }

    public static function postSetnewpassword($data) {

        $newpassword = Hash::make($data['newpassword']);
        $email = base64_decode($data['forgot_email']);
        $data_temp = array('password' => $newpassword);
        $where = array('email_address' => $email, 'forgot_token' => $data['forgot_token']);
        $result = DB::table('manage_users')->where($where)->update($data_temp);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    /* Check user is found. */

    public static function checkuseravailable($forgot_token, $email) {
        $email = base64_decode($email);
        $where = array('email_address' => $email, 'forgot_token' => $forgot_token);
        $result = DB::table('manage_users')->select('*')->where($where)->get();
       
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    //###########################################################
    //Function : setpassword
    //purpose : To set new password after getting mail of forgot password
    //Author: Jitendra Parmar
    //###########################################################
    public static function setpassword($data_array) {
        $data_temp = array(
            'password' => Hash::make($data_array['password']),
        );
        $result = DB::table('manage_users')->where(array('email_address' => $data_array['email']))->update($data_temp);
        if ($result) {
            return true;
        } else {
            return true;
        }
    }

}
