<?php
class Widgets extends Eloquent {

	//#################################################################
    // Name : postAddquote
    // Purpose : To add quots into database
    // In Params : writtenby,text,type
    // Out params : status
    //#################################################################
    public static function postAddquote($quote_data) {
         //global declaration
        $ReturnData = array();
		$quote_data['created_on'] = date('Y-m-d H:i:s');
        $InsertQuery = DB::table('quotes')->insert($quote_data);
        if ($InsertQuery) {
            $ReturnData['status'] = true;
        } else {
            $ReturnData['status'] = false;
        }
        return $ReturnData;
    }
	
	//#################################################################
    // Name : postDeletequote
    // Purpose : To delete quote from database
    // In Params : quote_id
    // Out params : status
    //#################################################################
    public static function postDeletequote($quote_data) {
         //global declaration
        $ReturnData = array();
        $DeleteQuote = DB::table('quotes')->where($quote_data)->delete();
        if ($DeleteQuote) {
            $ReturnData['status'] = true;
        } else {
            $ReturnData['status'] = false;
        }
        return $ReturnData;
    }

}

?>