<?php

/**
 * This file demonstrates User registration,login,forgot password,delete & reactive account.
 * @author Bhargav Bhanderi <bhargav@creolestudios.com>
 * @version 1.0
 * @package : Login,Register,Forgot Password,Re active & Delete account
 * @copyright  Creole Studios 2015
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 */
class Apistatuscode extends Eloquent {


    ## here declare global status codes for api
    ## 200 When Success is true it will return 200
    ## 201 When Success is false it will return 201 (general error).
    ## 301 Request User is not exist
    ## 302 User deactivated by admin
    ## 303 Email Address not Verified
    
    
    ## to get status of user.
    public static function getstatusofuser($email_address){
    
        $ReturnData = array();
        
        ## to check status of user.
        if(isset($email_address) && $email_address != '')
        {
            
                //query to fetch data.
                $UserQuery = DB::table('front_users')
                                ->select( DB::Raw('id as user_id'),'name','user_type', 'jobstatus_notification','transaction_notification','newjob_notification', 'email_address', 'gender', 'phone_number', 'os_type', 'my_referral_code', 'from_social_network', 'facebook_id', 'googleplus_id', 'created_on', 'status', 'user_delete', 'profile_pic', 'referral_code','user_type','language','version')
                                ->where('email_address', $email_address)
                                ->take(1)->first();

                $ResultLogin = json_decode(json_encode($UserQuery), true);
            
                if ($ResultLogin) {
                    if ($ResultLogin['user_delete'] == 1) { // if user deleted.
                        $ReturnData['status_code'] = STATUS_CODE_301;
                         $ReturnData['message'] =  trans('messages.LOGIN_FAIL_NO_ACCOUNT');

                    } else if ($ResultLogin['status'] == 0) { // if deactivated.
                        $ReturnData['status_code'] = STATUS_CODE_302;
                        $ReturnData['message'] =  trans('messages.LOGIN_FAIL_ACCOUNT_INACTIVE');

                    } else if ($ResultLogin['status'] == 2) { // if not verified.
                        $ReturnData['status_code'] = STATUS_CODE_303;
                         $ReturnData['message'] =  trans('messages.LOGIN_FAIL_ACCOUNT_NOT_VERIFIED');

                    } else {
                        $ReturnData['status_code'] = STATUS_CODE_200;
                        $ReturnData['message'] =  trans('messages.GENERAL_SUCCESS');
                    }
                } else {
                        $ReturnData['status_code'] = STATUS_CODE_301;
                        $ReturnData['message'] =  trans('messages.LOGIN_FAIL_NO_ACCOUNT');
                }
        }else {
            $ReturnData['status_code'] = STATUS_CODE_201;
        }
        
        return $ReturnData;
    }
    
    public static function getstatusofuserid($user_id){
    
        $ReturnData = array();
        
        ## to check status of user.
        if(isset($user_id) && $user_id != '')
        {
                //query to fetch data.
                $UserQuery = DB::table('front_users')
                                ->select( DB::Raw('id as user_id'),'name','user_type', 'jobstatus_notification','transaction_notification','newjob_notification', 'email_address', 'gender', 'phone_number', 'os_type', 'my_referral_code', 'from_social_network', 'facebook_id', 'googleplus_id', 'created_on', 'status', 'user_delete', 'profile_pic', 'referral_code','user_type','language','version')
                                ->where('id', $user_id)
                                ->take(1)->first();

                $ResultLogin = json_decode(json_encode($UserQuery), true);
            
                if ($ResultLogin) {
                    if ($ResultLogin['user_delete'] == 1) { // if user deleted.
                        $ReturnData['status_code'] = STATUS_CODE_301;
                        $ReturnData['message'] =  trans('messages.LOGIN_FAIL_NO_ACCOUNT');
                        

                    } else if ($ResultLogin['status'] == 0) { // if deactivated.
                        $ReturnData['status_code'] = STATUS_CODE_302;
                        $ReturnData['message'] =  trans('messages.LOGIN_FAIL_ACCOUNT_INACTIVE');

                    } else if ($ResultLogin['status'] == 2) { // if not verified.
                        $ReturnData['status_code'] = STATUS_CODE_303;
                        $ReturnData['message'] =  trans('messages.LOGIN_FAIL_ACCOUNT_NOT_VERIFIED');

                    } else {
                        $ReturnData['status_code'] = STATUS_CODE_200;
                        $ReturnData['message'] =  trans('messages.GENERAL_SUCCESS');
                    }
                } else {
                    $ReturnData['status_code'] = STATUS_CODE_301;
                    $ReturnData['message'] =  trans('messages.LOGIN_FAIL_NO_ACCOUNT');
                }
        
        }else {
            
            $ReturnData['status_code'] = STATUS_CODE_201;
            $ReturnData['message'] =  trans('messages.GENERAL_ERROR');
            
        }
        
        return $ReturnData;
    }
    
}