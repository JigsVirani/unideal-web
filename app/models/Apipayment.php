<?php
/**
 * This file demonstrates Add/Delete card,Add/Update/Delete bank details.
 * @author Bhargav Bhanderi <bhargav@creolestudios.com>
 * @version 1.0
 * @package : Add/Delete card,Add/Update/Delete bank details.
 * @copyright  Creole Studios 2015
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 */
class Apipayment extends Eloquent {

    //#################################################################
    // Name : ProcessInvite
    // Purpose : To process invite
    // In Params : token,user id
    // Out params : status
    //#################################################################
    public static function ProcessInvite($UserData) {

        //global declaration
        $ReturnData = array();

        //delete token from table        
        $InsertToken = DB::table('front_user_invites')->insert($UserData);

        if ($InsertToken) {
            $ReturnData['status'] = STATUS_TRUE;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
        //return data
        return $ReturnData;
    }

    //#################################################################
    // Name : AddBankAccount
    // Purpose : To add bank account in table
    // In Params : account details
    // Out params : status
    //#################################################################
    public static function AddBankAccount($Params) {

        //global declaration
        $ReturnData = array();
        //extract params
        extract($Params);
        //prepare array for insertion
        $InsertArray = array(
            'user_id' => $user_id,
            'account_number' => $account_number,
            'swift_code' => $swift_code,
            'bank_name' => $bank_name,
            'is_default' => 0,
            'created_on' => date('Y-m-d H:i:s')
        );
        #3 add data to table
        $AddBankAccount = DB::table('front_user_bank_details')->insertGetId($InsertArray);
        ## return data
        if ($AddBankAccount) {
            ## add account id to insert array for return
            unset($InsertArray['created_on']);
            $InsertArray['bank_details_id'] = $AddBankAccount;
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['bank_details'] = $InsertArray;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
        ## return data
        return $ReturnData;
    }

    //#################################################################
    // Name : BankAccounts
    // Purpose : To get bank account
    // In Params : user id
    // Out params : status
    //#################################################################
    public static function BankAccounts($UserId) {

        //global declaration
        $ReturnData = array();
        $DefaultBankId = '';

        ## query to fetch data
        $QueryBankAccount = DB::table('front_user_bank_details')
                ->select('bank_details_id', 'user_id', 'account_number', 'swift_code', 'bank_name','is_default')
                ->where('user_id', $UserId)
                ->orderby('bank_details_id', 'desc')
                ->get();

        if ($QueryBankAccount) {
            ##fetch data
            $BankData = json_decode(json_encode($QueryBankAccount), true);
            ## loop through data to get default bank
            foreach ($BankData as $BankVal)
            {
                if($BankVal['is_default'] == '1')
                {
                    $DefaultBankId = $BankVal['bank_details_id'];
                }
            }
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['bank_details'] = $BankData;
            $ReturnData['default_bank'] = $DefaultBankId;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
        ## return data
        return $ReturnData;
    }

    //#################################################################
    // Name : DeleteBankAccount
    // Purpose : To delete bank account
    // In Params : bank details id
    // Out params : status
    //#################################################################
    public static function DeleteBankAccount($Params) {
        //global declaration
        $ReturnData = array();
        //extract params
        extract($Params);
        ## query to delte bank account
        $DeleteBankAccount = DB::table('front_user_bank_details')->where(array('bank_details_id' =>$bank_details_id, 'user_id'=>$user_id))->delete();

        if ($DeleteBankAccount) {
            $ReturnData['status'] = STATUS_TRUE;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
        ## return data
        return $ReturnData;
    }

    //###########################################################
    //Function : CreateStripCard
    //purpose : To create strip card
    //input : stripe customer id and token
    //output : error/scuccess message
    //###########################################################
    public static function CreateStripCard($Params) {

        //global declaration
        $ReturnData = array();

        //extract params
        extract($Params);
        //initiate stripe
        $Stripe = Stripe::make(STRIPE_API_KEY_1);
        try {
            $AddCard = $Stripe->cards()->create($stripe_customer_id, $card_token);
            $CardDetailsArray['card_id'] = $AddCard['id'];
            $CardDetailsArray['last4'] = $AddCard['last4'];
            $CardDetailsArray['brand'] = $AddCard['brand'];
            $CardDetailsArray['exp_month'] = $AddCard['exp_month'];
            $CardDetailsArray['exp_year'] = $AddCard['exp_year'];
            ## update goal payment status for each goal/pledge for user
            $UpdateGoalPaymentStatus = Apiusers::UpdateSingleColumnWithTableName('goals', 'payment_made', '1', 'user_id', $user_id);
            ## set card as default card when its for plege
            ## at pledge time if is_pledge = 1 then set that card as default card
            if (isset($is_pledge) && $is_pledge == '1') {
                // update card as default card and app flag to done
                $UpdateArray = array('default_card' => $AddCard['id'], 'app_flag' => '4');
                $UpdateUserData = Apiusers::UpdateUserMultipleColumn($UpdateArray, $user_id);
            }
            ## prepare return data
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['card_data'] = $CardDetailsArray;
            ## return data
            return $ReturnData;
        } catch (Exception $e) {
            $ReturnData['status'] = STATUS_FALSE;
            $ReturnData['message'] = $e->getMessage();
            return $ReturnData;
        }
    }

    //#################################################################
    // Name : GetrPledgeId
    // Purpose : To get pledge id
    // In Params : user id
    // Out params : status
    //#################################################################
    public static function GetrPledgeId($UserId) {

        //global declaration
        $ReturnData = array();

        ## query to fetch data
        $QueryPlegeId = DB::table('goals')
                ->select('goal_id')
                ->where('user_id', $UserId)
                ->where('payment_made', '0')
                ->first();

        if ($QueryPlegeId) {
            ##fetch data
            $PledgeData = json_decode(json_encode($QueryPlegeId), true);
            $ReturnData['status'] = STATUS_TRUE;
            $ReturnData['pledge_details'] = $PledgeData;
        } else {
            $ReturnData['status'] = STATUS_FALSE;
        }
        ## return data
        return $ReturnData;
    }
	//###########################################################
    //Function : checkuserexists
    //purpose : Driver exists which want to withdraw funds.
	//Author: Himanshu Upadhyay
    //###########################################################
	public static function checkuserexists($user_data)
	{
	
		$result_select = DB::table('front_users')
                        ->select('id','first_name','email_address','status' ,'total_earnings' )
						->where('id', $user_data)			
						->take(1)->first();	
		$result = json_decode(json_encode($result_select), true);
		if($result)
		{
			if($result['status'] == 3) // if user deleted
			{
				
				$return_array['success'] = false;
				$return_array['message'] = USEREXISTS;
			}
			else if($result['status'] == 0) // if deactivated
			{
				$return_array['success'] = false;
				$return_array['message'] = USEREXISTS1 ;
			}
			else if($result['status'] == 2) // if deactivated
			{
				$return_array['success'] = false;
				$return_array['message'] = USEREXISTS2;
			}
			
			else
			{
				$return_array['success'] = true;
				$return_array['userdata'] = $result;
				
			}
			return $return_array;
		}
		else
		{
			$return_array['success'] = false;
			$return_array['message'] = USEREXISTS3;
			return $return_array;
		}
			$return_array['data'] = $data;
			return $return_array;
	}
	
	//###########################################################
    //Function : checkuserwithdraw
    //purpose : check user withdraw amount is > 0 or not.
	//Author: Himanshu Upadhyay
    //###########################################################
	public static function checkuserwithdraw($id)
	{
		$data_temp = array(
				'total_earnings' => 0,
				
			);
		$result1 = DB::table('front_users')->where(array('id'=>$id))->update($data_temp);
		if($result1)
		{
			return 1;
		}
		else
		{
			return 2; //already verified
		}
	}
	
	//###########################################################
    //Function : fetchbankdata
    //purpose : Fetch driver's bank data.
	//Author: Himanshu Upadhyay
    //###########################################################
	public static function fetchbankdata($user_id)
	{
		$result_select = DB::table('front_user_bank_details')
                        ->select('bank_details_id', 'account_number', 'bank_name')
						->where('user_id', $user_id)
						->where('is_default', 1)
						->first();
		$bank_info = json_decode(json_encode($result_select), true);
		if(!empty($bank_info))
		{
			return $bank_info;
		}
		else {
			return false;
		}
	}
	
	//###########################################################
    //Function : checkexisitingwithdrawrequest
    //purpose : Fetch existing request for withdrawal.
	//Author: Himanshu Upadhyay
    //###########################################################
	public static function checkexisitingwithdrawrequest($user_id)
	{
		$result_select = DB::table('fund_withdraw_request')
                        ->select('withdraw_user_id')
						->where('withdraw_user_id', $user_id)
						->where('withdraw_status', 0)
						->first();
		$bank_info = json_decode(json_encode($result_select), true);
		if(!empty($bank_info))
		{
			return true;
		}
		else {
			return false;
		}
	}
	
	//###########################################################
    //Function : postWithdrawamount
    //purpose : When user want to withdraw amount which is credited in his account.
	//Author: Himanshu Upadhyay
    //###########################################################
	public static function postWithdrawamount($data,$bank_id)
	{
		$id = Input::get('user_id');
		
		$result = DB::table('front_users')
                        ->select('id', 'first_name','email_address','phone_number','os_type','total_earnings','status')
						->where('id',$id)
						->take(1)->first();
	
		$result = json_decode(json_encode($result), true);
		if($result['total_earnings'] > 0)
		{
			$data_temp = array(
							'withdraw_user_id'	=> $data['user_id'],
							'withdraw_amount'	=> $result['total_earnings'],
							'withdraw_bank_id'	=> $bank_id
						);
			
			$result_id = DB::table('fund_withdraw_request')->insertGetId($data_temp);
			
			$parameter = array('user_id'=>base64_encode($data['user_id']), 'total_earnings' => base64_encode($result['total_earnings']), 'bank_id' => base64_encode($bank_id), 'withdraw_id' => base64_encode($result_id));
			$url = action('ApipaymentController@getWithdraw',$parameter);

			$data['username'] = $result['first_name'];
			$data['link'] = $url;

			Mail::send('emails.withdrawalrequestreceived', $data, function($message) use ($data) 
			{
				$message->from('noreply@remo.team', 'Remo : Fund Withdrawal Request');
				$message->to($data['email_address'])->subject( 'Fund withdrawal request received!' );
			});
			if($result)
			{
				$return_array['success'] = true;
				$return_array['data'] = $result;
				return $return_array;
			}
			else 
			{
				$return_array['success'] = false;
				return $return_array;
			}
		}
		else
		{
			$return_array['success'] = FALSE;
			return $return_array;
		}
		
	}
	
	//###########################################################
    //Function : fetchwithdrawalamount
    //purpose : Total amount for driver which he want to withdraw.
	//Author: Himanshu Upadhyay
    //###########################################################
	public static function fetchwithdrawalamount($user_id)
	{
		$result = DB::table('front_users')
							->select('total_earnings')
							->where('id',$user_id)
							->take(1)->first();
		$result = json_decode(json_encode($result), true);
		if($result['total_earnings'] >0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	//###########################################################
    //Function : checkiflastwithdrawrequest
    //purpose : Check if its last link for user
	//Author: 
    //###########################################################
	public static function checkiflastwithdrawrequest($withdraw_id,$user_id)
	{
		$result_select = DB::table('fund_withdraw_request')
                        ->select('withdraw_id')
						->where('withdraw_user_id', $user_id)
						->where('withdraw_status', 0)
						->orderby('withdraw_id','desc')
						->first();
		
		if($result_select)
		{
			$result = json_decode(json_encode($result_select), true);
			if($result['withdraw_id'] != $withdraw_id)
			{
				return 0;
			}
			else
			{
				return 1;
			}
			
		}
		else
		{
			return 0;
		}
	}
}
