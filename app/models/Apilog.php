<?php

//saveapilog


class Apilog extends Eloquent 
{

    public static function saveapilog($Params){
    
        
         $service_name = isset($Params['service_name']) ? $Params['service_name'] : 'N/A';
         $params = isset($Params['params']) ? $Params['params'] : 'N/A';
         $error = isset($Params['error']) ? $Params['error'] : 'N/A';
         $method = isset($Params['method']) ? $Params['method'] : 'N/A';
        
          $html = '<p><strong>Web Service Name: </strong>'. $service_name .'</p><br>';
          $html .= '<p><strong>Params: </strong>'. $params .'</p><br>';
          $html .= '<p><strong>Method:  </strong>'. $method .'</p>';
          $html .= '<p><strong>Error:  </strong>'. $error .'</p><br>';
          
        
        Mail::send(array(), array(), function ($message) use ($html) {
              $message->to('nirmalsinh@creolestudios.com')->cc('nidhi@creolestudios.com')
                ->subject('UNIDeal: Api error log')
                ->from('jignesh@creolestudios.com')
                ->setBody($html, 'text/html');
        });
        
      return true;  
    }
}
?>